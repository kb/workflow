workflow
********

Django application to interface with the Flowable workflow engine.

The inital version encapsulates the Flowable Rest API in the
``workflow/activiti.py`` module.

Development
===========

Before you can use this app you will need a Flowable REST interface.
The easiest way to get this is using our Kubernetes development cluster:
https://www.kbsoftware.co.uk/docs/dev-kubernetes-install.html

Virtual Environment
-------------------

If you have ``dev-scripts`` installed type::

  create-venv workflow

Otherwise::

  virtualenv --python=python3 venv-workflow
  # or
  python3 -m venv venv-workflow

  source venv-workflow/bin/activate
  pip install -r requirements/local.txt

Windows::

  python -m venv venv-workflow
  venv-workflow\Scripts\activate.bat

Daily Workflow
--------------

After Kubernetes is installed
https://www.kbsoftware.co.uk/docs/dev-kubernetes-install.html::

  export KUBECONFIG=$(k3d get-kubeconfig)
  k3d list
  k3d start k3s-default
  kubectl get pods -w

Testing
-------

::

  find . -name '*.pyc' -delete
  pytest --color=yes --tb=short --show-capture=no -x

Usage
-----

Django::

  source .env.fish
  ./init_dev.sh

- Browse to http://localhost:8000/, log in as ``admin``.
- Follow the the instructions on the home page...

Ember::

  cd front
  npm install
  ember s

- Browse to http://localhost:4200/tasks/index

Release and Deploy
==================

https://www.kbsoftware.co.uk/docs/
