#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

echo Drop database: $DATABASE_NAME
if [ "$DATABASE_HOST" = "" ];then
    # use unix sockets
    psql -U postgres -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
    psql -U postgres -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
else
    # use tcp sockets
    PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
    PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
fi


django-admin.py migrate --noinput
django-admin.py demo-data-login
django-admin.py init_app_workflow
django-admin.py demo_data_apps
# django-admin.py demo_data_workflow
# temporary while we are using the h2 in memory database
# if [ -d media/process ]; then
#     rm -r media/process
# fi

# NB this does not use the deployments model
# python workflow/test.py
django-admin.py runserver 0.0.0.0:8000
