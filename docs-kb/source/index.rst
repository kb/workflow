.. Workflow documentation master file, created by
   sphinx-quickstart on Mon Sep 12 15:59:41 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Workflow
========

Contents:

.. toctree::
   :maxdepth: 2

   workflow
   workflow-bpmn
   workflow-forms

Old Notes:

.. toctree::
   :maxdepth: 1

   old/README.rst
   old/old-workflow.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
