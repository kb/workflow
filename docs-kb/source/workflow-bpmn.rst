Workflow - Activiti BPMN Information
************************************

.. highlight:: xml

- :doc:`workflow`
- :doc:`workflow-forms`

.. warning:: PLEASE NOTE: If you are using the in-memory database save your
             work to a ``.bpmn20.xml`` file outside of Activiti before stopping
             tomcat

Requirements
============

- `Install Tomcat and Activiti`_
- Configure the database - either configure an actual database to change to the
  in-memory database (`Install Tomcat and Activiti`_).
- Run the activiti-app (see below) or install and run the eclipse plugin

Useful info
===========

Good introduction article:: `Getting started with the new Activiti 6 UI`_

Our Activiti notes:

- :doc:`workflow`
- https://gitlab.com/kb/workflow/blob/master/README.rst
- `Install Tomcat and Activiti`_

Running the activiti-app
========================

- In your browser browse to http://localhost:8080/activiti-app/
- The administrator username is hard coded.  I dont know what the password is.
- Choose 'kickstart' to edit a workflow
- Click 'import process' to upload an existing workflow or 'create process' to
  start a blank workflow.

.. important:: If using the in memory database after saving the file.  Don't
               forget to download the bpmn file before shutting down your
               computer of restarting tomcat.  Saving saves to the database not
               to the file system.

The download button is accesssed by saving the workflow then clicking on the
picture of the workflow in the list and then choosing the download button in
the top right area of the screen.

Troubleshooting
---------------

If you can't browse to that address, check you've installed the requirements
above and then ensure that tomcat is running using the command::

  service tomcat8 status

Ensure that activiti is installed by checking for the directory in the webapps
folder::

  ls -asl /var/lib/tomcat8/webapps/activiti-app/

Check that the database you require is configured::

  grep "^datasource.driver=" /var/lib/tomcat8/webapps/activiti-app/WEB-INF/classes/META-INF/activiti-app/activiti-app.properties

For the h2 in-memory database this should report::

  datasource.driver=org.h2.Driver

For a mysql database this should report::

  datasource.driver=com.mysql.jdbc.Driver

For a postgresql database this should report::

  datasource.driver=org.postgresql.jdbc.Driver

Expression Language (EL)
========================

By default (I think), Activiti uses the Java EE Expression Language (also
referred to as the EL):
http://docs.oracle.com/javaee/6/tutorial/doc/gjddd.html

Conventions for creating workflows used by the workflow app
===========================================================

Start task form variables
-------------------------

The following form variables must be defined for the start task of a worflow::

  objectPk     the object in the database that is affected by the workflow
  userName     The username of the person to assign the initial task to

The following snippet illustrates this::

  <startEvent id="request" activiti:initiator="initiator" activiti:isInterrupting="false">
    <extensionElements>
      <activiti:formProperty id="objectPk" name="Document ID" type="long" required="true"></activiti:formProperty>
      <activiti:formProperty id="userName" name="User name" type="string" required="true"></activiti:formProperty>
    </extensionElements>
  </startEvent>

Url name defined in the task formKey attribute
----------------------------------------------

Each user task in the workflow must define a formKey which contains the url name
used to access the task in the web app the urls.py should define this url which
should be passed a task id::

  <userTask id="familiarisationConfirmed" name="Familiarised with document ${documentTitle}" activiti:assignee="${userName}" activiti:formKey="flow.issue.familiarisation">
    ...
  </userTask>

Other notes
===========

The Activiti Designer does not seem to create enum form variables correctly.
It only creates the property it does not seem to have any way to add the
values.

e.g it creates::

  <activiti:formProperty id="confirmedUnderstood" name="Have you read and understood this document?" type="enum" required="true"></activiti:formProperty>

You can edit the bpmn20.xml file to add the values like this::

  <activiti:formProperty id="confirmedUnderstood" name="Have you read and understood this document?" type="enum" required="true">
    <activiti:value id="true" name="Read and understood"></activiti:value>
    <activiti:value id="false" name="Read and not understood"></activiti:value>
  </activiti:formProperty>

In the ``activiti:values`` line the ``id`` attribute is the value returned if
that option is selected, the ``name`` attribute is the text that is displayed
to the user.

Using variables
------------------

Task names can be constructed from the form variables submitted. e.g::

  <userTask id="familiarisationConfirmed" name="Familiarised with document ${documentTitle}" activiti:assignee="${userName}" activiti:formKey="flow.issue.familiarisation">
    ...
  </userTask>

In the above example the name combines fixed text with the ``documentTitle``
process variable.

You can also use the submitted variables to dynicamically set the ``assignee``,
see the previous snippet where it is set to the ``userName`` process variable.

Groups could be set in the same way, say you have a variable called
``groupName`` you can set the group of a task as follows::

    <potentialOwner>
      <resourceAssignmentExpression>
        <formalExpression>${groupName}</formalExpression>
      </resourceAssignmentExpression>
    </potentialOwner>
  </userTask>

I was not able to set the text of form variables using process variables::

  <activiti:formProperty id="confirmedUnderstood" name="Have you read and understood ${documentTitle}?" type="enum">
     <activiti:value id="true" name="Read and understood"></activiti:value>
  </activiti:formProperty>

When this is displayed it shows the literal text
``Have you read and understood ${documentTitle}?`` rather than including the
actual document title.

Set the value of a variable in the bpmn file
--------------------------------------------

To set the value of a variable in the bpmn file use the expression attribute as
follows::

  <activiti:formProperty id="variableWithValue" name="A variable that has a value assigned in the bpmn file" type="string" writeable="false" expression="Value Assigned"></activiti:formProperty>

Read only variables
-----------------------

The above snippet also illustrates that you can have read only variables by
setting the writeable attribute to ``false``.

An example of a workflow
------------------------

::

  <?xml version="1.0" encoding="UTF-8"?>
  <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://activiti.org/bpmn20">
    <process id="documentFamiliarisation" name="Document Familiarisation" isExecutable="true">
      <startEvent id="request" activiti:initiator="initiator" activiti:isInterrupting="false">
        <extensionElements>
          <activiti:formProperty id="objectPk" name="Document ID" type="long" required="true"></activiti:formProperty>
          <activiti:formProperty id="dateStarted" name="Date (dd-MM-yyyy)" type="date" datePattern="dd-MM-yyyy hh:mm" required="true"></activiti:formProperty>
          <activiti:formProperty id="documentTitle" name="Document Title" type="string"></activiti:formProperty>
          <activiti:formProperty id="userName" name="User" type="string" required="true"></activiti:formProperty>
        </extensionElements>
      </startEvent>
      <sequenceFlow id="flow1" sourceRef="request" targetRef="familiarisationConfirmed"></sequenceFlow>
      <userTask id="familiarisationConfirmed" name="Familiarised with document ${documentTitle}" activiti:assignee="${userName}" activiti:formKey="flow.issue.familiarisation">
        <documentation>You are required you to be familiar with the document ${documentTitle} (${issuePk}) to safely carry out your work. ${initiator}</documentation>
        <extensionElements>
          <activiti:formProperty id="confirmedUnderstood" name="Have you read and understood this document?" type="enum" required="true">
            <activiti:value id="true" name="Read and understood"></activiti:value>
            <activiti:value id="false" name="Read and not understood"></activiti:value>
          </activiti:formProperty>
          <activiti:formProperty id="familiarisationComments" name="Comments" type="string"></activiti:formProperty>
          <modeler:initiator-can-complete xmlns:modeler="http://activiti.com/modeler">false</modeler:initiator-can-complete>
        </extensionElements>
      </userTask>
      <sequenceFlow id="flow2" sourceRef="familiarisationConfirmed" targetRef="familiarisationConfirmedDecision"></sequenceFlow>
      <exclusiveGateway id="familiarisationConfirmedDecision" name="familiarisation confirmed?"></exclusiveGateway>
      <manualTask id="sendConfirmMail" name="Send confirmation e-mail"></manualTask>
      <sequenceFlow id="flow4" sourceRef="sendConfirmMail" targetRef="theEnd1"></sequenceFlow>
      <endEvent id="theEnd1"></endEvent>
      <userTask id="familiarisationHelp" name="Help with Familiarisation of ${documentTitle}" activiti:assignee="${userName}" activiti:formKey="flow.issue.form">
        <documentation>You have said you did not understand the document.  Please consult your manager
          Reason: ${familiarisationComments}</documentation>
        <extensionElements>
          <activiti:formProperty id="confirmedUnderstood" name="Have you read and understood this document?" type="enum">
            <activiti:value id="true" name="Read and understood"></activiti:value>
          </activiti:formProperty>
          <modeler:initiator-can-complete xmlns:modeler="http://activiti.com/modeler">false</modeler:initiator-can-complete>
        </extensionElements>
      </userTask>
      <endEvent id="theEnd2"></endEvent>
      <sequenceFlow id="flow6" sourceRef="familiarisationHelp" targetRef="theEnd2"></sequenceFlow>
      <parallelGateway id="sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625"></parallelGateway>
      <sequenceFlow id="flow5" sourceRef="familiarisationConfirmedDecision" targetRef="sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625">
        <conditionExpression xsi:type="tFormalExpression"><![CDATA[${confirmedUnderstood == 'false'}]]></conditionExpression>
      </sequenceFlow>
      <sequenceFlow id="sid-5475B1CD-E554-479A-9CF1-1BCEC68F27ED" sourceRef="sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625" targetRef="familiarisationHelp"></sequenceFlow>
      <userTask id="familiarisationProvideHelp" name="Provide help on ${documentTitle} to ${userName}" activiti:formKey="flow.issue.help">
        <extensionElements>
          <activiti:formProperty id="familiarisationHelpProvided" name="Have you provided help on this document?" type="enum">
            <activiti:value id="true" name="Help Provided"></activiti:value>
          </activiti:formProperty>
        </extensionElements>
        <potentialOwner>
          <resourceAssignmentExpression>
            <formalExpression>management</formalExpression>
          </resourceAssignmentExpression>
        </potentialOwner>
      </userTask>
      <endEvent id="sid-9DC8EC7E-374C-4C0A-8B7D-72219AD4BD5B"></endEvent>
      <sequenceFlow id="sid-F24FCF3D-FDFE-48E0-B8C0-30AAF1BA6D3F" sourceRef="familiarisationProvideHelp" targetRef="sid-9DC8EC7E-374C-4C0A-8B7D-72219AD4BD5B"></sequenceFlow>
      <sequenceFlow id="sid-2B81E508-C585-4FD2-95A0-A80426BC6A8E" sourceRef="sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625" targetRef="familiarisationProvideHelp"></sequenceFlow>
      <sequenceFlow id="flow3" sourceRef="familiarisationConfirmedDecision" targetRef="sendConfirmMail">
        <conditionExpression xsi:type="tFormalExpression"><![CDATA[${confirmedUnderstood == 'true'}]]></conditionExpression>
      </sequenceFlow>
    </process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_documentFamiliarisation">
      <bpmndi:BPMNPlane bpmnElement="documentFamiliarisation" id="BPMNPlane_documentFamiliarisation">
        <bpmndi:BPMNShape bpmnElement="request" id="BPMNShape_request">
          <omgdc:Bounds height="30.0" width="30.0" x="0.0" y="119.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="familiarisationConfirmed" id="BPMNShape_familiarisationConfirmed">
          <omgdc:Bounds height="60.0" width="100.0" x="80.0" y="105.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="familiarisationConfirmedDecision" id="BPMNShape_familiarisationConfirmedDecision">
          <omgdc:Bounds height="40.0" width="40.0" x="230.0" y="114.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="sendConfirmMail" id="BPMNShape_sendConfirmMail">
          <omgdc:Bounds height="60.0" width="100.0" x="320.0" y="0.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="theEnd1" id="BPMNShape_theEnd1">
          <omgdc:Bounds height="28.0" width="28.0" x="475.0" y="15.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="familiarisationHelp" id="BPMNShape_familiarisationHelp">
          <omgdc:Bounds height="60.0" width="100.0" x="465.0" y="135.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="theEnd2" id="BPMNShape_theEnd2">
          <omgdc:Bounds height="28.0" width="28.0" x="780.0" y="151.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625" id="BPMNShape_sid-1F72E95E-B25A-435B-AEA3-02FE3FC82625">
          <omgdc:Bounds height="40.0" width="40.0" x="405.0" y="240.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="familiarisationProvideHelp" id="BPMNShape_familiarisationProvideHelp">
          <omgdc:Bounds height="80.0" width="100.0" x="465.0" y="330.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape bpmnElement="sid-9DC8EC7E-374C-4C0A-8B7D-72219AD4BD5B" id="BPMNShape_sid-9DC8EC7E-374C-4C0A-8B7D-72219AD4BD5B">
          <omgdc:Bounds height="28.0" width="28.0" x="780.0" y="356.0"></omgdc:Bounds>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge bpmnElement="flow1" id="BPMNEdge_flow1">
          <omgdi:waypoint x="29.999432924408623" y="134.1304298515166"></omgdi:waypoint>
          <omgdi:waypoint x="80.0" y="134.56521739130434"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="flow2" id="BPMNEdge_flow2">
          <omgdi:waypoint x="180.0" y="134.58333333333334"></omgdi:waypoint>
          <omgdi:waypoint x="230.16528925619835" y="134.16528925619835"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="flow3" id="BPMNEdge_flow3">
          <omgdi:waypoint x="250.0" y="114.0"></omgdi:waypoint>
          <omgdi:waypoint x="250.0" y="30.0"></omgdi:waypoint>
          <omgdi:waypoint x="320.0" y="30.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="flow4" id="BPMNEdge_flow4">
          <omgdi:waypoint x="420.0" y="30.0"></omgdi:waypoint>
          <omgdi:waypoint x="432.0" y="30.0"></omgdi:waypoint>
          <omgdi:waypoint x="432.0" y="30.0"></omgdi:waypoint>
          <omgdi:waypoint x="475.00215401185983" y="29.24557624540596"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="flow5" id="BPMNEdge_flow5">
          <omgdi:waypoint x="250.0" y="154.0"></omgdi:waypoint>
          <omgdi:waypoint x="250.0" y="260.0"></omgdi:waypoint>
          <omgdi:waypoint x="405.0" y="260.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="flow6" id="BPMNEdge_flow6">
          <omgdi:waypoint x="565.0" y="165.0"></omgdi:waypoint>
          <omgdi:waypoint x="780.0" y="165.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="sid-5475B1CD-E554-479A-9CF1-1BCEC68F27ED" id="BPMNEdge_sid-5475B1CD-E554-479A-9CF1-1BCEC68F27ED">
          <omgdi:waypoint x="435.1756756756757" y="250.17567567567568"></omgdi:waypoint>
          <omgdi:waypoint x="486.8848167539267" y="195.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="sid-F24FCF3D-FDFE-48E0-B8C0-30AAF1BA6D3F" id="BPMNEdge_sid-F24FCF3D-FDFE-48E0-B8C0-30AAF1BA6D3F">
          <omgdi:waypoint x="565.0" y="370.0"></omgdi:waypoint>
          <omgdi:waypoint x="780.0" y="370.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge bpmnElement="sid-2B81E508-C585-4FD2-95A0-A80426BC6A8E" id="BPMNEdge_sid-2B81E508-C585-4FD2-95A0-A80426BC6A8E">
          <omgdi:waypoint x="434.04522613065325" y="270.95477386934675"></omgdi:waypoint>
          <omgdi:waypoint x="482.30593607305934" y="330.0"></omgdi:waypoint>
        </bpmndi:BPMNEdge>
      </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
  </definitions>


.. _`Getting started with the new Activiti 6 UI`: http://bpmn20inaction.blogspot.co.uk/2015/09/getting-started-with-new-activiti-6-ui.html
.. _`Install Tomcat and Activiti`: https://www.kbsoftware.co.uk/docs/dev-activiti.html
