Old README.rst for 'workflow'
*****************************

Django application to interface with a workflow engine.

The inital version encapsulates the Activiti Rest API in the
workflow/activiti.py module.  Higher level classes / functions provide utilities
that can be used directly in a project or base classes that can be extended in a
project.

It's a work in progress!

ToDo
====

- Fix issue that process_start sets the activitiID to None
- Fix issue with Date saved as type string
- Models to save relevent workflow data in app
- handling enum variables and boolean variables (done)
- user creation
- assign task to users (via variable in the workflow)
- list of tasks for a user
- list of completed processes
- use postgres database rather than the in-memory H2 database

Notes
=====

To start process use::
  http://<host>:<port>/workflow/process-start/<deployment id (see process definition list)>

To perform a task::
  http://<host>:<port>/workflow/task/<task id (see task list)>

To view tasks for a user ::
  http://<host>:<port>/workflow/task/<user>/list/

  e.g.
    http://localhost:8000/workflow/task/gonzo/list

Install
=======

Before you can use this app you need tomcat8 and the activiti rest interface
installed

Install tomcat and dependancies::

  sudo apt install tomcat8

Download Activiti-6 beta from http://activiti.org/download.html

Extract the zip and copy the war files activiti-app.war and activiti-rest.war
from the wars directory to /var/lib/tomcat8/webapps::

  cd <directory expanded activiti to>/wars/
  sudo cp activiti-rest.war /var/lib/tomcat8/webapps/

Start tomcat using the command::

  sudo service tomcat8 start

or::

  sudo /opt/tomcat/bin/startup.sh

The activiti-rest war will also be extracted (default db config is h2 in
memory) you can test access using::

  curl -u kermit:kermit -XGET http://localhost:8080/activiti-rest/service/repository/deployments/

Virtual Environment
-------------------

If you have dev-scripts installed type::

  create-venv workflow

Otherwise::

  virtualenv --python=python3 venv-workflow
  source venv-workflow/bin/activate

  pip install -r requirements/local.txt

Windows::

  python -m venv venv-workflow
  venv-workflow\Scripts\activate.bat

Testing
=======

The ``Activiti`` class requires three ``settings``.
These are as follows (with their default values)::

  ACTIVITI_HOST = 'localhost'
  ACTIVITI_PORT = 8080
  ACTIVITI_PATH = 'activiti-rest'

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

::

  ./init_dev.sh p

You can find a sample workflow ``bpmn20.xml`` file in::

  example/tests/data/holidayRequest.bpmn20.xml

Release
=======

https://www.kbsoftware.co.uk/docs/
