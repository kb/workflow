Old Activiti Notes
******************

.. highlight:: python

PLEASE NOTE: This document is very much a work in progress!!!!!

Warning: Activiti In-Memory database
------------------------------------
We are currently running Activiti with an inmemory database so all data is lost
when tomcat restarts.  This is useful for this stage of development and
testing but each time you restart tomcat you will probably need to delete all
items from the ProcessDefinition tables. To do this, either run "./init_dev.sh"
or from the django shell (django-admin shell) and run the following::

  from workflow.models import ProcessDefinition
  ProcessDefinition.objects.all().delete()

Deploy a workflow
=================

Log in to navigator UI as a superuser (e.g. admin)

Choose Settings | Workflow Settings

Click Deploy Workflow

Choose a file and Click submits

To use a workflow
=================

Update list of users::

  django-admin activiti_users

Deploy workflow::

  activiti.deploy_process_definition(process_definition_file_name)

Start a process: pass the process key and the variables required by the start
task  (activiti will start the latest version of this process definition)::

  activiti = Activiti(username=user.username)
  activiti.process_start(process_key, variables)

Run create_familiarisation_workflow
===================================

NB documentFamiliarisation workflow should be deployed from the Navigator UI
(see above)

The following code will create a familiarisation workflow for a user called joey
for each document which has a status of 'Current'::

  from flow.service import create_familiarisation_workflow

  from dash.models import Contact, DocumentIssue

  con = Contact.objects.get(user__username='joey')
  staff = Contact.objects.get(user__username='staff')

  status_messages = []

  for doc in DocumentIssue.objects.all():
      print (
          doc.id, doc.title, ":", doc.status == "Current", ":",
          doc.product, ":", doc.process
      )
      if (doc.status == 'Current'):
          create_familiarisation_workflow(
              Document.WORKFLOW_FAMilIARISATION, staff.user, con.user,
              doc
          )


Forms
=====
Forms in Activiti need are related to a process (effectively the process start
task) or a task within the process.

The workflow app provides the following mixin::

  class WorkflowFormMixin(forms.Form):

This initialises the form.  Creating the django form fields from the process or
task variables (supplied by the view)

Where the view to start a process requires user input the following can be used::

  class WorkflowProcessStartForm(WorkflowFormMixin):

Where the view to complete a task requires user input the following can be used
::

  class WorkflowTaskForm(WorkflowFormMixin):

If you wish to add, remove or hide fields. You can override the __init__ method
and amend the self.fields dictionary as appropriate.

Using forms
-----------
Because the mixin creates a standard django forms most of the conveniences of
django forms can be used for example you can render the form using the
_form.html template from the base app.

The view that uses the form needs inherit from::

  class WorkflowFormView(FormView):

rather than simply FormView.  To use this View directly you need to define
get_form_kwargs and form_valid methods see below

Alternatively you can inherit from the higher level Views either::

  class ProcessStartView(LoginRequiredMixin, BaseMixin, WorkflowFormView):

or ::

  class TaskPerformView(LoginRequiredMixin, BaseMixin, WorkflowFormView):

Both of these views provide a form_valid that retrieves the form data as a list
of name / value dictionaries (the format required by Activiti).
Then calls form_valid_workflow and then interacts with Activiti (using either
process_start or task_complete respectively)

The default form_valid_workflow simply returns the variables passed as
follows::

  def form_valid_workflow(self, form, variables):
    return variables

If you need to do extra processing before interacting with activiti you should
override this method (it must return the updated variables array)


Using WorkflowFormView directly
-------------------------------

To tell the form which activiti process to start or task to perform the view
must provide a get_form_kwargs method as follows (this example is from
ProcessStartView)::

  def get_form_kwargs(self):
      kwargs = super().get_form_kwargs()
      kwargs['process_id'] = self._process_definition_id
      return kwargs

For a task you should provide a method similar to this::

  def get_form_kwargs(self):
      kwargs = super().get_form_kwargs()
      kwargs['task_id'] = self._task_id
      return kwargs

You must also provide a form_valid method for a process start view it would be
something like this::

  def form_valid(self, form):
      with transaction.atomic():
          variables = self._get_workflow_form_data(
              form, process_id=self._process_definition_id
          )

          # your own custom processing here

          response = self.activiti.process_start(
              self._process_definition['key'], variables
          )

          if 'id' in response:
              messages.success(self.request, "Process started")
          else:
              # an exception return
              message = "Process start Failed : {} {}".format(
                  response['exception'], response['message']
              )
              messages.error(self.request, message)
              raise ActivitiError(message)
          return HttpResponseRedirect(self.get_success_url())

For a task::

  def form_valid(self, form):
      variables = self._get_workflow_form_data(form, task_id=self._task_id)
      with transaction.atomic():

          # your own custom processing here

          response = self.activiti.task_complete(self._task['id'], variables)
          if 'exception' in response:
              # raise exception
              message = "{} : {}".format(
                  response['exception'], response['message']
              )
              ActivitiError(message)

          return HttpResponseRedirect(self.get_success_url())

Variables in the Workflow bpmn file
===================================

Variables defined in a ``formProperty`` of the workflow task in a workflow bpmn
file are accessible using ``form_variable_info`` method of the Activiti class.
This means we can provide information to our application about a task using
read only form property defined in the bpmn file.

Consider the following user task::

  <userTask id="handleRequest" name="Handle vacation request" >
    <extensionElements>
      <activiti:formProperty id="managerMotivation" name="Motivation" type="string" />
      <activiti:formProperty id="webappUrlName" name="Webapp Url Name" type="string" expression="flow.vacation.approve" writable="false"/>
    </extensionElements>
  </userTask>

The value of webappUrlName for a task (e.g. task with an id of 127) can be
retrieved as follows::

  activiti = Activiti('kermit', 'kermit')

  activiti.form_variable_info(task_id=143)

This will return::

  {
    'deploymentId': '127',
    'formKey': None,
    'formProperties': [
      {
        'datePattern': None,
        'enumValues': [],
        'id': 'managerMotivation',
        'name': 'Motivation',
        'readable': True,
        'required': False,
        'type': 'string',
        'value': None,
        'writable': True
      },
      {
        'datePattern': None,
        'enumValues': [],
        'id': 'webappUrlName',
        'name': 'Webapp Url Name',
        'readable': True,
        'required': False,
        'type': 'string',
        'value': 'flow.vacation.approve',
        'writable': False
      }
    ],
    'processDefinitionId': None,
    'processDefinitionUrl': None,
    'taskId': '143',
    'taskUrl': 'http://localhost:8080/activiti-rest/service/runtime/tasks/143'
  }

You can see the value field for the webappUrlName variable defines the value
'flow.vacation.approve' (the assigned to the expression attribute in the bpmn
file).
