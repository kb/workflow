Flowable
********

.. highlight:: python

- :doc:`workflow-bpmn`
- :doc:`workflow-forms`

To use a workflow
=================

Update list of users:

.. code-block:: bash

  django-admin activiti_users

Deploy workflow::

  activiti.deploy_process_definition(process_definition_file_name)

Start a process: pass the process key and the variables required by the start
task  (activiti will start the latest version of this process definition)::

  activiti = Activiti(username=user.username)
  activiti.process_start(process_key, variables)
