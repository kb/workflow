# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView
from rest_framework import routers
from rest_framework.authtoken import views

from .views import (
    HomeView,
    MyTaskFormView,
    MyTaskFormViewMissingDataDocument,
    SettingsView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(
        r"^tasks/$",
        view=RedirectView.as_view(
            url=reverse_lazy("project.home"), permanent=False
        ),
        name="project.tasks",
    ),
    re_path(r"^workflow/", view=include("workflow.urls")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(
            url=reverse_lazy("project.home"), permanent=False
        ),
        name="project.dash",
    ),
    re_path(
        r"^example/task/(?P<task_id>\d+)/$",
        view=MyTaskFormView.as_view(),
        name="example.task",
    ),
    re_path(
        r"^example/task/(?P<task_id>\d+)/missing/data/document/$",
        view=MyTaskFormViewMissingDataDocument.as_view(),
        name="example.task.missing.data.document",
    ),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
