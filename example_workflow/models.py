# -*- encoding: utf-8 -*-
import random

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from faker import Faker


class DummyWorkflowObject(models.Model):
    name = models.CharField(max_length=100)

    def create_workflow(self, workflow):
        return False, "Not Written Yet"

    def get_workflow_users(self, workflow):
        return self.dummyworkflowobjectuser_set.all()

    class Meta:
        verbose_name = "Dummy object for workflow"
        verbose_name_plural = "Dummy object for workflows"

    def __str__(self):
        return self.name


class DummyWorkflowObjectUser(models.Model):
    obj = models.ForeignKey(DummyWorkflowObject, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Dummy User for workflow"
        verbose_name_plural = "Dummy User for workflows"

    def __str__(self):
        return "{} {}".format(self.obj.name, self.user)


class ExampleContactManager(models.Manager):
    def user_in_a_group(self, user, group_pks):
        """Used by the workflow app - does a user belong to a group?"""
        result = False
        qs = user.groups.filter(pk__in=group_pks)
        if qs.exists():
            result = True
        return result


class ExampleContact(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    manager = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        related_name="contact_manager",
        on_delete=models.CASCADE,
    )
    delegate = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        related_name="contact_delegate",
        on_delete=models.CASCADE,
    )
    objects = ExampleContactManager()

    class Meta:
        ordering = ["user__username"]
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return "{}".format(self.get_full_name())

    def attachments(self):
        """Get the attachments for the workflow object.

        For more information, see ``_attachments`` in ``HistoryTaskMixin``.

        """
        return [
            {
                "created": datetime(2020, 1, 11, 15, 23),
                "download_url": "http://localhost:8000/link/link/1/",
                "original_file_name": "linklink1.doc",
            },
            {
                "created": datetime(2020, 1, 22, 15, 23),
                "download_url": "http://localhost:8000/link/link/2/",
                "original_file_name": "linklink2.doc",
            },
        ]

    def email(self):
        return self.user.email

    def get_full_name(self):
        return self.user.get_full_name() or self.user.username

    def get_workflow_field(self, field_name, data_type, mapping):
        if field_name == "age":
            return random.randint(20, 80)
        elif field_name == "hr_group_pk":
            return 44
        elif field_name == "music_group_pk":
            return 33
        elif field_name == "number_of_days":
            return random.randint(1, 14)
        elif field_name == "start_date":
            return random.choice(
                (
                    date.today() + relativedelta(days=1),
                    date.today() + relativedelta(days=10),
                    date.today() + relativedelta(days=20),
                    date.today() + relativedelta(days=30),
                    date.today() + relativedelta(days=60),
                )
            )
        elif field_name == "supervisor_pk":
            return 22
        elif field_name == "time_off_manager_group_pk":
            return 55
        elif field_name == "vacation_motivation":
            fake = Faker()
            return "Holiday with {} to {}".format(fake.name(), fake.country())
        else:
            return None

    def is_delegate_for(self, contact):
        result = False
        try:
            ExampleContact.objects.get(
                user=contact.user, delegate__user__username=self.user.username
            )
            result = True
        except ExampleContact.DoesNotExist:
            pass
        return result


class ExampleGroupManager(models.Manager):
    def current(self):
        return self.model.objects.all()


class ExampleGroup(models.Model):
    name = models.CharField(max_length=30)
    objects = ExampleGroupManager()

    class Meta:
        ordering = ("pk",)
        verbose_name = "Example Group"
        verbose_name_plural = "Example Groups"

    def __str__(self):
        return f"{self.name}"
