# -*- encoding: utf-8 -*-
from .base import *


ACTIVITI_HOST = "localhost"
ACTIVITI_PORT = 8080
ACTIVITI_PATH = "activiti-rest"

DATABASE_NAME = get_env_variable("DATABASE_NAME")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": DATABASE_NAME,
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": get_env_variable("DATABASE_PASS"),
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": get_env_variable("DATABASE_PORT"),
    }
}

MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

REDIS_HOST = "localhost"
REDIS_PORT = "6379"

# https://dramatiq.io/reference.html#middleware
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {"url": "redis://{}:{}/0".format(REDIS_HOST, REDIS_PORT)},
    "MIDDLEWARE": [
        # drops messages that have been in the queue for too long
        "dramatiq.middleware.AgeLimit",
        # cancels actors that run for too long
        "dramatiq.middleware.TimeLimit",
        # lets you chain success and failure callbacks
        "dramatiq.middleware.Callbacks",
        # automatically retries failed tasks with exponential backoff
        "dramatiq.middleware.Retries",
    ],
}

# KB Software queue name (to allow multiple sites on one server)
DRAMATIQ_QUEUE_NAME = "{}".format(DATABASE_NAME)


def show_toolbar(request):
    return True


SHOW_TOOLBAR_CALLBACK = show_toolbar

INTERNAL_IPS = ["127.0.0.1"]
