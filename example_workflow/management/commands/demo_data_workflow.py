# -*- encoding: utf-8 -*-
import attr
import json
import random

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.db import transaction

from example.models import ExampleContact
from workflow.activiti import Activiti
from workflow.models import ScheduledWorkflow


class Command(BaseCommand):
    help = "Create demo data for 'workflow'"
    PROCESS_KEY = "timeOffRequest"

    def _contact(self):
        user = self._user()
        try:
            contact = ExampleContact.objects.get(user=user)
        except ExampleContact.DoesNotExist:
            contact = ExampleContact(user=user)
            contact.save()
        return contact

    def _group(self, name):
        try:
            Group.objects.get(name=name)
        except Group.DoesNotExist:
            group = Group(name=name)
            group.save()
            self.stdout.write("Created group: {}".format(name))

    def _user(self):
        user_name = random.choice(
            [x.username for x in get_user_model().objects.all()]
        )
        return get_user_model().objects.get(username=user_name)

    def handle(self, *args, **options):
        count = 0
        self.stdout.write("{}".format(self.help))
        activiti = Activiti()
        for x in range(10):
            user = self._user()
            content_object = self._contact()
            scheduled = ScheduledWorkflow.objects.create_scheduled_workflow(
                self.PROCESS_KEY, user, content_object, [user]
            )
            with transaction.atomic():
                qs = ScheduledWorkflow.objects.create_workflows(scheduled.pk)
            for scheduled_workflow_user in qs:
                data = activiti.process_status(
                    scheduled_workflow_user.get_process_id()
                )
                self.stdout.write("")
                self.stdout.write(
                    "data for 'process_id' {}".format(
                        scheduled_workflow_user.get_process_id()
                    )
                )
                self.stdout.write(json.dumps(attr.asdict(data), indent=4))
            count = count + len(qs)
        self._group("Managers")
        self._group("Directors")
        self.stdout.write("{} ({} records) - Complete".format(self.help, count))
