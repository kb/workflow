# -*- encoding: utf-8 -*-
from .base import *


ACTIVITI_HOST = get_env_variable("ACTIVITI_HOST")
ACTIVITI_PORT = get_env_variable("ACTIVITI_PORT")
ACTIVITI_PATH = ""
ACTIVITI_USER = get_env_variable("ACTIVITI_USER")
ACTIVITI_PASS = get_env_variable("ACTIVITI_PASS")

DATABASE_NAME = get_env_variable("DATABASE_NAME")
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": DATABASE_NAME,
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": get_env_variable("DATABASE_PASS"),
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": get_env_variable("DATABASE_PORT"),
    }
}

REDIS_HOST = get_env_variable("REDIS_HOST")
REDIS_PORT = get_env_variable("REDIS_PORT")
# https://dramatiq.io/reference.html#middleware
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {"url": "redis://{}:{}/0".format(REDIS_HOST, REDIS_PORT)},
    "MIDDLEWARE": [
        # drops messages that have been in the queue for too long
        "dramatiq.middleware.AgeLimit",
        # cancels actors that run for too long
        "dramatiq.middleware.TimeLimit",
        # lets you chain success and failure callbacks
        "dramatiq.middleware.Callbacks",
        # automatically retries failed tasks with exponential backoff
        "dramatiq.middleware.Retries",
    ],
}
# KB Software queue name (to allow multiple sites on one server)
DRAMATIQ_QUEUE_NAME = DATABASE_NAME

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda r: DEBUG,
    "INTERCEPT_REDIRECTS": False,
    "ENABLE_STACKTRACES": True,
}

INSTALLED_APPS += ("django_extensions", "debug_toolbar")
MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

# Need for Emberjs
CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOWED_ORIGINS = ("http://localhost:8000", "http://localhost:4200")

CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False

INSTALLED_APPS += ("corsheaders",)
MIDDLEWARE = ("corsheaders.middleware.CorsMiddleware",) + MIDDLEWARE
