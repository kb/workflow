# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.urls import reverse
from django.views.generic import TemplateView

from base.view_utils import BaseMixin
from workflow.views import TaskFormView
from .forms import MyTaskForm


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class MyTaskFormView(TaskFormView):
    form_class = MyTaskForm
    template_name = "example/my_task_form.html"

    def get_success_url(self):
        return reverse("project.dash")

    def user_has_perm(self, task):
        return True

    def copy_to_document_management(self):
        pass

    def write_data_document(self, pdf):
        with open("temp.pdf", "wb") as f:
            f.write(pdf)


class MyTaskFormViewMissingDataDocument(TaskFormView):
    form_class = MyTaskForm
    template_name = "example/my_task_form.html"

    def get_success_url(self):
        return reverse("project.dash")

    def user_has_perm(self, task):
        return True


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"
