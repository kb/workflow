# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User

from workflow.forms import WorkflowFormMixin


class MyTaskForm(WorkflowFormMixin):
    foo_select = forms.ModelMultipleChoiceField(queryset=None, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["foo_select"].queryset = User.objects.all()
        self.order_fields(self._fields_in_order())

    def _fields_in_order(self):
        """Move the ``foo_select`` field to the beginning of the list."""
        result = [obj for obj in self.fields.keys()]
        result.remove("foo_select")
        result.insert(0, "foo_select")
        return result

    def clean(self):
        cleaned_data = super().clean()
        self._validate_accept_reject_comment(
            cleaned_data, message="Please enter a good reason."
        )


class MyTestForm(WorkflowFormMixin):
    pass
