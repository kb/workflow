# -*- encoding: utf-8 -*-
import pytest
import pytz
import requests

from datetime import date, datetime
from django.urls import reverse
from django.utils.timezone import localtime
from enum import Enum
from unittest import mock

from example_workflow.forms import MyTestForm
from login.tests.factories import TEST_PASSWORD, UserFactory
from workflow.activiti import Variable


def _mock_form_variable_info_bool(value):
    return {
        "marmite": Variable(
            name="Marmite", value=value, data_type=bool, writable=True
        )
    }
    # return [
    #    {
    #        "datePattern": None,
    #        "required": False,
    #        "type": "boolean",
    #        "writable": True,
    #        "enumValues": [],
    #        "readable": True,
    #        "name": "Marmite",
    #        "value": value,
    #        "id": "marmite",
    #    }
    # ]


def _mock_form_variable_info_date(value):
    return {
        "birthday": Variable(
            data_type=date,
            name="Birthday",
            required=True,
            value=value,
            writable=True,
        )
    }
    # return [
    #    {
    #        "writable": True,
    #        "value": value,
    #        "required": True,
    #        "readable": True,
    #        "type": "date",
    #        "name": "Birthday",
    #        "id": "birthday",
    #        "enumValues": [],
    #        "datePattern": "dd-MM-yyyy hh:mm",
    #    }
    # ]


def _mock_form_variable_info_enum(value):
    return {
        "decision": Variable(
            data_type=Enum,
            required=True,
            value=value,
            writable=True,
            choices={"accept": "Accept", "reject": "Reject"},
        )
    }
    # return [
    #    {
    #        "id": "decision",
    #        "datePattern": None,
    #        "writable": True,
    #        "readable": True,
    #        "value": value,
    #        "name": None,
    #        "type": "enum",
    #        "required": True,
    #        "enumValues": [
    #            {"id": "accept", "name": "Accept"},
    #            {"id": "reject", "name": "Reject"},
    #        ],
    #    }
    # ]


def _mock_form_variable_info_long(value):
    return {
        "age": Variable(
            name="Age", data_type=int, required=True, value=value, writable=True
        )
    }
    # return [
    #    {
    #        "writable": True,
    #        "value": value,
    #        "required": True,
    #        "readable": True,
    #        "type": "long",
    #        "name": "Age",
    #        "id": "age",
    #        "enumValues": [],
    #        "datePattern": None,
    #    }
    # ]


def _mock_task_status(user_pk):
    return {
        "assignee": user_pk,
        "category": "jersey",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "description": "Lovely summers day",
        "dueDate": None,
        "processDefinitionId": "makeHay:1:303",
        "name": "Make hay",
        "processInstanceId": "321",
        "id": "336",
        "formKey": None,
        "variables": [],
    }


@pytest.mark.django_db
def test_date_field(client):
    """Test the form date field"""
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        # "workflow.activiti.Activiti.task_claim"
        # ) as mock_task_claim, mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ) as mock_task_complete, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "from_date": Variable(
                value=None, data_type=date, writable=True, required=True
            )
        }
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        url = reverse("example.task", args=[99])
        data = {"from_date": "22/09/2017"}
        response = client.post(url, data)
        assert requests.codes.found == response.status_code
        # mock_task_claim.assert_called_with(336, user)
        mock_task_complete.assert_called_with(
            336,
            {
                "from_date": Variable(
                    value="2017-09-22",
                    data_type=date,
                    writable=True,
                    required=True,
                )
            },
            additional_variables={},
        )


@pytest.mark.django_db
def test_date_field_read_only(client):
    """Test the form date field"""
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        # "workflow.activiti.Activiti.task_claim"
        # ) as mock_task_claim, mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ) as mock_task_complete, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "from_date": Variable(
                value=None, data_type=date, writable=True, required=True
            ),
            "fruit": Variable(
                value=None, data_type=str, writable=False, required=True
            ),
        }
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        url = reverse("example.task", args=[99])
        data = {"from_date": "22/09/2017", "fruit": "Orange"}
        response = client.post(url, data)
        assert requests.codes.found == response.status_code
        # mock_task_claim.assert_called_with(336, user)
        mock_task_complete.assert_called_with(
            336,
            {
                "from_date": Variable(
                    value="2017-09-22",
                    data_type=date,
                    writable=True,
                    required=True,
                )
            },
            additional_variables={},
        )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "value,expect",
    [
        ("", None),
        ("0", False),
        ("1", True),
        ("false", False),
        ("true", True),
        (None, None),
    ],
)
def test_form_field_bool(value, expect):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        user = UserFactory()
        mock_form_variable.return_value = _mock_form_variable_info_bool(value)
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        form = MyTestForm(task_id=99, user=UserFactory())
        # check
        assert 1 == len(form.fields)
        assert "marmite" in form.fields
        field = form.fields["marmite"]
        assert expect == field.initial
        assert field.required is False


@pytest.mark.django_db
def test_form_field_date():
    """Check initial value of field."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        user = UserFactory()
        mock_form_variable.return_value = _mock_form_variable_info_date(
            "2013-03-22T20:13:59.000+01:00"
        )
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        form = MyTestForm(task_id=99, user=UserFactory())
        # check
        assert 1 == len(form.fields)
        assert "birthday" in form.fields
        field = form.fields["birthday"]
    assert localtime(
        datetime(2013, 3, 22, 19, 13, 59, 0, tzinfo=pytz.utc)
    ) == localtime(field.initial)
    assert field.required is True


@pytest.mark.django_db
def test_form_field_date_empty():
    """Check initial value of field."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        user = UserFactory()
        mock_form_variable.return_value = _mock_form_variable_info_date("")
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        form = MyTestForm(task_id=99, user=UserFactory())
        # check
        assert 1 == len(form.fields)
        assert "birthday" in form.fields
        field = form.fields["birthday"]
        assert field.initial is None
        assert field.required is True


@pytest.mark.django_db
@pytest.mark.parametrize("value,expect", [("accept", "accept"), ("", "")])
def test_form_field_enum(value, expect):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        user = UserFactory()
        mock_form_variable.return_value = _mock_form_variable_info_enum(value)
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        form = MyTestForm(task_id=99, user=UserFactory())
        # check
        assert 1 == len(form.fields)
        assert "decision" in form.fields
        field = form.fields["decision"]
        assert expect == field.initial
        assert field.required is True


@pytest.mark.django_db
@pytest.mark.parametrize("value,expect", [(None, None), ("", ""), (52, 52)])
def test_form_field_long(value, expect):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        user = UserFactory()
        mock_form_variable.return_value = _mock_form_variable_info_long(value)
        mock_task_status.return_value = _mock_task_status(user.pk)
        # test
        form = MyTestForm(task_id=99, user=UserFactory())
        # check
        assert 1 == len(form.fields)
        assert "age" in form.fields
        field = form.fields["age"]
        assert expect == field.initial
        assert field.required is True
