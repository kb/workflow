# -*- encoding: utf-8 -*-
import pytest

from example_workflow.models import ExampleContact
from login.tests.factories import GroupFactory, UserFactory


@pytest.mark.django_db
def test_user_in_a_group():
    """Is the user in the group?"""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    assert ExampleContact.objects.user_in_a_group(user, [group.pk]) is True


@pytest.mark.django_db
def test_user_in_a_group_multi():
    """Is the user in the group?"""
    group_1 = GroupFactory()
    group_2 = GroupFactory()
    group_3 = GroupFactory()
    group_4 = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group_2)
    user.groups.add(group_4)
    assert ExampleContact.objects.user_in_a_group(user, [group_1.pk]) is False
    assert ExampleContact.objects.user_in_a_group(user, [group_2.pk]) is True
    assert ExampleContact.objects.user_in_a_group(user, [group_3.pk]) is False
    assert ExampleContact.objects.user_in_a_group(user, [group_4.pk]) is True
    assert (
        ExampleContact.objects.user_in_a_group(user, [group_1.pk, group_2.pk])
        is True
    )
    assert (
        ExampleContact.objects.user_in_a_group(user, [group_1.pk, group_3.pk])
        is False
    )
    assert (
        ExampleContact.objects.user_in_a_group(user, [group_2.pk, group_4.pk])
        is True
    )
    assert (
        ExampleContact.objects.user_in_a_group(user, [group_3.pk, group_4.pk])
        is True
    )
    assert (
        ExampleContact.objects.user_in_a_group(
            user, [group_1.pk, group_2.pk, group_3.pk, group_4.pk]
        )
        is True
    )


@pytest.mark.django_db
def test_user_in_a_group_not():
    """Is the user in the group?"""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    group_with_no_user = GroupFactory()
    assert (
        ExampleContact.objects.user_in_a_group(user, [group_with_no_user.pk])
        is False
    )
