# -*- encoding: utf-8 -*-
import factory

from example_workflow.models import (
    DummyWorkflowObject,
    DummyWorkflowObjectUser,
    ExampleContact,
    ExampleGroup,
)
from login.tests.factories import UserFactory


class DummyWorkflowObjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DummyWorkflowObject

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


class DummyWorkflowObjectUserFactory(factory.django.DjangoModelFactory):
    obj = factory.SubFactory(DummyWorkflowObjectFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = DummyWorkflowObjectUser


class ExampleContactFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = ExampleContact


class ExampleGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExampleGroup
