# -*- encoding: utf-8 -*-
import pytest
import requests

from decimal import Decimal
from django import forms
from django.urls import reverse
from enum import Enum
from unittest import mock

from example_workflow.forms import MyTaskForm
from login.tests.factories import GroupFactory, UserFactory
from workflow.activiti import Variable
from workflow.forms import WorkflowFormMixin
from workflow.models import ActivitiError, MappingPlugin, WorkflowError
from workflow.tests.factories import MappingFactory, WorkflowFactory


def _mock_form_variable_info():
    return {
        "my_comment": Variable(name="Comment", value=None, data_type=str),
        "accept_request": Variable(
            name="Accept or Reject",
            value=None,
            choices={
                "accept": "Accept the change request",
                "reject_comment": "Reject the change request",
            },
            data_type=Enum,
            required=True,
        ),
    }


def _mock_task_status():
    return {
        "assignee": None,
        "category": "jersey",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "description": "Lovely summers day",
        "dueDate": None,
        "processDefinitionId": "makeHay:1:303",
        "name": "Make hay",
        "processInstanceId": "321",
        "id": "336",
        "formKey": None,
        "variables": [],
    }


@pytest.mark.django_db
def test_heading_no_title_help_text():
    """Heading fields without a title must have help text."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(name="", value=None, data_type=str)
        }
        mock_task_status.return_value = _mock_task_status()
        # setup
        MappingFactory(
            workflow=WorkflowFactory(process_key="makeHay"),
            help_text="Cricket",
            variable_name="manager_heading",
        )
        # test
        form = WorkflowFormMixin(data={"a": "a"}, user="apple", task_id=888)
        assert ["manager_heading"] == [x.name for x in form.visible_fields()]
        field = form.fields["manager_heading"]
        assert "Cricket" == field.help_text
        assert "" == field.label


@pytest.mark.django_db
def test_heading_no_title_no_help_text():
    """Heading fields without a title must have help text."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(name="", value=None, data_type=str)
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        with pytest.raises(WorkflowError) as e:
            WorkflowFormMixin(data={"a", "b"}, user="apple", task_id=888)
        assert (
            "The 'manager_heading' field does not have a "
            "label ('name' in the XML file) so must have "
            "'help_text' defined in the field mapping"
        ) in str(e.value)


@pytest.mark.django_db
def test_heading_no_value():
    """Heading fields should not have a value."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(
                name="Manager", value="Orange", data_type=str
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        with pytest.raises(WorkflowError) as e:
            WorkflowFormMixin(data={"a", "b"}, user="apple", task_id=888)
        assert (
            "Heading fields ('manager_heading') should "
            "not have a 'value': ('Orange')"
        ) in str(e.value)


@pytest.mark.django_db
def test_heading_not_writable():
    """Heading fields should not be 'writable'."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(
                name="Manager", value=None, data_type=str, writable=True
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        with pytest.raises(WorkflowError) as e:
            WorkflowFormMixin(data={"a", "b"}, user="apple", task_id=888)
        assert (
            "Heading fields must not be 'writable': 'manager_heading'"
        ) in str(e.value)


@pytest.mark.django_db
def test_heading_title_with_help_text():
    """Heading fields can have a title and help text."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(
                name="Football", value=None, data_type=str
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # setup
        MappingFactory(
            workflow=WorkflowFactory(process_key="makeHay"),
            help_text="Cricket",
            variable_name="manager_heading",
        )
        # test
        form = WorkflowFormMixin(data={"a": "a"}, user="apple", task_id=888)
        assert ["manager_heading"] == [x.name for x in form.visible_fields()]
        field = form.fields["manager_heading"]
        assert "Cricket" == field.help_text
        assert "Football" == field.label


@pytest.mark.django_db
def test_visible_fields():
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_task_status.return_value = _mock_task_status()
        # test
        form_data = {"data": "data"}
        form = WorkflowFormMixin(data=form_data, user="apple", task_id=888)
        assert ["my_comment", "accept_request"] == [
            x.name for x in form.visible_fields()
        ]


@pytest.mark.django_db
def test_visible_fields_heading():
    """Heading fields should be visible.

    A field which has ``_heading`` appended to it's ID needs to be included in
    the list of ``visible_fields``, so the label and help text can be rendered.

    """
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "manager_heading": Variable(
                name="Manager", value=None, data_type=str
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        form_data = {"data": "data"}
        form = WorkflowFormMixin(data=form_data, user="apple", task_id=888)
        visible_fields = form.visible_fields()
        # 'manager_heading' should appear in 'visible_fields'
        assert ["manager_heading"] == [x.name for x in visible_fields]
        manager_heading = visible_fields[0]
        assert manager_heading.is_hidden is True
        # 'manager_heading' should not appear in 'hidden_fields'
        assert [] == [x.name for x in form.hidden_fields()]


@pytest.mark.django_db
def test_hidden_fields():
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "audit_description": Variable(
                name="Audit Description", value=None, data_type=str
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        form_data = {"data": "data"}
        form = WorkflowFormMixin(data=form_data, user="apple", task_id=888)
        assert ["audit_description"] == [x.name for x in form.hidden_fields()]


@pytest.mark.django_db
def test_workflow_form_mixin(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {}
        mock_task_status.return_value = _mock_task_status()
        # test
        user = UserFactory()
        response = client.post(
            reverse("example.task", args=[99]), {"foo_select": user.pk}
        )
        assert requests.codes.found == response.status_code
        assert reverse("project.dash") == response["Location"]


@pytest.mark.django_db
def test_workflow_form_mixin_double(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "total": Variable(name="Total", value=None, data_type=Decimal)
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        response = client.post(
            reverse("example.task", args=[99]), {"total": 123.45}
        )
        assert requests.codes.found == response.status_code
        assert reverse("project.dash") == response["Location"]


@pytest.mark.django_db
def test_workflow_form_mixin_enum(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "colour": Variable(
                name="Favourite Colour",
                value=None,
                data_type=Enum,
                choices={"blue": "Blue", "green": "Green"},
                required=True,
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        workflow = WorkflowFactory(process_key="makeHay")
        MappingFactory(
            workflow=workflow,
            variable_name="colour",
            mapping_type=MappingPlugin.ENUM,
        )
        response = client.post(
            reverse("example.task", args=[99]), {"colour": "blue"}
        )
        assert requests.codes.found == response.status_code
        assert reverse("project.dash") == response["Location"]


@pytest.mark.django_db
def test_workflow_form_mixin_long(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        variable_info = _mock_form_variable_info()
        variable_info.update(
            {"score": Variable(name="totalScore", value=1212, data_type=int)}
        )
        mock_form_variable_info.return_value = variable_info
        mock_task_status.return_value = _mock_task_status()
        # setup
        WorkflowFactory(process_key="makeHay")
        # test
        form = MyTaskForm(task_id=3, user=UserFactory())
        # check
        assert "score" in form.fields
        field = form.fields["score"]
        assert type(field) == forms.CharField
        assert 1212 == field.initial


@pytest.mark.django_db
def test_workflow_form_mixin_no_comment_accept(client):
    """If we select 'accept', then there is no need to enter a comment."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_task_status.return_value = _mock_task_status()
        # test
        url = reverse("example.task", args=[99])
        data = {"accept_request": "accept"}
        response = client.post(url, data)
        assert requests.codes.found == response.status_code


@pytest.mark.django_db
def test_workflow_form_mixin_no_comment_invalid(client):
    """The accept/reject field is not of type 'enum'."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {
            "accept_request": Variable(
                name="Accept or Reject", value=None, data_type=str
            )
        }
        mock_task_status.return_value = _mock_task_status()
        # test
        url = reverse("example.task", args=[99])
        data = {"acceptRequest": "panda"}
        response = client.post(url, data)
        assert requests.codes.found == response.status_code


@pytest.mark.django_db
def test_workflow_form_mixin_no_comment_reject(client):
    """If we select 'reject', then we must enter a comment."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_task_status.return_value = _mock_task_status()
        # test
        url = reverse("example.task", args=[99])
        data = {"accept_request": "reject_comment"}
        response = client.post(url, data)
        assert requests.codes.ok == response.status_code
        form = response.context["form"]
        assert ["Please enter a good reason."] == form.errors["my_comment"]


# Moved to ``test_create_field_user_in_group`` (``work/tests/test_plugin.py``)
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group(client):
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        # mock return
#        variable_info = _mock_form_variable_info()
#        variable_info.update(
#            {
#                "manager_pk": Variable(
#                    name="User in Group",
#                    value=None,
#                    data_type=int,
#                    writable=True,
#                )
#            }
#        )
#        mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory()
#        user = UserFactory(pk=14, first_name="P", last_name="Kimber")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        MappingFactory(
#            workflow=workflow,
#            variable_name="manager_pk",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        # test
#        form = MyTaskForm(task_id=3, user=user)
#        # check
#        assert "manager_pk" in form.fields
#        assert type(form.fields["manager_pk"]) == forms.ChoiceField
#        assert 2 == len(form.fields["manager_pk"].choices)
#        assert [(None, "---------"), (14, "P Kimber")] == form.fields[
#            "manager_pk"
#        ].choices


# Moved to ``test_create_field_user_in_group`` (``work/tests/test_plugin.py``)
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group_deleted(client):
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        # mock return
#        variable_info = _mock_form_variable_info()
#        variable_info.update(
#            {
#                "manager": Variable(
#                    name="Manager", value=None, data_type=str, writable=True
#                )
#            }
#        )
#        mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory()
#        user = UserFactory(pk=14, first_name="P", last_name="Kimber")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        mapping = MappingFactory(
#            workflow=workflow,
#            variable_name="manager",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        mapping.set_deleted(user)
#        # test
#        form = MyTaskForm(task_id=3, user=user)
#        # check
#        assert "manager" in form.fields
#        assert type(form.fields["manager"]) == forms.CharField


# Should be moved to ``test_create_field_user_in_group``
# (``work/tests/test_plugin.py``), but I haven't!
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group_incorrect_field_type(client):
#    """The 'USER_IN_GROUP' mapping will only work with an integer field."""
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        # mock return
#        variable_info = _mock_form_variable_info()
#        variable_info.update(
#            {
#                "manager_pk": Variable(
#                    name="User in Group",
#                    value=None,
#                    data_type=str,
#                    writable=True,
#                )
#            }
#        )
#        mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory()
#        user = UserFactory(first_name="P", last_name="Kimber")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        MappingFactory(
#            workflow=workflow,
#            variable_name="manager_pk",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        # test
#        with pytest.raises(ActivitiError) as e:
#            MyTaskForm(task_id=3, user=user)
#        assert (
#            "Field 'manager_pk' must be an 'int' field for the "
#            "'user-in-group' mapping. The current type is 'str'"
#        ) in str(e.value)


# Moved to ``test_create_field_user_in_group`` (``work/tests/test_plugin.py``)
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group_read_only(client):
#    """Check ready only field for a user in a group."""
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        user = UserFactory(pk=14, first_name="P", last_name="Kimber")
#        # mock return
#        variable_info = _mock_form_variable_info()
#        variable_info.update(
#            {
#                "manager_pk": Variable(
#                    name="User in Group", data_type=int, value=user.pk
#                )
#            }
#        )
#        mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory(name="Fruit")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        MappingFactory(
#            workflow=workflow,
#            variable_name="manager_pk",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        # test
#        form = MyTaskForm(task_id=3, user=user)
#        # check
#        assert "manager_pk" in form.fields
#        field = form.fields["manager_pk"]
#        assert type(field) == forms.CharField
#        assert type(field.widget) == forms.TextInput
#        assert "readonly" in field.widget.attrs
#        assert field.widget.attrs["readonly"] is True
#        assert "P Kimber (Fruit Group)" == field.initial


# Moved to ``test_create_field_user_in_group`` (``work/tests/test_plugin.py``)
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group_read_only_invalid_user(client):
#    """Check ready only field for an invalid user in a group."""
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        # mock return
#        variable_info = _mock_form_variable_info()
#        variable_info.update(
#            {
#                "manager_pk": Variable(
#                    name="User in Group", data_type=int, value=9999
#                )
#            }
#        )
#        mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory(name="Fruit")
#        user = UserFactory(pk=14, first_name="P", last_name="Kimber")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        MappingFactory(
#            workflow=workflow,
#            variable_name="manager_pk",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        # test
#        form = MyTaskForm(task_id=3, user=user)
#        # check
#        assert "manager_pk" in form.fields
#        field = form.fields["manager_pk"]
#        assert type(field) == forms.CharField
#        assert type(field.widget) == forms.TextInput
#        assert "readonly" in field.widget.attrs
#        assert field.widget.attrs["readonly"] is True
#        assert "User in Fruit Group" == field.initial


# Should be moved to ``test_create_field_user_in_group``
# (``work/tests/test_plugin.py``), but I haven't!
# @pytest.mark.django_db
# def test_workflow_form_mixin_user_in_group_required(client):
#    """Must enter a required field."""
#    with mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti.task_claim"
#    ), mock.patch(
#        "workflow.activiti.Activiti.task_complete"
#    ), mock.patch(
#        "workflow.activiti.Activiti._task_status"
#    ) as mock_task_status:
#        # mock return
#        mock_form_variable_info.return_value = {
#            "manager_pk": Variable(
#                data_type=int,
#                name="User in Group",
#                required=True,
#                value=None,
#                writable=True,
#            )
#        }
#        # mock_form_variable_info.return_value = variable_info
#        mock_task_status.return_value = _mock_task_status()
#        # setup
#        group = GroupFactory()
#        user = UserFactory(first_name="P", last_name="Kimber")
#        user.groups.add(group)
#        workflow = WorkflowFactory(process_key="makeHay")
#        MappingFactory(
#            workflow=workflow,
#            variable_name="manager_pk",
#            mapping_type=MappingPlugin.USER_IN_GROUP,
#            group=group,
#        )
#        # test
#        url = reverse("example.task", args=[99])
#        response = client.post(url)
#        assert requests.codes.ok == response.status_code
#        form = response.context["form"]
#        assert ["This field is required."] == form.errors["manager_pk"]


@pytest.mark.django_db
def test_workflow_form_mixin_required(client):
    """Must enter a required field."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_task_status.return_value = _mock_task_status()
        # test
        url = reverse("example.task", args=[99])
        response = client.post(url)
        assert requests.codes.ok == response.status_code
        form = response.context["form"]
        assert ["This field is required."] == form.errors["accept_request"]
