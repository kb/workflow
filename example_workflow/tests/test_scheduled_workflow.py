# -*- encoding: utf-8 -*-
import pytest
import responses
import uuid

from django.conf import settings
from django.core.files import File
from http import HTTPStatus
from unittest import mock

from example_workflow.tests.factories import ExampleContactFactory
from login.tests.factories import UserFactory
from workflow.activiti import ActivitiError, Variable
from workflow.models import (
    ProcessDefinition,
    ScheduledWorkflow,
    WorkflowError,
)
from workflow.tests.factories import (
    MappingFactory,
    ProcessDefinitionFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowFactory,
)


process_definition_file_name = "holidayRequest.bpmn20.xml"
process_definition_file_path = (
    "example/tests/data/" + process_definition_file_name
)


def _mock_form_variable_info():
    return {
        "age": Variable(
            name="Age", value=None, data_type=int, required=True, writable=True
        ),
        "object_pk": Variable(
            name="Object ID",
            value=None,
            data_type=int,
            required=True,
            writable=True,
        ),
        "user_pk": Variable(
            name="User ID",
            value=None,
            data_type=int,
            required=True,
            writable=True,
        ),
    }


def _mock_form_variable_info_no_user_pk():
    """The 'startEvent' for the workflow has no 'user_pk'."""
    return {
        "age": Variable(
            name="Age", value=None, data_type=int, required=True, writable=True
        ),
        "object_pk": Variable(
            name="Object ID",
            value=None,
            data_type=int,
            required=True,
            writable=True,
        ),
    }


def deploy_holiday_request():
    """What is this?  Is it a test?"""
    definition_file = File(
        open(process_definition_file_path), name=process_definition_file_name
    )
    process_definition = ProcessDefinition(
        process_definition=definition_file,
        version=ProcessDefinition.objects.default_key_version_number(),
    )
    process_definition.save()
    return process_definition.deploy()


@pytest.mark.django_db
@responses.activate
def test_create_workflows():
    user = UserFactory()
    responses.add(
        responses.POST,
        "http://{}:{}/{}service/runtime/process-instances/".format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
        ),
        json={"id": "8d3ecaec-e7d6-4924-bf70-2233b03cb06d"},
        status=HTTPStatus.CREATED,
    )
    responses.add(
        responses.POST,
        (
            "http://{}:{}/{}service/runtime/process-instances/"
            "8d3ecaec-e7d6-4924-bf70-2233b03cb06d/identitylinks"
        ).format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
        ),
        json={
            "url": "http://localhost:8080/flowable-rest/service/runtime/process-"
            "instances/e61a2011-73b1-11ec-bf7c-a46bb699319e/identitylinks/users/"
            "{}/participant".format(user.pk),
            "user": "{}".format(user.pk),
            "group": None,
            "type": "participant",
        },
        status=HTTPStatus.CREATED,
    )
    responses.add(
        responses.GET,
        "http://{}:{}/{}service/form/form-data?processDefinitionId=6".format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
        ),
        json={
            "formProperties": [
                {
                    "enumValues": [],
                    "id": "userPk",
                    "name": "patrick",
                    "required": False,
                    "value": user.pk,
                    "writable": False,
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    process_key = "makeCoffee"
    workflow = WorkflowFactory(process_key=process_key)
    MappingFactory(workflow=workflow, variable_name="age")
    ProcessDefinitionFactory(process_key=process_key, workflow_id="6")
    scheduled = ScheduledWorkflowFactory(
        content_object=ExampleContactFactory(),
        process_definition=process_key,
    )
    scheduled_user = ScheduledWorkflowUserFactory(
        scheduled_workflow=scheduled,
        user=user,
    )
    ExampleContactFactory(user=user)
    # test
    result = ScheduledWorkflow.objects.create_workflows(scheduled.pk)
    # check
    assert [scheduled_user] == result
    scheduled_user.refresh_from_db()
    assert (
        "8d3ecaec-e7d6-4924-bf70-2233b03cb06d"
        == scheduled_user.get_process_id()
    )


@pytest.mark.django_db
def test_create_workflows_no_user_pk():
    """Set the user in the start event when we create a workflow.

    https://www.kbsoftware.co.uk/docs/app-workflow.html#start-event-startevent

    """
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.process_start"
    ) as mock_process_start:
        # mock return
        process_uuid = str(uuid.uuid4())
        mock_form_variable_info.return_value = (
            _mock_form_variable_info_no_user_pk()
        )
        mock_process_start.return_value = process_uuid
        # setup
        process_key = "makeCoffee"
        workflow = WorkflowFactory(process_key=process_key)
        MappingFactory(workflow=workflow, variable_name="age")
        ProcessDefinitionFactory(process_key=process_key, workflow_id=456)
        scheduled = ScheduledWorkflowFactory(
            content_object=ExampleContactFactory(),
            process_definition=process_key,
        )
        scheduled_user = ScheduledWorkflowUserFactory(
            scheduled_workflow=scheduled
        )
        ExampleContactFactory(user=scheduled_user.user)
        # test
        with pytest.raises(ActivitiError) as e:
            ScheduledWorkflow.objects.create_workflows(scheduled.pk)
        assert (
            "ScheduledWorkflow - cannot create workflow. "
            "The 'user_pk' variable is not defined in the 'startEvent' "
            "for 'makeCoffee' (variables ['age', 'object_pk']). "
            "The 'user_pk' is used by the 'started_by' filter for "
            "workflow processes"
        ) in str(e.value)


@pytest.mark.django_db
def test_for_content_object():
    u1 = UserFactory()
    u2 = UserFactory()
    ScheduledWorkflowFactory(content_object=u1, process_definition="a")
    ScheduledWorkflowFactory(content_object=u2, process_definition="b")
    ScheduledWorkflowFactory(content_object=u1, process_definition="c")
    ScheduledWorkflowFactory(content_object=u1, process_definition="d")
    qs = ScheduledWorkflow.objects.for_content_object(u1)
    qs = qs.order_by("process_definition")
    assert [u1, u1, u1] == [obj.content_object for obj in qs]
    assert ["a", "c", "d"] == [obj.process_definition for obj in qs]


@pytest.mark.django_db
def test_for_content_object_process_definition():
    """Filter by 'process_definition'."""
    u1 = UserFactory()
    u2 = UserFactory()
    ScheduledWorkflowFactory(pk=1, content_object=u1, process_definition="a")
    ScheduledWorkflowFactory(pk=2, content_object=u2, process_definition="b")
    ScheduledWorkflowFactory(pk=3, content_object=u1, process_definition="c")
    ScheduledWorkflowFactory(pk=4, content_object=u1, process_definition="a")
    qs = ScheduledWorkflow.objects.for_content_object(
        u1, process_definitions=["a"]
    ).order_by("pk")
    assert [u1, u1] == [obj.content_object for obj in qs]
    assert [1, 4] == [obj.pk for obj in qs]


@pytest.mark.django_db
def test_for_content_object_string():
    """'process_definitions' must not be a string."""
    u1 = UserFactory()
    ScheduledWorkflowFactory(content_object=u1, process_definition="a")
    with pytest.raises(WorkflowError) as e:
        ScheduledWorkflow.objects.for_content_object(
            u1, process_definitions="a"
        )
    assert "is a list (not a string)" in str(e.value)


@pytest.mark.django_db
def test_str():
    obj = ScheduledWorkflowFactory(
        process_definition="abc", initiator=UserFactory(username="pat")
    )
    assert "'abc' initiator 'pat'" in str(obj)


@pytest.mark.django_db
def test_scheduled_users():
    # setup
    obj = ScheduledWorkflowFactory()
    for item in (("ben", None), ("matt", 1), ("pat", None)):
        user_name, process_id = item
        ScheduledWorkflowUserFactory(
            scheduled_workflow=obj,
            user=UserFactory(username=user_name),
            process_id=process_id,
        )
    # deleted
    ScheduledWorkflowUserFactory(
        scheduled_workflow=obj, user=UserFactory(username="kit"), deleted=True
    )
    # another
    ScheduledWorkflowUserFactory()
    assert ["ben", "pat"] == [
        obj.user.username for obj in obj.scheduled_users()
    ]
