# -*- encoding: utf-8 -*-
import pytest

from example_workflow.models import ExampleGroup
from workflow.models import WorkflowError
from workflow.tests.factories import FlowListTypeFactory


@pytest.mark.django_db
def test_has_group_class():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
        group_app="example_workflow",
        group_module="models",
        group_class="ExampleGroup",
        group_description="Shade",
    )
    assert flow_list_type.has_group_class() is True


@pytest.mark.django_db
def test_has_group_class_not():
    flow_list_type = FlowListTypeFactory(
        name="",
        group_app="example_workflow",
        group_module="models",
        group_class="",
    )
    assert flow_list_type.has_group_class() is False


@pytest.mark.django_db
def test_import_group_class():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
        group_app="example_workflow",
        group_module="models",
        group_class="ExampleGroup",
    )
    group_class = flow_list_type.import_group_class()
    assert ExampleGroup == group_class


@pytest.mark.django_db
def test_import_group_class_does_not_exist():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
        group_app="example_workflow",
        group_module="models-does-not-exist",
        group_class="ExampleGroupDoesNotExist",
    )
    with pytest.raises(WorkflowError) as e:
        flow_list_type.import_group_class()
    assert (
        "Cannot find 'ExampleGroupDoesNotExist'. Do you have "
        "'models-does-not-exist.py' in your 'example_workflow' package?"
    ) in str(e.value)


@pytest.mark.django_db
def test_import_group_class_empty():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
        group_app="",
        group_module="",
        group_class="",
    )
    with pytest.raises(WorkflowError) as e:
        flow_list_type.import_group_class()
    assert "Cannot find ''. Do you have '.py' in your '' package?" in str(
        e.value
    )
