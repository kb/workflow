# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data_workflow():
    call_command("demo-data-login")
    # PJK 28/11/2019, Not sure how to test this at the moment...
    # call_command("demo_data_workflow")
