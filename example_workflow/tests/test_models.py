# -*- encoding: utf-8 -*-
import pytest
import responses

from datetime import date, datetime
from dateutil.tz import tzoffset
from django.conf import settings
from enum import Enum
from http import HTTPStatus
from unittest import mock

from example_workflow.tests.factories import ExampleContactFactory
from login.tests.factories import GroupFactory, UserFactory
from workflow.activiti import Activiti, TaskData, Variable
from workflow.models import (
    get_contact_model,
    Mapping,
    MappingPlugin,
    user_in_task_group,
    user_owns_task,
)
from workflow.tests.factories import MappingFactory, WorkflowFactory


def _mock_task():
    return TaskData(
        pk=223,
        assignee=1,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        description="",
        deleted=False,
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Having checked the PO and GRN",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="flow",
        user_can_delete_workflow=False,
        variables=[],
    )


def _mock_task_identity_list(group_pk):
    return [
        {
            "url": (
                "http://localhost:8080/activiti-rest/service/runtime/tasks"
                "/2843/identitylinks/groups/1/candidate"
            ),
            "user": None,
            "group": group_pk,
            "type": "candidate",
        }
    ]


@pytest.mark.django_db
def test_get_contact_model():
    assert get_contact_model() is not None


@pytest.mark.django_db
def test_get_contact_model_get():
    ExampleContactFactory(user=UserFactory(username="sam"))
    get_contact_model().objects.get(user__username="sam")


@pytest.mark.django_db
def test_auto_generate_variables():
    """Auto generate variables e.g. email address for a user.

    Test copied to ``dash/tests/test_models.py`` in the ``bpm`` project.
    Also copied to ``example_work/tests/test_models.py`` in the ``work`` app.

    """
    group = GroupFactory(name="Carnival")
    chair = UserFactory(
        first_name="Clare", last_name="Green", email="clare@kbsoftware.co.uk"
    )
    ExampleContactFactory(user=chair)
    workflow = WorkflowFactory(process_key="apple")
    mapping_user = MappingFactory(
        workflow=workflow,
        variable_name="chair",
        mapping_type=MappingPlugin.USER,
    )
    Mapping.objects.generate_extra(mapping_user)
    mapping_group = MappingFactory(
        workflow=workflow,
        variable_name="committee_pk",
        mapping_type=MappingPlugin.GROUP,
        group=group,
    )
    Mapping.objects.generate_extra(mapping_group)
    variables = {
        "chair": Variable(value=chair.pk, data_type=int),
        "committee_pk": Variable(value=group.pk, data_type=int),
    }
    plugin = MappingPlugin()
    result = plugin.auto_generate_variables(workflow.process_key, variables)
    expect = {
        "chair_email": Variable(value="clare@kbsoftware.co.uk", data_type=str),
        "chair_full_name": Variable(value="Clare Green", data_type=str),
        "committee_name": Variable(value="Carnival", data_type=str),
    }
    assert expect == result


@pytest.mark.django_db
def test_init_workflow_variables():
    user = UserFactory(
        pk=32,
        first_name="Pat",
        last_name="Kimber",
        email="patrick@kbsoftware.co.uk",
    )
    contact = ExampleContactFactory(pk=33, user=user)
    form_variables = {
        "api_url": Variable(value=None, data_type=str),
        "colour": Variable(value=None, data_type=str),
        "object_pk": Variable(value=None, data_type=int),
        "user_name": Variable(value=None, data_type=str),
        "user_pk": Variable(value=None, data_type=int),
    }
    workflow = WorkflowFactory()
    result = MappingPlugin().init_workflow_variables(
        workflow.process_key, form_variables, user, contact
    )
    # '33' is from 'init_workflow_variables' in 'example_workflow/models.py'
    assert {
        "api_url": Variable(
            value="http://127.0.0.1:3042/api/0.1", data_type=str
        ),
        "colour": Variable(value="", data_type=str),
        "object_pk": Variable(value=33, data_type=int),
        "user_name": Variable(value="Pat Kimber", data_type=str),
        "user_email": Variable(value="patrick@kbsoftware.co.uk", data_type=str),
        "user_full_name": Variable(value="Pat Kimber", data_type=str),
        "user_pk": Variable(value=32, data_type=int),
    } == result


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data_type,value",
    [(None, ""), (str, ""), (Enum, ""), (date, ""), (int, 0)],
)
def test_init_workflow_variables_missing(data_type, value):
    """No mapping for a workflow variable, so create an empty one.

    Fix issues where a workflow is expecting a variable to exist e.g::

      Unknown property used in expression: ${requestType == "newDocument"}

    In this example, if the ``requestType`` is empty or not mapped, then a
    variable won't be created:
    https://www.kbsoftware.co.uk/crm/ticket/5142/

    """
    user = UserFactory(
        first_name="Pat",
        last_name="Kimber",
        email="patrick@kbsoftware.co.uk",
    )
    contact = ExampleContactFactory(user=user)
    form_variables = {
        "colour": Variable(value=None, data_type=data_type),
    }
    workflow = WorkflowFactory()
    result = MappingPlugin().init_workflow_variables(
        workflow.process_key, form_variables, user, contact
    )
    assert {"colour": Variable(value=value, data_type=data_type)} == result


@pytest.mark.django_db
def test_init_workflow_variables_mapping():
    """The supervisor ID is passed to the function.

    See ``test_init_workflow_variables_mapping_content_object`` for the same
    feature, but using the ``content_object``.

    """
    user = UserFactory(
        first_name="Pat", last_name="Kimber", email="patrick@kbsoftware.co.uk"
    )
    manager = UserFactory(
        first_name="M", last_name="Manager", email="manager@kbsoftware.co.uk"
    )
    ExampleContactFactory(user=manager)
    contact_content_object = ExampleContactFactory(user=user)
    workflow = WorkflowFactory(process_key="kiwi")
    # We use `manager_pk' as the variable name because it isn't in
    # 'init_workflow_variables' ('example_workflow/models.py').
    MappingFactory(
        workflow=workflow,
        variable_name="manager_pk",
        mapping_type=MappingPlugin.IMPORT_USER,
    )
    form_variables = {
        "manager_pk": Variable(value=manager.pk, data_type=int),
        "user_pk": Variable(value=None, data_type=int),
    }
    plugin = MappingPlugin()
    result = plugin.init_workflow_variables(
        workflow.process_key, form_variables, user, contact_content_object
    )
    assert {
        "manager_pk": Variable(value=manager.pk, data_type=int),
        "manager_email": Variable(
            value="manager@kbsoftware.co.uk", data_type=str
        ),
        "manager_full_name": Variable(value="M Manager", data_type=str),
        "user_email": Variable(value="patrick@kbsoftware.co.uk", data_type=str),
        "user_full_name": Variable(value="Pat Kimber", data_type=str),
        "user_pk": Variable(value=user.pk, data_type=int),
    } == result


@pytest.mark.django_db
def test_init_workflow_variables_mapping_content_object():
    """The supervisor ID is from the 'ExampleContactFactory'."""
    user = UserFactory(
        first_name="Pat", last_name="Kimber", email="patrick@kbsoftware.co.uk"
    )
    # '22' is from 'init_workflow_variables' in 'example_workflow/models.py'
    supervisor = UserFactory(
        pk=22, first_name="S", last_name="Super", email="super@kbsoftware.co.uk"
    )
    ExampleContactFactory(user=supervisor)
    contact_content_object = ExampleContactFactory(user=user)
    workflow = WorkflowFactory(process_key="kiwi")
    MappingFactory(
        workflow=workflow,
        variable_name="supervisor_pk",
        mapping_type=MappingPlugin.IMPORT_USER,
    )
    form_variables = {
        "supervisor_pk": Variable(value=None, data_type=int),
        "user_pk": Variable(value=None, data_type=int),
    }
    plugin = MappingPlugin()
    result = plugin.init_workflow_variables(
        workflow.process_key, form_variables, user, contact_content_object
    )
    assert {
        "supervisor_pk": Variable(value=supervisor.pk, data_type=int),
        "supervisor_email": Variable(
            value="super@kbsoftware.co.uk", data_type=str
        ),
        "supervisor_full_name": Variable(value="S Super", data_type=str),
        "user_email": Variable(value="patrick@kbsoftware.co.uk", data_type=str),
        "user_full_name": Variable(value="Pat Kimber", data_type=str),
        "user_pk": Variable(value=user.pk, data_type=int),
    } == result


@pytest.mark.django_db
def test_init_workflow_variables_mapping_not(caplog):
    user = UserFactory()
    contact_content_object = ExampleContactFactory(user=user)
    form_variables = {"cannot_be_found": Variable(value=None, data_type=int)}
    workflow = WorkflowFactory(process_key="apple")
    plugin = MappingPlugin()
    result = plugin.init_workflow_variables(
        workflow.process_key, form_variables, user, contact_content_object
    )
    assert {"cannot_be_found": Variable(value=0, data_type=int)} == result
    assert (
        "Workflow 'apple' cannot find mapping value for 'cannot_be_found' "
        "('int') (created default / empty value)"
    ) in [x.message for x in caplog.records]


@pytest.mark.django_db
def test_init_workflow_variables_mapping_group():
    user = UserFactory(
        first_name="Pat", last_name="Kimber", email="patrick@kbsoftware.co.uk"
    )
    ExampleContactFactory(user=user)
    contact = ExampleContactFactory()
    group = GroupFactory()
    workflow = WorkflowFactory(process_key="kiwi")
    MappingFactory(
        workflow=workflow,
        variable_name="music_group_pk",
        mapping_type=MappingPlugin.GROUP,
        group=group,
    )
    form_variables = {
        "music_group_pk": Variable(value=None, data_type=int),
        "user_pk": Variable(value=None, data_type=int),
    }
    plugin = MappingPlugin()
    result = plugin.init_workflow_variables(
        workflow.process_key, form_variables, user, contact
    )
    # '33' is from 'init_workflow_variables' in 'example_workflow/models.py'
    assert {
        "music_group_pk": Variable(value=33, data_type=int),
        "user_email": Variable(value="patrick@kbsoftware.co.uk", data_type=str),
        "user_full_name": Variable(value="Pat Kimber", data_type=str),
        "user_pk": Variable(value=user.pk, data_type=int),
    } == result


@pytest.mark.django_db
@responses.activate
def test_user_in_task_group():
    """Is the user in the group for this task?"""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    with mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        mock_identity_link.return_value = _mock_task_identity_list(group.pk)
        assert user_in_task_group(Activiti(), user, _mock_task()) is True


@pytest.mark.django_db
@responses.activate
def test_user_in_task_group_not():
    """Is the user in the group for this task?"""
    group = GroupFactory()
    group_with_no_user = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    with mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        mock_identity_link.return_value = _mock_task_identity_list(
            group_with_no_user.pk
        )
        assert user_in_task_group(Activiti(), user, _mock_task()) is False


@pytest.mark.django_db
@responses.activate
def test_user_owns_task():
    """The user is the assignee, so they own the task."""
    user = UserFactory()
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee=user.pk,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    assert user_owns_task(Activiti(), user, task) is True


@pytest.mark.django_db
@responses.activate
def test_user_owns_task_delegate():
    """The user has been delegated the task by another user!"""
    user_1 = UserFactory()
    user_2 = UserFactory()
    contact_1 = ExampleContactFactory(user=user_1)
    contact_2 = ExampleContactFactory(user=user_2, delegate=contact_1)
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee=user_2.pk,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    assert user_owns_task(Activiti(), user_1, task) is True


@pytest.mark.django_db
@responses.activate
def test_user_owns_task_invalid_assignee(caplog):
    """The assignee is invalid (should be the user ID / an integer)."""
    user = UserFactory()
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee="abcde",
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    assert user_owns_task(Activiti(), user, task) is False
    assert (
        "Assignee for task 'c3d77488-dd5f-11ea-98d7-c6e9a110df66' "
        "is not an integer: 'abcde'"
    ) in [x.message for x in caplog.records]


@pytest.mark.django_db
@responses.activate
def test_user_owns_task_not():
    """The user does *not* own the task."""
    user_1 = UserFactory()
    user_2 = UserFactory()
    contact_1 = ExampleContactFactory(user=user_1)
    contact_2 = ExampleContactFactory(user=user_2)
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee=user_2.pk,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    assert user_owns_task(Activiti(), user_1, task) is False


@pytest.mark.django_db
@responses.activate
def test_user_owns_task_group():
    """The user owns the task because they are in the group."""
    group = GroupFactory()
    user = UserFactory()
    # add the user to the group
    user.groups.add(group)
    contact = ExampleContactFactory(user=user)
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee=None,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    responses.add(
        responses.GET,
        "http://{}:{}/{}service/runtime/tasks/{}/identitylinks".format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
            "c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        ),
        json=[
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/tasks"
                    "/2843/identitylinks/groups/1/candidate"
                ),
                "user": None,
                "group": group.pk,
                "type": "candidate",
            }
        ],
        status=HTTPStatus.OK,
    )
    assert user_owns_task(Activiti(), user, task) is True


@pytest.mark.django_db
@responses.activate
def test_user_owns_task_group_not():
    """The user is *not* in the group which owns the task."""
    group = GroupFactory()
    user = UserFactory()
    # do *not* add the user to the group
    # user.groups.add(group)
    contact = ExampleContactFactory(user=user)
    task = TaskData(
        pk="c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        assignee=None,
        category="Approve Invoice",
        claim_time=None,
        completed=None,
        created=datetime(
            2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
        ),
        definition_id="invoiceApproval",
        deleted=False,
        description="",
        due_date=datetime(2017, 8, 2, 8, 31),
        end_time=None,
        is_group_task=False,
        is_work=False,
        name="Check the PO",
        process_id=167,
        process_key="invoiceApproval",
        process_name="Invoice Approval",
        status="",
        url="https://www.hatherleigh.net/task/cabbfa58-dd5f-11ea-98d7",
        url_name="",
        user_can_delete_workflow=False,
        variables=[],
    )
    responses.add(
        responses.GET,
        "http://{}:{}/{}service/runtime/tasks/{}/identitylinks".format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
            "c3d77488-dd5f-11ea-98d7-c6e9a110df66",
        ),
        json=[
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/tasks"
                    "/2843/identitylinks/groups/1/candidate"
                ),
                "user": None,
                "group": group.pk,
                "type": "candidate",
            }
        ],
        status=HTTPStatus.OK,
    )
    assert user_owns_task(Activiti(), user, task) is False
