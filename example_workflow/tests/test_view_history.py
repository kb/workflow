# -*- encoding: utf-8 -*-
import pytest
import uuid

from collections import OrderedDict
from datetime import datetime
from dateutil.tz import tzoffset
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from example_workflow.tests.factories import ExampleContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from workflow.tests.factories import (
    MappingFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowFactory,
)


def _mock_history_task():
    return {
        "order": "desc",
        "total": 4,
        "sort": "endTime",
        "size": 4,
        "data": [
            {
                "category": "Check/Complete Invoice Information",
                "formKey": "flow",
                "claimTime": "2017-07-03T08:31:10.104+01:00",
                "executionId": "199",
                "id": "202",
                "description": "When you have finished please click the 'Complete Task' button to send it to your Manager:",
                "workTimeInMillis": 71,
                "taskDefinitionKey": "sid-EC1A150E-131B-4758-86BF-10025A5D2993",
                "processInstanceId": "167",
                "dueDate": "2017-08-02T08:27:56.877+01:00",
                "assignee": "1",
                "deleteReason": None,
                "startTime": "2017-07-03T08:27:56.878+01:00",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/167",
                "endTime": "2017-07-03T08:31:10.175+01:00",
                "priority": 50,
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/202",
                "variables": [],
                "tenantId": "",
                "processDefinitionId": "invoiceApproval:1:55",
                "owner": None,
                "name": "Check/Complete invoice information fields for the shown invoice ",
                "parentTaskId": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/invoiceApproval:1:55",
                "durationInMillis": 193297,
            }
        ],
        "start": 0,
    }


def _mock_process_instance_list():
    return {
        "start": 0,
        "data": [
            {
                "activityId": None,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/52",
                "completed": False,
                "processDefinitionId": "changeRequest:1:43",
                "businessKey": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/changeRequest:1:43",
                "variables": [
                    {
                        "value": 1,
                        "name": "objectPk",
                        "scope": "global",
                        "type": "integer",
                    }
                ],
                "id": "52",
                "tenantId": "",
                "suspended": False,
                "ended": False,
            }
        ],
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    }
                ],
                "suspended": False,
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "url": "http://localhost:8080/activiti-rest/a/61",
                "completed": False,
                "processDefinitionName": "timeOff",
                "processDefinitionId": "timeOff:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/b/invoiceApproval:1:43",
            }
        ]
    }


def _mock_user_task_list():
    return {
        "data": [
            {
                "id": "cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "assignee": 1,
                "category": "Approve Invoice",
                "claimTime": None,
                "createTime": "2017-07-03T08:31:10.000+01:00",
                "delegationState": None,
                "description": None,
                "dueDate": "2020-09-12T12:23:28.116Z",
                "executionId": "c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "executionUrl": "http://172.20.0.2/service/runtime/executions/c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "formKey": "flow",
                "name": "Having checked the PO and GRN",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processDefinitionUrl": "http://172.20.0.2/service/repository/process-definitions/invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processInstanceId": "c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "processInstanceUrl": "http://172.20.0.2/service/runtime/process-instances/c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "scopeDefinitionId": None,
                "scopeId": None,
                "scopeType": None,
                "suspended": False,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "tenantId": "",
                "url": "http://172.20.0.2/service/runtime/tasks/cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "variables": [],
            }
        ],
        "total": 1,
        "start": 0,
        "sort": "createTime",
        "order": "desc",
        "size": 1,
    }


@pytest.mark.django_db
@pytest.mark.parametrize("process_id", ["64", None])
def test_history_task(client, process_id):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        mock_user_task_list.return_value = _mock_user_task_list()
        # setup
        scheduled_workflow = ScheduledWorkflowFactory(
            content_object=ExampleContactFactory()
        )
        if process_id is None:
            process_id = str(uuid.uuid4())
            ScheduledWorkflowUserFactory(
                scheduled_workflow=scheduled_workflow, process_uuid=process_id
            )
        else:
            ScheduledWorkflowUserFactory(
                scheduled_workflow=scheduled_workflow, process_id=process_id
            )
        workflow = WorkflowFactory(name="Time Off", process_key="timeOff")
        MappingFactory(
            workflow=workflow,
            variable_name="object_pk",
            history_summary_column=1,
        )
        # test
        response = client.get(
            reverse("workflow.history.task", args=[process_id])
        )
        assert HTTPStatus.OK == response.status_code
        result = response.context["object_list"]
        assert [
            {
                "assignee": 1,
                "category": "Approve Invoice",
                "claim_time": None,
                "completed": None,
                "created": datetime(
                    2017, 7, 3, 8, 31, 10, 0, tzinfo=tzoffset(None, 3600)
                ),
                "deleted": False,
                "end_time": None,
                "name": "Having checked the PO and GRN",
                "process_id": "c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "process_name": "Time Off",
                "process_variables": OrderedDict([("Object Pk", "29")]),
            },
            {
                "assignee": 1,
                "category": "Check/Complete Invoice Information",
                "claim_time": datetime(
                    2017, 7, 3, 8, 31, 10, 104000, tzinfo=tzoffset(None, 3600)
                ),
                "completed": True,
                "created": None,
                "deleted": False,
                "end_time": datetime(
                    2017, 7, 3, 8, 31, 10, 175000, tzinfo=tzoffset(None, 3600)
                ),
                "name": "Check/Complete invoice information fields for the shown invoice ",
                "process_id": 167,
                "process_name": "Time Off",
                "process_variables": OrderedDict([("Object Pk", "29")]),
            },
        ] == result
        assert "attachments" in response.context
        assert [
            {
                "link": {
                    "url": "http://localhost:8000/link/link/1/",
                    "file_name": "linklink1.doc",
                    "created": datetime(2020, 1, 11, 15, 23),
                    "title": "My Link is Link 1",
                }
            },
            {
                "link": {
                    "url": "http://localhost:8000/link/link/1/",
                    "file_name": "linklink1.doc",
                    "created": datetime(2020, 1, 11, 15, 23),
                    "title": "My Link is Link 1",
                }
            },
        ]
