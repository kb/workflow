# -*- encoding: utf-8 -*-
import pytest
import requests

from django.urls import reverse
from unittest import mock

from example_workflow.tests.factories import ExampleContactFactory
from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory
from workflow.activiti import ActivitiError, Variable
from workflow.models import MappingPlugin, WorkflowData, WorkflowError
from workflow.tests.factories import (
    MappingFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowFactory,
)


def _mock_historic_process():
    return {
        "size": 1,
        "order": "desc",
        "sort": "endTime",
        "start": 0,
        "total": 1,
        "data": [
            {
                "url": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/64",
                "deleteReason": None,
                "startUserId": "kermit",
                "name": None,
                "endActivityId": "sid-285E0710-74F1-4B10-80E3-97FFA88E5252",
                "endTime": "2017-06-14T10:39:11.188+01:00",
                "superProcessInstanceId": None,
                "startActivityId": "startEvent1",
                "startTime": "2017-06-14T10:38:59.001+01:00",
                "processDefinitionId": "timeOff:1:63",
                "tenantId": "",
                "processDefinitionName": "timeOff",
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "id": "64",
                "businessKey": None,
                "durationInMillis": 12187,
                "variables": [
                    {
                        "name": "contact",
                        "value": "joey",
                        "type": "string",
                        "scope": " local",
                    },
                    {
                        "name": "fromDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                    {
                        "name": "toDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                ],
            }
        ],
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "name": "objectPk",
                        "value": 29,
                        "type": "integer",
                        "scope": "local",
                    },
                    {
                        "name": "contact",
                        "value": "joey",
                        "type": "string",
                        "scope": " local",
                    },
                    {
                        "name": "toDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/61",
                "completed": True,
                "processDefinitionId": "timeOff:1:43",
                "processDefinitionName": "timeOff",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:43",
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


def _mock_task_status():
    return {
        "assignee": None,
        "category": "jersey",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "description": "Lovely summers day",
        "dueDate": None,
        "processDefinitionId": "makeHay:1:303",
        "name": "Make hay",
        "processInstanceId": "321",
        "id": "336",
        "formKey": None,
        "variables": [],
    }


@pytest.mark.django_db
def test_get(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {}
        mock_task_status.return_value = _mock_task_status()
        # test
        user = UserFactory()
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        response = client.get(reverse("example.task", args=[99]))
        assert requests.codes.ok == response.status_code


@pytest.mark.django_db
def test_get_completed(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.return_value = {}
        mock_task_status.side_effect = ActivitiError(
            "Tiger", requests.codes.not_found
        )
        # test
        user = UserFactory()
        assert client.login(username=user.username, password=TEST_PASSWORD)
        response = client.get(reverse("example.task", args=[99]))
        assert requests.codes.found == response.status_code
        assert (
            reverse("workflow.task.history", args=[99]) == response["Location"]
        )


@pytest.mark.django_db
def test_get_activiti_error(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status:
        # mock return
        mock_form_variable_info.side_effect = ActivitiError(
            "Lion", requests.codes.server_error
        )
        mock_task_status.return_value = _mock_task_status()
        # test
        user = UserFactory()
        assert client.login(username=user.username, password=TEST_PASSWORD)
        response = client.get(reverse("example.task", args=[99]))
        assert requests.codes.found == response.status_code
        assert (
            reverse("workflow.task.history.error", args=[99])
            == response["Location"]
        )


@pytest.mark.django_db
def test_post_copy_to_document_management(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "example_workflow.views.MyTaskFormView.copy_to_document_management"
    ) as mock_copy_to_document_management:
        # mock return
        mock_form_variable_info.return_value = {
            "copy_to_document_management": Variable(
                data_type=str,
                java_name="copyToDocumentManagement",
                name="Copy to Document Management",
                value="Copy to Alfresco",
            )
        }
        mock_historic_process.return_value = _mock_historic_process()
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status()
        # test
        user = UserFactory()
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        response = client.post(reverse("example.task", args=[99]), {})
        assert requests.codes.found == response.status_code
        assert reverse("project.dash") == response["Location"]
        assert mock_copy_to_document_management.called is True


@pytest.mark.django_db
def test_post_data_document(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ), mock.patch(
        "example_workflow.views.MyTaskFormView.write_data_document"
    ) as mock_write_data_document:
        # mock return
        mock_form_variable_info.return_value = {
            "data_document": Variable(
                data_type=str,
                java_name="dataDocument",
                name="Data Document",
                value="Customer Data",
            ),
            "coffee": Variable(
                data_type=bool,
                java_name="coffee",
                name="Coffee",
                value=True,
                writable=True,
            ),
            "manager": Variable(
                data_type=str,
                java_name="manager",
                name="Manager",
                value=None,
                writable=True,
            ),
        }
        mock_historic_process.return_value = _mock_historic_process()
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status()
        # test
        assert 0 == WorkflowData.objects.count()
        user = UserFactory()
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        data = {"manager": "joey"}
        response = client.post(reverse("example.task", args=[99]), data)
        assert requests.codes.found == response.status_code
        assert reverse("project.dash") == response["Location"]
        assert mock_write_data_document.called is True


@pytest.mark.django_db
def test_post_data_document_missing_write_data_document(client):
    """The view does not have a 'write_data_document' method."""
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ):
        # mock return
        mock_form_variable_info.return_value = {
            "data_document": Variable(
                data_type=str,
                java_name="dataDocument",
                name="Data Document",
                writable=True,
                value="Customer Data",
            ),
            "manager": Variable(
                data_type=str,
                java_name="manager",
                name="Manager",
                writable=True,
                value=None,
            ),
        }
        mock_historic_process.return_value = _mock_historic_process()
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status()
        # test
        assert 0 == WorkflowData.objects.count()
        user = UserFactory()
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        with pytest.raises(WorkflowError) as e:
            client.post(
                reverse("example.task.missing.data.document", args=[99]),
                {"manager": "joey"},
            )
        assert "does not have a 'write_data_document' method" in str(e.value)


@pytest.mark.django_db
def test_post_data_download(client):
    group = GroupFactory(name="Orange")
    student = UserFactory(
        first_name="Penny", last_name="Smith", email="penny@kbsoftware.co.uk"
    )
    student.groups.add(group)
    ExampleContactFactory(user=student)
    sw = ScheduledWorkflowFactory(process_definition="makeHay")
    ScheduledWorkflowUserFactory(scheduled_workflow=sw, process_id=321)
    workflow = WorkflowFactory(process_key="makeHay")
    MappingFactory(
        workflow=workflow,
        variable_name="manager",
        mapping_type=MappingPlugin.IMPORT,
        data_download=True,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="student_email",
        mapping_type=MappingPlugin.AUTO_GENERATE,
        data_download=True,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="student_full_name",
        mapping_type=MappingPlugin.AUTO_GENERATE,
        data_download=True,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="student_pk",
        group=group,
        mapping_type=MappingPlugin.USER,
        data_download=True,
    )
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti.task_complete"
    ):
        # mock return
        mock_form_variable_info.return_value = {
            "data_download": Variable(
                data_type=str,
                java_name="dataDownload",
                name="Data Download",
                writable=True,
                value="Customer Data",
            ),
            "manager": Variable(
                data_type=str,
                java_name="manager",
                name="Manager",
                writable=True,
                value=None,
            ),
            "student_pk": Variable(
                data_type=int,
                java_name="studentPk",
                name="Student ID",
                required=True,
                writable=True,
                value=None,
            ),
        }
        mock_historic_process.return_value = _mock_historic_process()
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status()
        # test
        assert 0 == WorkflowData.objects.count()
        user = UserFactory()
        assert (
            client.login(username=user.username, password=TEST_PASSWORD) is True
        )
        data = {"manager": "joey", "student_pk": student.pk}
        response = client.post(reverse("example.task", args=[99]), data)
        assert requests.codes.found == response.status_code, response.context[
            "form"
        ].errors
        assert reverse("project.dash") == response["Location"]
        assert 1 == WorkflowData.objects.count()
        workflow_data = WorkflowData.objects.first()
        assert {
            "manager": "joey",
            "student_email": "penny@kbsoftware.co.uk",
            "student_full_name": "Penny Smith",
            "student_pk": "Penny Smith",
        } == workflow_data.data
