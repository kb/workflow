source venv-workflow/bin/activate.fish

if command -q k3d
  echo "Using Kubernetes"
  set -x KUBECONFIG (k3d get-kubeconfig)
  set -x ACTIVITI_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x ACTIVITI_PASS "test"
  set -x ACTIVITI_PATH ""
  set -x ACTIVITI_PORT "80"
  set -x ACTIVITI_USER "rest-admin"
  set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x DATABASE_PASS "postgres"
  set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  set -x REDIS_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x REDIS_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)
  echo "KUBECONFIG:" $KUBECONFIG
else
  set -x ACTIVITI_HOST "localhost"
  set -x ACTIVITI_PASS "kermit"
  set -x ACTIVITI_PATH "flowable-rest"
  set -x ACTIVITI_PORT "8080"
  set -x ACTIVITI_USER "kermit"
  set -x DATABASE_HOST ""
  set -x DATABASE_PASS ""
  set -x DATABASE_PORT ""
  set -x REDIS_HOST "localhost"
  set -x REDIS_PORT "6379"
end

set -x DATABASE_NAME "dev_app_workflow_$USER"
set -x DATABASE_USER "postgres"
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DJANGO_SETTINGS_MODULE "example_workflow.dev_$USER"
set -x DOMAIN "dev"
set -x HOST_NAME "https://www.hatherleigh.net"
set -x HOST_NAME_PATH_WORK "https://www.hatherleigh.net/task/"
set -x LOG_FOLDER ""
set -x LOG_SUFFIX "dev"
set -x MAIL_TEMPLATE_TYPE "django"
set -x RECAPTCHA_PRIVATE_KEY "your private key"
set -x RECAPTCHA_PUBLIC_KEY "your public key"
set -x SECRET_KEY "the_secret_key"

source .private.fish

echo "ACTIVITI_HOST:" $ACTIVITI_HOST
echo "ACTIVITI_PORT:" $ACTIVITI_PORT
echo "ACTIVITI_USER:" $ACTIVITI_USER
echo "ACTIVITI_PASS:" $ACTIVITI_PASS
echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_PASS:" $DATABASE_PASS
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
echo "REDIS_HOST:" $REDIS_HOST
echo "REDIS_PORT:" $REDIS_PORT
