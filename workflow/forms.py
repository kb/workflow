# -*- encoding: utf-8 -*-
import logging

from datetime import date
from decimal import Decimal, ROUND_UP
from django import forms
from django.contrib.auth.models import Group
from enum import Enum

from .activiti import (
    Activiti,
    activiti_bool,
    activiti_date,
    ActivitiError,
    camelcase_to_snake,
    camelcase_to_title,
    process_id_to_key,
    snake_to_camel_case,
)
from .models import (
    is_comment,
    Mapping,
    MappingPlugin,
    ProcessDefinition,
    RESERVED_VARIABLE_NAMES,
    Workflow,
    WorkflowData,
    WorkflowError,
)


logger = logging.getLogger(__name__)


def round_string_as_decimal(value):
    if value:
        return Decimal(value).quantize(Decimal(".01"), rounding=ROUND_UP)
    else:
        return Decimal()


class MappingModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{}".format(camelcase_to_snake(obj.variable_name))


class WorkflowModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{} ({})".format(obj.name, obj.process_key)


class EmptyForm(forms.Form):
    pass


class HistorySecurityForm(forms.Form):
    workflow = WorkflowModelChoiceField(
        label="", queryset=Group.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        query_set = kwargs.pop("query_set")
        report = kwargs.pop("report")
        super().__init__(*args, **kwargs)
        f = self.fields["workflow"]
        f.widget.attrs.update({"class": "chosen-select pure-u-1-2"})
        if report:
            f.empty_label = "Choose a workflow..."
        else:
            f.empty_label = "All workflows..."
        f.queryset = query_set


class MappingConfigForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        can_edit_mapping_type = kwargs.pop("can_edit_mapping_type")
        super().__init__(*args, **kwargs)
        # css_class
        f = self.fields["css_class"]
        f.widget.attrs.update({"class": "chosen-select pure-input-1-3"})
        f.label = "CSS Class"
        # help_text
        f = self.fields["help_text"]
        f.widget.attrs.update({"class": "pure-input-2-3", "rows": 4})
        # mapping_type
        if can_edit_mapping_type:
            f = self.fields["mapping_type"]
            f.choices = self.instance.mapping_choices()
            f.widget.attrs.update({"class": "chosen-select pure-input-1-3"})
        else:
            del self.fields["mapping_type"]

    class Meta:
        model = Mapping
        fields = (
            "mapping_type",
            "help_text",
            "css_class",
            "multi_line",
            "history_summary",
            "history_summary_order",
            "data_download",
            "view_task_data",
        )


class MappingMapDecimalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("decimal_number",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1-3"})

    class Meta:
        model = Mapping
        fields = ("decimal_number",)


class MappingMapFlowList(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("flow_list_type",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select pure-input-1-3"})

    class Meta:
        model = Mapping
        fields = ("flow_list_type",)


class MappingMapGroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("group",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select pure-input-1-3"})

    class Meta:
        model = Mapping
        fields = ("group",)


class MappingMapIntegerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("integer_number",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1-3"})

    class Meta:
        model = Mapping
        fields = ("integer_number",)


class MappingMapUserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("user",):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select pure-input-1-3"})

    class Meta:
        model = Mapping
        fields = ("user",)


class ProcessDefinitionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in self.fields:
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["process_definition"].required = False

    class Meta:
        model = ProcessDefinition
        fields = ("process_definition",)


class WorkflowDataEmptyForm(forms.ModelForm):
    class Meta:
        model = WorkflowData
        fields = ()


class WorkflowEmptyForm(forms.ModelForm):
    class Meta:
        model = Workflow
        fields = ()


class WorkflowFormMixin(forms.Form):
    def _comment_required(self, name, cleaned_data):
        result = False
        choice = cleaned_data.get(name)
        if choice:
            try:
                match = choice.endswith("_comment") or choice.endswith("_note")
            except AttributeError:
                match = False
            if match:
                try:
                    # is this a choice field?
                    if self.fields[name].choices:
                        result = True
                except AttributeError:
                    # looks like this isn't a choice field
                    pass
        return result

    def _css_class(self, mapping, default):
        if mapping and mapping.css_class:
            return mapping.css_class
        else:
            return default

    def _field_heading(self, name, variable, writable, mapping, value):
        """Fields with a name appended with ``_heading`` are used as headings.

        Headings:

        - must not be ``writable``.
        - must not have a value.
        - if they don't have a label, they must have help text.

        """
        label = variable.name
        if value:
            raise WorkflowError(
                "Heading fields ('{}') should not have a "
                "'value': ('{}')".format(name, value)
            )
        if not label:
            if mapping and mapping.help_text:
                pass
            else:
                raise WorkflowError(
                    "The '{}' field does not have a label ('name' "
                    "in the XML file) so must have 'help_text' defined "
                    "in the field mapping".format(name)
                )
        if writable:
            raise WorkflowError(
                "Heading fields must not be 'writable': '{}'".format(name)
            )
        self.fields[name] = forms.CharField(
            label=label, widget=forms.widgets.HiddenInput()
        )

    def _field_integer(self, field_id, variable, mapping):
        mapping_found = False
        label = variable.name or camelcase_to_title(field_id)
        if variable.writable:
            # Moved to 'work/plugin.py'
            # if mapping and mapping.mapping_type == MappingPlugin.USER_IN_GROUP:
            #    mapping_found = True
            #    users = mapping.group.user_set.all().order_by("username")
            #    choices = [(None, "---------")]
            #    for user in users:
            #        choices.append((user.pk, user.get_full_name()))
            #    self.fields[field_id] = forms.ChoiceField(
            #        choices=choices, initial=variable.value, label=label
            #    )
            # else:
            self.fields[field_id] = forms.IntegerField(
                label=label, initial=variable.value
            )
        else:  # read only field
            initial = None
            if mapping and mapping.mapping_type == MappingPlugin.GROUP:
                mapping_found = True
                initial = "{} Group".format(mapping.group.name)
            # Moved to 'work/plugin.py'
            # if mapping and mapping.mapping_type == MappingPlugin.USER_IN_GROUP:
            #    mapping_found = True
            #    user_name = ""
            #    pk = variable.value
            #    if pk:
            #        try:
            #            user = mapping.group.user_set.get(pk=pk)
            #            user_name = user.get_full_name()
            #        except get_user_model().DoesNotExist:
            #            logger.error(
            #                "Integer field: Cannot find user "
            #                "{} in the group".format(pk)
            #            )
            #    if user_name:
            #        initial = "{} ({} Group)".format(
            #            user_name, mapping.group.name
            #        )
            #    else:
            #        initial = "User in {} Group".format(mapping.group.name)
            self.fields[field_id] = forms.CharField(
                initial=initial or variable.value, label=label, max_length=2000
            )
        return mapping_found

    def _field_plugin(self, field_id, variable, mapping):
        """Create the form field for a workflow plugin.

        Keyword arguments:
        field_id -- the name of the activiti field e.g. ``attendees``
        variable -- an Activiti ``Variable`` (``data_type``, ``value`` etc)
        mapping -- the workflow ``Mapping`` for the field

        """
        field, css_class = MappingPlugin().create_field(
            field_id, variable, mapping
        )
        if field:
            self.fields[field_id] = field
        else:
            raise WorkflowError(
                "Plugin could not create field for '{}'".format(field_id)
            )
        return css_class

    def _is_heading(self, name):
        check = name.lower()
        return check.endswith("_heading")

    def _mapping(self, process_key, variable_name):
        mapping = None
        try:
            mapping = Mapping.objects.get(
                workflow__process_key=process_key,
                variable_name=variable_name,
                deleted=False,
            )
        except Mapping.DoesNotExist:
            pass
        return mapping

    def _process_key(self, activiti, task_id, process_id):
        if process_id:
            raise ActivitiError(
                "Write the code to get the process key from the process_id"
            )
        elif task_id:
            data = activiti.task_status(task_id)
        return process_id_to_key(data.definition_id)

    def _validate_accept_reject_comment(self, cleaned_data, message=None):
        """Check the field names to see if a comment is required.

        If we have a comment field, and a choices field and the selected item
        ends with ``_comment`` or ``_note``, then make sure the user has
        entered a comment.

        .. tip:: For similar code, see ``is_comment`` and ``_comment_required``
                 in ``_validate_variables`` (``work/api.py``).

        """
        field_name_comment = field_name_required = None
        for name in self.fields.keys():
            if is_comment(name):
                field_name_comment = name
            if self._comment_required(name, cleaned_data):
                field_name_required = name
            if field_name_comment and field_name_required:
                break
        if field_name_comment and field_name_required:
            comment = cleaned_data.get(field_name_comment)
            if not comment:
                if not message:
                    message = "Please enter a reason..."
                self.add_error(field_name_comment, message)

    def __init__(self, *args, **kwargs):
        """Create the form fields from the workflow variables.

        - We ignore reserved variable names e.g. ``editMetadata``.
          See ``RESERVED_VARIABLE_NAMES`` in ``workflow/models.py``.

        - For ``Mapping`` of a group, where we want the user to select a
          contact from a list...

          The workflow will have a group PK e.g. ``timeOffManagerGroupPk``.
          The form will need to fill in a ``timeOffManagerPk`` by allowing the
          user to select from a list of contacts in the
          ``timeOffManagerGroupPk``.

          ``field_type`` would be a number.  The mapping for
          ``timeOffManagerPk`` could select the ``timeOffManagerGroupPk``?  So
          why do we need a ``timeOffManagerGroupPk``?

        """
        process_id = kwargs.pop("process_id", None)
        task_id = kwargs.pop("task_id", None)
        user = kwargs.pop("user", None)
        if not user:
            raise ActivitiError(
                "Incorrectly configured Workflow form declare get_form_kwargs "
                "and pass a user"
            )
        if not (process_id or task_id):
            raise ActivitiError(
                "Incorrectly configured Workflow form declare get_form_kwargs "
                "and pass a task id or a process id"
            )
        super().__init__(*args, **kwargs)
        activiti = Activiti()
        process_key = self._process_key(activiti, task_id, process_id)
        form_data = activiti.form_variable_info(
            task_id=task_id, process_id=process_id
        )
        self.choice_field_for_workflow_audit = None
        for field_id, variable in form_data.items():
            mapping = self._mapping(process_key, field_id)
            css_class = []
            mapping_found = False
            label = variable.name or camelcase_to_title(
                snake_to_camel_case(field_id)
            )
            if field_id in RESERVED_VARIABLE_NAMES:
                continue
            elif mapping and mapping.is_plugin():
                # non-legacy field
                x = self._field_plugin(field_id, variable, mapping)
                if x:
                    css_class.extend(x)
                x = self._css_class(mapping, None)
                if x:
                    css_class.append(x)
            elif variable.data_type is date:
                self.fields[field_id] = forms.DateField(
                    label=label, initial=activiti_date(variable.value)
                )
                css_class.append(self._css_class(mapping, "pure-input-2-3"))
                if variable.writable:
                    css_class.append("datepicker")
            elif mapping and mapping.is_decimal():
                self.fields[field_id] = forms.DecimalField(
                    initial=round_string_as_decimal(variable.value),
                    label=label,
                    decimal_places=2,
                )
                css_class.append(self._css_class(mapping, "pure-input-2-3"))
            elif variable.data_type is int:
                mapping_found = self._field_integer(
                    field_id,
                    variable,
                    mapping,
                )
                css_class.append(self._css_class(mapping, "pure-input-2-3"))
            elif variable.data_type == bool:
                self.fields[field_id] = forms.BooleanField(
                    initial=activiti_bool(variable.value), label=label
                )
            elif variable.data_type is Enum:
                choices = []
                for k, v in variable.choices.items():
                    choices.append((k, v))
                if not choices:
                    raise ActivitiError(
                        "Activiti workflow has no choices defined for 'enum' "
                        "field '{}' (Hint: update the XML file)".format(
                            field_id
                        )
                    )
                if not self.choice_field_for_workflow_audit:
                    self.choice_field_for_workflow_audit = field_id
                self.fields[field_id] = forms.ChoiceField(
                    choices=choices,
                    initial=variable.value,
                    label=label,
                    widget=forms.RadioSelect,
                )
            else:
                rows = (
                    mapping.multi_line
                    if mapping and mapping.multi_line
                    else None
                )
                if field_id == "audit_description":
                    self.fields[field_id] = forms.CharField(
                        initial=variable.value,
                        label=label,
                        max_length=2000,
                        widget=forms.widgets.HiddenInput,
                    )
                elif is_comment(field_id):
                    self.fields[field_id] = forms.CharField(
                        label=label,
                        max_length=2000,
                        widget=forms.widgets.Textarea(
                            attrs={"rows": rows or 4}
                        ),
                    )
                elif self._is_heading(field_id):
                    self._field_heading(
                        field_id,
                        variable,
                        variable.writable,
                        mapping,
                        variable.value,
                    )
                else:
                    params = {}
                    if rows:
                        params.update(
                            {
                                "widget": forms.widgets.Textarea(
                                    attrs={"rows": rows}
                                )
                            }
                        )
                    self.fields[field_id] = forms.CharField(
                        initial=variable.value,
                        label=label,
                        max_length=2000,
                        **params,
                    )
                css_class.append(self._css_class(mapping, "pure-input-1"))
            if not variable.writable:
                # read only if the user cannot update the field
                self.fields[field_id].widget.attrs.update({"readonly": True})
            if mapping and mapping.help_text:
                self.fields[field_id].help_text = mapping.help_text
            self.fields[field_id].required = variable.required
            self.fields[field_id].widget.attrs.update(
                {"class": " ".join(css_class)}
            )
            # if not mapping_found:
            #    if (
            #        mapping
            #        and mapping.mapping_type == MappingPlugin.USER_IN_GROUP
            #    ):
            #        raise ActivitiError(
            #            "Field '{}' must be an 'int' field for the "
            #            "'user-in-group' mapping. The current type is "
            #            "'{}'".format(field_id, variable.data_type.__name__)
            #        )

    def audit(self):
        """Build a description for the audit record.

        The Activiti form must have an 'audit_description' field.  This is not
        rendered in the actual form (see above).  If the form has a choice
        field we append the label of the selected item to the audit.

        """
        try:
            description = self.cleaned_data["audit_description"]
        except KeyError:
            raise WorkflowError(
                "The workflow task does not have an 'audit_description'"
            )
        if not description:
            raise WorkflowError("The workflow 'audit_description' is empty")
        answer = label = None
        if self.choice_field_for_workflow_audit:
            field_name = self.choice_field_for_workflow_audit
            answer = self.cleaned_data[field_name]
            f = self.fields[self.choice_field_for_workflow_audit]
            label = dict(f.choices)[answer]
        if label:
            description = "{} {}".format(description, label)
        return (answer, description)

    def comment(self):
        """If there is a comment (or note) field, return the label and text."""
        result = (None, None)
        for name in self.fields.keys():
            if is_comment(name):
                label = self.fields[name].label
                comment = self.cleaned_data[name]
                result = (label, comment)
        return result

    def hidden_fields(self):
        """Don't include heading fields in the list of hidden fields.

        Override ``hidden_fields`` from ``django/forms/forms.py``

        """
        result = []
        for field in self:
            add = False
            if field.is_hidden:
                add = not self._is_heading(field.name)
            if add:
                result.append(field)
        return result

    def visible_fields(self):
        """Include heading fields in the list of visible fields.

        Override ``visible_fields`` from ``django/forms/forms.py``

        """
        result = []
        for field in self:
            add = True
            if field.is_hidden:
                add = self._is_heading(field.name)
            if add:
                result.append(field)
        return result


class WorkflowProcessStartForm(WorkflowFormMixin):
    class Meta:
        fields = ()


class WorkflowTaskForm(WorkflowFormMixin):
    class Meta:
        fields = ()


class WorkflowForm(forms.ModelForm):
    history_summary_column_1 = MappingModelChoiceField(
        label="History - Column 1",
        queryset=Mapping.objects.none(),
        required=False,
    )
    history_summary_column_2 = MappingModelChoiceField(
        label="History - Column 2",
        queryset=Mapping.objects.none(),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in ("name", "url_redirect"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        query_set = Mapping.objects.for_workflow(self.instance)
        fields = {
            "groups": None,
            "history_summary_column_1": query_set,
            "history_summary_column_2": query_set,
            "security_delete": None,
            "security_history": None,
            "security_history_my": None,
        }
        for name, qs in fields.items():
            self.fields[name].widget.attrs.update(
                {"class": "chosen-select pure-input-1-3"}
            )
            if qs:
                self.fields[name].queryset = qs

    class Meta:
        model = Workflow
        fields = (
            "name",
            "icon",
            "security_type",
            "groups",
            "security_delete",
            "security_history",
            "security_history_my",
            "config_description",
            "config_url_name",
            "url_redirect",
        )

    def clean(self):
        cleaned_data = super().clean()
        groups = cleaned_data.get("groups")
        security_type = cleaned_data.get("security_type")
        if security_type == Workflow.GROUPS:
            if not groups:
                raise forms.ValidationError(
                    "Please select a security group",
                    code="workflow__security_type__groups",
                )
