# -*- encoding: utf-8 -*-
import attr
import collections
import datetime
import json
import logging
import os.path
import re
import requests
import urllib.parse

from datetime import date
from dateutil import parser
from decimal import Decimal
from distutils.util import strtobool
from django.conf import settings
from django.core import serializers
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from enum import Enum
from requests.auth import HTTPBasicAuth


logger = logging.getLogger(__name__)


def activiti_bool(boolean_as_str):
    if type(boolean_as_str) is bool:
        result = boolean_as_str
    elif boolean_as_str is None:
        result = None
    elif boolean_as_str == "":
        result = None
    else:
        result = bool(strtobool(boolean_as_str))
    return result


def activiti_date(date_as_str):
    """Convert an Activiti date string into a 'datetime'.

    03/09/2021, Code copied to ``date_or_none`` in ``api/api_utils.py`` for
    use in REST API calls.

    """
    if date_as_str:
        result = parser.parse(date_as_str)
    else:
        result = None
    return result


def build_django_url(url_name, task_id, process_id, name):
    """Build the Django URL for this workflow task.

    .. note:: Workflow tasks with an empty ``url_name`` (``formKey``) will be
              executed by the Ember ``work`` app.

    .. warning:: This function will raise an error if an old workflow is using
                 an out of date URL name e.g. ``flow.request.owner.next``.

    ``is_work`` will be ``True`` if the task can be handled by our Ember UI.

    Used by the ``bpm`` project (see ``dash/tasks.py``).

    TODO Find some way to report errors to our monitoring system.

    """
    url = None
    is_work = True
    if url_name:
        try:
            url = reverse(url_name, args=[task_id])
            is_work = False
        except NoReverseMatch:
            pass
    if not url:
        # Create an ember URL
        url = settings.HOST_NAME_PATH_WORK
        if url_name:
            custom_route = url_name.replace("-", "/")
            url = urllib.parse.urljoin(url, f"{custom_route}/")
        url = urllib.parse.urljoin(url, str(task_id))
    return url, is_work


def camelcase_to_snake(string):
    s1 = re.sub("(.)([A-Z][a-z]+)", r"\1 \2", string)
    return re.sub("([a-z0-9])([A-Z])", r"\1 \2", s1).lower().replace(" ", "_")


def camelcase_to_title(string):
    s1 = re.sub("(.)([A-Z][a-z]+)", r"\1 \2", string)
    return re.sub("([a-z0-9])([A-Z])", r"\1 \2", s1).title()


def convert_date_iso(obj):
    """Return a string representing the date/time in ISO 8601 format."""
    return (
        obj.isoformat()
        if (
            isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date)
        )
        else None
    )


def current_workflows(variable_name, pk):
    """List of current workflows linked to a variable.

    .. tip:: The ``variable_name`` will be converted to camelcase
             e.g. ``document_pk``` will be converted to ```documentPk``.

    .. tip:: Copied from the ``document_workflows`` function (in another app).

    """
    activiti = Activiti()
    return activiti.process(
        variable_filters=[
            {
                "name": snake_to_camel_case(variable_name),
                "value": pk,
                "operation": "equals",
                "type": "integer",
            }
        ],
    )


def data_to_snake(data):
    result = {}
    for k, v in data.items():
        result[camelcase_to_snake(k)] = v
    return result


def get_data_type(field_type):
    result = None
    if field_type:
        result = Activiti.DATA_TYPES[field_type]
    return result


def int_if_possible(data):
    """The 'process_id' used to be an 'int'... It is now a 'uuid'."""
    try:
        return int(data)
    except (TypeError, ValueError):
        return data


def process_id_to_key(process_id):
    result = process_id
    pos = process_id.find(":")
    if pos != -1:
        result = process_id[:pos]
    return result


def response_message(response):
    message = (
        f"{response.status_code} "
        f"error: {response.reason} "
        f"for url: {response.url}"
    )
    try:
        data = json.dumps(response.json())
        message = message + f": {data}"
    except (AttributeError, ValueError):
        pass
    # log the error even if the call to 'json()' fails
    logger.error(message)
    return message


def snake_to_camel_case(snake_str):
    """Convert snake case to camel case.

    We capitalize the first letter of each component except the first one with
    the ``title`` method and join them together.

    """
    components = snake_str.split("_")
    return components[0] + "".join(x.title() for x in components[1:])


def variables_as_detail(variables, is_runtime=None):
    """

    Keyword arguments:
    is_runtime -- Some REST API calls only return the ``name``, ``type``,
                  ``value`` and ``scope`` for the variables.
                  Set ``is_runtime`` to ``True`` if that is what you expect.

    Called after:

    1. Get form data (``GET form/form-data``):
       https://flowable.com/open-source/docs/bpmn/ch15-REST/#get-form-data

    """
    required = writable = False
    result = collections.OrderedDict()
    for x in variables:
        if is_runtime is None:
            enum_values = x["enumValues"]
            choices = {item["id"]: item["name"] for item in enum_values}
            java_name = x["id"]
            required = x["required"]
            writable = x["writable"]
        else:
            choices = {}
            java_name = x["name"]
            if "id" in x:
                raise ActivitiError(
                    "REST API variables contain an 'id' field so "
                    f"'is_runtime' should by 'None': {x.keys()}"
                )
        key = camelcase_to_snake(java_name)
        item_type = x.get("type")
        if item_type:
            data_type = Activiti.DATA_TYPES.get(item_type)
        else:
            data_type = None
        result[key] = Variable(
            data_type=data_type,
            java_name=java_name,
            name=x["name"],
            value=x["value"],
            choices=choices,
            required=required,
            writable=writable,
        )
    return result


class ActivitiError(Exception):
    def __init__(self, value, status=None):
        Exception.__init__(self)
        self.status = status
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))

    @property
    def status_code(self):
        return self.status


class ActivitiTaskAlreadyClaimedException(ActivitiError):
    """Thrown when a task is already claimed.

    Name copied from
    http://www.activiti.org/userguide/#_exception_strategy

    """

    pass


@attr.s
class Group:
    """Activiti group detail."""

    group_id = attr.ib()
    name = attr.ib()
    group_type = attr.ib()


@attr.s
class Definition:
    """Activiti process definition detail."""

    process_key = attr.ib()
    name = attr.ib()
    version = attr.ib()
    workflow_id = attr.ib()


@attr.s
class Deployment:
    deployment_id = attr.ib()
    name = attr.ib()
    url = attr.ib()
    created = attr.ib()


@attr.s
class HistoricProcessInstance:
    pk = attr.ib()
    process_key = attr.ib()
    user = attr.ib()
    completed = attr.ib()
    start_time = attr.ib()
    end_time = attr.ib()
    variables = attr.ib()


# 22/03/2023, Replace with 'TaskData`?
# @attr.s
# class HistoricTaskData:
#    task_id = attr.ib()
#    process_id = attr.ib()
#    assignee = attr.ib()
#    category = attr.ib()
#    deleted = attr.ib()
#    start_time = attr.ib()
#    due_date = attr.ib()
#    claim_time = attr.ib()
#    end_time = attr.ib()
#    name = attr.ib()


@attr.s
class IdentityLink:
    """Identity link.

    An identity link is used to associate a task with a certain identity.
    https://flowable.com/open-source/docs/javadocs-5/org/activiti/engine/task/IdentityLink.html

    For example:

    - a user can be an assignee (= identity link type) for a task
    - a group can be a candidate-group (= identity link type) for a task

    """

    user = attr.ib()
    group = attr.ib()
    identity_type = attr.ib()


@attr.s
class ProcessDefinition:
    process_definition_id = attr.ib()
    name = attr.ib()
    description = attr.ib()
    key = attr.ib()
    version = attr.ib()
    deployment_id = attr.ib()
    url = attr.ib()
    deployment_url = attr.ib()
    category = attr.ib()
    suspended = attr.ib()
    start_form_defined = attr.ib()


@attr.s
class ProcessInstance:
    """Activiti process instance detail."""

    pk = attr.ib()
    # process_id = attr.ib()
    activiti_id = attr.ib()
    business_key = attr.ib()
    completed = attr.ib()
    definition_id = attr.ib()
    definition_url = attr.ib()
    ended = attr.ib()
    process_key = attr.ib()
    start_time = attr.ib()
    suspended = attr.ib()
    url = attr.ib()
    user = attr.ib()
    variables = attr.ib()

    @property
    def end_time(self):
        """A current process will never have an end time.

        Added here so we can use the ``ProcessSerializer`` (from the ``work``
        app) for ``HistoricProcessInstance`` (and ``ProcessInstance``).

        """
        return None


@attr.s
class TaskData:
    """Activiti task detail."""

    pk = attr.ib()
    # task_id = attr.ib()
    assignee = attr.ib()
    category = attr.ib()
    claim_time = attr.ib()
    created = attr.ib()
    definition_id = attr.ib()
    deleted = attr.ib()
    description = attr.ib()
    due_date = attr.ib()
    name = attr.ib()
    process_id = attr.ib()
    process_key = attr.ib()
    process_name = attr.ib()
    url_name = attr.ib()
    variables = attr.ib()
    # 22/03/2022, Adding the following
    completed = attr.ib()
    end_time = attr.ib()
    is_group_task = attr.ib()
    is_work = attr.ib()
    status = attr.ib()
    url = attr.ib()
    user_can_delete_workflow = attr.ib()


@attr.s
class Variable:
    """Variable detail.

    Arguments:
    choices -- A dict of choices (for a drop down) (Enum data type only)

    """

    data_type = attr.ib()
    value = attr.ib()
    choices = attr.ib(default={})
    # data_type_as_str = attr.ib(default=None)
    java_name = attr.ib(default=None)
    name = attr.ib(default=None)
    required = attr.ib(default=False)
    writable = attr.ib(default=False)

    @property
    def data_type_as_str(self):
        return self.data_type.__name__ if self.data_type else "unknown"


class Activiti:
    BOOLEAN = "boolean"
    DATE = "date"
    DOUBLE = "double"
    ENUM = "enum"
    INTEGER = "integer"
    JSON = "json"
    LONG = "long"
    SHORT = "short"
    STRING = "string"

    DATA_TYPES = {
        BOOLEAN: bool,
        DATE: date,
        DOUBLE: Decimal,
        ENUM: Enum,
        INTEGER: int,
        JSON: dict,
        LONG: int,
        SHORT: int,
        STRING: str,
    }

    @property
    def _auth_details(self):
        return HTTPBasicAuth(settings.ACTIVITI_USER, settings.ACTIVITI_PASS)

    def _delete(self, url):
        """Activiti ``DELETE`` to ``REST`` API."""
        return requests.delete(url, auth=self._auth_details)

    def _get(self, url, params=None):
        """low level interface with requests."""
        return requests.get(url, params=params, auth=self._auth_details)

    def _group_task_list(self, group, start=None, size=None):
        """List of tasks for the groups.

        .. note:: ``group`` is a group id e.g. ``3``.

        # .. note:: ``group_pks`` is a collection of group ids e.g. ``[3, 6]``.

        See ``_user_task_list`` for keyword parameter

        """
        url = "{}/runtime/tasks/".format(self._service_url)
        if not size:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if not start:
            start = 0
        params = {
            "start": start,
            "size": size,
            "sort": "createTime",
            "order": "desc",
            # "candidateGroups": ",".join([str(x) for x in group_pks]),
            "candidateGroups": group,
            "includeTaskLocalVariables": True,
            "includeProcessVariables": True,
        }
        response = self._get(url, params=params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _historic_process(self, process_id):
        """Get the status of a historic process.

        .. note:: We are using the same URL as ``_history_process`` because it
                  has the option to ``includeProcessVariables``.  Using the URL
                  which includes the ``process_id`` does not return variables.

        For parameters, see:
        https://flowable.com/open-source/docs/bpmn/ch15-REST/#list-of-historic-process-instances

        """
        url = "{}/history/historic-process-instances/".format(self._service_url)
        params = {
            "includeProcessVariables": True,
            "processInstanceId": process_id,
        }
        response = self._get(url, params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _history_process(
        self,
        user=None,
        process_key=None,
        started_after=None,
        started_before=None,
        finished=None,
        finished_after=None,
        finished_before=None,
        started_by=None,
        start=None,
        size=None,
        sort=None,
        order=None,
        variable_filters=None,
    ):
        """Query process data and return a list of historic processes.

        .. note:: We are using the same URL as ``_historic_process`` (see above)

        For parameters, see:
        https://flowable.com/open-source/docs/bpmn/ch15-REST/#list-of-historic-process-instances

        The ``finished`` parameter is confusing.  The Flowable documentation
        says, *Only select historic process instances that are completely,
        finished*.

        05/02/2021, Sam and I looked at this:

        - (We think) if we don't set ``finished=True`` then the result will
          include current workflows.
        - We are using ``_process_instances`` for finding current workflows,
          but it has very few options for sorting etc.
        - We now have the option to use ``_historic_process`` for the extra sort
          parameters etc.
        - It would be nice to use ``_historic_process`` for current workflows,
          but we can't find a way to exclude completed workflows!

        """
        url = "{}/query/historic-process-instances/".format(self._service_url)
        if size is None:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if start is None:
            start = 0
        if order is None:
            order = "desc"
        if sort is None:
            sort = "startTime"
        if variable_filters is None:
            variable_filters = []
        params = {
            "start": start,
            "size": size,
            "sort": sort,
            "order": order,
            "includeProcessVariables": True,
        }
        if finished or finished is None:
            params["finished"] = True
        if process_key:
            params["processDefinitionKey"] = process_key
        # finished
        if finished_after:
            params["finishedAfter"] = finished_after.strftime(
                "%Y-%m-%dT%H:%M:%S"
            )
        if finished_before:
            params["finishedBefore"] = finished_before.strftime(
                "%Y-%m-%dT%H:%M:%S"
            )
        # started
        if started_after:
            params["startedAfter"] = started_after.strftime("%Y-%m-%dT%H:%M:%S")
        if started_before:
            params["startedBefore"] = started_before.strftime(
                "%Y-%m-%dT%H:%M:%S"
            )
        if user:
            if started_by:
                # Note: workflows are not started by the user...
                # params["startedBy"] = user.pk
                # They are started in the background by the
                # ``ScheduledWorkflowUser``....
                # We add the ``userPk`` to every workflow when we start it, so
                # filter by this instead.
                # https://www.kbsoftware.co.uk/docs/app-workflow.html#start-event-startevent
                #
                # Note: 13/01/2022, We are adding a user to the process on
                # creation (``process_identity_add_participant``), so perhaps
                # we won't need the ``userPk`` in future? For details, see
                # `commit 8361c1103cf09021a795c648cc867ee2549736a6`_
                variable_filters.append(
                    {
                        "name": "userPk",
                        "value": user.pk,
                        "operation": "equals",
                        "type": "integer",
                    }
                )
            else:
                params["involvedUser"] = user.pk
        if variable_filters:
            params["variables"] = variable_filters
        response = self._post(url, params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _history_task(
        self,
        user=None,
        process_id=None,
        process_key=None,
        start=None,
        size=None,
    ):
        """Get the status of a historic task.

        For parameters, see:
        https://flowable.com/open-source/docs/bpmn/ch15-REST/#get-historic-task-instances

        .. note:: I wanted to get the form variables, so I tried adding
                  ``includeTaskLocalVariables`` to the parameters.  I don't get
                  anything.  I think this is because we don't use task local
                  variables.  They appear to need Java code?

        """
        url = "{}/history/historic-task-instances/".format(self._service_url)
        if not size:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if not start:
            start = 0
        params = {
            "start": start,
            "size": size,
            "sort": "endTime",
            "order": "desc",
            "finished": True,
            # "includeTaskLocalVariables": True,
            # "includeProcessVariables": True,
        }
        # user
        if user:
            params["taskInvolvedUser"] = user.pk
        # process
        if process_id:
            params["processInstanceId"] = process_id
        elif process_key:
            params["processDefinitionKey"] = process_key
        response = self._get(url, params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _post(self, url, data, content_type=None):
        """Activiti ``POST`` to ``REST`` API."""
        if content_type is None:
            content_type = "application/json"
        encoded_data = json.dumps(
            data, cls=serializers.json.DjangoJSONEncoder
        ).encode("utf-8")
        headers = {"accept": "application/json", "content-type": content_type}
        return requests.post(
            url, data=encoded_data, auth=self._auth_details, headers=headers
        )

    def _process_diagram(self, process_id):
        """Download a process diagram.

        .. note:: Diagrams only available for current tasks (via the REST API):
                  https://github.com/flowable/flowable-engine/issues/1630

        """
        url = "{}/runtime/process-instances/{}/diagram/".format(
            self._service_url, process_id
        )
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            return response.content
        else:
            raise ActivitiError(response_message(response))

    def _process_identity_links(self, process_id):
        """Get involved people for process instance.

        https://wwv.flowable.com/open-source/docs/bpmn/ch15-REST/#get-involved-people-for-process-instance

        """
        url = "{}/runtime/process-instances/{}/identitylinks".format(
            self._service_url, process_id
        )
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(
                response_message(response), response.status_code
            )

    def _process_instances(
        self,
        user=None,
        process_key=None,
        started_by=None,
        start=None,
        size=None,
        variable_filters=None,
    ):
        """List of process instances.

        .. warning:: PJK 30/05/2023, I think this is *current* workflows only.

        .. warning:: We get the user from the process variables, so be careful
                     if you want to set ``includeProcessVariables`` to
                     anything other than ``True``.

        For parameters, see:
        https://flowable.com/open-source/docs/bpmn/ch15-REST/#list-of-process-instances

        .. note:: Renamed from ``_process_instance_list`` on 29/11/2020

        """
        url = "{}/query/process-instances/".format(self._service_url)
        if size is None:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if start is None:
            start = 0
        if variable_filters is None:
            variable_filters = []
        params = {
            "start": start,
            "size": size,
            "sort": "processDefinitionKey",
            "order": "asc",
            "includeProcessVariables": True,
        }
        if process_key:
            params["processDefinitionKey"] = process_key
        if user:
            if started_by:
                # Note: workflows are not started by the user...
                # params["startedBy"] = user.pk
                # They are started in the background by the
                # ``ScheduledWorkflowUser``....
                # We add the ``userPk`` to every workflow when we start it, so
                # filter by this instead.
                # https://www.kbsoftware.co.uk/docs/app-workflow.html#start-event-startevent
                #
                # Note: 13/01/2022, We are adding a user to the process on
                # creation (``process_identity_add_participant``), so perhaps
                # we won't need the ``userPk`` in future? For details, see
                # `commit 8361c1103cf09021a795c648cc867ee2549736a6`_
                variable_filters.append(
                    {
                        "name": "userPk",
                        "value": user.pk,
                        "operation": "equals",
                        "type": "integer",
                    }
                )
            else:
                params["involvedUser"] = user.pk
        if variable_filters:
            params["variables"] = variable_filters
        response = self._post(url, params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _process_status(self, process_id):  # =None, activity_id=None):
        """Details of a process instance (with variables).

        .. note:: We call the ``list`` method because it allows us to return
                  variables with the same call.
                  ``runtime/process-instances/{processInstanceId}``
                  does not return the variables.

        """
        # params = {}
        # if process_id:
        # params.update({"id": process_id})
        # if activity_id:
        # params.update({"activityId": activity_id})
        # if params:
        # params.update({"includeProcessVariables": True})
        params = {"id": process_id, "includeProcessVariables": True}
        url = ("{}/runtime/process-instances").format(self._service_url)
        response = self._get(url, params=params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _put(self, url, data, content_type=None):
        """Activiti ``PUT`` to ``REST`` API."""
        if content_type is None:
            content_type = "application/json"
        encoded_data = json.dumps(
            data, cls=serializers.json.DjangoJSONEncoder
        ).encode("utf-8")
        headers = {"accept": "application/json", "content-type": content_type}
        return requests.put(
            url, data=encoded_data, auth=self._auth_details, headers=headers
        )

    def _row_to_process_data(self, row):
        """Convert a row containing process data."""
        user = None
        # user
        try:
            user = int(row["startUserId"])
        except (KeyError, TypeError, ValueError):
            pass
        # variables
        variables = variables_as_detail(row["variables"], is_runtime=True)
        if user is None:
            user = self._user_from_variables(variables)
        definition_id = row["processDefinitionId"]
        return ProcessInstance(
            activiti_id=row["activityId"],
            business_key=row["businessKey"],
            completed=row["completed"],
            definition_id=definition_id,
            definition_url=row["processDefinitionUrl"],
            ended=row["ended"],
            # process_id=int_if_possible(row["id"]),
            pk=int_if_possible(row["id"]),
            process_key=row["processDefinitionName"],
            start_time=activiti_date(row["startTime"]),
            suspended=row["suspended"],
            url=row["url"],
            user=user,
            variables=variables,
        )

    def _row_to_process_data_historic(self, row):
        """Convert a row containing historic process data."""
        end_time = start_time = user = None
        # created
        start_time = activiti_date(row["startTime"])
        # end time
        end_time = activiti_date(row["endTime"])
        # user
        try:
            user = int(row["startUserId"])
        except (TypeError, ValueError):
            pass
        # variables
        variables = variables_as_detail(row["variables"], is_runtime=True)
        if user is None:
            user = self._user_from_variables(variables)
        return HistoricProcessInstance(
            pk=int_if_possible(row["id"]),
            # process_id=int_if_possible(row["id"]),
            process_key=row["processDefinitionName"],
            user=user,
            completed=True,
            start_time=start_time,
            end_time=end_time,
            variables=variables,
        )

    def _row_to_historic_task_data(self, row):
        """Convert a row containing historic task data.

        .. note:: I think all tasks, current and historic, are probably in
                  here.

        ``deleteReason``

          email to Sam, 12/06/2020,
          It seems that the ``deleteReason`` is empty for tasks completed by a
          user.
          I am thinking of striking through the whole of the line if the task
          was deleted and amending the text in the ``Complete`` column to say
          ``(Deleted 12/06/2020)`` (with no icon).
          I don't think the actual text
          e.g. ``boundary event (sid-572BD8FF-B66D-48C1-AAC9-DC4E62DAF7F6)``
          needs to be included.
          https://www.kbsoftware.co.uk/crm/ticket/4669/

        """
        claim_time = due_date = end_time = start_time = None
        # claim time
        claim_time = activiti_date(row["claimTime"])
        # due date
        due_date = activiti_date(row["dueDate"])
        # end time
        end_time = activiti_date(row["endTime"])
        # start time
        start_time = activiti_date(row["startTime"])
        # assignee
        try:
            assignee = int(row["assignee"])
        except (TypeError, ValueError):
            assignee = None
        return TaskData(
            pk=int_if_possible(row["id"]),
            # task_id=int_if_possible(row["id"]),
            assignee=assignee,
            category=row["category"],
            claim_time=claim_time,
            created=start_time,
            # start_time=start_time,
            definition_id=None,  # 22/03/2023, we may be able to fill this in?
            deleted=bool(row["deleteReason"]),
            description=None,  # 22/03/2023, we may be able to fill this in?
            due_date=due_date,
            end_time=end_time,
            name=row["name"],
            process_id=int_if_possible(row["processInstanceId"]),
            process_key=None,  # 22/03/2023, we may be able to fill this in?
            process_name=None,  # 22/03/2023, we may be able to fill this in?
            url_name=None,  # 22/03/2023, we may be able to fill this in?
            variables=None,  # 22/03/2023, we may be able to fill this in?
            completed=None,  # 22/03/2023, we may be able to fill this in?
            is_group_task=False,  # 22/03/2023, we may be able to fill this in?
            is_work=False,  # 22/03/2023, we may be able to fill this in?
            status="",  # 22/03/2023, we may be able to fill this in?
            url=None,  # 22/03/2023, we may be able to fill this in?
            user_can_delete_workflow=False,
        )

    def _row_to_identity_link(self, row):
        """Convert a row containing an identity link."""
        try:
            group = int(row["group"])
        except (TypeError, ValueError):
            group = None
        try:
            user = int(row["user"])
        except (TypeError, ValueError):
            user = None
        return IdentityLink(user=user, group=group, identity_type=row["type"])

    def _row_to_process_definition(self, row):
        return Definition(
            process_key=row["key"],
            name=row["name"],
            version=row["version"],
            workflow_id=row["id"],
        )

    def _row_to_task_data(self, row, is_runtime=None):
        """Convert a row containing task data to a ``namedtuple``.

        Arguments:
        is_runtime -- Some REST API calls only return the ``name``, ``type``,
                      ``value`` and ``scope`` for the variables.
                      Set ``is_runtime`` to ``True`` if that is what you expect.

        #process_id -- Flowable process ID
        #variables_to_detail -- set to ``True`` if you want variable details.

        .. note:: ``process_key`` is a convenience variable to help us get the
                  ID of the process.
                  e.g. for  ``processDefinitionId`` of
                  ``documentFamiliarisation:1:303``, the ``process_key`` will be
                  ``documentFamiliarisation``.
                  The ``processDefinitionId`` has always been in the format,
                  ``documentFamiliarisation:1:303`` (with the process key before
                  the ``:``), so we hope it will stay like this!

        """
        variables = variables_as_detail(row["variables"], is_runtime=True)
        due_date = row["dueDate"]
        end_time = activiti_date(row.get("endTime"))
        try:
            assignee = int(row["assignee"])
        except (TypeError, ValueError):
            assignee = None
        definition_id = row["processDefinitionId"]
        name = row["name"]
        process_id = int_if_possible(row["processInstanceId"])
        process_key = process_id_to_key(definition_id)
        task_id = int_if_possible(row["id"])
        url_name = row["formKey"]
        url, is_work = build_django_url(url_name, task_id, process_id, name)
        return TaskData(
            assignee=assignee,
            category=row["category"],
            claim_time=None,
            completed=False,
            created=activiti_date(row["createTime"]),
            definition_id=definition_id,
            deleted=False,
            description=row["description"],
            due_date=activiti_date(due_date) if due_date else "",
            end_time=end_time,
            is_work=is_work,
            name=name,
            process_id=process_id,
            process_key=process_key,
            process_name=None,  # 22/03/2023, There may be a way to get this?
            pk=task_id,
            # task_id=int_if_possible(row["id"]),
            url=url,
            url_name=url_name,
            variables=variables,
            is_group_task=False,  # 22/03/2023, May be a way to get this?
            status=None,  # 22/03/2023, May be a way to get this?
            user_can_delete_workflow=False,
        )

    def _upload_file(self, url, file_path):
        """Upload a file using post, optionally with some data"""
        content_type = ("application/xml",)
        if not os.path.exists(file_path):
            raise ActivitiError(
                "Cannot find workflow file: '{}'".format(file_path)
            )
        file_name = os.path.basename(file_path)
        files = {"file": (file_name, open(file_path, "rb"), content_type)}
        return requests.post(url, files=files, auth=self._auth_details)

    def _user_from_variables(self, variables):
        result = None
        user_pk_variable = variables.get("user_pk")
        if user_pk_variable and user_pk_variable.value:
            result = int(user_pk_variable.value)
        return result

    def _user_task_list(
        self,
        user=None,
        process_id=None,
        process_key=None,
        start=None,
        size=None,
    ):
        """List of tasks for a user.

        Keyword arguments:
        user -- get tasks for this Django user
        start -- get the page starting at (default 0)
        size -- page size (default 'PAGE_SIZE' i.e. 20)

        .. tip:: See the ``user_tasks`` method for documentation.

        From Paging and sorting:
        http://www.activiti.org/userguide/index.html#restPagingAndSort

        """
        url = "{}/runtime/tasks/".format(self._service_url)
        if not size:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if not start:
            start = 0
        params = {
            "start": start,
            "size": size,
            "sort": "createTime",
            "order": "desc",
            "includeTaskLocalVariables": True,
            "includeProcessVariables": True,
        }
        if process_id:
            params.update({"processInstanceId": process_id})
        if process_key:
            params.update({"processDefinitionKey": process_key})
        if user:
            params.update({"assignee": user.pk})
        response = self._get(url, params=params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    @property
    def _service_url(self):
        """URL for the Activiti REST interface."""
        result = "http://{}:{}/".format(
            settings.ACTIVITI_HOST, settings.ACTIVITI_PORT
        )
        if settings.ACTIVITI_PATH:
            result = "{}{}/".format(result, settings.ACTIVITI_PATH)
        return "{}service".format(result)

    def _task_identity_links(self, task_id):
        """Get the task status.

        Start tasks do not have a task ID, so the task attributes are found by
        passing in the process ID.  See ``task_status_process``.

        """
        url = "{}/runtime/tasks/{}/identitylinks".format(
            self._service_url, task_id
        )
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(
                response_message(response), response.status_code
            )

    def _task_status(self, task_id):
        """Get the task status.

        Start tasks do not have a task ID, so the task attributes are found by
        passing in the process ID.  See ``task_status_process``.

        """
        url = "{}/runtime/tasks/{}".format(self._service_url, task_id)
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(
                response_message(response), response.status_code
            )

    def _task_status_historic(self, task_id):
        """Get the status of a historic task."""
        url = "{}/history/historic-task-instances/{}/".format(
            self._service_url, task_id
        )
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _task_status_process(self, process_id):
        params = {
            "processInstanceId": process_id,
            "includeProcessVariables": True,
            "includeTaskLocalVariables": True,
            "sort": "id",
        }
        url = "{}/runtime/tasks".format(self._service_url)
        response = self._get(url, params=params)
        if response.status_code == requests.codes.ok:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def _variables_to_activiti(self, variables):
        result = []
        for field_id, variable in variables.items():
            result.append(
                {"name": snake_to_camel_case(field_id), "value": variable.value}
            )
        return result

    # def _variables_as_dict(self, variables):
    #    """Convert the Activiti variables into a simple dictionary.
    #    .. note:: I tried to convert the values to the correct type by using the
    #              ``type`` key.  This didn't work because the ``type`` is returned
    #              as a ``string`` or ``long`` even though the actual type in the
    #              file is something else e.g. ``date``.
    #    """
    #    result = OrderedDict()
    #    for item in variables:
    #        name = item["name"]
    #        key = camelcase_to_snake(name)
    #        result[key] = item["value"]
    #    return result

    def deploy_process_definition(self, file_path):
        """Deploy a process definition and return the deployment ID."""
        url = "{}/repository/deployments/".format(self._service_url)
        response = self._upload_file(url, file_path)
        if response.status_code == requests.codes.created:
            data = response.json()
            deployment_id = data["id"]
            return deployment_id
        else:
            raise ActivitiError(response_message(response))

    def deployment_list(self, page_number=None, page_size=None):
        result = []
        total = 0
        if page_size is None:
            page_size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if page_number:
            start = page_number * page_size
        else:
            start = 0
        params = {
            "start": start,
            "size": page_size,
            "sort": "deployTime",
            "order": "desc",
        }
        response = self._get(
            "{}/repository/deployments/".format(self._service_url),
            params=params,
        )
        if response.status_code == requests.codes.ok:
            json_data = response.json()
            total = json_data["total"]
            data = json_data["data"]
            for row in data:
                created = None
                created = activiti_date(row["deploymentTime"])
                result.append(
                    Deployment(
                        deployment_id=row["id"],
                        url=row["url"],
                        created=created,
                        name=row["name"],
                    )
                )
        else:
            raise ActivitiError(response_message(response))
        return result, total

    def form_variable_info(self, task_id=None, process_id=None):
        """Get the form variables for a task or process.

        .. note:: Use the ``task_id`` or the ``process_id``.  Don't use both.

        Using the task ID will get the form variables for a specified task.

        Using the process ID will get the form variables for the start (first)
        task.  The process hasn't yet been created, so the task doesn't have
        an ID, so we need to ask the process definition for the form variables
        for the task in the process.

        """
        params = {}
        if task_id:
            params.update({"taskId": task_id})
        if process_id:
            params.update({"processDefinitionId": process_id})
        if params:
            url = "{}/form/form-data".format(self._service_url)
            response = self._get(url, params=params)
            if response.status_code == requests.codes.ok:
                data = response.json()
                if "formProperties" in data:
                    return variables_as_detail(data["formProperties"])
                else:
                    message = "Cannot find form properties for "
                    if task_id:
                        message = message + "task '{}'".format(task_id)
                    if process_id:
                        message = message + "process '{}'".format(process_id)
                    raise ActivitiError(message)
            else:
                raise ActivitiError(response_message(response))
        else:
            raise ActivitiError(
                "Cannot retrieve form data. "
                "No 'taskId' or 'processDefinitionId' provided."
            )

    # def group_task_list(self, group_pks):
    def group_task_list(self, group):
        count = 0
        start = 0
        size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        while True:
            # json_data = self._group_task_list(group_pks, start, size)
            json_data = self._group_task_list(group, start, size)
            data = json_data["data"]
            total = json_data["total"]
            for row in data:
                count = count + 1
                result = self._row_to_task_data(row)
                yield result
            if count >= total:
                break
            # prepare for the next iteration
            start = start + size

    def historic_process(self, process_id):
        """Return data for a historic process.

        .. note:: This method uses the same pattern as ``process_status``.

        """
        process_status = self._historic_process(process_id)
        data = process_status["data"]
        if len(data) == 0:
            # process not found (404), so make a dummy process instance
            result = HistoricProcessInstance(
                pk=process_id,
                # process_id=process_id,
                process_key=None,
                user=None,
                completed=True,
                start_time=None,
                end_time=None,
                variables=None,
            )
        elif len(data) == 1:
            row = data[0]
            result = self._row_to_process_data_historic(row)
        else:
            raise ActivitiError(
                "Historic process status for '{}' - expected no data or one "
                "row of data: '{}'".format(process_id, len(data))
            )
        return result

    def history_process(
        self,
        user=None,
        process_key=None,
        started_after=None,
        started_before=None,
        finished=None,
        finished_after=None,
        finished_before=None,
        started_by=None,
        start=None,
        size=None,
        sort=None,
        order=None,
        variable_filters=None,
    ):
        """List of historic processes.

        - convert the data to a list of ``HistoricProcessInstance``.

        .. tip:: Use ``history_task`` to get a list of tasks and
                 ``task_status_historic`` to get the status of a single task.

        """
        data = self._history_process(
            user=user,
            process_key=process_key,
            started_after=started_after,
            started_before=started_before,
            finished=finished,
            finished_after=finished_after,
            finished_before=finished_before,
            started_by=started_by,
            start=start,
            size=size,
            sort=sort,
            order=order,
            variable_filters=variable_filters,
        )
        result = []
        for row in data["data"]:
            result.append(self._row_to_process_data_historic(row))
        total = data["total"]
        return result, total

    def history_task(
        self,
        user=None,
        process_id=None,
        process_key=None,
        start=None,
        size=None,
    ):
        """Query task data and return a list of historic tasks.

        - convert the data to a list of ``HistoricTaskData`` instances.

        .. tip:: Use ``task_status_historic`` to get the status of a single
                 task.

        """
        data = self._history_task(
            user=user,
            process_id=process_id,
            process_key=process_key,
            start=start,
            size=size,
        )
        result = []
        for row in data["data"]:
            result.append(self._row_to_historic_task_data(row))
        return result

    def is_deployed(self, name):
        deployments = self.deployment_list()
        for deployment in deployments["data"]:
            if deployment["name"] == name:
                return True
        return False

    def process(
        self,
        user=None,
        process_key=None,
        started_by=None,
        start=None,
        size=None,
        variable_filters=None,
    ):
        """List of processes.

        .. note:: Renamed from ``process_instance_list`` on 29/11/2020

        """
        data = self._process_instances(
            user=user,
            process_key=process_key,
            started_by=started_by,
            start=start,
            size=size,
            variable_filters=variable_filters,
        )
        result = []
        for row in data["data"]:
            result.append(self._row_to_process_data(row))
        total = data["total"]
        return result, total

    def process_definition(self, process_definition):
        """Get the process definition.

        The ``process_definition`` will look like this ``changeRequest:1:43``.

        .. note:: Check ``process_definition_for_deployment`` (see below).

        """
        url = "{}/repository/process-definitions/{}/".format(
            self._service_url, process_definition
        )
        response = self._get(url)
        if response.status_code == requests.codes.ok:
            data = response.json()
            return self._row_to_process_definition(data)
        else:
            raise ActivitiError(response_message(response))

    def process_definition_for_deployment(self, deployment_id):
        """Get the process definition by deployment ID.

        .. note:: Check ``process_definition`` (see above).

        """
        url = "{}/repository/process-definitions/".format(self._service_url)
        params = {"deploymentId": deployment_id}
        response = self._get(url, params=params)
        if response.status_code == requests.codes.ok:
            info = response.json()
            data = info["data"]
            if len(data) == 1:
                d = data[0]
                return self._row_to_process_definition(d)
            else:
                raise ActivitiError(
                    "Process definition for '{}' - expected one row of "
                    "data: '{}'".format(deployment_id, len(data))
                )
        else:
            raise ActivitiError(response_message(response))

    def process_definition_list(self, page_number=None, page_size=None):
        result = []
        total = 0
        if page_size is None:
            page_size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if page_number:
            start = page_number * page_size
        else:
            start = 0
        params = {
            "start": start,
            "size": page_size,
            "sort": "version",
            "order": "desc",
        }
        response = self._get(
            "{}/repository/process-definitions".format(self._service_url),
            params=params,
        )
        if response.status_code == requests.codes.ok:
            json_data = response.json()
            total = json_data["total"]
            data = json_data["data"]
            for row in data:
                result.append(
                    ProcessDefinition(
                        process_definition_id=row["id"],
                        name=row["name"],
                        description=row["description"],
                        key=row["key"],
                        version=row["version"],
                        deployment_id=row["deploymentId"],
                        url=row["url"],
                        deployment_url=row["deploymentUrl"],
                        category=row["category"],
                        suspended=row["suspended"],
                        start_form_defined=row["startFormDefined"],
                    )
                )
        else:
            raise ActivitiError(response_message(response))
        return result, total

    def process_diagram(self, process_id):
        return self._process_diagram(process_id)

    def process_delete(self, process_id):
        """Delete a process.

        From "15.5.2. Delete a process instance":
        https://www.activiti.org/userguide/#_process_instances

        """
        url = "{}/runtime/process-instances/{}/".format(
            self._service_url, process_id
        )
        response = self._delete(url)
        if response.status_code == requests.codes.no_content:
            pass
        else:
            raise ActivitiError(response_message(response))

    def process_identity_list(self, process_id):
        """Get involved people for process instance."""
        identity_links = []
        json_identity_links = self._process_identity_links(process_id)
        # read identity links and convert to a list of named tuples.
        for link in json_identity_links:
            identity_links.append(self._row_to_identity_link(link))
        return identity_links

    def process_identity_add_participant(self, process_id, user):
        """Add an involved user to a process instance.

        Starting a process does not add the user to the process:
        https://www.flowable.com/open-source/docs/bpmn/ch15-REST/#start-a-process-instance
        So this method can be used to add the user after creating the process.

        Documentation:
        https://www.flowable.com/open-source/docs/bpmn/ch15-REST/#add-an-involved-user-to-a-process-instance

        """
        url = "{}/runtime/process-instances/{}/identitylinks".format(
            self._service_url, process_id
        )
        params = {
            "user": "{}".format(user.pk),
            # warning: the documentation says to use 'userId',
            # but the call fails unless we use 'user'!
            # "userId": "{}".format(user.pk),
            "type": "participant",
        }
        response = self._post(url, params)
        if response.status_code == requests.codes.created:
            return response.json()
        else:
            raise ActivitiError(response_message(response))

    def process_start(self, process_key, variables):
        """Start a workflow process.

        To add a ``business_key``, just add to ``data`` below e.g::

          data = {
              'businessKey': business_key,

        I added a business key because I thought the process ID was changing.
        I passed in the primary key of the ``ScheduledWorkflowUser`` model, but
        then found the process ID was *changing* because I was creating new
        processes.  So I no longer needed the business key - but - the business
        key might be useful in future for identifying all the business
        processes for an issue (or something).

        """
        url = f"{self._service_url}/runtime/process-instances/"
        data = {
            "processDefinitionKey": process_key,
            "variables": self._variables_to_activiti(variables),
        }
        response = self._post(url, data)
        if response.status_code == requests.codes.created:
            data = response.json()
            if "id" in data:
                return data["id"]
            else:
                raise ActivitiError(
                    f"Process '{process_key}' started, "
                    "but the process ID was not returned"
                )
        else:
            raise ActivitiError(response_message(response))

    def process_status(self, process_id):
        """Read process data and convert to a named tuple with data we need.

        The ``process_status`` looks like this::

          {
            'data': [{
                'businessKey': None,
                'id': '61',
                'variables': [
                    {'value': 29, 'type': 'integer', 'name': 'objectPk' ...},
                    {'value': 'joey', 'type': 'string', 'name': 'contact' ...},
                ],
                'suspended': False,
                # ...

        We return a ``namedtuple`` in this format::

          data = activiti.process_status(61)
          data.process_id
          data.variables.contact
          data.variables.object_pk

        .. note:: This method will not find a instance data for completed
                  processes.

        .. note:: This method uses the same pattern as ``historic_process``.

        """
        process_status = self._process_status(process_id)
        data = process_status["data"]
        if len(data) == 0:
            # process not found (404), so make a dummy process instance
            result = ProcessInstance(
                pk=process_id,
                url=None,
                definition_id=None,
                process_key=None,
                definition_url=None,
                activiti_id=None,
                business_key=None,
                start_time=None,
                suspended=True,
                user=None,
                ended=True,
                completed=True,
                variables=None,
            )
        elif len(data) == 1:
            result = self._row_to_process_data(data[0])
        else:
            raise ActivitiError(
                "Process status for '{}' - expected no data or one row of "
                "data: '{}'".format(process_id, len(data))
            )
        return result

    def process_variables(self, process_id, variables):
        """Update variables for a process ID.

        Create (or update) variables on a process instance
        https://flowable.com/open-source/docs/bpmn/ch15-REST/#create-or-update-variables-on-a-process-instance

        """
        url = "{}/runtime/process-instances/{}/variables/".format(
            self._service_url, process_id
        )
        response = self._put(url, self._variables_to_activiti(variables))
        if response.status_code == requests.codes.created:
            return response.content
        else:
            raise ActivitiError(response_message(response))

    def task_claim(self, task_id, user):
        """Claim a task.


        .. note:: We use this method for group tasks (which don't have an
                  assignee).  We assign the current user to the task as we
                  complete it, so the history reports include it.

        TODO should we allow a user to claim a task where the assignee is
             already set to another user?

        """
        url = "{}/runtime/tasks/{}".format(self._service_url, task_id)
        data = {"action": "claim", "assignee": user.pk}
        response = self._post(url, data)
        if response.status_code == requests.codes.ok:
            return response
        elif response.status_code == requests.codes.conflict:
            raise ActivitiTaskAlreadyClaimedException(
                response_message(response)
            )
        else:
            raise ActivitiError(response_message(response))

    def task_complete(self, task_id, variables, *, additional_variables=None):
        """Complete a task.

        The ``additional_variables`` are used to add the ID of the user who
        claimed a group task (search for ``GROUP_TASK_CLAIMED_BY_USER_PK``).
        https://www.kbsoftware.co.uk/crm/ticket/7478/

        .. note:: If this is a group task, should we claim it for the user, so
                  we know who completed it?

        """
        url = "{}/runtime/tasks/{}".format(self._service_url, task_id)
        if additional_variables:
            variables.update(additional_variables)
        data = {
            "action": "complete",
            "variables": self._variables_to_activiti(variables),
        }
        response = self._post(url, data)
        if response.status_code == requests.codes.ok:
            return response
        else:
            raise ActivitiError(response_message(response))

    def task_group_id(self, task_id):
        """Get the first group for the task.

        The ``try``, ``except`` was added after we had an issue where the call
        was raising::

          workflow.activiti.ActivitiError:
          'ActivitiError, 404 error: Not Found for url:
          http://localhost:8080/activiti-rest/service/runtime/...
          ... tasks/43019/identitylinks:
          {"message": "Not found",
          "exception": "Could not find a task with id '43019'."}'

        https://www.kbsoftware.co.uk/crm/ticket/2247/

        """
        result = None
        try:
            identity_list = self.task_identity_list(task_id)
            for identity in identity_list:
                if identity.group:
                    result = identity.group
                    break
        except ActivitiError:
            pass
        return result

    def task_identity_list(self, task_id):
        """Read task identity links and convert to a lost of named tuples."""
        json_identity_links = self._task_identity_links(task_id)
        identity_links = []
        for link in json_identity_links:
            identity_links.append(self._row_to_identity_link(link))

        return identity_links

    def task_update(self, task_id, data):
        """Update a task.

        15/11/2024, Used in a management command / fix #7413

        The data should look like this::

          {"description" : "My task description"}

        Or like this::

          activiti.task_update(task_id, {"assignee": user.pk})

        """
        url = "{}/runtime/tasks/{}".format(self._service_url, task_id)
        response = self._put(url, data)
        if response.status_code == requests.codes.ok:
            return response
        else:
            raise ActivitiError(response_message(response))

    def task_status(self, task_id):
        """Read task data and convert to a named tuple with data we need."""
        task = self._task_status(task_id)
        return self._row_to_task_data(task)

    def task_status_historic(self, task_id):
        """Read task data and convert to a named tuple with data we need.

        Use ``task_history`` to query for multiple historic tasks.

        """
        data = self._task_status_historic(task_id)
        return self._row_to_historic_task_data(data)

    def task_status_process(self, process_id, variables_to_detail=None):
        """List of tasks for a process ID.

        Arguments:
        process_id -- Flowable process ID
        variables_to_detail -- set to ``True`` if you want variable details.

        """
        result = []
        task = self._task_status_process(process_id)
        data = task["data"]
        for row in data:
            result.append(self._row_to_task_data(row, variables_to_detail))
        return result

    def user_tasks(
        self,
        user=None,
        process_id=None,
        process_key=None,
        start=None,
        size=None,
    ):
        """A list of tasks for a user (as ``TaskData`` objects).

        We have three methods for getting a list of tasks

        1. ``_user_task_list`` returns the raw data from Flowable.
        2. ``user_tasks`` converts the Flowable data into ``TaskData`` objects.
        3. ``user_tasks_generator`` is a generator returning the task data.

        """
        if not size:
            size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        if not start:
            start = 0
        json_data = self._user_task_list(
            user, process_id, process_key, start, size
        )
        data = json_data["data"]
        total = json_data["total"]
        result = []
        for row in data:
            result.append(self._row_to_task_data(row, is_runtime=True))
        return result, total

    def user_tasks_generator(
        self,
        user=None,
        process_id=None,
        process_key=None,
    ):
        """Generator returning tasks for a user (as ``TaskData`` objects).

        .. tip:: See the ``user_tasks`` method for documentation.

        .. note:: Was called ``user_task_list`` until 12/08/2020

        .. warning:: The ``user_tasks`` method is called multiple times over a
                     period of time.  If tasks are completed, the ``total` count
                     will change between calls to the API.

        """
        count = 0
        size = settings.REST_FRAMEWORK["PAGE_SIZE"]
        start = 0
        while True:
            data, total = self.user_tasks(
                user, process_id, process_key, start, size
            )
            for row in data:
                count = count + 1
                yield row
            if count >= total:
                break
            # prepare for the next iteration
            start = start + size
