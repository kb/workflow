# -*- encoding: utf-8 -*-
import logging

from report.service import ReportMixin
from workflow.models import ScheduledWorkflow, ScheduledWorkflowUser


logger = logging.getLogger(__name__)


def delete_workflows_for_content_object(obj, user, process_definitions=None):
    """Delete active workflows for a content object.

    Keyword arguments:
    obj -- content object
    user -- user deleting the workflow
    process_definitions -- list of process definitions (default None)

    """
    count = 0
    comment = "Delete workflows for content object '{}' " "({} {})".format(
        obj, obj.__class__.__name__, obj.pk
    )
    qs = scheduled_workflow_user_for_content_object(obj, process_definitions)
    pks = [row.pk for row in qs]
    for pk in pks:
        scheduled_workflow_user = ScheduledWorkflowUser.objects.get(pk=pk)
        if scheduled_workflow_user.delete_workflow_process(user, comment):
            count = count + 1
    return count


def scheduled_workflow_user_for_content_object(obj, process_definitions=None):
    """Return the ``ScheduledWorkflowUser`` instances for ``obj``.

    Keyword arguments:
    obj -- content object
    process_definitions -- list of process definitions (default None)

    """
    # workflows for the object
    flows = ScheduledWorkflow.objects.for_content_object(
        obj, process_definitions
    )
    # find the process ids for the workflows
    return (
        ScheduledWorkflowUser.objects.filter(scheduled_workflow__in=flows)
        .exclude(deleted=True)
        .exclude(deleted_process=True)
    )


class WorkflowDataReport(ReportMixin):
    """A sample report"""

    def run_csv_report(self, csv_writer):
        csv_writer.writerow(("number", "square", "cube"))
        for num in range(1, 101):
            csv_writer.writerow((num, num * num, num * num * num))
        return True
