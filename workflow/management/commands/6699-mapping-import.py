# -*- encoding: utf-8 -*-
import attr
import json

from django.contrib.auth.models import Group, User
from django.core.management.base import BaseCommand
from workflow.activiti import ActivitiError

from workflow.models import FlowListType, Mapping, Workflow


@attr.s
class MappingImport:
    css_class = attr.ib()
    data_download = attr.ib()
    decimal_number = attr.ib()
    flowable_data_type = attr.ib()
    flow_list_type = attr.ib()
    group = attr.ib()
    help_text = attr.ib()
    history_summary = attr.ib()
    history_summary_column = attr.ib()
    history_summary_order = attr.ib()
    integer_number = attr.ib()
    mapping_code = attr.ib()
    mapping_type = attr.ib()
    multi_line = attr.ib()
    parameters = attr.ib()
    user = attr.ib()
    view_task_data = attr.ib()


class Command(BaseCommand):
    help = "Import mapping for a workflow #6699"

    def _flow_list_type(self, flow_list_type_name):
        result = None
        if flow_list_type_name:
            try:
                result = FlowListType.objects.get(name=flow_list_type_name)
            except FlowListType.DoesNotExist:
                raise ActivitiError(
                    "Cannot find Django 'FlowListType': "
                    f"'{flow_list_type_name}'. You may want to create it "
                    "then run this command again."
                )
        return result

    def _group(self, group_name):
        result = None
        if group_name:
            try:
                result = Group.objects.get(name=group_name)
            except Group.DoesNotExist:
                raise ActivitiError(
                    f"Cannot find Django 'Group': '{group_name}'"
                    "You may want to create it then run this command again."
                )
        return result

    def _user(self, user_name):
        result = None
        if user_name:
            try:
                result = User.objects.get(username=user_name)
            except User.DoesNotExist:
                raise ActivitiError(f"Cannot find 'User': '{user_name}'")
        return result

    def add_arguments(self, parser):
        parser.add_argument("file_name", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        file_name = options["file_name"]
        with open(file_name) as f:
            workflows = json.load(f)
        # from rich.pretty import pprint
        # pprint(data, expand_all=True)
        for process_key, data in workflows.items():
            self.stdout.write(f"1. Importing: '{process_key}'")
            workflow = Workflow.objects.get(process_key=process_key)
            workflow_data = data["workflow"]
            workflow.name = workflow_data["name"]
            workflow.icon = workflow_data["icon"]
            workflow.security_type = workflow_data["security_type"]
            workflow.url_name = workflow_data["url_name"]
            workflow.config_description = workflow_data["config_description"]
            workflow.config_url_name = workflow_data["config_url_name"]
            workflow.url_redirect = workflow_data["url_redirect"]
            workflow.save()
            self.stdout.write(f"2.     Found: {workflow.name}")
            mappings = data["mappings"]
            self.stdout.write(f"3.     Found: {len(mappings)} mappings")
            for variable_name, mapping in mappings.items():
                self.stdout.write(f"       - {variable_name}")
                mapping_import = MappingImport(**mapping)
                mapping = Mapping.objects.init_mapping(workflow, variable_name)
                mapping.css_class = mapping_import.css_class
                mapping.data_download = mapping_import.data_download
                mapping.decimal_number = mapping_import.decimal_number
                mapping.flowable_data_type = mapping_import.flowable_data_type
                mapping.flow_list_type = self._flow_list_type(
                    mapping_import.flow_list_type
                )
                mapping.group = self._group(mapping_import.group)
                mapping.help_text = mapping_import.help_text
                mapping.history_summary = mapping_import.history_summary
                mapping.history_summary_column = (
                    mapping_import.history_summary_column
                )
                mapping.history_summary_order = (
                    mapping_import.history_summary_order
                )
                mapping.integer_number = mapping_import.integer_number
                mapping.mapping_code = mapping_import.mapping_code
                mapping.mapping_type = mapping_import.mapping_type
                mapping.multi_line = mapping_import.multi_line
                mapping.parameters = mapping_import.parameters
                mapping.user = self._user(mapping_import.user)
                mapping.view_task_data = mapping_import.view_task_data
                mapping.save()
        self.stdout.write(f"{self.help} - Complete")
