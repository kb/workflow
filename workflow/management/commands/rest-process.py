# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from workflow.activiti import Activiti


class Command(BaseCommand):
    help = "Flowable REST - process"

    def _display_variable(self, process_status, variable_name):
        print(f"_display_variable '{variable_name}'".format("-" * 80))
        data = process_status["data"]
        for row in data:
            variables = row["variables"]
            print()
            for row in variables:
                if row["name"] == variable_name:
                    print(
                        "{} variables in total {}".format(
                            len(variables), "-" * 80
                        )
                    )
                    pprint(row, expand_all=True)

    def add_arguments(self, parser):
        parser.add_argument("process_id", type=str)
        parser.add_argument("variable_name", default=None, type=str, nargs="?")

    def handle(self, *args, **options):
        process_id = options["process_id"]
        variable_name = options["variable_name"]
        self.stdout.write(f"{self.help}: {process_id}")
        activiti = Activiti()
        print()
        print("_user_task_list {}".format("-" * 80))
        x = activiti._user_task_list(process_id=process_id)
        pprint(x, expand_all=True)
        print()
        print("_process_status {}".format("-" * 80))
        process_status = activiti._process_status(process_id)
        pprint(process_status, expand_all=True)
        if variable_name:
            self._display_variable(process_status, variable_name)
        print()
        print("_historic_process {}".format("-" * 80))
        historic_process_status = activiti._historic_process(process_id)
        pprint(historic_process_status, expand_all=True)
        if variable_name:
            self._display_variable(historic_process_status, variable_name)
        print()
        print("_history_task {}  ".format("-" * 80))
        x = activiti._history_task(process_id=process_id)
        pprint(x, expand_all=True)
        self.stdout.write(f"{self.help}: {process_id} - Complete")
