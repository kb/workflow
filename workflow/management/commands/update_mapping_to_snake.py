# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from workflow.activiti import camelcase_to_snake
from workflow.models import Mapping


class Command(BaseCommand):
    help = "Update workflow mapping to snake case"

    def handle(self, *args, **options):
        count = 0
        self.stdout.write("{}...".format(self.help))
        pks = [x.pk for x in Mapping.objects.all()]
        for pk in pks:
            mapping = Mapping.objects.get(pk=pk)
            variable_name = mapping.variable_name
            x = camelcase_to_snake(variable_name)
            if x == variable_name:
                pass
            else:
                mapping.variable_name = x
                mapping.save()
                self.stdout.write(
                    "{}: from '{}' to '{}'".format(
                        mapping.workflow.process_key, variable_name, x
                    )
                )
                count = count + 1
        self.stdout.write(
            "{}... Updated {} mappings...".format(self.help, count)
        )
