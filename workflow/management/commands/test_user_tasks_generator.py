# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from workflow.activiti import Activiti


class Command(BaseCommand):
    help = "Testing 'user_tasks_generator' (it never ends)!"

    def handle(self, *args, **options):
        import prettyprinter

        prettyprinter.prettyprinter.install_extras(["attrs"])
        activiti = Activiti()
        user = User.objects.get(pk=1)
        print()
        print("Found {}".format(user))
        count = 0
        for x in activiti.user_tasks_generator(user=user):
            count = count + 1
            if count > 55:
                break
            x.variables = {}
            # print()
            # print("count: {}".format(count))
            prettyprinter.cpprint(
                {
                    "count": count,
                    # "task_id": x.task_id,
                    # "created": x.created.strftime("%d/%m/%Y %H:%M"),
                }
            )
        print()
        x = list(activiti.user_tasks_generator(user=user))
        print("Found {} tasks (as a 'list')".format(len(x)))

        self.stdout.write("{} - Complete".format(self.help))

        # 1   30  20
        # 2   30  10  30
        #
        # 1   30  20
        # 2   31  11  31
        #
        # 1   30  20
        # 2   29  9   29
        #
        # Page    Total   Data    Count
        # 1       21      20      20
        # 2       18      []      20
        #
        # Page    Total   Data    Count
        # 1       19      19      19
