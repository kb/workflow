# -*- encoding: utf-8 -*-
import csv

# import prettyprinter

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from workflow.activiti import activiti_date, Activiti
from workflow.models import get_contact_model


class Command(BaseCommand):
    help = "Workflow - history process"

    def add_arguments(self, parser):
        parser.add_argument("process_key", type=str)
        parser.add_argument("filter", type=str)
        parser.add_argument("after", type=str)
        parser.add_argument("before", type=str)
        parser.add_argument("variable_name", type=str, nargs="?")

    def handle(self, *args, **options):
        process_key = options["process_key"]
        filter_by = options["filter"]
        after = activiti_date(options["after"])
        before = activiti_date(options["before"])
        variable_name = options.get("variable_name")
        if not filter_by in ("finished", "started"):
            raise CommandError("The 'filter' must be 'finished' or 'started'")
        self.stdout.write(
            "{} - {} ({} between {} and {})".format(
                self.help,
                process_key,
                filter_by,
                after,
                before,
            )
        )
        # prettyprinter.prettyprinter.install_extras(["attrs"])
        activiti = Activiti()
        contact_model = get_contact_model()
        size = 20
        start = 0
        file_name = "history-process-{}-{}-between-{}-and-{}.csv".format(
            process_key,
            filter_by,
            after.strftime("%Y-%m-%d-%H-%M-%S"),
            before.strftime("%Y-%m-%d-%H-%M-%S"),
        )
        # file_name = "temp.csv"
        with open(file_name, "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            headings = [
                "Workflow",
                "User",
                "Started",
                "Ended",
                # "Completed",
            ]
            if variable_name:
                headings.append(variable_name)
            headings.append("Process ID")
            csv_writer.writerow(headings)
            while True:
                parameters = dict(
                    process_key=process_key,
                    start=start,
                    size=size,
                    # started_after: started_after,
                    # started_before: started_before,
                    finished=False,
                )
                if filter_by == "finished":
                    parameters.update(
                        dict(
                            finished_after=after,
                            finished_before=before,
                        )
                    )
                else:
                    parameters.update(
                        dict(
                            started_after=after,
                            started_before=before,
                        )
                    )
                data, total = activiti.history_process(**parameters)
                #     process_key=process_key,
                #     start=start,
                #     size=size,
                #     started_after=started_after,
                #     started_before=started_before,
                #     finished=False,
                # )
                start = start + size
                for row in data:
                    # prettyprinter.cpprint(row)
                    contact = variable_value = None
                    variable = row.variables.get("user_pk")
                    if variable:
                        contact = contact_model.objects.get(
                            user__pk=variable.value
                        )
                    if variable_name:
                        variable = row.variables.get(variable_name)
                        if variable:
                            variable_value = variable.value
                    data = [
                        row.process_key,
                        str(contact) if contact else "",
                        row.start_time.strftime("%d/%m/%Y %H:%M"),
                        (
                            row.end_time.strftime("%d/%m/%Y %H:%M")
                            if row.end_time
                            else ""
                        ),
                        # row.completed,
                    ]
                    if variable_value:
                        data.append(variable_value)
                    data.append(row.process_id)
                    csv_writer.writerow(data)
                if len(data) < size:
                    break
        self.stdout.write("{} - Complete".format(self.help))
