# -*- encoding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from workflow.models import ScheduledWorkflow, ScheduledWorkflowUser


class Command(BaseCommand):
    help = "Workflow - create pending workflows"

    def add_arguments(self, parser):
        parser.add_argument("process_definition", type=str, nargs="?")

    def handle(self, *args, **options):
        count = 0
        end_date = timezone.now()
        start_date = end_date + relativedelta(minutes=-45)
        process_definition = options["process_definition"]
        if process_definition:
            process_definition = process_definition.strip()
        self.stdout.write(
            "{}{} (from {} to {})".format(
                self.help,
                (
                    " for '{}'".format(process_definition)
                    if process_definition
                    else ""
                ),
                start_date.strftime("%d/%m/%Y %H:%M"),
                end_date.strftime("%d/%m/%Y %H:%M"),
            )
        )
        pending = ScheduledWorkflowUser.objects.pending_with_options(
            process_definition=process_definition,
            start_date=start_date,
            end_date=end_date,
        ).order_by("-pk")
        scheduled_workflow_pks = sorted(
            set([x.scheduled_workflow.pk for x in pending]), reverse=True
        )
        self.stdout.write(
            "Found {} pending records".format(len(scheduled_workflow_pks))
        )
        for pk in scheduled_workflow_pks:
            with transaction.atomic():
                result = ScheduledWorkflow.objects.create_workflows(pk)
                if result:
                    self.stdout.write(
                        "Created {} user workflows for workflow "
                        "{}: {}".format(len(result), pk, result)
                    )
                    count = count + 1
        self.stdout.write(
            "{} - Complete (created {} workflows)".format(self.help, count)
        )
