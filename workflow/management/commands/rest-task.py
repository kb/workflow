# -*- encoding: utf-8 -*-
from rich.pretty import pprint

from django.core.management.base import BaseCommand

from workflow.activiti import Activiti


class Command(BaseCommand):
    help = "Flowable REST - task"

    def add_arguments(self, parser):
        parser.add_argument("task_id", type=str)

    def handle(self, *args, **options):
        task_id = options["task_id"]
        self.stdout.write(f"{self.help}: {task_id}")
        activiti = Activiti()
        print()
        print("task_status {}".format("-" * 80))
        task = activiti.task_status(task_id)
        pprint(task, expand_all=True)
        print()
        # print("_user_task_list {}".format("-" * 80))
        # x = activiti._user_task_list(process_id=task.process_id)
        # prettyprinter.cpprint(x)
        # print()
        # print("_process_status {}".format("-" * 80))
        # x = activiti._process_status(task.process_id)
        # prettyprinter.cpprint(x)
        # print()
        # print("_history_task {}  ".format("-" * 80))
        # x = activiti._history_task(process_id=task.process_id)
        # prettyprinter.cpprint(x)
        self.stdout.write("To check process data...")
        self.stdout.write(f"django-admin rest-process {task.process_id}")
        print()
        self.stdout.write(
            f"{self.help}: {task_id} - Complete".format(self.help, task_id)
        )
