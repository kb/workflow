# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from workflow.activiti import Activiti


class Command(BaseCommand):
    help = "List the value of a variable for a Flowable workflow"

    def add_arguments(self, parser):
        parser.add_argument("process_key", type=str)
        parser.add_argument("variable_name", type=str)

    def _historic_processes(
        self, activiti, csv_writer, process_key, variable_name, start, size
    ):
        count = 0
        while True:
            processes, total = activiti.history_process(
                process_key=process_key, start=start, size=size
            )
            start = start + size
            if processes:
                for process in processes:
                    count = count + 1
                    for name, variable in process.variables.items():
                        if name == variable_name:
                            csv_writer.writerow(
                                [
                                    process.pk,
                                    "Complete",
                                    process.start_time.strftime(
                                        "%d/%m/%Y %H:%M"
                                    ),
                                    variable.value,
                                ]
                            )
            else:
                break
        if count == total:
            pass
        else:
            raise CommandError(
                f"Found {count} historic processes, "
                f"but there should be {total}"
            )
        print(f"Found {total} Historic Processes")

    def _processes(
        self, activiti, csv_writer, process_key, variable_name, start, size
    ):
        count = 0
        while True:
            processes, total = activiti.process(
                process_key=process_key, start=start, size=size
            )
            start = start + size
            if processes:
                for process in processes:
                    count = count + 1
                    for name, variable in process.variables.items():
                        if name == variable_name:
                            csv_writer.writerow(
                                [
                                    process.pk,
                                    "Current",
                                    process.start_time.strftime(
                                        "%d/%m/%Y %H:%M"
                                    ),
                                    variable.value,
                                ]
                            )
            else:
                break
        if count == total:
            pass
        else:
            raise CommandError(
                f"Found {count} historic processes, "
                f"but there should be {total}"
            )
        print(f"Found {total} Current Processes")

    def handle(self, *args, **options):
        """Export variable for a Flowable workflow."""
        process_key = options["process_key"]
        variable_name = options["variable_name"]
        self.stdout.write(f"{self.help}")
        self.stdout.write(f"'Export '{variable_name}' for {process_key}")
        activiti = Activiti()
        start = 0
        size = 20
        year_month_day = timezone.now().strftime("%Y-%m-%d")
        file_name = f"{year_month_day}-{process_key}-{variable_name}.csv"
        with open(file_name, "w", newline="", encoding="utf-8") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            csv_writer.writerow(["process", "status", "created", "value"])
            self._processes(
                activiti, csv_writer, process_key, variable_name, start, size
            )
            self._historic_processes(
                activiti, csv_writer, process_key, variable_name, start, size
            )
        self.stdout.write("Complete...")
