# -*- encoding: utf-8 -*-
import json

from datetime import datetime
from django.core.management.base import BaseCommand

from workflow.models import Mapping, Workflow


class Command(BaseCommand):
    help = "Export mapping for a workflow #6699"

    def add_arguments(self, parser):
        parser.add_argument("workflow", type=str)

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        process_key = options["workflow"]
        self.stdout.write(f"Exporting 1. '{process_key}'")
        workflow = Workflow.objects.get(process_key=process_key)
        self.stdout.write(f"          2. {workflow.name}")
        data = {
            "workflow": {
                "name": workflow.name,
                "icon": workflow.icon,
                "security_type": workflow.security_type,
                "url_name": workflow.url_name,
                "config_description": workflow.config_description,
                "config_url_name": workflow.config_url_name,
                "url_redirect": workflow.url_redirect,
            },
            "mappings": {},
        }
        mappings = Mapping.objects.filter(workflow=workflow).order_by(
            "variable_name"
        )
        for mapping in mappings:
            flow_list_type_name = group_name = user_name = None
            flow_list_type = mapping.flow_list_type
            if flow_list_type:
                flow_list_type_name = mapping.flow_list_type.name
            group = mapping.group
            if group:
                group_name = mapping.group.name
            user = mapping.user
            if user:
                user_name = mapping.user.username
            data["mappings"][mapping.variable_name] = {
                "flowable_data_type": mapping.flowable_data_type,
                "mapping_type": mapping.mapping_type,
                "mapping_code": mapping.mapping_code,
                "help_text": mapping.help_text,
                "flow_list_type": flow_list_type_name,
                "group": group_name,
                "user": user_name,
                "decimal_number": mapping.decimal_number,
                "integer_number": mapping.integer_number,
                "css_class": mapping.css_class,
                "multi_line": mapping.multi_line,
                "history_summary": mapping.history_summary,
                "history_summary_column": mapping.history_summary_column,
                "history_summary_order": mapping.history_summary_order,
                "data_download": mapping.data_download,
                "view_task_data": mapping.view_task_data,
                "parameters": mapping.parameters,
            }
        data = {workflow.process_key: data}
        from rich.pretty import pprint

        pprint(data, expand_all=True)
        with open(f"{workflow.process_key}-mapping-export.json", "w") as f:
            json.dump(data, f, indent=4)
        self.stdout.write(f"{self.help} - Complete")
