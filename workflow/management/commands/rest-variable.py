# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from rich.pretty import pprint

from workflow.activiti import Activiti, Variable


class Command(BaseCommand):
    help = "Flowable REST - variable"

    def add_arguments(self, parser):
        parser.add_argument("process_id", type=str)
        parser.add_argument("variable_name", type=str)
        parser.add_argument("value", default=None, type=str, nargs="?")

    def handle(self, *args, **options):
        """Variable for a live process."""
        process_id = options["process_id"]
        variable_name = options["variable_name"]
        value = options["value"]
        self.stdout.write("{}: {}".format(self.help, process_id))
        activiti = Activiti()
        print("_process_status {}".format("-" * 80))
        x = activiti._process_status(process_id)
        found = False
        for row in x["data"]:
            variables = row["variables"]
            for row in variables:
                if row["name"] == variable_name:
                    pprint(row, expand_all=True)
                    found = True
                    is_string = bool(row["type"] == "string")
        # should we update the value?
        if value:
            if found and is_string:
                self.stdout.write(
                    f"Update '{variable_name}' "
                    f"(for process {process_id}) to '{value}'"
                )
                result = activiti.process_variables(
                    process_id,
                    {
                        variable_name: Variable(
                            name="", value=value, data_type=str
                        )
                    },
                )
                pprint(result, expand_all=True)
            else:
                if not found:
                    raise CommandError(
                        "Cannot find variable '{}' in process {}".format(
                            variable_name, process_id
                        )
                    )
                if not is_string:
                    raise CommandError(
                        "The '{}' variable is not a 'string' type. "
                        "The code needs updating to handle other types.".format(
                            variable_name
                        )
                    )
        self.stdout.write(
            f"{self.help}: '{variable_name}' (process {process_id}) - Complete"
        )
        self.stdout.write(
            "\nNote: if you are using this management command to remove a "
            "non-alpha character from a variable, then don't forget to run "
            "the HTTP task API at the same time.\n"
        )
