# -*- encoding: utf-8 -*-
import logging

from django.db import transaction
from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4

from report.models import ReportError
from report.pdf import NumberedCanvas, PDFReport
from report.service import ReportMixin
from workflow.models import Workflow, WorkflowData


logger = logging.getLogger(__name__)


class TableReport(PDFReport):
    """Create a simple table."""

    def _text_to_paragraph(self, data):
        result = []
        for row in data:
            para = []
            for x in row:
                para.append(self._para(str(x)))
            result.append(para)
        return result

    def _table(self, data):
        return platypus.Table(
            self._text_to_paragraph(data),
            colWidths=[120, 360],
            style=[
                ("GRID", (0, 0), (-1, -1), self.GRID_LINE_WIDTH, colors.grey),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 3),
            ],
        )

    def _title(self, title):
        return platypus.Table(
            [[self._bold(title)]],
            colWidths=[480],
            style=[
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
                ("LEFTPADDING", (0, 0), (-1, -1), 0),
            ],
        )

    def create(self, buff, data, title, paras=None):
        """Create a PDF report.

        Keyword arguments:
        title -- page title
        data -- data to be added to a ReportLab table.
        paras -- additional data which probably won't fit in a table cell!

        """
        if paras is None:
            paras = []
        doc = platypus.SimpleDocTemplate(buff, title=title, pagesize=A4)
        # Container for the 'Flowable' objects
        elements = []
        elements.append(self._title(title))
        elements.append(self._table(data))
        elements.append(platypus.Spacer(1, 12))
        for heading, text in paras:
            elements.append(self._bold(heading))
            elements.append(self._para(str(text)))
        elements.append(platypus.Spacer(1, 12))
        doc.build(elements, canvasmaker=NumberedCanvas)


class WorkflowDataReport(ReportMixin):
    """

    Questions for Malcolm

    1. Does the ``ReportSchedule`` instance just get re-used?
    2. How does it handle history i.e. go and get a previous version of the
       report?
    3. How do the parameters work?
    4. If the ``ReportSchedule`` instance gets reused, do we end up with lots
       of CSV files without a model instance?
    5. Does the retry count increment forever?

    """

    def run_csv_report(self, csv_writer):
        result = False
        if not self.schedule:
            raise ReportError("No 'schedule' for report")
        if not self.schedule.parameters:
            raise ReportError("No 'parameters' for report schedule")
        process_key = self.schedule.parameters.get("process_key")
        if not process_key:
            raise ReportError("No 'process_key' parameter")
        try:
            workflow = Workflow.objects.get(process_key=process_key)
        except Workflow.DoesNotExist:
            raise ReportError(
                "Workflow '{}' does not exist".format(process_key)
            )
        variables = [x.variable_name for x in workflow.data_download()]
        if variables:
            csv_writer.writerow(variables)
        else:
            raise ReportError(
                "No data download columns selected for '{}'. Report schedule "
                "'{}'".format(process_key, self.schedule.pk)
            )
        # get the pending rows
        qs = WorkflowData.objects.pending_report(process_key)
        pks = [x.pk for x in qs.order_by("created")]
        result = True
        with transaction.atomic():
            for pk in pks:
                data = WorkflowData.objects.select_for_update(nowait=True).get(
                    pk=pk
                )
                row = []
                for name in variables:
                    row.append(data.data.get(name, ""))
                csv_writer.writerow(row)
                data.reported = True
                data.save()
                result = True
        return result
