# -*- encoding: utf-8 -*-
import io
import logging
import requests

from collections import OrderedDict
from datetime import date
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.core.files.base import ContentFile
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    FormView,
    ListView,
    UpdateView,
    TemplateView,
)

from base.view_utils import BaseMixin, ElasticPaginator, RedirectNextMixin
from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from report.models import ReportSpecification
from report.views import ReportCsvMixin
from .activiti import (
    Activiti,
    ActivitiError,
    convert_date_iso,
    int_if_possible,
    Variable,
)
from .forms import (
    EmptyForm,
    HistorySecurityForm,
    MappingConfigForm,
    MappingMapDecimalForm,
    MappingMapFlowList,
    MappingMapGroupForm,
    MappingMapIntegerForm,
    MappingMapUserForm,
    ProcessDefinitionForm,
    WorkflowDataEmptyForm,
    WorkflowEmptyForm,
    WorkflowForm,
)
from .models import (
    completed_reports,
    GROUP_TASK_CLAIMED_BY_USER_PK,
    HIDDEN_VARIABLE_NAMES,
    Mapping,
    MappingPlugin,
    ProcessDefinition,
    RESERVED_VARIABLE_NAMES,
    ScheduledWorkflowUser,
    user_owns_task,
    Workflow,
    workflow_form_includes_delete_workflow,
    WorkflowData,
    WorkflowError,
)
from .report import TableReport


logger = logging.getLogger(__name__)


def check_security_history_perm(user, process_key):
    """Check for a user with history security permissions."""
    try:
        workflow = Workflow.objects.get(process_key=process_key)
        if user.is_superuser:
            pass
        elif not workflow.has_security_history(user):
            raise PermissionDenied(
                "User '{}' does not have permission to view task history "
                "for '{}' processes".format(user.username, process_key)
            )
    except Workflow.DoesNotExist:
        raise WorkflowError(
            "Check permissions for viewing history: cannot find "
            "workflow with 'process_key' of '{}'".format(process_key)
        )


def download_process_diagram(request, process_id):
    """Download process diagram.

    Copied to ``work.api._download_diagram``.

    """
    activiti = Activiti()
    if request.user.is_superuser:
        pass
    elif request.user.is_anonymous:
        raise PermissionDenied()
    else:
        # check workflow security
        process_status = activiti.process_status(process_id)
        try:
            workflow = Workflow.objects.get(
                process_key=process_status.process_key
            )
        except Workflow.DoesNotExist:
            raise WorkflowError(
                "Cannot find workflow '{}'".format(process_status.process_key)
            )
        if not workflow.has_security_history(request.user):
            raise PermissionDenied()
    # get the diagram
    diagram = activiti.process_diagram(process_id)
    file_to_send = ContentFile(diagram)
    response = HttpResponse(file_to_send, "image/png")
    response["Content-Length"] = file_to_send.size
    response["Content-Disposition"] = 'attachment; filename="somefile.png"'
    return response


class History:
    """Task and process history.

    .. note:: This is a utility class - not a view mixin.
              19/01/2020, I tried to move it to ``service.py`` but the model
              imports don't work.

    .. note:: As of 3rd July, this includes current tasks (so perhaps history
              is not a good name for the class.  See "To Do" note on
              https://www.kbsoftware.co.uk/crm/ticket/2265/ for suggested
              refactor.

    """

    def _columns(self, workflow, variables):
        """Get the history columns from the process variables."""
        column_1 = column_2 = ""
        mappings = workflow.history_summary_columns()
        keys = [x.variable_name for x in mappings]
        for key in keys:
            variable = variables.get(key)
            if variable:
                if not column_1:
                    column_1 = variable.value
                else:
                    if not column_2:
                        column_2 = variable.value
                        break
        return column_1, column_2

    def _variables(self, workflow, variables):
        """Get the history variables from the process variables."""
        result = OrderedDict()
        for mapping in workflow.history_summary():
            key = mapping.variable_name
            value = variables.get(key)
            if value:
                name = key.replace("_", " ")
                name = name.title()
                result[name] = mapping.format_activiti_data(value)
        return result

    def activiti(self):
        return Activiti()

    def current_tasks(self, user, process_id=None, process_key=None):
        activiti = self.activiti()
        tasks, total = activiti.user_tasks(
            user, process_id=process_id, process_key=process_key
        )
        return tasks

    def historic_tasks(self, user=None, process_id=None, process_key=None):
        """Get a list of completed tasks."""
        return self.activiti().history_task(
            user=user, process_id=process_id, process_key=process_key
        )

    def merge_tasks_and_processes(
        self, tasks, processes, completed=None, get_full_name=None
    ):
        """Add process information to the list of tasks.

        Lookup process information in the processes ``dict``.

        .. note:: I think this method is only being used to get tasks for a
                  single process, so it is probably more complicated than it
                  needs to be.

        """
        result = []
        for task in tasks:
            process = processes[task.process_id]
            assignee = task.assignee
            deleted = False
            try:
                deleted = task.deleted
            except AttributeError:
                pass
            if get_full_name:
                try:
                    user = get_user_model().objects.get(pk=assignee)
                    assignee = user.get_full_name()
                except get_user_model().DoesNotExist:
                    logger.error(
                        # raise WorkflowError(
                        "Merging tasks and processes: cannot find "
                        "user '{}'".format(assignee)
                    )
            claim_time = created = end_time = None
            if completed:
                claim_time = task.claim_time
                end_time = task.end_time
            else:
                created = task.created
            result.append(
                {
                    "process_id": task.process_id,
                    "process_name": process["name"],
                    "process_variables": process["variables"],
                    "assignee": assignee,
                    "category": task.category,
                    "completed": completed,
                    "claim_time": claim_time,
                    "created": created,
                    "deleted": deleted,
                    "end_time": end_time,
                    "name": task.name,
                }
            )
        return result

    def processes(self, process_ids):
        """Get process information for the list of IDs."""
        result = OrderedDict()
        activiti = self.activiti()
        for process_id in process_ids:
            if process_id not in result:
                col_1 = col_2 = created = end_time = name = variables = None
                workflow_pk = None
                # status will be a 'ProcessInstance'
                status = activiti.process_status(process_id)
                if status.completed:
                    # status will be 'HistoricProcessData'
                    status = activiti.historic_process(process_id)
                    # note: status is 'HistoricProcessData'
                    created = status.start_time
                    end_time = status.end_time
                else:
                    # note: status is still a 'ProcessInstance'
                    created = status.start_time
                try:
                    workflow = Workflow.objects.get(
                        process_key=status.process_key
                    )
                    name = workflow.name
                    col_1, col_2 = self._columns(workflow, status.variables)
                    variables = self._variables(workflow, status.variables)
                    workflow_pk = workflow.pk
                except Workflow.DoesNotExist:
                    logger.error(
                        "Cannot find workflow '{}'".format(status.process_key)
                    )
                result[process_id] = {
                    "name": name,
                    "completed": status.completed,
                    "created": created,
                    "end_time": end_time,
                    "column_1": col_1,
                    "column_2": col_2,
                    "deleted": False,
                    "process_id": status.pk,
                    "process_key": status.process_key,
                    "variables": variables,
                    "workflow_pk": workflow_pk,
                }
        return result

    def task_process_ids(self, user, process_key):
        """Get process ids for current and historic tasks."""
        tasks = []
        current_tasks = self.current_tasks(user, process_key)
        for x in current_tasks:
            tasks.append(
                dict(
                    task_id=x.pk,
                    process_id=x.process_id,
                    task_time=x.created,
                )
            )
        historic_tasks = self.historic_tasks(user=user, process_key=process_key)
        for x in historic_tasks:
            tasks.append(
                dict(
                    task_id=x.pk,
                    process_id=x.process_id,
                    task_time=x.end_time,
                )
            )
        # sort tasks by created date/end time
        tasks = sorted(tasks, key=lambda k: k["task_time"], reverse=True)
        # remove duplicate process ids
        return list(OrderedDict.fromkeys([x["process_id"] for x in tasks]))


class HistoryProcessMixin:
    template_name = "workflow/history_process_list.html"

    def _workflow_pk(self):
        result = None
        value = self.request.GET.get("workflow")
        if value:
            result = int(value)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        has_security_history = Workflow.objects.has_security_history(
            self.request.user
        )
        context.update(
            dict(form=self._form(), has_security_history=has_security_history)
        )
        return context

    def get_initial(self):
        return {"workflow": self._workflow_pk()}


class HistoryProcessView(
    LoginRequiredMixin, HistoryProcessMixin, BaseMixin, ListView
):
    def _form(self):
        kwargs = {"initial": self.get_initial()}
        qs = Workflow.objects.for_user(self.request.user)
        return HistorySecurityForm(query_set=qs, report=False, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(report=False))
        return context

    def get_queryset(self):
        result = []
        process_key = None
        workflow_pk = self._workflow_pk()
        if workflow_pk:
            workflow = Workflow.objects.get(pk=workflow_pk)
            process_key = workflow.process_key
        history = History()
        process_ids = history.task_process_ids(
            user=self.request.user, process_key=process_key
        )
        result = history.processes(process_ids)
        return result


class HistoryProcessReportView(
    LoginRequiredMixin, HistoryProcessMixin, BaseMixin, ListView
):
    def _check_report_perm(self):
        if Workflow.objects.has_security_history(self.request.user):
            pass
        else:
            raise PermissionDenied(
                "User '{}' does not have permission to view "
                "workflow history".format(self.request.user)
            )

    def _form(self):
        kwargs = {"initial": self.get_initial()}
        qs = Workflow.objects.security_history(self.request.user)
        return HistorySecurityForm(query_set=qs, report=True, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(report=True))
        return context

    def get_queryset(self):
        self._check_report_perm()
        result = []
        history = History()
        process_key = None
        workflow_pk = self._workflow_pk()
        if workflow_pk:
            workflow = Workflow.objects.get(pk=workflow_pk)
            process_key = workflow.process_key
        # for reporting, we need the user to select a process key
        if process_key:
            process_ids = history.task_process_ids(
                user=None, process_key=process_key
            )
            result = history.processes(process_ids)
        return result


class HistoryTaskMixin:
    template_name = "workflow/history_task_list.html"

    def _attachments(self):
        """Get the attachments for the workflow object.

        If the content object for the workflow has an ``attachments`` method
        then it will be called.

        20/01/2020, To see an example of ``attachments``, check the
        ``FlowRecord`` model in our ``work`` project (``work/models.py``).

        The ``FlowRecord`` model uses the ``Record`` class from our ``record``
        app:
        https://www.kbsoftware.co.uk/crm/ticket/2571/

        The ``history_task_list.html`` template looks for the following fields
        in the content object (``FlowRecord`` in this example).

        - ``download_url``
        - ``original_file_name``,
        - ``created`` (a ``datetime``)

        """
        process_id = self._process_id()
        try:
            schedule = ScheduledWorkflowUser.objects.get_process_id(process_id)
        except ScheduledWorkflowUser.DoesNotExist:
            raise WorkflowError(
                "Cannot find 'ScheduledWorkflowUser' with 'process_id' "
                "of '{}'".format(process_id)
            )
        try:
            result = schedule.scheduled_workflow.content_object.attachments()
        except AttributeError:
            result = []
        return result

    def _process(self):
        """Get the process data for a single process."""
        process_id = self._process_id()
        history = History()
        processes = history.processes([process_id])
        process = processes[process_id]
        return process

    def _process_id(self):
        process_id = self.kwargs["process_id"]
        return process_id

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(process=self._process(), attachments=self._attachments())
        )
        return context


class HistoryTaskReportView(
    LoginRequiredMixin, HistoryTaskMixin, RedirectNextMixin, BaseMixin, ListView
):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(report=True, process_id=self._process_id()))
        return context

    def get_queryset(self):
        history = History()
        process = self._process()
        process_id = process["process_id"]
        check_security_history_perm(self.request.user, process["process_key"])
        current_tasks = history.current_tasks(user=None, process_id=process_id)
        historic_tasks = history.historic_tasks(
            user=None, process_id=process_id
        )
        # processes
        tasks = current_tasks + historic_tasks
        process_ids = {x.process_id for x in tasks}
        processes = history.processes(process_ids)
        # tasks
        current = history.merge_tasks_and_processes(
            current_tasks, processes, get_full_name=True
        )
        history = history.merge_tasks_and_processes(
            historic_tasks, processes, completed=True, get_full_name=True
        )
        return current + history


class HistoryTaskView(
    LoginRequiredMixin, HistoryTaskMixin, RedirectNextMixin, BaseMixin, ListView
):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(report=False, process_id=self._process_id()))
        return context

    def get_queryset(self):
        history = History()
        process_id = self._process_id()
        current_tasks = history.current_tasks(
            self.request.user, process_id=process_id
        )
        historic_tasks = history.historic_tasks(
            self.request.user, process_id=process_id
        )
        # processes
        tasks = current_tasks + historic_tasks
        process_ids = {x.process_id for x in tasks}
        processes = history.processes(process_ids)
        # tasks
        current = history.merge_tasks_and_processes(current_tasks, processes)
        history = history.merge_tasks_and_processes(
            historic_tasks, processes, completed=True
        )
        return current + history


class ProcessInstanceDeleteMixin:
    form_class = EmptyForm
    template_name = "workflow/process_instance_delete.html"

    def _activiti(self):
        return Activiti()

    def form_valid(self, form):
        activiti = Activiti()
        process_id = self._process_id()
        process_status = activiti.process_status(process_id)
        # save the 'process_key' for use in `get_success_url`
        self.process_key_for_success_url = process_status.process_key
        if not (process_status.completed or process_status.suspended):
            activiti.process_delete(process_id)
            messages.success(
                self.request,
                "Workflow process {} has been deleted".format(process_id),
            )
        else:
            messages.error(
                self.request,
                "Could not delete workflow process {}".format(process_id),
            )
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        process_id = self._process_id()
        process_status = self._activiti().process_status(process_id)
        context.update(
            dict(
                process_status=process_status,
                variables=sorted(process_status.variables.items()),
            )
        )
        return context


class ProcessInstanceDeleteView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    ProcessInstanceDeleteMixin,
    BaseMixin,
    FormView,
):
    def _process_id(self):
        return int_if_possible(self.kwargs["process_id"])

    def get_success_url(self):
        return reverse("workflow.process.instance.list")


class TaskProcessInstanceDeleteView(
    LoginRequiredMixin, ProcessInstanceDeleteMixin, BaseMixin, FormView
):
    def _process_id(self):
        task = self._task()
        return task.process_id

    def _task(self):
        task_id = self._task_id()
        task = self._activiti().task_status(task_id)
        if not self._user_has_perm(task):
            raise PermissionError(
                "'{}' does not have permission to delete the workflow "
                "for task {}".format(self.request.user, task_id)
            )
        return task

    def _task_id(self):
        task_id = self.kwargs["task_id"]
        return task_id

    def _user_has_perm(self, task):
        """Does the user have permission to delete the task?"""
        result = False
        activiti = self._activiti()
        if workflow_form_includes_delete_workflow(activiti, task.pk):
            result = user_owns_task(activiti, self.request.user, task)
        return result

    def _workflow(self, process_key):
        result = None
        if process_key:
            try:
                result = Workflow.objects.get(process_key=process_key)
            except Workflow.DoesNotExist:
                pass
        if not result:
            logger.error("Cannot find workflow '{}'".format(process_key))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        task = self._task()
        context.update(dict(task=task))
        return context

    def get_success_url(self):
        """After delete, redirect to specified URL or the list of tasks...

        We try and redirect to ``url_redirect`` from the ``Workflow`` model.

        I thought we should be using ``url_has_allowed_host_and_scheme`` and
        ``iri_to_uri`` (see https://www.kbsoftware.co.uk/crm/ticket/4955/),
        but these were added to prevent XSS issues
        e.g. ``http://localhost:8000/dash/?next=javascript:alert(1)``.
        ``url_redirect`` can only be updated by a super user, so no need to
        validate here...

        """
        next_url = process_key = None
        try:
            process_key = self.process_key_for_success_url
        except AttributeError:
            pass
        if process_key:
            workflow = self._workflow(process_key)
            if workflow and workflow.url_redirect:
                next_url = workflow.url_redirect
        return next_url or reverse("project.tasks")


class ProcessInstanceDetailView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "workflow/process_instance_detail.html"

    def _process_id(self):
        return int_if_possible(self.kwargs["process_id"])

    def _workflow(self, process_key):
        result = None
        if process_key:
            try:
                result = Workflow.objects.get(process_key=process_key)
            except Workflow.DoesNotExist:
                pass
        if not result:
            logger.error("Cannot find workflow '{}'".format(process_key))
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        activiti = Activiti()
        process_id = self._process_id()
        data = activiti.task_status_process(
            process_id, variables_to_detail=True
        )
        process_status = activiti.process_status(process_id)
        context.update(
            dict(
                data=data,
                process_id=process_id,
                workflow=self._workflow(process_status.process_key),
            )
        )
        return context


class PaginateMixin:
    paginate_by_for_search = 15

    def _criteria(self):
        return self.request.GET.get("criteria") or self.request.GET.get("q")

    def _page_number(self):
        page_number = 1
        if self.page_kwarg in self.request.GET:
            try:
                page_number = int(self.request.GET.get(self.page_kwarg))
            except ValueError:
                pass
        return page_number

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page_number = self._page_number()
        page_obj = ElasticPaginator(
            page_number, self.get_paginate_by_for_search(), self.total
        )
        context.update(
            dict(
                criteria=self._criteria(),
                is_paginated=page_obj.has_other_pages(),
                page=page_obj,
            )
        )
        return context

    def get_paginate_by_for_search(self):
        """Get the number of items to paginate by, or ``20`` as a default."""
        return self.paginate_by_for_search or 20


class DeploymentListView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    PaginateMixin,
    BaseMixin,
    ListView,
):
    page_title = "Deployments"
    template_name = "workflow/deployment_list.html"

    def get_queryset(self):
        activiti = Activiti()
        page_number = self._page_number() - 1
        result, self.total = activiti.deployment_list(
            page_number=page_number, page_size=self.get_paginate_by_for_search()
        )
        return result


class ProcessDefinitionListView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    PaginateMixin,
    BaseMixin,
    ListView,
):
    page_title = "Process Definitions"
    template_name = "workflow/process_definition_list.html"

    def get_queryset(self):
        activiti = Activiti()
        page_number = self._page_number() - 1
        result, self.total = activiti.process_definition_list(
            page_number=page_number, page_size=self.get_paginate_by_for_search()
        )
        return result


class ProcessInstanceListView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    PaginateMixin,
    BaseMixin,
    ListView,
):
    """List of process instances.

    .. note:: The Flowable REST API does not have an option to order by date
              created (or anything similar).

    The pagination code is copied from ``search/views.py`` and uses the
    ``ElasticPaginator`` from the ``base`` app.

    """

    page_title = "Process Instances"
    template_name = "workflow/process_instance_list.html"

    def get_queryset(self):
        activiti = Activiti()
        page_number = self._page_number() - 1
        size = self.get_paginate_by_for_search()
        start = page_number * size
        result, self.total = activiti.process(start=start, size=size)
        return result


class TaskListView(
    LoginRequiredMixin,
    SuperuserRequiredMixin,
    PaginateMixin,
    BaseMixin,
    ListView,
):
    page_title = "Tasks"
    template_name = "workflow/task_list.html"

    def get_queryset(self):
        activiti = Activiti()
        page_number = self._page_number() - 1
        data, self.total = activiti.user_tasks(
            start=page_number, size=self.get_paginate_by_for_search()
        )
        return data


class DeploymentDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "workflow/workflow_detail.html"


class MappingConfigUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingConfigForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            result = super().form_valid(form)
            Mapping.objects.generate_extra(self.object)
        return result

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(
            dict(can_edit_mapping_type=self.object.can_edit_mapping_type())
        )
        return result

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class MappingListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    template_name = "workflow/mapping_list.html"

    def _workflow(self):
        pk = self.kwargs["workflow_pk"]
        return Workflow.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"workflow": self._workflow()})
        return context

    def get_queryset(self):
        workflow = self._workflow()
        return workflow.mapping()


class WorkflowDeleteView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Workflow
    form_class = WorkflowEmptyForm
    template_name = "workflow/workflow_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("workflow.list")


class WorkflowListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    model = Workflow
    template_name = "workflow/workflow_list.html"

    def get_queryset(self):
        return Workflow.objects.current().order_by("process_key")


class WorkflowUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Workflow
    form_class = WorkflowForm

    def _mapping(self, column_number):
        result = None
        try:
            result = Mapping.objects.get(
                workflow=self.object, history_summary_column=column_number
            )
        except Mapping.DoesNotExist:
            pass
        return result

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            columns = [
                form.cleaned_data["history_summary_column_1"],
                form.cleaned_data["history_summary_column_2"],
            ]
            self.object.set_history_summary_columns(columns)
            result = super().form_valid(form)
        return result

    def get_initial(self):
        result = super().get_initial()
        result.update(
            dict(
                history_summary_column_1=self._mapping(1),
                history_summary_column_2=self._mapping(2),
            )
        )
        return result

    def get_success_url(self):
        return reverse("workflow.list")


class MappingMapDecimalUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingMapDecimalForm

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class MappingMapFlowListUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingMapFlowList

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class MappingMapGroupUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingMapGroupForm

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class MappingMapIntegerUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingMapIntegerForm

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class MappingMapUserUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    model = Mapping
    form_class = MappingMapUserForm

    def get_success_url(self):
        return reverse("workflow.mapping.list", args=[self.object.workflow.pk])


class WorkflowDataCreateView(LoginRequiredMixin, BaseMixin, CreateView):
    form_class = WorkflowDataEmptyForm
    model = WorkflowData
    template_name = "workflow/workflow_data_create.html"

    def _workflow(self):
        pk = self.kwargs["workflow_pk"]
        return Workflow.objects.get(pk=pk)

    def form_valid(self, form):
        workflow = self._workflow()
        self.report_schedule = ReportSpecification.objects.schedule(
            WorkflowData.REPORT_SLUG,
            self.request.user,
            parameters={"process_key": workflow.process_key},
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(workflow=self._workflow()))
        return context

    def get_success_url(self):
        return reverse("workflow.data.report", args=[self.report_schedule.pk])


class WorkflowDataListView(LoginRequiredMixin, BaseMixin, ListView):
    """Download data from the workflow."""

    paginate_by = 20
    template_name = "workflow/workflow_data_list.html"

    def _form(self):
        kwargs = {"initial": self.get_initial()}
        qs = Workflow.objects.security_history(self.request.user)
        return HistorySecurityForm(query_set=qs, report=True, **kwargs)

    def _workflow(self):
        result = None
        workflow_pk = self._workflow_pk()
        if workflow_pk:
            result = Workflow.objects.get(pk=workflow_pk)
        return result

    def _workflow_data(self, workflow):
        result = []
        qs = WorkflowData.objects.data_download(workflow.process_key)
        mappings = workflow.history_summary_columns()
        keys = [x.variable_name for x in mappings]
        for row in qs:
            column_1 = column_2 = None
            for key in keys:
                value = row.data.get(key)
                if value:
                    if not column_1:
                        column_1 = value
                    else:
                        if not column_2:
                            column_2 = value
                            break
            result.append(
                dict(
                    column_1=column_1,
                    column_2=column_2,
                    created=row.created,
                    reported=row.reported,
                )
            )
        return result

    def _workflow_pk(self):
        result = None
        value = self.request.GET.get("workflow")
        if value:
            result = int(value)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pending = False
        workflow = self._workflow()
        if workflow:
            qs = WorkflowData.objects.pending_report(workflow.process_key)
            pending = qs.count()
        context.update(
            dict(form=self._form(), pending=pending, workflow=workflow)
        )
        return context

    def get_initial(self):
        return {"workflow": self._workflow_pk()}

    def get_queryset(self):
        result = []
        workflow = self._workflow()
        if workflow:
            if not workflow.has_security_history(self.request.user):
                raise PermissionDenied(
                    "User '{}' does not have permission "
                    "to view data (task history) for '{}' "
                    "processes".format(self.request.user, workflow.process_key)
                )
            result = self._workflow_data(workflow)
        return result


class WorkflowDataReportView(
    LoginRequiredMixin, ReportCsvMixin, BaseMixin, DetailView
):
    template_name = "workflow/workflow_data_csv_report.html"

    def format_data(self, titles, data):
        return titles, data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        process_key = self.object.parameters.get("process_key")
        workflow = Workflow.objects.get(process_key=process_key)
        context.update(dict(can_update_report=False, workflow=workflow))
        return context


class WorkflowDataReportListView(LoginRequiredMixin, BaseMixin, ListView):
    """List of completed workflow data reports."""

    paginate_by = 20
    template_name = "workflow/workflow_data_report_list.html"

    def _process_key(self):
        return self.kwargs["process_key"]

    def _workflow(self):
        process_key = self._process_key()
        try:
            return Workflow.objects.get(process_key=process_key)
        except Workflow.DoesNotExist:
            raise WorkflowError(
                "Workflow '{}' does not exist".format(process_key)
            )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(workflow=self._workflow()))
        return context

    def get_queryset(self):
        process_key = self._process_key()
        return completed_reports(process_key)


class WorkflowDeployView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, FormView
):
    """Deploy a workflow.

    PJK TODO Is this an update or create view?  ``ProcessDefinitionForm`` is a
    model form.  The documentation doesn't appear to have any reference to
    ``self.object``:
    https://ccbv.co.uk/projects/Django/1.8/django.views.generic.edit/FormView/

    PJK TODO, Some duplicate code in
    ``flow/management/commands/demo_workflows.py``

    """

    template_name = "workflow/workflow_deploy.html"
    form_class = ProcessDefinitionForm
    model = ProcessDefinition

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            # slug and version are unique together - we don't want to break
            # new deployments because of a previous failed deployment
            self.object.version = (
                ProcessDefinition.objects.default_key_version_number()
            )
            form.save()
            try:
                message = ProcessDefinition.objects.deploy(
                    self.object, self.request.user
                )
                messages.success(self.request, message)
            except ActivitiError as e:
                logger.exception(e)
                messages.error(self.request, str(e))
        return HttpResponseRedirect(reverse("workflow.deployment.list"))


class WorkflowFormView(FormView):
    def _activiti(self):
        return Activiti()

    def _form_variables(self, task_id=None, process_id=None):
        if not task_id and not process_id:
            raise ActivitiError(
                "Cannot get form data as neither a task id or a process id "
                "has been supplied"
            )
        return self._activiti().form_variable_info(task_id, process_id)

    def _get_workflow_form_data(self, form, task_id):
        """Retrieve the Activiti variables from the Django form.

        - If the ``field_id`` is in ``RESERVED_VARIABLE_NAMES``, then add to the
          variables without updating the value.
        - Ignore readonly variables (not ``writable``).

        """
        logger.debug(
            "'_get_workflow_form_data' {} for {}".format(
                task_id, self.request.user.username
            )
        )
        task = self._activiti().task_status(task_id)
        workflow_fields = self._form_variables(task_id)
        variables = OrderedDict()
        for field_id, variable in workflow_fields.items():
            if field_id in RESERVED_VARIABLE_NAMES:
                variables[field_id] = variable
            elif variable.writable:
                field_value = form.cleaned_data.get(field_id)
                if field_value is None and field_id in RESERVED_VARIABLE_NAMES:
                    field_value = variable.name
                if variable.data_type is date:
                    variable.value = convert_date_iso(field_value)
                else:
                    mapping = self._mapping(task.process_key, field_id)
                    if mapping and mapping.is_plugin():
                        variable.value = MappingPlugin().field_value(
                            field_id, field_value, mapping
                        )
                    else:
                        variable.value = field_value
                variables[field_id] = variable
        return variables

    def _mapping(self, process_key, variable_name):
        mapping = None
        try:
            mapping = Mapping.objects.get(
                workflow__process_key=process_key,
                variable_name=variable_name,
                deleted=False,
            )
        except Mapping.DoesNotExist:
            pass
        return mapping


class TaskHistoryView(LoginRequiredMixin, BaseMixin, TemplateView):
    """Task history - summary view.

    This view is used for two URLs:

    1. /task/<task_id>/history/'
    2. /task/<task_id>/history/error/'

    Both views are virtually the same.  The only difference is we display the
    ``error`` view when we are redirecting here because of an error.

    """

    template_name = "workflow/task_history.html"

    def _group(self, group_pk):
        try:
            return Group.objects.get(pk=group_pk)
        except Group.DoesNotExist:
            return None

    def _task_id(self):
        return self.kwargs["task_id"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        assignee = group = None
        is_error = "error" in self.request.path
        task_id = self._task_id()
        activiti = Activiti()
        task = activiti.task_status_historic(task_id)
        group_id = activiti.task_group_id(task_id)
        if group_id:
            group = self._group(group_id)
        if task.assignee:
            assignee = get_user_model().objects.get(pk=task.assignee)
        context.update(
            dict(
                assignee=assignee,
                group=group,
                group_id=group_id,
                is_error=is_error,
                task=task,
                user=self.request.user,
            )
        )
        return context


class TaskFormView(WorkflowFormView):
    """Inherit from this view to process a workflow task.

    Create a form e.g::

      from workflow.forms import WorkflowFormMixin
      class MyTaskForm(WorkflowFormMixin):
          pass

    Create a view e.g::

      from workflow.views import TaskFormView
      class MyTaskFormView(LoginRequiredMixin, BaseMixin, TaskFormView):
          template_name = 'flow/my.html'
          form_class = MyTaskForm
          def get_success_url(self):
              return reverse('project.dash')

    Create a URL (which must include the ``task_id`` key) e.g::

      url(regex=r'^my/(?P<task_id>\d+)/$',
          view=MyTaskFormView.as_view(),
          name='flow.my'
          ),

    """

    def _copy_to_document_management(self, transaction):
        if hasattr(self, "copy_to_document_management"):
            self.copy_to_document_management(transaction)
        else:
            raise WorkflowError(
                "The workflow has asked to copy documents to the "
                "document management system, but '{}' view does "
                "not have a 'copy_to_document_management' "
                "method".format(self.__class__.__name__)
            )

    def _create_data_document(self, variables):
        """Create a data document.

        A data document is a simple PDF document with a title and a table
        containing a list of the variables.

        .. note:: The task form view must implement the ``write_data_document``
                  method.

        Copied to ``_create_pdf_for_variables`` (``work.api.WorkDownloadView``).

        """
        data = []
        title = ""
        for field_id, variable in variables.items():
            if variable.name == "data_document":
                title = variable.value
            if field_id in HIDDEN_VARIABLE_NAMES:
                pass
            elif field_id in RESERVED_VARIABLE_NAMES:
                pass
            else:
                data.append([field_id, variable.value])
        if data:
            with io.BytesIO() as buff:
                report = TableReport()
                report.create(buff, data, title)
                pdf = buff.getvalue()
            if hasattr(self, "write_data_document"):
                file_name = "{}.pdf".format(title.replace(" ", "-"))
                self.write_data_document(title, pdf, file_name)
            else:
                raise WorkflowError(
                    "The workflow has asked to create a PDF file by using "
                    "the 'data_document' variable, but the '{}' view does "
                    "not have a 'write_data_document' "
                    "method".format(self.__class__.__name__)
                )

    def _is_copy_to_document_management(self, variables):
        """Check the form variables - should we copy documents to Alfresco?"""
        return "copy_to_document_management" in variables

    def _is_data_document(self, variables):
        "Check the form variables to see if we should create a document." ""
        return "data_document" in variables

    def _is_data_download(self, variables):
        "Check the form variables to see if we should download the data." ""
        return "data_download" in variables

    def _redirect_to_completed_task(self, error=None):
        task_id = self._task_id()
        if error is None:
            url_name = "workflow.task.history"
        else:
            url_name = "workflow.task.history.error"
        url = reverse(url_name, args=[task_id])
        return HttpResponseRedirect(url)

    def _task(self):
        task_id = self._task_id()
        return self._activiti().task_status(task_id)

    def _task_found(self):
        result = True
        try:
            task = self._task()
            if not self.user_has_perm(task):
                result = False
        except ActivitiError as e:
            if e.status_code == requests.codes.not_found:
                result = False
        return result

    def _task_id(self):
        return self.kwargs["task_id"]

    def form_valid(self, form):
        """The form is valid, so complete the task.

        If the task does not have an assignee, it must be a group task, so
        claim the task for the user first, so the history reports include the
        task.

        """
        task = self._task()
        variables = self._get_workflow_form_data(form, task_id=task.pk)
        with transaction.atomic():
            variables = self.form_valid_workflow(form, variables)
            plugin = MappingPlugin()
            variables.update(
                plugin.auto_generate_variables(task.process_key, variables)
            )
            activiti = self._activiti()
            # do this before activiti calls so transaction will rollback
            if self._is_copy_to_document_management(variables):
                self._copy_to_document_management(transaction)
            if self._is_data_document(variables):
                self._create_data_document(variables)
            if self._is_data_download(variables):
                WorkflowData.objects.create_workflow_data(
                    task.process_id, variables
                )
            additional_variables = {}
            if not task.assignee:
                activiti.task_claim(task.pk, self.request.user)
                additional_variables.update(
                    {
                        GROUP_TASK_CLAIMED_BY_USER_PK: Variable(
                            value=self.request.user.pk, data_type=int
                        ),
                    }
                )
            activiti.task_complete(
                task.pk, variables, additional_variables=additional_variables
            )
        return HttpResponseRedirect(self.get_success_url())

    def form_valid_workflow(self, form, variables):
        """Override this method if you want to perform a particular operation.

        Also useful if you want to start another workflow or save some data.

        .. note:: Must return ``variables``.

        """
        return variables

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        kwargs["task_id"] = self._task_id()
        return kwargs

    def get(self, request, *args, **kwargs):
        """Copy of ``get`` from ``FormView``."""
        if self._task_found():
            # standard get
            try:
                return self.render_to_response(self.get_context_data())
            except ActivitiError as e:
                logger.exception(e)
                return self._redirect_to_completed_task(error=True)
        else:
            return self._redirect_to_completed_task()

    def post(self, request, *args, **kwargs):
        """Copy of ``post`` from ``FormView``."""
        if self._task_found():
            # standard post
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            return self._redirect_to_completed_task()
