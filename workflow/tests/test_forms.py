# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal

from workflow.forms import round_string_as_decimal


@pytest.mark.django_db
def test_round_string_as_decimal():
    assert Decimal("12.34") == round_string_as_decimal("12.34")


@pytest.mark.django_db
def test_round_string_as_decimal_rounding():
    assert Decimal("1234.57") == round_string_as_decimal("1234.56789")


@pytest.mark.django_db
def test_round_string_as_decimal_none():
    assert Decimal() == round_string_as_decimal(None)
