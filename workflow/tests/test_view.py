# -*- encoding: utf-8 -*-
import os
import pytest

from datetime import date
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory
from workflow.activiti import ActivitiError, Definition, Variable
from workflow.models import Mapping, MappingPlugin, Workflow, WorkflowError
from workflow.tests.factories import MappingFactory, WorkflowFactory
from workflow.views import check_security_history_perm


def _mock_deploy_process_definition():
    return [
        201,
        {
            "deploymentTime": "2016-05-27T16:32:07.005+01:00",
            "category": None,
            "tenantId": "",
            "name": "vacationRequest2Manager.bpmn20.xml",
            "id": "296",
            "url": (
                "http://localhost:8080/activiti-rest/service/repository/"
                "deployments/296"
            ),
        },
    ]


def _mock_form_variable_info():
    return {
        "number_of_days": Variable(
            data_type=int,
            name="Number of days",
            required=True,
            value=None,
            writable=True,
        ),
        "start_date": Variable(
            data_type=date,
            name="First day of holiday (dd-MM-yyy)",
            required=True,
            value=None,
            writable=True,
        ),
        "delete_workflow": Variable(
            data_type=bool, name="Motivation", value=None, writable=True
        ),
        "manager": Variable(
            data_type=str, name="Manager", value=None, writable=True
        ),
    }


def _mock_process_status(process_key):
    return {
        "data": [
            {
                "activityId": None,
                "businessKey": None,
                "completed": False,
                "ended": False,
                "id": "61",
                "processDefinitionId": "{}:1:43".format(process_key),
                "processDefinitionName": process_key,
                "processDefinitionUrl": "http://localhost:8080/b/timeOffRequest:1:43",
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/a/61",
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    }
                ],
            }
        ]
    }


def _mock_task_status(assignee=None):
    return {
        "assignee": assignee,
        "category": "blue",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "description": "You are required...",
        "dueDate": None,
        "formKey": None,
        "id": "336",
        "name": "Requested...",
        "processDefinitionId": "timeOffRequest:1:303",
        "processInstanceId": "32",
        "variables": [],
    }


def _mock_task_status_historic():
    return {
        "id": "79",
        "assignee": None,
        "category": None,
        "deleteReason": None,
        "startTime": "2016-05-22T22:22:22.248+01:00",
        "dueDate": "2016-05-28T08:08:08.248+01:00",
        "claimTime": "2016-05-27T16:52:42.248+01:00",
        "endTime": "2016-05-27T16:52:42.248+01:00",
        "name": "Familiarised with document Oil COSSH Assessment",
        "processInstanceId": "260",
    }


def _mock_task_status_process():
    return {
        "data": [
            {
                "assignee": None,
                "category": None,
                "createTime": "2016-05-27T16:52:42.248+01:00",
                "delegationState": None,
                "description": "You are required you to be familiar with the document Oil COSSH Assessment (29) to safely carry out your work. admin",
                "dueDate": None,
                "executionId": "333",
                "executionUrl": "http://localhost:8080/activiti-rest/service/runtime/executions/333",
                "formKey": None,
                "id": "336",
                "name": "Familiarised with document Oil COSSH Assessment",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "documentFamiliarisation:1:303",
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:303",
                "processInstanceId": "321",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/runtime/process-instances/321",
                "suspended": False,
                "taskDefinitionKey": "familiarisationConfirmed",
                "tenantId": "",
                "url": "http://localhost:8080/activiti-rest/service/runtime/tasks/336",
                "variables": [],
            }
        ]
    }


def _mock_task_identity_list(group_pk=None):
    if group_pk is None:
        group_pk = "444"
    return [
        {
            "url": (
                "http://localhost:8080/activiti-rest/service/runtime/tasks"
                "/2843/identitylinks/groups/1/candidate"
            ),
            "user": None,
            "group": group_pk,
            "type": "candidate",
        }
    ]


@pytest.mark.django_db
def test_check_security_history_perm():
    user = UserFactory(username="patrick")
    group = GroupFactory()
    user.groups.add(group)
    workflow = WorkflowFactory(process_key="holiday")
    workflow.security_history.add(group)
    check_security_history_perm(user, "holiday")


@pytest.mark.django_db
def test_check_security_history_perm_no_permission():
    user = UserFactory(username="patrick")
    WorkflowFactory(process_key="holiday")
    with pytest.raises(PermissionDenied) as e:
        check_security_history_perm(user, "holiday")
    assert (
        "User 'patrick' does not have permission to "
        "view task history for 'holiday' processes"
    ) in str(e.value)


@pytest.mark.django_db
def test_check_security_history_perm_no_workflow():
    user = UserFactory(username="patrick")
    with pytest.raises(WorkflowError) as e:
        check_security_history_perm(user, "holiday")
    assert (
        "Check permissions for viewing history: cannot "
        "find workflow with 'process_key' of 'holiday'"
    ) in str(e.value)


@pytest.mark.django_db
def test_check_security_history_perm_superuser():
    user = UserFactory(username="patrick", is_staff=True, is_superuser=True)
    WorkflowFactory(process_key="holiday")
    check_security_history_perm(user, "holiday")


@pytest.mark.django_db
def test_check_security_history_perm_superuser_no_workflow():
    user = UserFactory(username="patrick", is_staff=True, is_superuser=True)
    with pytest.raises(WorkflowError) as e:
        check_security_history_perm(user, "holiday")
    assert (
        "Check permissions for viewing history: cannot "
        "find workflow with 'process_key' of 'holiday'"
    ) in str(e.value)


@pytest.mark.parametrize("process_key,expect", [("", None), ("apple", "apple")])
@pytest.mark.django_db
def test_process_instance_detail(client, process_key, expect):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status_process"
    ) as mock_task_status:
        # mock return values
        mock_process_status.return_value = _mock_process_status(process_key)
        mock_task_status.return_value = _mock_task_status_process()
        # setup
        WorkflowFactory(process_key=process_key)
        # test
        response = client.get(
            reverse("workflow.process.instance.detail", args=[99])
        )
        assert HTTPStatus.OK == response.status_code
    assert "process_id" in response.context
    assert 99 == response.context["process_id"]
    assert "workflow" in response.context
    workflow = response.context["workflow"]
    if expect:
        assert expect == workflow.process_key
    else:
        assert response.context["workflow"] is None


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url_name,is_error",
    [("workflow.task.history", False), ("workflow.task.history.error", True)],
)
def test_task_history(client, url_name, is_error):
    group = GroupFactory()
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status_historic"
    ) as mock_task_status_historic, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        # mock return values
        mock_form_variable_info.return_value = []
        mock_identity_link.return_value = _mock_task_identity_list(group.pk)
        mock_task_status_historic.return_value = _mock_task_status_historic()
        # login
        user = UserFactory()
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse(url_name, args=[99])
        response = client.get(url)
        assert HTTPStatus.OK == response.status_code
        assert "is_error" in response.context
        assert response.context["is_error"] is is_error
        assert "group" in response.context
        assert group == response.context["group"]


@pytest.mark.django_db
def test_task_history_no_group(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status_historic"
    ) as mock_task_status_historic, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        # mock return values
        mock_form_variable_info.return_value = []
        mock_identity_link.return_value = _mock_task_identity_list()
        mock_task_status_historic.return_value = _mock_task_status_historic()
        # login
        user = UserFactory()
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse("workflow.task.history", args=[99])
        response = client.get(url)
        assert HTTPStatus.OK == response.status_code
        assert "group" in response.context
        assert response.context["group"] is None


@pytest.mark.django_db
def test_task_history_identity_links_404(client):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status_historic"
    ) as mock_task_status_historic, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        # mock return values
        mock_form_variable_info.return_value = []
        mock_identity_link.side_effect = ActivitiError("Could not find task..")
        mock_task_status_historic.return_value = _mock_task_status_historic()
        # login
        user = UserFactory()
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse("workflow.task.history", args=[99])
        response = client.get(url)
        assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
@pytest.mark.parametrize(
    "create_workflow_model,url_redirect,expect",
    [
        (False, "", reverse("project.tasks")),
        (True, "", reverse("project.tasks")),
        (True, "https://www.pkimber.net/", "https://www.pkimber.net/"),
    ],
)
def test_task_process_instance_delete(
    client, create_workflow_model, url_redirect, expect
):
    group = GroupFactory()
    user = UserFactory()
    # give the user permission to delete the process
    user.groups.add(group)
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        if create_workflow_model:
            WorkflowFactory(
                process_key="timeOffRequest", url_redirect=url_redirect
            )
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_identity_link.return_value = _mock_task_identity_list(group.pk)
        mock_process_status.return_value = _mock_process_status(
            "timeOffRequest"
        )
        mock_task_status.return_value = _mock_task_status()
        # login
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse("workflow.task.process.instance.delete", args=[123])
        response = client.post(url)
        assert HTTPStatus.FOUND == response.status_code, response.context[
            "form"
        ].errors
        assert expect == response.url


@pytest.mark.django_db
def test_workflow_deploy(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Mapping.objects.count()
    assert 0 == Workflow.objects.count()
    bpmn_file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "data",
        "timeOffRequestV1.bpmn20.xml",
    )
    with mock.patch(
        "workflow.activiti.Activiti.deploy_process_definition"
    ) as mock_deploy_proc_def, mock.patch(
        "workflow.activiti.Activiti.process_definition_for_deployment"
    ) as mock_process_definition_for_deployment:
        # mock return values
        mock_deploy_proc_def.return_value = _mock_deploy_process_definition()
        data = Definition(
            process_key="grass", name="Fruit", version=3, workflow_id="apple"
        )
        mock_process_definition_for_deployment.return_value = data
        with open(bpmn_file_name) as bpmn_file:
            response = client.post(
                reverse("workflow.deploy"), {"process_definition": bpmn_file}
            )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("workflow.deployment.list") == response.url
    assert 1 == Workflow.objects.count()
    workflow = Workflow.objects.first()
    assert "grass" == workflow.process_key
    result = [
        (x.variable_name, x.mapping_type, x.deleted)
        for x in Mapping.objects.filter(workflow=workflow)
    ]
    expect = [
        ("orange_pk", MappingPlugin.UNDEFINED, False),
        ("request_comment", MappingPlugin.UNDEFINED, False),
        ("to_date", MappingPlugin.IMPORT_DATE, False),
        ("user_email", MappingPlugin.AUTO_GENERATE, False),
        ("user_full_name", MappingPlugin.AUTO_GENERATE, False),
        ("user_pk", MappingPlugin.AUTO_GENERATE, False),
    ]
    assert expect == result


@pytest.mark.django_db
def test_workflow_deploy_auto_generate_variables(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == Mapping.objects.count()
    workflow = WorkflowFactory(process_key="grass")
    MappingFactory(workflow=workflow, variable_name="does_not_exist")
    MappingFactory(workflow=workflow, variable_name="orange_pk")
    MappingFactory(
        workflow=workflow, variable_name="orange_email", deleted=True
    )
    assert 1 == Workflow.objects.count()
    bpmn_file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "data",
        "timeOffRequestV1.bpmn20.xml",
    )
    with mock.patch(
        "workflow.activiti.Activiti.deploy_process_definition"
    ) as mock_deploy_proc_def, mock.patch(
        "workflow.activiti.Activiti.process_definition_for_deployment"
    ) as mock_process_definition_for_deployment:
        # mock return values
        mock_deploy_proc_def.return_value = _mock_deploy_process_definition()
        data = Definition(
            process_key="grass", name="Fruit", version=3, workflow_id="apple"
        )
        mock_process_definition_for_deployment.return_value = data
        with open(bpmn_file_name) as bpmn_file:
            response = client.post(
                reverse("workflow.deploy"), {"process_definition": bpmn_file}
            )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("workflow.deployment.list") == response.url
    assert 1 == Workflow.objects.count()
    workflow = Workflow.objects.first()
    assert "grass" == workflow.process_key
    result = [
        (x.variable_name, x.mapping_type, x.deleted)
        for x in Mapping.objects.filter(workflow=workflow)
    ]
    expect = [
        ("does_not_exist", MappingPlugin.USER, True),
        ("orange_email", MappingPlugin.AUTO_GENERATE, False),
        ("orange_full_name", MappingPlugin.AUTO_GENERATE, False),
        ("orange_pk", MappingPlugin.USER, False),
        ("request_comment", MappingPlugin.UNDEFINED, False),
        ("to_date", MappingPlugin.IMPORT_DATE, False),
        ("user_email", MappingPlugin.AUTO_GENERATE, False),
        ("user_full_name", MappingPlugin.AUTO_GENERATE, False),
        ("user_pk", MappingPlugin.AUTO_GENERATE, False),
    ]
    assert expect == result
