# -*- encoding: utf-8 -*-
import pytest

from decimal import Decimal
from django.urls import reverse

from login.tests.factories import GroupFactory, UserFactory
from workflow.activiti import Activiti, Variable
from workflow.models import (
    get_contact_model,
    Map,
    Mapping,
    MappingPlugin,
    WorkflowError,
)
from workflow.tests.factories import MappingFactory, WorkflowFactory


def _contact():
    user = UserFactory()
    contact_model = get_contact_model()
    contact = contact_model(user=user)
    contact.save()
    return contact


@pytest.mark.django_db
def test_for_workflow():
    """Get the mappings for a workflow."""
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="a")
    mapping = MappingFactory(workflow=workflow, variable_name="b")
    mapping.set_deleted(UserFactory())
    MappingFactory(variable_name="c")
    MappingFactory(workflow=workflow, variable_name="d")
    assert ["a", "d"] == [
        x.variable_name for x in Mapping.objects.for_workflow(workflow)
    ]


@pytest.mark.parametrize(
    "mapping_type,expected",
    [
        (MappingPlugin.DECIMAL, True),
        (MappingPlugin.ENUM, False),
        (MappingPlugin.GROUP, False),
        (MappingPlugin.IMPORT, False),
        (MappingPlugin.IMPORT_BOOLEAN, False),
        (MappingPlugin.IMPORT_DATE, False),
        (MappingPlugin.IMPORT_DECIMAL, True),
        (MappingPlugin.IMPORT_INTEGER, False),
        (MappingPlugin.INTEGER, False),
        (MappingPlugin.UNDEFINED, False),
        (MappingPlugin.USER, False),
    ],
)
@pytest.mark.django_db
def test_is_decimal(mapping_type, expected):
    mapping = MappingFactory(mapping_type=mapping_type)
    assert mapping.is_decimal() is expected


@pytest.mark.parametrize(
    "mapping_type,expected",
    [
        (MappingPlugin.DECIMAL, True),
        (MappingPlugin.ENUM, False),
        (MappingPlugin.GROUP, True),
        (MappingPlugin.IMPORT, True),
        (MappingPlugin.IMPORT_BOOLEAN, False),
        (MappingPlugin.IMPORT_DATE, False),
        (MappingPlugin.IMPORT_DECIMAL, True),
        (MappingPlugin.IMPORT_INTEGER, True),
        (MappingPlugin.INTEGER, True),
        (MappingPlugin.UNDEFINED, True),
        (MappingPlugin.USER, True),
    ],
)
@pytest.mark.django_db
def test_can_edit_mapping_type(mapping_type, expected):
    """Does the mapping type require another selection?"""
    mapping = MappingFactory(mapping_type=mapping_type)
    assert mapping.can_edit_mapping_type() is expected


@pytest.mark.parametrize(
    "mapping_type,expected",
    [
        (MappingPlugin.DECIMAL, True),
        (MappingPlugin.ENUM, True),
        (MappingPlugin.GROUP, True),
        (MappingPlugin.IMPORT, True),
        (MappingPlugin.IMPORT_BOOLEAN, True),
        (MappingPlugin.IMPORT_DATE, True),
        (MappingPlugin.IMPORT_DECIMAL, True),
        (MappingPlugin.IMPORT_INTEGER, True),
        (MappingPlugin.INTEGER, True),
        (MappingPlugin.UNDEFINED, False),
        (MappingPlugin.USER, True),
    ],
)
@pytest.mark.django_db
def test_can_map(mapping_type, expected):
    """Is this a valid mapping type?"""
    mapping = MappingFactory(mapping_type=mapping_type)
    assert mapping.can_map() is expected


@pytest.mark.parametrize(
    "mapping_type,value,expected",
    [
        (MappingPlugin.DECIMAL, "12.345", "12.34"),
        (MappingPlugin.DECIMAL, "12.AbC", None),
        (MappingPlugin.DECIMAL, None, None),
        (MappingPlugin.GROUP, "", ""),
        (MappingPlugin.GROUP, "345", "HR"),
        (MappingPlugin.GROUP, "Personnel", "Personnel"),
        (MappingPlugin.GROUP, None, ""),
        (MappingPlugin.IMPORT, "ABC", "ABC"),
        (MappingPlugin.IMPORT_BOOLEAN, "False", False),
        (MappingPlugin.IMPORT_BOOLEAN, "True", True),
        (MappingPlugin.IMPORT_BOOLEAN, False, False),
        (MappingPlugin.IMPORT_BOOLEAN, True, True),
        (MappingPlugin.IMPORT_DATE, "2017-06-21", "21/06/2017"),
        (MappingPlugin.IMPORT_DECIMAL, "56.7", "56.70"),
        (MappingPlugin.IMPORT_DECIMAL, "XyZ.7", None),
        (MappingPlugin.IMPORT_INTEGER, "78", 78),
        (MappingPlugin.IMPORT_INTEGER, "XyZ78", None),
        (MappingPlugin.USER, "", ""),
        (MappingPlugin.USER, "346", "Pat K"),
        (MappingPlugin.USER, "Patrick", "Patrick"),
        (MappingPlugin.USER, None, ""),
    ],
)
@pytest.mark.django_db
def test_format_activiti_data(mapping_type, value, expected):
    GroupFactory(pk=345, name="HR")
    UserFactory(pk=346, first_name="Pat", last_name="K")
    UserFactory(pk=347, first_name="Andy", last_name="J")
    mapping = MappingFactory(mapping_type=mapping_type)
    assert expected == mapping.format_activiti_data(
        Variable(data_type=None, value=value)
    )


@pytest.mark.django_db
def test_format_activiti_data_with_sep():
    GroupFactory(pk=345, name="HR")
    mapping = MappingFactory(mapping_type=MappingPlugin.GROUP)
    assert "HR" == mapping.format_activiti_data(
        Variable(data_type=None, value="345"), sep="|"
    )


@pytest.mark.parametrize(
    "mapping_type,variable_name,expected",
    [
        (MappingPlugin.GROUP, "science", ["science_name"]),
        (MappingPlugin.GROUP, "science_pk", ["science_name"]),
        (MappingPlugin.INTEGER, "science_pk", []),
        (MappingPlugin.USER, "student", ["student_email", "student_full_name"]),
        (
            MappingPlugin.USER,
            "student_pk",
            ["student_email", "student_full_name"],
        ),
    ],
)
@pytest.mark.django_db
def test_generate_extra(mapping_type, variable_name, expected):
    mapping = MappingFactory(
        variable_name=variable_name, mapping_type=mapping_type
    )
    assert expected == mapping.generate_extra()


@pytest.mark.parametrize(
    "mapping_type,expected",
    [
        (MappingPlugin.DECIMAL, True),
        (MappingPlugin.ENUM, False),
        (MappingPlugin.GROUP, True),
        (MappingPlugin.IMPORT, False),
        (MappingPlugin.IMPORT_BOOLEAN, False),
        (MappingPlugin.IMPORT_DATE, False),
        (MappingPlugin.IMPORT_DECIMAL, False),
        (MappingPlugin.IMPORT_INTEGER, False),
        (MappingPlugin.INTEGER, True),
        (MappingPlugin.UNDEFINED, False),
        (MappingPlugin.USER, True),
    ],
)
@pytest.mark.django_db
def test_is_map_extra(mapping_type, expected):
    """Does the mapping type require another selection?"""
    mapping = MappingFactory(mapping_type=mapping_type)
    assert mapping.is_map_extra() is expected


@pytest.mark.django_db
def test_init_mapping():
    workflow = WorkflowFactory()
    mapping = Mapping.objects.init_mapping(
        workflow, "apple", None, MappingPlugin.USER
    )
    assert MappingPlugin.USER == mapping.mapping_type
    assert "" == mapping.flowable_data_type
    # data type should not change (because both types are editable)
    mapping = Mapping.objects.init_mapping(
        workflow, "apple", Activiti.STRING, MappingPlugin.GROUP
    )
    assert MappingPlugin.USER == mapping.mapping_type
    assert Activiti.STRING == mapping.flowable_data_type
    # https://www.kbsoftware.co.uk/crm/ticket/5104/
    assert Mapping.CSS_TWO_THIRDS == mapping.css_class
    assert mapping.data_download is False
    assert mapping.history_summary is True
    assert mapping.view_task_data is True


@pytest.mark.django_db
def test_init_mapping_default():
    workflow = WorkflowFactory()
    mapping = Mapping.objects.init_mapping(workflow, "apple")
    assert MappingPlugin.UNDEFINED == mapping.mapping_type
    assert "" == mapping.flowable_data_type


@pytest.mark.parametrize(
    "initial,new,expect",
    [
        (
            MappingPlugin.DECIMAL,
            MappingPlugin.IMPORT_BOOLEAN,
            MappingPlugin.IMPORT_BOOLEAN,
        ),
        (
            MappingPlugin.DECIMAL,
            MappingPlugin.IMPORT_DATE,
            MappingPlugin.IMPORT_DATE,
        ),
        (
            MappingPlugin.DECIMAL,
            MappingPlugin.IMPORT_DECIMAL,
            MappingPlugin.DECIMAL,
        ),
        (MappingPlugin.DECIMAL, MappingPlugin.USER, MappingPlugin.DECIMAL),
        (MappingPlugin.DECIMAL, None, MappingPlugin.DECIMAL),
        (
            MappingPlugin.IMPORT_DATE,
            MappingPlugin.DECIMAL,
            MappingPlugin.DECIMAL,
        ),
        (
            MappingPlugin.IMPORT_DATE,
            MappingPlugin.ENUM,
            MappingPlugin.IMPORT_DATE,
        ),
        (MappingPlugin.IMPORT_BOOLEAN, None, MappingPlugin.IMPORT_BOOLEAN),
        (MappingPlugin.IMPORT_DATE, None, MappingPlugin.IMPORT_DATE),
    ],
)
@pytest.mark.django_db
def test_init_mapping_update(initial, new, expect):
    """Test update for when the type switches between editable and not."""
    workflow = WorkflowFactory()
    Mapping.objects.init_mapping(workflow, "apple", None, initial)
    mapping = Mapping.objects.init_mapping(workflow, "apple", None, new)
    assert expect == mapping.mapping_type


@pytest.mark.django_db
def test_factory():
    MappingFactory()


@pytest.mark.django_db
def test_map_as_str():
    mapping = MappingFactory(mapping_type=MappingPlugin.UNDEFINED)
    assert "Undefined" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_decimal():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.DECIMAL, decimal_number=30.03
    )
    assert "30.03" == mapping.map_as_str()


@pytest.mark.parametrize(
    "mapping_type",
    [
        MappingPlugin.ENUM,
        MappingPlugin.IMPORT,
        MappingPlugin.IMPORT_BOOLEAN,
        MappingPlugin.IMPORT_DATE,
        MappingPlugin.IMPORT_DECIMAL,
        MappingPlugin.IMPORT_INTEGER,
    ],
)
@pytest.mark.django_db
def test_map_as_str_empty(mapping_type):
    mapping = MappingFactory(mapping_type=mapping_type)
    assert "" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_decimal_no_decimal():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.DECIMAL, decimal_number=None
    )
    assert "Undefined" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_group():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.GROUP, group=GroupFactory(name="Orange")
    )
    assert "Orange" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_group_no_group():
    mapping = MappingFactory(mapping_type=MappingPlugin.GROUP, group=None)
    assert "Undefined" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_integer():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.INTEGER, integer_number=3000
    )
    assert "3000" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_integer_no_integer():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.INTEGER, integer_number=None
    )
    assert "Undefined" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_user():
    mapping = MappingFactory(
        mapping_type=MappingPlugin.USER,
        user=UserFactory(first_name="Olli", last_name="Over"),
    )
    assert "Olli Over" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_as_str_user_no_user():
    mapping = MappingFactory(mapping_type=MappingPlugin.USER, user=None)
    assert "Undefined" == mapping.map_as_str()


@pytest.mark.django_db
def test_map_for_process_key():
    """Get the map for a workflow process key."""
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="a")
    mapping = MappingFactory(workflow=workflow, variable_name="b")
    mapping.set_deleted(UserFactory())
    MappingFactory(variable_name="c")
    MappingFactory(workflow=workflow, variable_name="d")
    assert {
        "a": Map(variable_name="a", mapping_type=MappingPlugin.USER),
        "d": Map(variable_name="d", mapping_type=MappingPlugin.USER),
    } == Mapping.objects.map_for_process_key(workflow)


@pytest.mark.django_db
def test_map_url_decimal():
    mapping = MappingFactory(mapping_type=MappingPlugin.DECIMAL)
    expect = reverse("workflow.mapping.update.map.decimal", args=[mapping.pk])
    assert expect == mapping.map_url()


@pytest.mark.django_db
def test_map_url_group():
    mapping = MappingFactory(mapping_type=MappingPlugin.GROUP)
    expect = reverse("workflow.mapping.update.map.group", args=[mapping.pk])
    assert expect == mapping.map_url()


@pytest.mark.parametrize(
    "mapping_type",
    [
        MappingPlugin.ENUM,
        MappingPlugin.IMPORT,
        MappingPlugin.IMPORT_BOOLEAN,
        MappingPlugin.IMPORT_DATE,
        MappingPlugin.IMPORT_DECIMAL,
        MappingPlugin.IMPORT_INTEGER,
    ],
)
@pytest.mark.django_db
def test_map_url_none(mapping_type):
    mapping = MappingFactory(mapping_type=mapping_type)
    assert mapping.map_url() is None


@pytest.mark.django_db
def test_map_url_integer():
    mapping = MappingFactory(mapping_type=MappingPlugin.INTEGER)
    expect = reverse("workflow.mapping.update.map.integer", args=[mapping.pk])
    assert expect == mapping.map_url()


@pytest.mark.django_db
def test_map_url_undefined():
    mapping = MappingFactory(mapping_type=MappingPlugin.UNDEFINED)
    assert mapping.map_url() is None


@pytest.mark.django_db
def test_map_url_user():
    mapping = MappingFactory(mapping_type=MappingPlugin.USER)
    expect = reverse("workflow.mapping.update.map.user", args=[mapping.pk])
    assert expect == mapping.map_url()


@pytest.mark.django_db
def test_map_value_decimal():
    mapping = MappingFactory(
        variable_name="fruit",
        mapping_type=MappingPlugin.DECIMAL,
        decimal_number=Decimal("12.34"),
    )
    result = mapping.init_workflow_variable(str, _contact())
    assert {"fruit": Variable(value=Decimal("12.34"), data_type=str)} == result
    assert isinstance(result["fruit"].value, Decimal) is True


@pytest.mark.django_db
def test_map_value_group():
    group = GroupFactory(pk=43)
    mapping = MappingFactory(
        variable_name="fruit", mapping_type=MappingPlugin.GROUP, group=group
    )
    result = mapping.init_workflow_variable(int, _contact())
    assert {"fruit": Variable(value=43, data_type=int)} == result
    assert isinstance(result["fruit"].value, int) is True


@pytest.mark.parametrize(
    "mapping_type",
    [
        MappingPlugin.ENUM,
        MappingPlugin.IMPORT,
        MappingPlugin.IMPORT_BOOLEAN,
        MappingPlugin.IMPORT_DATE,
        MappingPlugin.IMPORT_DECIMAL,
        MappingPlugin.IMPORT_INTEGER,
    ],
)
@pytest.mark.django_db
def test_map_value_none(mapping_type):
    mapping = MappingFactory(mapping_type=mapping_type)
    assert {} == mapping.init_workflow_variable(str, _contact())


@pytest.mark.django_db
def test_map_value_integer():
    mapping = MappingFactory(
        variable_name="fruit",
        mapping_type=MappingPlugin.INTEGER,
        integer_number=444,
    )
    result = mapping.init_workflow_variable(str, _contact())
    assert {"fruit": Variable(value=444, data_type=str)} == result
    assert isinstance(result["fruit"].value, int) is True


@pytest.mark.django_db
def test_map_value_no_mapping_type():
    workflow = WorkflowFactory(process_key="apple")
    mapping = MappingFactory(
        workflow=workflow,
        variable_name="orange",
        mapping_type=MappingPlugin.UNDEFINED,
    )
    with pytest.raises(WorkflowError) as e:
        mapping.init_workflow_variable(str, _contact())
    assert "Mapping 'apple:orange' has not been set-up" in str(e.value)


@pytest.mark.django_db
def test_map_value_no_mapping_group():
    workflow = WorkflowFactory(process_key="apple")
    mapping = MappingFactory(
        workflow=workflow,
        variable_name="orange",
        mapping_type=MappingPlugin.GROUP,
        group=None,
    )
    with pytest.raises(WorkflowError) as e:
        mapping.init_workflow_variable(str, _contact())
    assert "Mapping 'apple:orange' has not been set-up" in str(e.value)


@pytest.mark.django_db
def test_map_value_no_mapping_user():
    workflow = WorkflowFactory(process_key="apple")
    mapping = MappingFactory(
        workflow=workflow,
        variable_name="orange",
        mapping_type=MappingPlugin.USER,
        user=None,
    )
    with pytest.raises(WorkflowError) as e:
        mapping.init_workflow_variable(str, _contact())
    assert "Mapping 'apple:orange' has not been set-up" in str(e.value)


@pytest.mark.django_db
def test_map_value_user():
    mapping = MappingFactory(
        variable_name="fruit",
        mapping_type=MappingPlugin.USER,
        user=UserFactory(pk=44),
    )
    result = mapping.init_workflow_variable(int, _contact())
    assert {"fruit": Variable(value=44, data_type=int)} == result
    assert isinstance(result["fruit"].value, int) is True


@pytest.mark.parametrize(
    "mapping_type,expected",
    [
        (MappingPlugin.DECIMAL, "Money"),
        (MappingPlugin.ENUM, "Choice"),
        (MappingPlugin.GROUP, "Group"),
        (MappingPlugin.IMPORT, "Imported"),
        (MappingPlugin.IMPORT_BOOLEAN, "Imported True/False"),
        (MappingPlugin.IMPORT_DATE, "Imported Date"),
        (MappingPlugin.IMPORT_DECIMAL, "Imported Money"),
        (MappingPlugin.IMPORT_INTEGER, "Imported Whole Number"),
        (MappingPlugin.INTEGER, "Whole Number"),
        (MappingPlugin.UNDEFINED, "Undefined"),
        (MappingPlugin.USER, "User"),
    ],
)
@pytest.mark.django_db
def test_mapping_type_as_str(mapping_type, expected):
    """Does the mapping type require another selection?"""
    mapping = MappingFactory(mapping_type=mapping_type)
    assert expected == mapping.mapping_type_as_str()


@pytest.mark.django_db
def test_str():
    workflow = WorkflowFactory(process_key="apple")
    mapping = MappingFactory(
        workflow=workflow, variable_name="cake", mapping_type=MappingPlugin.USER
    )
    assert "'apple.cake' ['user']" == str(mapping)


@pytest.mark.django_db
def test_str_mapping_type_undefined():
    workflow = WorkflowFactory(process_key="apple")
    mapping = MappingFactory(
        workflow=workflow,
        variable_name="cake",
        mapping_type=MappingPlugin.UNDEFINED,
    )
    assert "'apple.cake' ['undefined']" == str(mapping)


@pytest.mark.parametrize(
    "expect,name",
    [("acceptRequest", "accept_request"), ("pathChoice", "path_choice")],
)
@pytest.mark.django_db
def test_variable_name_as_java(expect, name):
    mapping = MappingFactory(variable_name=name)
    assert expect == mapping.variable_name_as_java()
