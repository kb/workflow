# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from workflow.tests.factories import MappingFactory, WorkflowFactory


@pytest.mark.django_db
def test_init_app_workflow():
    call_command("init_app_workflow")


@pytest.mark.django_db
def test_update_mapping_to_snake():
    workflow = WorkflowFactory(process_key="fruit")
    mapping_1 = MappingFactory(workflow=workflow, variable_name="objectPk")
    mapping_2 = MappingFactory(workflow=workflow, variable_name="acceptRequest")
    mapping_3 = MappingFactory(workflow=workflow, variable_name="apple")
    call_command("update_mapping_to_snake")
    mapping_1.refresh_from_db()
    mapping_2.refresh_from_db()
    mapping_3.refresh_from_db()
    assert "object_pk" == mapping_1.variable_name
    assert "accept_request" == mapping_2.variable_name
    assert "apple" == mapping_3.variable_name
