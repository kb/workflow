# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory
from workflow.tests.factories import (
    MappingFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowDataFactory,
    WorkflowFactory,
)


@pytest.mark.django_db
def test_delete(client):
    user = UserFactory(is_superuser=True)
    x = WorkflowFactory()
    assert x.is_deleted is False
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.post(reverse("workflow.delete", args=[x.pk]))
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("workflow.list") == response.url
    x.refresh_from_db()
    assert x.is_deleted is True


@pytest.mark.django_db
def test_list(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    WorkflowFactory(process_key="z")
    WorkflowFactory(process_key="a")
    response = client.get(reverse("workflow.list"))
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    qs = response.context["object_list"]
    assert ["a", "z"] == [x.process_key for x in qs]


@pytest.mark.django_db
def test_update(client):
    user = UserFactory(is_superuser=True)
    obj = WorkflowFactory()
    mapping_1 = MappingFactory(workflow=obj)
    mapping_2 = MappingFactory(workflow=obj)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.update", args=[obj.pk])
    data = {
        "name": "Apple",
        "icon": "fa-camera",
        "security_type": "public",
        "history_summary_column_1": mapping_1.pk,
        "history_summary_column_2": mapping_2.pk,
        "url_redirect": "https://www.pkimber.net/",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("workflow.list") == response["Location"]
    obj.refresh_from_db()
    assert "Apple" == obj.name
    assert "fa-camera" == obj.icon
    assert "" == obj.config_description
    assert "" == obj.config_url_name
    assert "https://www.pkimber.net/" == obj.url_redirect
    assert 0 == obj.groups.count()
    mapping_1.refresh_from_db()
    assert 1 == mapping_1.history_summary_column
    mapping_2.refresh_from_db()
    assert 2 == mapping_2.history_summary_column


@pytest.mark.django_db
def test_update_config(client):
    user = UserFactory(is_superuser=True)
    obj = WorkflowFactory()
    MappingFactory(workflow=obj)
    MappingFactory(workflow=obj)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.update", args=[obj.pk])
    data = {
        "name": "Apple",
        "icon": "fa-camera",
        "security_type": "public",
        "config_description": "Extra",
        "config_url_name": "flow.workflow.update",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    obj.refresh_from_db()
    assert "Extra" == obj.config_description
    assert "flow.workflow.update" == obj.config_url_name
    assert "" == obj.url_redirect


@pytest.mark.django_db
def test_update_groups(client):
    group_1 = GroupFactory(name="g1")
    group_2 = GroupFactory(name="g2")
    group_3 = GroupFactory(name="g3")
    user = UserFactory(is_superuser=True)
    obj = WorkflowFactory()
    mapping_1 = MappingFactory(workflow=obj, history_summary_column=30)
    mapping_2 = MappingFactory(workflow=obj, history_summary_column=40)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.update", args=[obj.pk])
    data = {
        "name": "Apple",
        "icon": "fa-camera",
        "security_type": "groups",
        "groups": [group_1.pk, group_2.pk],
        "history_summary_column_2": mapping_2.pk,
        "security_delete": [group_1.pk],
        "security_history": [group_2.pk, group_3.pk],
        "security_history_my": [group_1.pk, group_3.pk],
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("workflow.list") == response["Location"]
    obj.refresh_from_db()
    assert "Apple" == obj.name
    assert "fa-camera" == obj.icon
    assert ["g1", "g2"] == [obj.name for obj in obj.groups.all()]
    assert ["g1"] == [obj.name for obj in obj.security_delete.all()]
    assert ["g2", "g3"] == sorted(
        [obj.name for obj in obj.security_history.all()]
    )
    assert ["g1", "g3"] == [obj.name for obj in obj.security_history_my.all()]
    mapping_1.refresh_from_db()
    assert mapping_1.history_summary_column is None
    mapping_2.refresh_from_db()
    assert 1 == mapping_2.history_summary_column


@pytest.mark.django_db
def test_update_no_group(client):
    user = UserFactory(is_superuser=True)
    obj = WorkflowFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.update", args=[obj.pk])
    data = {"name": "Apple", "icon": "fa-camera", "security_type": "groups"}
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {"__all__": ["Please select a security group"]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_workflow_data_list(client):
    group = GroupFactory()
    user = UserFactory()
    user.groups.add(group)
    workflow = WorkflowFactory(process_key="timeOff")
    workflow.security_history.add(group)
    sw = ScheduledWorkflowFactory(process_definition="timeOff")
    su = ScheduledWorkflowUserFactory(scheduled_workflow=sw, process_id=1)
    WorkflowDataFactory(scheduled_workflow_user=su, data="a")
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = url_with_querystring(
        reverse("workflow.data.list"), workflow=workflow.pk
    )
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
