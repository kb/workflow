# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.contrib.auth.models import User
from django.urls import reverse
from unittest import mock

from login.tests.factories import GroupFactory, TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from report.tests.factories import (
    ReportScheduleFactory,
    ReportSpecificationFactory,
)
from workflow.activiti import Variable
from workflow.models import WorkflowData
from workflow.tests.factories import (
    MappingFactory,
    ScheduledWorkflowUserFactory,
    WorkflowFactory,
)


def _mock_form_variable_info():
    return {
        "number_of_days": Variable(
            data_type=int,
            name="Number of days",
            required=True,
            value=None,
            writable=True,
        ),
        "start_date": Variable(
            data_type=date,
            name="First day of holiday (dd-MM-yyy)",
            required=True,
            value=None,
            writable=True,
        ),
        "delete_workflow": Variable(
            data_type=bool, name="Motivation", value=None, writable=True
        ),
        "manager": Variable(
            data_type=str, name="Manager", value=None, writable=True
        ),
    }


def _mock_history_task(assignee=None):
    if assignee is None:
        assignee = "1"
    return {
        "size": 5,
        "start": 0,
        "total": 5,
        "data": [
            {
                "taskDefinitionKey": "sid-05F94E95",
                "variables": [],
                "workTimeInMillis": 75,
                "processDefinitionId": "timeOffRequest:1:63",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/260",
                "name": "Accept or reject time off request from 2017-06-12 to 2017-06-15 for Maria Anders",
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/286",
                "id": "286",
                "durationInMillis": 30291,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "processInstanceId": "260",
                "endTime": "2017-06-13T11:09:59.043+01:00",
                "startTime": "2017-06-13T11:09:28.752+01:00",
                "assignee": assignee,
                "executionId": "272",
                "description": None,
                "priority": 50,
                "deleteReason": None,
                "parentTaskId": None,
                "dueDate": "2017-07-13T11:09:28.752+01:00",
                "tenantId": "",
                "owner": None,
                "formKey": "flow",
                "category": None,
                "claimTime": "2017-06-13T11:09:58.968+01:00",
            }
        ],
        "order": "desc",
        "sort": "endTime",
    }


def _mock_process_instance_list():
    return {
        "start": 0,
        "total": 1,
        "data": [
            {
                "activityId": None,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/52",
                "completed": False,
                "processDefinitionId": "changeRequest:1:43",
                "businessKey": None,
                "processDefinitionName": "changeRequest",
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/changeRequest:1:43",
                "variables": [
                    {
                        "value": 1,
                        "name": "objectPk",
                        "scope": "global",
                        "type": "integer",
                    }
                ],
                "id": "52",
                "tenantId": "",
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "ended": False,
            }
        ],
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    }
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/a/61",
                "completed": False,
                "processDefinitionName": "timeOff",
                "processDefinitionId": "timeOff:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/b/timeOffRequest:1:43",
            }
        ]
    }


def _mock_task_identity_list(group=None):
    if group is None:
        group = "444"
    return [
        {
            "url": (
                "http://localhost:8080/activiti-rest/service/runtime/tasks"
                "/2843/identitylinks/groups/1/candidate"
            ),
            "user": None,
            "group": group,
            "type": "candidate",
        }
    ]


def _mock_task_status(assignee=None):
    return {
        "assignee": assignee,
        "category": "blue",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "description": "You are required...",
        "dueDate": None,
        "formKey": None,
        "id": "336",
        "name": "Requested...",
        "processDefinitionId": "timeOffRequest:1:303",
        "processInstanceId": "32",
        "variables": [],
    }


def _mock_task_status_historic():
    return {
        "id": "202",
        "assignee": None,
        "category": None,
        "deleteReason": None,
        "startTime": "2016-05-22T22:22:22.248+01:00",
        "dueDate": "2016-05-28T08:08:08.248+01:00",
        "claimTime": "2016-05-27T16:52:42.248+01:00",
        "endTime": "2016-05-27T16:52:42.248+01:00",
        "name": "Familiarised with document Oil COSSH Assessment",
        "processInstanceId": "260",
    }


def _mock_task_status_process():
    return {
        "data": [
            {
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/runtime/process-instances/321",
                "tenantId": "",
                "suspended": False,
                "dueDate": None,
                "createTime": "2016-05-27T16:52:42.248+01:00",
                "owner": None,
                "name": "Familiarised with document Oil COSSH Assessment",
                "id": "336",
                "url": "http://localhost:8080/activiti-rest/service/runtime/tasks/336",
                "processDefinitionId": "documentFamiliarisation:1:303",
                "formKey": None,
                "parentTaskUrl": None,
                "parentTaskId": None,
                "assignee": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:303",
                "category": None,
                "taskDefinitionKey": "familiarisationConfirmed",
                "variables": [],
                "delegationState": None,
                "executionId": "333",
                "priority": 50,
                "processInstanceId": "321",
                "executionUrl": "http://localhost:8080/activiti-rest/service/runtime/executions/333",
                "description": "You are required you to be familiar with the document Oil COSSH Assessment (29) to safely carry out your work. admin",
            }
        ]
    }


def _mock_user_task_list():
    return {
        "data": [
            {
                "id": "cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "assignee": 1,
                "category": "Approve Invoice",
                "claimTime": None,
                "createTime": "2017-07-03T08:31:10.000+01:00",
                "delegationState": None,
                "description": None,
                "dueDate": "2020-09-12T12:23:28.116Z",
                "executionId": "c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "executionUrl": "http://172.20.0.2/service/runtime/executions/c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "formKey": "flow",
                "name": "Having checked the PO and GRN",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processDefinitionUrl": "http://172.20.0.2/service/repository/process-definitions/invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processInstanceId": "c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "processInstanceUrl": "http://172.20.0.2/service/runtime/process-instances/c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "scopeDefinitionId": None,
                "scopeId": None,
                "scopeType": None,
                "suspended": False,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "tenantId": "",
                "url": "http://172.20.0.2/service/runtime/tasks/cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "variables": [],
            }
        ],
        "total": 1,
        "start": 0,
        "sort": "createTime",
        "order": "desc",
        "size": 1,
    }


@pytest.mark.django_db
def test_history_process(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        mock_user_task_list.return_value = _mock_user_task_list()
        # setup
        WorkflowFactory(name="Time Off", process_key="timeOff")
        # test
        url = reverse("workflow.history.process")
        perm_check.auth(url)


@pytest.mark.django_db
def test_history_process_report(client):
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        # setup user with history permissions
        group = GroupFactory()
        user = UserFactory()
        user.groups.add(group)
        workflow = WorkflowFactory(process_key="my_process_key")
        workflow.security_history.add(group)
        # test
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("workflow.history.process.report")
        response = client.get(url)
        assert 200 == response.status_code
        # check the form has a choice field containing the workflow
        form = response.context["form"]
        f = form.fields["workflow"]
        assert 1 == f.choices.queryset.count()
        workflow = f.choices.queryset.first()
        assert "my_process_key" == workflow.process_key


@pytest.mark.django_db
def test_history_process_report_staff(client):
    """A member of staff cannot view the page (is this correct)?"""
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        # setup a member of staff
        user = UserFactory(is_staff=True)
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse("workflow.history.process.report")
        response = client.get(url)
        assert 403 == response.status_code


@pytest.mark.django_db
def test_history_process_report_no_perm(client):
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        # setup user with no history permissions
        user = UserFactory(username="pat")
        assert client.login(username=user.username, password=TEST_PASSWORD)
        # test
        url = reverse("workflow.history.process.report")
        response = client.get(url)
        assert 403 == response.status_code
        assert (
            "User 'pat' does not have permission to view workflow history"
        ) == response.context["exception"]


@pytest.mark.django_db
def test_history_task(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        mock_user_task_list.return_value = _mock_user_task_list()
        # setup
        ScheduledWorkflowUserFactory(process_id=99)
        WorkflowFactory(name="Time Off", process_key="timeOff")
        # test
        url = reverse("workflow.history.task", args=[99])
        perm_check.auth(url)


@pytest.mark.django_db
def test_history_task_report(client):
    user = UserFactory()
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task(user.pk)
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        mock_user_task_list.return_value = _mock_user_task_list()
        # setup
        ScheduledWorkflowUserFactory(process_id=99)
        # setup user with history permissions
        group = GroupFactory()
        user.groups.add(group)
        workflow = WorkflowFactory(process_key="timeOff")
        workflow.security_history.add(group)
        # test
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("workflow.history.task.report", args=[99])
        response = client.get(url)
        assert 200 == response.status_code


@pytest.mark.django_db
def test_history_task_report_no_perm(client):
    user = UserFactory(username="pat")
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_history_task.return_value = _mock_history_task(user.pk)
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        # setup user without history permissions
        user.groups.add(GroupFactory())
        workflow = WorkflowFactory(process_key="timeOff")
        workflow.security_history.add(GroupFactory())
        # test
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("workflow.history.task.report", args=[99])
        response = client.get(url)
        assert 403 == response.status_code
        assert (
            "User 'pat' does not have permission to view task history for "
            "'timeOff' processes"
        ) == response.context["exception"]


@pytest.mark.django_db
def test_history_task_report_staff(client):
    """A member of staff cannot view the page (is this correct)?"""
    user = UserFactory(is_staff=True, username="staff")
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_history_task.return_value = _mock_history_task(user.pk)
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        # setup user without history permissions
        WorkflowFactory(process_key="timeOff")
        # test
        assert client.login(username=user.username, password=TEST_PASSWORD)
        url = reverse("workflow.history.task.report", args=[99])
        response = client.get(url)
        assert 403 == response.status_code
        assert (
            "User 'staff' does not have permission to view task history for "
            "'timeOff' processes"
        ) == response.context["exception"]


@pytest.mark.django_db
def test_mapping_list(perm_check):
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow)
    MappingFactory(workflow=workflow)
    url = reverse("workflow.mapping.list", args=[workflow.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_config(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.config", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_map_decimal(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.map.decimal", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_map_flow_list(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.map.flow.list", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_map_group(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.map.group", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_map_integer(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.map.integer", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_mapping_update_map_user(perm_check):
    mapping = MappingFactory()
    url = reverse("workflow.mapping.update.map.user", args=[mapping.pk])
    perm_check.admin(url)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url_name", ["workflow.task.history", "workflow.task.history.error"]
)
def test_task_history(perm_check, url_name):
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti.task_claim"
    ), mock.patch(
        "workflow.activiti.Activiti._task_status_historic"
    ) as mock_task_status_historic, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link:
        # mock return values
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_identity_link.return_value = _mock_task_identity_list()
        mock_task_status_historic.return_value = _mock_task_status_historic()
        # test
        url = reverse("workflow.task.history", args=[99])
        perm_check.auth(url)


@pytest.mark.django_db
def test_process_instance_delete(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock return values
        mock_process_status.return_value = _mock_process_status()
        # test
        url = reverse("workflow.process.instance.delete", args=[99])
        perm_check.admin(url)


@pytest.mark.django_db
def test_process_instance_detail(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_status_process"
    ) as mock_task_status:
        # mock return values
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status_process()
        # setup
        WorkflowFactory(process_key="timeOff")
        # test
        url = reverse("workflow.process.instance.detail", args=[99])
        perm_check.admin(url)


@pytest.mark.django_db
def test_process_instance_diagram(client):
    user = UserFactory()
    # security
    group = GroupFactory()
    user.groups.add(group)
    workflow = WorkflowFactory(process_key="timeOff")
    workflow.security_history.add(group)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.process.instance.diagram", args=[99])
    with mock.patch(
        "workflow.activiti.Activiti._process_diagram"
    ) as mock_process_diagram, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock
        mock_process_diagram.return_value = "my-image"
        mock_process_status.return_value = _mock_process_status()
        # test
        response = client.get(url)
    assert 200 == response.status_code


@pytest.mark.django_db
def test_process_instance_diagram_forbidden(client):
    user = UserFactory()
    WorkflowFactory(process_key="timeOff")
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.process.instance.diagram", args=[99])
    with mock.patch(
        "workflow.activiti.Activiti._process_diagram"
    ) as mock_process_diagram, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock
        mock_process_diagram.return_value = "my-image"
        mock_process_status.return_value = _mock_process_status()
        # test
        response = client.get(url)
    assert 403 == response.status_code


@pytest.mark.django_db
def test_process_instance_diagram_superuser(client):
    user = UserFactory(is_superuser=True)
    WorkflowFactory(process_key="timeOff")
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.process.instance.diagram", args=[99])
    with mock.patch(
        "workflow.activiti.Activiti._process_diagram"
    ) as mock_process_diagram, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status:
        # mock
        mock_process_diagram.return_value = "my-image"
        mock_process_status.return_value = _mock_process_status()
        # test
        response = client.get(url)
    assert 200 == response.status_code


@pytest.mark.django_db
def test_process_instance_list(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list:
        # mock return values
        mock_instance_list.return_value = _mock_process_instance_list()
        # test
        url = reverse("workflow.process.instance.list")
        perm_check.admin(url)


@pytest.mark.django_db
def test_task_process_instance_delete(perm_check):
    group = GroupFactory()
    for user_name in ("staff", "web"):
        user = User.objects.get(username=user_name)
        # give the user permission to delete the process
        user.groups.add(group)
    with mock.patch(
        "workflow.activiti.Activiti.form_variable_info"
    ) as mock_form_variable_info, mock.patch(
        "workflow.activiti.Activiti._task_status"
    ) as mock_task_status, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_identity_link, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        mock_form_variable_info.return_value = _mock_form_variable_info()
        mock_identity_link.return_value = _mock_task_identity_list(group.pk)
        mock_process_status.return_value = _mock_process_status()
        mock_task_status.return_value = _mock_task_status()
        # test
        url = reverse("workflow.task.process.instance.delete", args=[123])
        perm_check.auth(url)


# @pytest.mark.django_db
# def test_workflow_data_download(client):
#     user = UserFactory()
#     group = GroupFactory()
#     user.groups.add(group)
#     workflow = WorkflowFactory(process_key='timeOff')
#     workflow.security_history.add(group)
#     assert client.login(username=user.username, password=TEST_PASSWORD)
#     url = reverse('workflow.data.download', args=['timeOff'])
#     #with mock.patch(
#     #        'workflow.activiti.Activiti._process_status'
#     #    ) as mock_process_status:
#     #    # mock return values
#     #    mock_process_status.return_value = _mock_process_status()
#     response = client.get(url)
#     assert 200 == response.status_code, response.context['exception']
#     # scheduled_data_download(None, 23)
#
#
# @pytest.mark.django_db
# def test_workflow_data_download_no_login(client):
#     url = reverse('workflow.data.download', args=[23])
#     response = client.get(url)
#     assert 302 == response.status_code
#     assert reverse('login') in response['Location']
#
#
# @pytest.mark.django_db
# def test_workflow_data_download_no_perm(client):
#     user = UserFactory(username='pat')
#     workflow = WorkflowFactory(process_key='timeOff')
#     assert client.login(username=user.username, password=TEST_PASSWORD)
#     url = reverse('workflow.data.download', args=['timeOff'])
#     #with mock.patch(
#     #        'workflow.activiti.Activiti._process_status'
#     #    ) as mock_process_status:
#     #    # mock return values
#     #    mock_process_status.return_value = _mock_process_status()
#     response = client.get(url)
#     assert 403 == response.status_code
#     assert (
#         "User 'pat' does not have permission to download workflow "
#         "data (view task history) for 'timeOff' processes"
#     ) == response.context['exception']


@pytest.mark.django_db
def test_workflow_data_create(perm_check):
    workflow = WorkflowFactory()
    url = reverse("workflow.data.create", args=[workflow.pk])
    perm_check.auth(url)


@pytest.mark.django_db
def test_workflow_data_list(perm_check):
    url = reverse("workflow.data.list")
    perm_check.auth(url)


@pytest.mark.django_db
def test_workflow_data_report(perm_check):
    WorkflowFactory(process_key="red")
    report_schedule = ReportScheduleFactory(parameters={"process_key": "red"})
    url = reverse("workflow.data.report", args=[report_schedule.pk])
    perm_check.auth(url)


@pytest.mark.django_db
def test_workflow_data_report_list(perm_check):
    WorkflowFactory(process_key="timeOff")
    report = ReportSpecificationFactory(slug=WorkflowData.REPORT_SLUG)
    ReportScheduleFactory(report=report, parameters={"process_key": "timeOff"})
    url = reverse("workflow.data.report.list", args=["timeOff"])
    perm_check.auth(url)


@pytest.mark.django_db
def test_workflow_delete(perm_check):
    workflow = WorkflowFactory()
    url = reverse("workflow.delete", args=[workflow.pk])
    perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_deploy(perm_check):
    url = reverse("workflow.deploy")
    perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_deployment_list(perm_check):
    with mock.patch("workflow.activiti.Activiti.deployment_list") as mock_api:
        mock_api.return_value = ([], 0)
        url = reverse("workflow.deployment.list")
        perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_list(perm_check):
    WorkflowFactory()
    url = reverse("workflow.list")
    perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_process_definition_list(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti.process_definition_list"
    ) as mock_api:
        mock_api.return_value = ([], 0)
        url = reverse("workflow.process.definition.list")
        perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_task_list(perm_check):
    with mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_tasks:
        # mock return
        mock_user_tasks.return_value = _mock_user_task_list()
        url = reverse("workflow.task.list")
        perm_check.admin(url)


@pytest.mark.django_db
def test_workflow_update(perm_check):
    workflow = WorkflowFactory()
    url = reverse("workflow.update", args=[workflow.pk])
    perm_check.admin(url)
