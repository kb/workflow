# -*- encoding: utf-8 -*-
import pytest
import pytz
import uuid

from datetime import datetime
from django.utils import timezone
from freezegun import freeze_time
from unittest import mock

from login.tests.factories import UserFactory
from workflow.models import (
    ScheduledWorkflow,
    ScheduledWorkflowUser,
    WorkflowError,
)
from workflow.tests.factories import WorkflowFactory
from .factories import ScheduledWorkflowFactory, ScheduledWorkflowUserFactory


def _mock_process_status(process_id=None):
    if process_id is None:
        process_id = 61
    return {
        "data": [
            {
                "businessKey": None,
                "id": process_id,
                "variables": [],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/61",
                "completed": False,
                "processDefinitionName": "invoiceApproval",
                "processDefinitionId": "invoiceApproval:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:43",
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


@pytest.mark.django_db
def test_current():
    ScheduledWorkflowUserFactory(user=UserFactory(username="w1"))
    w2 = ScheduledWorkflowUserFactory(user=UserFactory(username="w2"))
    w3 = ScheduledWorkflowUserFactory(user=UserFactory(username="w3"))
    ScheduledWorkflowUserFactory(process_id=12, user=UserFactory(username="w4"))
    ScheduledWorkflowUserFactory(user=UserFactory(username="w5"))
    w6 = ScheduledWorkflowUserFactory(
        process_id=13, user=UserFactory(username="w6")
    )
    # delete
    w2.set_deleted(UserFactory())
    w3.set_deleted_process(UserFactory(), "c3")
    w6.set_deleted_process(UserFactory(), "c6")
    # test
    qs = ScheduledWorkflowUser.objects.current()
    assert ["w1", "w4", "w5"] == [x.user.username for x in qs]
    assert ["w2"] == [
        x.user.username
        for x in ScheduledWorkflowUser.objects.filter(deleted=True)
    ]
    assert ["c3", "c6"] == [
        x.deleted_comment
        for x in ScheduledWorkflowUser.objects.filter(deleted_process=True)
    ]


@pytest.mark.django_db
def test_delete_workflow_process():
    """Delete the workflow process."""
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        process_uuid = str(uuid.uuid4())
        mock_process_status.return_value = _mock_process_status(process_uuid)
        user = UserFactory()
        x = ScheduledWorkflowUserFactory(process_uuid=process_uuid)
        assert x.deleted_process is False
        with freeze_time(datetime(2020, 3, 11, 8, 0, 0, tzinfo=pytz.utc)):
            x.delete_workflow_process(user, "Started in error")
        x.refresh_from_db()
        assert x.deleted_process is True
        assert "Started in error" == x.deleted_comment
        assert (
            datetime(2020, 3, 11, 8, 0, 0, tzinfo=pytz.utc)
            == x.date_deleted_process
        )
        assert user == x.user_deleted_process
        assert x.deleted is False


@pytest.mark.django_db
def test_delete_workflow_process_already_deleted():
    """Try to delete a workflow process which has already been deleted.

    ``deleted_process`` is already set to ``True``, so the other
    ``deleted_process`` fields should not be updated.

    """
    user = UserFactory()
    x = ScheduledWorkflowUserFactory(
        process_uuid=str(uuid.uuid4()), deleted_process=True
    )
    x.delete_workflow_process(user, "Started in error")
    x.refresh_from_db()
    assert x.deleted_process is True
    assert "" == x.deleted_comment
    assert x.date_deleted_process is None
    assert x.user_deleted_process is None
    assert x.deleted is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "deleted_comment",
    [None, ""],
)
def test_delete_workflow_process_no_comment(deleted_comment):
    """A workflow process can only be deleted with a comment."""
    user = UserFactory()
    x = ScheduledWorkflowUserFactory(
        deleted_process=False, process_uuid=str(uuid.uuid4())
    )
    with pytest.raises(WorkflowError) as e:
        x.delete_workflow_process(user, deleted_comment)
    assert "A workflow cannot be deleted without a comment" in str(e.value)


@pytest.mark.django_db
def test_delete_workflow_process_no_process_id():
    """Try to delete a workflow process which hasn't been created.

    If the workflow process hasn't been created yet (``get_process_id``) is
    ``None``, then just *soft-delete* the scheduled workflow for the user so
    the process doesn't get created.

    """
    process_uuid = str(uuid.uuid4())
    user = UserFactory()
    x = ScheduledWorkflowUserFactory(deleted_process=False)
    with freeze_time(datetime(2020, 3, 11, 8, 0, 0, tzinfo=pytz.utc)):
        x.delete_workflow_process(user, "Started in error")
    x.refresh_from_db()
    # check no process
    assert x.process_id is None
    assert x.process_uuid is None
    # check soft delete of ``scheduled_workflow_user``
    assert x.deleted is True
    assert user == x.user_deleted
    assert datetime(2020, 3, 11, 8, 0, 0, tzinfo=pytz.utc) == x.date_deleted
    # check process is not deleted
    assert x.deleted_process is False
    assert "" == x.deleted_comment
    assert x.date_deleted_process is None
    assert x.user_deleted_process is None


@pytest.mark.django_db
def test_factory():
    ScheduledWorkflowFactory()


@pytest.mark.django_db
def test_factory_user():
    ScheduledWorkflowUserFactory()


@pytest.mark.django_db
def test_manager_get_process_id():
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_id=123, user=UserFactory()
    )
    assert (
        scheduled_workflow_user
        == ScheduledWorkflowUser.objects.get_process_id(123)
    )


@pytest.mark.django_db
def test_manager_get_process_id_uuid():
    process_uuid = str(uuid.uuid4())
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_uuid=process_uuid, user=UserFactory()
    )
    assert (
        scheduled_workflow_user
        == ScheduledWorkflowUser.objects.get_process_id(process_uuid)
    )


@pytest.mark.django_db
def test_manager_get_process_id_both():
    process_uuid = str(uuid.uuid4())
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_id=456, process_uuid=process_uuid, user=UserFactory()
    )
    assert (
        scheduled_workflow_user
        == ScheduledWorkflowUser.objects.get_process_id(456)
    )


@pytest.mark.django_db
def test_manager_get_process_id_both_empty():
    ScheduledWorkflowUserFactory(
        process_id=None, process_uuid=None, user=UserFactory()
    )
    with pytest.raises(ScheduledWorkflowUser.DoesNotExist):
        ScheduledWorkflowUser.objects.get_process_id(898989)


@pytest.mark.django_db
def test_manager_get_process_id_both_empty_uuid():
    ScheduledWorkflowUserFactory(
        process_id=None, process_uuid=None, user=UserFactory()
    )
    with pytest.raises(ScheduledWorkflowUser.DoesNotExist):
        ScheduledWorkflowUser.objects.get_process_id(str(uuid.uuid4()))


@pytest.mark.django_db
def test_get_process_id():
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_id=123, user=UserFactory()
    )
    assert 123 == scheduled_workflow_user.get_process_id()


@pytest.mark.django_db
def test_get_process_id_uuid():
    """Get the process ID (a UUID this time).

    .. note:: The process ID (UUID) must be returned as a ``string``.

    """
    process_uuid = uuid.uuid4()
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_uuid=process_uuid, user=UserFactory()
    )
    assert str(process_uuid) == scheduled_workflow_user.get_process_id()


@pytest.mark.django_db
def test_get_process_id_both():
    process_uuid = str(uuid.uuid4())
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_id=456, process_uuid=process_uuid, user=UserFactory()
    )
    assert 456 == scheduled_workflow_user.get_process_id()


@pytest.mark.django_db
def test_get_process_id_both_empty():
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        process_id=None, process_uuid=None, user=UserFactory()
    )
    assert scheduled_workflow_user.get_process_id() is None


@pytest.mark.django_db
def test_get_workflow():
    workflow = WorkflowFactory(process_key="invoiceApproval")
    scheduled_workflow = ScheduledWorkflowFactory(
        process_definition="invoiceApproval"
    )
    scheduled_workflow_user = ScheduledWorkflowUserFactory(
        scheduled_workflow=scheduled_workflow
    )
    assert workflow == scheduled_workflow_user.get_workflow()


@pytest.mark.django_db
def test_pending():
    ScheduledWorkflowUserFactory(user=UserFactory(username="w1"))
    w2 = ScheduledWorkflowUserFactory(user=UserFactory(username="w2"))
    w3 = ScheduledWorkflowUserFactory(user=UserFactory(username="w3"))
    ScheduledWorkflowUserFactory(process_id=12, user=UserFactory(username="w4"))
    ScheduledWorkflowUserFactory(user=UserFactory(username="w5"))
    w6 = ScheduledWorkflowUserFactory(
        process_id=13, user=UserFactory(username="w6")
    )
    ScheduledWorkflowUserFactory(
        process_uuid=str(uuid.uuid4()), user=UserFactory(username="w7")
    )
    # delete
    w2.set_deleted(UserFactory())
    w3.set_deleted_process(UserFactory(), "c3")
    w6.set_deleted_process(UserFactory(), "c6")
    # test
    qs = ScheduledWorkflowUser.objects.pending()
    assert ["w1", "w5"] == [x.user.username for x in qs]


@pytest.mark.django_db
def test_pending_with_options():
    # before start_date
    with freeze_time(datetime(2020, 3, 11, 8, 0, 0, tzinfo=pytz.utc)):
        s1 = ScheduledWorkflowFactory(process_definition="timeOff")
        ScheduledWorkflowUserFactory(
            scheduled_workflow=s1, user=UserFactory(username="w1")
        )
    # start_date
    start_date = datetime(2020, 3, 11, 9, 0, 0, tzinfo=pytz.utc)
    # after start_date
    with freeze_time(datetime(2020, 3, 11, 10, 0, 0, tzinfo=pytz.utc)):
        # different 'process_definition'
        s2 = ScheduledWorkflowFactory(process_definition="duvetDay")
        ScheduledWorkflowUserFactory(
            scheduled_workflow=s2, user=UserFactory(username="w2")
        )
        # matching 'process_definition'
        s3 = ScheduledWorkflowFactory(process_definition="timeOff")
        ScheduledWorkflowUserFactory(
            scheduled_workflow=s3, user=UserFactory(username="w3")
        )
    # end date
    end_date = datetime(2020, 3, 11, 11, 0, 0, tzinfo=pytz.utc)
    # after end date
    with freeze_time(datetime(2020, 3, 11, 12, 0, 0, tzinfo=pytz.utc)):
        s4 = ScheduledWorkflowFactory(process_definition="timeOff")
        ScheduledWorkflowUserFactory(
            scheduled_workflow=s4, user=UserFactory(username="w4")
        )
    # test
    qs = ScheduledWorkflowUser.objects.pending_with_options(
        process_definition="timeOff",
        start_date=start_date,
        end_date=end_date,
    )
    assert ["w3"] == [x.user.username for x in qs]


@pytest.mark.django_db
def test_set_deleted_process():
    user = UserFactory()
    obj = ScheduledWorkflowUserFactory()
    assert obj.is_deleted_process is False
    obj.set_deleted_process(user, "deleted_comment")
    obj.refresh_from_db()
    assert obj.is_deleted_process is True
    assert timezone.now().date() == obj.date_deleted_process.date()
    assert user == obj.user_deleted_process
    # not sure why the record isn't marked as deleted...
    assert obj.deleted is False
    assert "deleted_comment" == obj.deleted_comment


@pytest.mark.django_db
def test_str_pending():
    scheduled_workflow = ScheduledWorkflow(
        process_definition="abc",
        initiator=UserFactory(username="pat"),
        content_object=UserFactory(),
    )
    scheduled_workflow.save()
    obj = ScheduledWorkflowUser(
        scheduled_workflow=scheduled_workflow, user=UserFactory(username="ben")
    )
    assert "'abc' initiator 'pat'" in str(obj)
    assert "'ben' pending" in str(obj)


@pytest.mark.django_db
def test_str_process_id():
    with freeze_time(datetime(2023, 7, 21, 19, 35, 0)):
        scheduled_workflow = ScheduledWorkflow(
            process_definition="abc",
            initiator=UserFactory(username="pat"),
            content_object=UserFactory(),
        )
        scheduled_workflow.save()
    scheduled_workflow_user = ScheduledWorkflowUser(
        scheduled_workflow=scheduled_workflow,
        user=UserFactory(username="ben"),
        process_id=321,
    )
    scheduled_workflow_user.save()
    assert (
        "'abc' initiator 'pat' created 21/07/2023 19:35 "
        "for 'ben' process '321'"
    ) == str(scheduled_workflow_user)


@pytest.mark.django_db
def test_str_process_uuid():
    with freeze_time(datetime(2023, 7, 21, 19, 35, 0)):
        scheduled_workflow = ScheduledWorkflow(
            process_definition="abc",
            initiator=UserFactory(username="pat"),
            content_object=UserFactory(),
        )
        scheduled_workflow.save()
    process_uuid = uuid.uuid4()
    scheduled_workflow_user = ScheduledWorkflowUser(
        scheduled_workflow=scheduled_workflow,
        user=UserFactory(username="ben"),
        process_id=None,
        process_uuid=process_uuid,
    )
    scheduled_workflow_user.save()
    assert (
        "'abc' initiator 'pat' created 21/07/2023 19:35 for "
        f"'ben' process '{process_uuid}'"
    ) == str(scheduled_workflow_user)
