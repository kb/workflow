# -*- encoding: utf-8 -*-
from workflow.activiti import camelcase_to_snake


def test_camelcase_to_snake():
    assert "object_pk" == camelcase_to_snake("objectPk")


def test_camelcase_to_snake_multi():
    assert "pat_kim_and_kim" == camelcase_to_snake("patKimAndKim")
