# -*- encoding: utf-8 -*-
import pytest

from collections import OrderedDict
from datetime import datetime
from dateutil.tz import tzoffset
from django.urls import reverse
from http import HTTPStatus
from unittest import mock

from login.tests.factories import TEST_PASSWORD, UserFactory
from workflow.tests.factories import MappingFactory, WorkflowFactory
from workflow.views import History


def _mock_history_task():
    return {
        "order": "desc",
        "total": 4,
        "sort": "endTime",
        "size": 4,
        "data": [
            {
                "category": "Check/Complete Invoice Information",
                "formKey": "flow",
                "claimTime": "2017-07-03T08:31:10.104+01:00",
                "executionId": "199",
                "id": "202",
                "description": "When you have finished please click the 'Complete Task' button to send it to your Manager:",
                "workTimeInMillis": 71,
                "taskDefinitionKey": "sid-EC1A150E-131B-4758-86BF-10025A5D2993",
                "processInstanceId": "167",
                "dueDate": "2017-08-02T08:27:56.877+01:00",
                "assignee": "1",
                "deleteReason": None,
                "startTime": "2017-07-03T08:27:56.878+01:00",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/167",
                "endTime": "2017-07-03T08:31:10.175+01:00",
                "priority": 50,
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/202",
                "variables": [],
                "tenantId": "",
                "processDefinitionId": "invoiceApproval:1:55",
                "owner": None,
                "name": "Check/Complete invoice information fields for the shown invoice ",
                "parentTaskId": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/invoiceApproval:1:55",
                "durationInMillis": 193297,
            },
            {
                "category": None,
                "formKey": "flow",
                "claimTime": "2017-07-03T08:05:14.041+01:00",
                "executionId": "106",
                "id": "122abc122",
                "description": None,
                "workTimeInMillis": 259,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "processInstanceId": "94abc94",
                "dueDate": "2017-08-02T08:05:11.267+01:00",
                "assignee": "1",
                "deleteReason": "Oranges",
                "startTime": "2017-07-03T08:05:11.267+01:00",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/94",
                "endTime": "2017-07-03T08:05:14.300+01:00",
                "priority": 50,
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/122",
                "variables": [],
                "tenantId": "",
                "processDefinitionId": "timeOffRequest:1:63",
                "owner": None,
                "name": "Accept or reject time off request from 2017-07-10 to 2017-07-11 for Maria Anders",
                "parentTaskId": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "durationInMillis": 3033,
            },
            {
                "category": "Enter your Request",
                "formKey": "flow",
                "claimTime": None,
                "executionId": "106",
                "id": "109",
                "description": "When you have finished please click the 'Complete Task' button to send it to your Manager:",
                "workTimeInMillis": None,
                "taskDefinitionKey": "sid-EC1A150E-131B-4758-86BF-10025A5D2993",
                "processInstanceId": "94abc94",
                "dueDate": "2017-08-02T08:05:04.543+01:00",
                "assignee": "1",
                "deleteReason": "Apples",
                "startTime": "2017-07-03T08:05:04.543+01:00",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/94",
                "endTime": "2017-07-03T08:05:11.266+01:00",
                "priority": 50,
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/109",
                "variables": [],
                "tenantId": "",
                "processDefinitionId": "timeOffRequest:1:63",
                "owner": None,
                "name": "Please fill out the form and check before submitting your request to your manager",
                "parentTaskId": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "durationInMillis": 6723,
            },
            {
                "category": "Enter your Request",
                "formKey": "flow",
                "claimTime": None,
                "executionId": "76",
                "id": "79",
                "description": "When you have finished please click the 'Complete Task' button to send it to your Manager:",
                "workTimeInMillis": None,
                "taskDefinitionKey": "sid-EC1A150E-131B-4758-86BF-10025A5D2993",
                "processInstanceId": "64",
                "dueDate": "2017-08-02T08:04:55.256+01:00",
                "assignee": "1",
                "deleteReason": None,
                "startTime": "2017-07-03T08:04:55.268+01:00",
                "processInstanceUrl": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/64",
                "endTime": "2017-07-03T08:05:01.653+01:00",
                "priority": 50,
                "url": "http://localhost:8080/activiti-rest/service/history/historic-task-instances/79",
                "variables": [],
                "tenantId": "",
                "processDefinitionId": "timeOffRequest:1:63",
                "owner": None,
                "name": "Please fill out the form and check before submitting your request to your manager",
                "parentTaskId": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "durationInMillis": 6385,
            },
        ],
        "start": 0,
    }


def _mock_process_instance_list():
    return {
        "start": 0,
        "data": [
            {
                "activityId": None,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/52",
                "completed": False,
                "processDefinitionId": "changeRequest:1:43",
                "businessKey": None,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/changeRequest:1:43",
                "variables": [
                    {
                        "value": 1,
                        "name": "objectPk",
                        "scope": "global",
                        "type": "integer",
                    }
                ],
                "id": "52",
                "tenantId": "",
                "suspended": False,
                "ended": False,
            }
        ],
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    }
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/a/61",
                "completed": False,
                "processDefinitionId": "timeOff:1:43",
                "processDefinitionName": "timeOff",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/b/timeOffRequest:1:43",
            }
        ]
    }


def _mock_user_task_list():
    return {
        "data": [
            {
                "id": 333,
                # 167
                # "id": "cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "assignee": 1,
                "category": "Approve Invoice",
                "claimTime": None,
                "createTime": "2017-07-03T08:31:10.000+01:00",
                "delegationState": None,
                "description": None,
                "dueDate": "2020-09-12T12:23:28.116Z",
                "executionId": "c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "executionUrl": "http://172.20.0.2/service/runtime/executions/c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "formKey": "flow",
                "name": "Having checked the PO and GRN",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processDefinitionUrl": "http://172.20.0.2/service/repository/process-definitions/invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processInstanceId": "167",  # "a3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "processInstanceUrl": "http://172.20.0.2/service/runtime/process-instances/c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "scopeDefinitionId": None,
                "scopeId": None,
                "scopeType": None,
                "suspended": False,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "tenantId": "",
                "url": "http://172.20.0.2/service/runtime/tasks/cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "variables": [],
            },
            {
                "id": 444,
                # "id": 150,
                "assignee": 1,
                "category": "Approve Invoice",
                "claimTime": None,
                "createTime": "2017-07-03T08:31:10.000+01:00",
                "delegationState": None,
                "description": None,
                "dueDate": "2020-09-12T12:23:28.116Z",
                "executionId": "c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "executionUrl": "http://172.20.0.2/service/runtime/executions/c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "formKey": "flow",
                "name": "Having checked the PO and GRN",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processDefinitionUrl": "http://172.20.0.2/service/repository/process-definitions/invoiceApproval:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processInstanceId": "b3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "processInstanceUrl": "http://172.20.0.2/service/runtime/process-instances/c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "scopeDefinitionId": None,
                "scopeId": None,
                "scopeType": None,
                "suspended": False,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "tenantId": "",
                "url": "http://172.20.0.2/service/runtime/tasks/cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "variables": [],
            },
            {
                "id": 555,
                # "id": 133,
                "assignee": 1,
                "category": "Approve Invoice",
                "claimTime": None,
                "createTime": "2017-07-03T08:31:10.000+01:00",
                "delegationState": None,
                "description": None,
                "dueDate": "2020-09-12T12:23:28.116Z",
                "executionId": "c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "executionUrl": "http://172.20.0.2/service/runtime/executions/c325b0b4-dd5f-11ea-98d7-c6e9a110df66",
                "formKey": "flow",
                "name": "Having checked the PO and GRN",
                "owner": None,
                "parentTaskId": None,
                "parentTaskUrl": None,
                "priority": 50,
                "processDefinitionId": "timeOffRequest:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processDefinitionUrl": "http://172.20.0.2/service/repository/process-definitions/timeOffRequest:37:11462d33-dd5d-11ea-98d7-c6e9a110df66",
                "processInstanceId": "c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "processInstanceUrl": "http://172.20.0.2/service/runtime/process-instances/c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                "scopeDefinitionId": None,
                "scopeId": None,
                "scopeType": None,
                "suspended": False,
                "taskDefinitionKey": "sid-05F94E95-3052-40FD-A51C-75FB188F1711",
                "tenantId": "",
                "url": "http://172.20.0.2/service/runtime/tasks/cabbfa58-dd5f-11ea-98d7-c6e9a110df66",
                "variables": [],
            },
        ],
        "total": 3,
        "start": 0,
        "sort": "createTime",
        "order": "desc",
        "size": 3,
    }


# def _mock_user_task_list():
#    task_data = [
#        TaskData(
#            assignee=1,
#            category="Approve Invoice",
#            created=datetime(
#                2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
#            ),
#            definition_id="invoiceApproval",
#            description="",
#            due_date=datetime(2017, 8, 2, 8, 31),
#            name="Having checked the PO and GRN",
#            process_id=167,
#            process_key="invoiceApproval",
#            task_id=223,
#            url_name="flow",
#            variables=[],
#        ),
#        TaskData(
#            assignee=1,
#            category="Approve Invoice",
#            created=datetime(
#                2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
#            ),
#            definition_id="invoiceApproval",
#            description="",
#            due_date=datetime(2017, 8, 2, 8, 31),
#            name="Please fill out the form",
#            process_id=150,
#            process_key="timeOffRequest",
#            task_id=223,
#            url_name="flow",
#            variables=[],
#        ),
#        TaskData(
#            assignee=1,
#            category="Approve Invoice",
#            created=datetime(
#                2017, 7, 3, 8, 31, 10, 177000, tzinfo=tzoffset(None, 3600)
#            ),
#            definition_id="invoiceApproval",
#            description="",
#            due_date=datetime(2017, 8, 2, 8, 31),
#            name="Please fill out the form and check",
#            process_id=133,
#            process_key="timeOffRequest",
#            task_id=223,
#            url_name="flow",
#            variables=[],
#        ),
#    ]
#    return task_data, 3


@pytest.mark.django_db
def test_history_process(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD)
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._process_instances"
    ) as mock_instance_list, mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_instance_list.return_value = _mock_process_instance_list()
        mock_process_status.return_value = _mock_process_status()
        mock_user_task_list.return_value = _mock_user_task_list()
        # setup
        workflow = WorkflowFactory(name="Time Off", process_key="timeOff")
        MappingFactory(
            workflow=workflow, variable_name="m1", history_summary=True
        )
        MappingFactory(
            workflow=workflow, variable_name="m2", history_summary=False
        )
        MappingFactory(
            workflow=workflow, variable_name="m3", history_summary=True
        )
        # test
        response = client.get(reverse("workflow.history.process"))
        assert HTTPStatus.OK == response.status_code
        assert (
            OrderedDict(
                [
                    (
                        167,
                        {
                            "column_1": "",
                            "column_2": "",
                            "completed": False,
                            "created": datetime(
                                2016,
                                5,
                                29,
                                18,
                                36,
                                25,
                                tzinfo=tzoffset(None, 3600),
                            ),
                            "deleted": False,
                            "end_time": None,
                            "name": "Time Off",
                            "process_id": 61,
                            "process_key": "timeOff",
                            "variables": OrderedDict(),
                            "workflow_pk": workflow.pk,
                        },
                    ),
                    (
                        "b3249f34-dd5f-11ea-98d7-c6e9a110df66",
                        {
                            "column_1": "",
                            "column_2": "",
                            "completed": False,
                            "created": datetime(
                                2016,
                                5,
                                29,
                                18,
                                36,
                                25,
                                tzinfo=tzoffset(None, 3600),
                            ),
                            "deleted": False,
                            "end_time": None,
                            "name": "Time Off",
                            "process_id": 61,
                            "process_key": "timeOff",
                            "variables": OrderedDict(),
                            "workflow_pk": workflow.pk,
                        },
                    ),
                    (
                        "c3249f34-dd5f-11ea-98d7-c6e9a110df66",
                        {
                            "column_1": "",
                            "column_2": "",
                            "completed": False,
                            "created": datetime(
                                2016,
                                5,
                                29,
                                18,
                                36,
                                25,
                                tzinfo=tzoffset(None, 3600),
                            ),
                            "deleted": False,
                            "end_time": None,
                            "name": "Time Off",
                            "process_id": 61,
                            "process_key": "timeOff",
                            "variables": OrderedDict(),
                            "workflow_pk": workflow.pk,
                        },
                    ),
                    (
                        "94abc94",
                        {
                            "column_1": "",
                            "column_2": "",
                            "completed": False,
                            "created": datetime(
                                2016,
                                5,
                                29,
                                18,
                                36,
                                25,
                                tzinfo=tzoffset(None, 3600),
                            ),
                            "deleted": False,
                            "end_time": None,
                            "name": "Time Off",
                            "process_id": 61,
                            "process_key": "timeOff",
                            "variables": OrderedDict(),
                            "workflow_pk": workflow.pk,
                        },
                    ),
                    (
                        64,
                        {
                            "column_1": "",
                            "column_2": "",
                            "completed": False,
                            "created": datetime(
                                2016,
                                5,
                                29,
                                18,
                                36,
                                25,
                                tzinfo=tzoffset(None, 3600),
                            ),
                            "deleted": False,
                            "end_time": None,
                            "name": "Time Off",
                            "process_id": 61,
                            "process_key": "timeOff",
                            "variables": OrderedDict(),
                            "workflow_pk": workflow.pk,
                        },
                    ),
                ]
            )
            == response.context["object_list"]
        )


@pytest.mark.django_db
def test_task_process_ids():
    user = UserFactory()
    with mock.patch(
        "workflow.activiti.Activiti._history_task"
    ) as mock_history_task, mock.patch(
        "workflow.activiti.Activiti._user_task_list"
    ) as mock_user_task_list:
        # mock return values
        mock_history_task.return_value = _mock_history_task()
        mock_user_task_list.return_value = _mock_user_task_list()
        # test
        history = History()
        assert [
            167,
            "b3249f34-dd5f-11ea-98d7-c6e9a110df66",  # 150
            "c3249f34-dd5f-11ea-98d7-c6e9a110df66",  # 133
            "94abc94",
            64,
        ] == history.task_process_ids(user, "abc")
