# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import GroupFactory, UserFactory
from workflow.models import Mapping, Workflow, WorkflowError
from workflow.tests.factories import MappingFactory, WorkflowFactory


# @pytest.mark.django_db
# def test_auto_generate_columns():
#    w = WorkflowFactory()
#    m1 = MappingFactory(
#        workflow=w, variable_name="m1", mapping_type=MappingPlugin.AUTO_GENERATE
#    )
#    MappingFactory(workflow=w, variable_name="m2", history_summary_column=None)
#    m3 = MappingFactory(
#        workflow=w, variable_name="m3", mapping_type=MappingPlugin.AUTO_GENERATE
#    )
#    MappingFactory(workflow=w, variable_name="m4")
#    MappingFactory(workflow=WorkflowFactory(), variable_name="m5")
#    assert ["m1", "m3"] == [x.variable_name for x in w.auto_generate_columns()]


@pytest.mark.django_db
def test_can_create_public():
    user = UserFactory()
    workflow = WorkflowFactory(security_type=Workflow.PUBLIC)
    assert workflow.can_create(user) is True


@pytest.mark.django_db
def test_can_create_group():
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    workflow = WorkflowFactory(security_type=Workflow.GROUPS)
    workflow.groups.add(group)
    assert workflow.can_create(user) is True


@pytest.mark.django_db
def test_can_create_group_not_a_member():
    user = UserFactory()
    group = GroupFactory()
    workflow = WorkflowFactory(security_type=Workflow.GROUPS)
    workflow.groups.add(group)
    assert workflow.can_create(user) is False


@pytest.mark.django_db
def test_can_create_superuser():
    user = UserFactory(is_superuser=True)
    workflow = WorkflowFactory(security_type=Workflow.GROUPS)
    assert workflow.can_create(user) is True


@pytest.mark.django_db
def test_can_create_system():
    user = UserFactory()
    workflow = WorkflowFactory(security_type=Workflow.SYSTEM)
    assert workflow.can_create(user) is False


@pytest.mark.django_db
def test_current():
    WorkflowFactory(process_key="c")
    workflow = WorkflowFactory(process_key="b")
    workflow.set_deleted(UserFactory())
    WorkflowFactory(process_key="a")
    assert ["a", "c"] == [x.process_key for x in Workflow.objects.current()]


@pytest.mark.django_db
def test_data_download():
    w = WorkflowFactory()
    MappingFactory(workflow=w, variable_name="m1", data_download=True)
    MappingFactory(workflow=w, variable_name="m2", data_download=False)
    m = MappingFactory(workflow=w, variable_name="m3", data_download=True)
    m.set_deleted(UserFactory())
    MappingFactory(workflow=w, variable_name="m4", data_download=True)
    MappingFactory(
        workflow=WorkflowFactory(), variable_name="m5", data_download=True
    )
    MappingFactory(workflow=w, variable_name="m6", data_download=True)
    assert set(["m1", "m4", "m6"]) == {
        x.variable_name for x in w.data_download()
    }


@pytest.mark.django_db
def test_factory():
    WorkflowFactory()


@pytest.mark.django_db
def test_for_user():
    user = UserFactory()
    group = GroupFactory()
    user.groups.add(group)
    w1 = WorkflowFactory(name="w1", security_type=Workflow.GROUPS)
    w1.groups.add(group)
    WorkflowFactory(name="w2", security_type=Workflow.PUBLIC)
    WorkflowFactory(name="w3", security_type=Workflow.SYSTEM)
    w4 = WorkflowFactory(name="w4", security_type=Workflow.GROUPS)
    w4.groups.add(GroupFactory())
    assert ["w1", "w2"] == [x.name for x in Workflow.objects.for_user(user)]


@pytest.mark.django_db
def test_for_user_duplicate():
    """A workflow in two user groups, should be returned once (not twice).

    https://www.kbsoftware.co.uk/crm/ticket/2727/

    """
    user = UserFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g1)
    user.groups.add(g2)
    workflow = WorkflowFactory(name="w1", security_type=Workflow.GROUPS)
    workflow.groups.add(g1)
    workflow.groups.add(g2)
    assert ["w1"] == [x.name for x in Workflow.objects.for_user(user)]


@pytest.mark.django_db
def test_has_security_delete():
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_delete.add(g1, g2)
    # test
    assert workflow.has_security_delete(user) is True


@pytest.mark.django_db
def test_has_security_delete_not():
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_delete.add(g1)
    # test
    assert workflow.has_security_delete(user) is False


@pytest.mark.django_db
def test_has_security_history():
    """Note: Both the model and manager have 'has_security_history' methods."""
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_history.add(g1, g2)
    # test
    assert workflow.has_security_history(user) is True
    assert workflow.has_security_history_my(user) is False


@pytest.mark.django_db
def test_has_security_history_manager():
    """Note: Both the model and manager have 'has_security_history' methods."""
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_history.add(g1, g2)
    # test
    assert Workflow.objects.has_security_history(user) is True


@pytest.mark.django_db
def test_has_security_history_manager_not():
    """Note: Both the model and manager have 'has_security_history' methods."""
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_history.add(g1)
    # test
    assert Workflow.objects.has_security_history(user) is False


@pytest.mark.django_db
def test_has_security_history_my():
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_history_my.add(g1, g2)
    # test
    assert workflow.has_security_history(user) is False
    assert workflow.has_security_history_my(user) is True


@pytest.mark.django_db
def test_has_security_history_my_not():
    user = UserFactory()
    workflow = WorkflowFactory(process_key="w1")
    # test
    assert workflow.has_security_history(user) is False
    assert workflow.has_security_history_my(user) is False


@pytest.mark.django_db
def test_has_security_history_my_superuser():
    user = UserFactory(is_superuser=True)
    workflow = WorkflowFactory(process_key="w1")
    # test
    assert workflow.has_security_history(user) is True
    assert workflow.has_security_history_my(user) is True


@pytest.mark.django_db
def test_has_security_history_not():
    """Note: Both the model and manager have 'has_security_history' methods."""
    user = UserFactory()
    WorkflowFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    user.groups.add(g2)
    # workflow
    workflow = WorkflowFactory(process_key="w1")
    workflow.security_history.add(g1)
    # test
    assert workflow.has_security_history(user) is False


@pytest.mark.django_db
def test_has_security_history_superuser():
    """Note: Both the model and manager have 'has_security_history' methods."""
    user = UserFactory(is_superuser=True)
    workflow = WorkflowFactory(process_key="w1")
    assert workflow.has_security_history(user) is True


@pytest.mark.django_db
def test_history_summary():
    w = WorkflowFactory()
    MappingFactory(
        workflow=w,
        variable_name="m1",
        history_summary=True,
        history_summary_column=2,
    )
    MappingFactory(workflow=w, variable_name="m2", history_summary=False)
    m = MappingFactory(workflow=w, variable_name="m3", history_summary=True)
    m.set_deleted(UserFactory())
    # default for 'history_summary' is 'True'
    MappingFactory(workflow=w, variable_name="m4")
    MappingFactory(
        workflow=WorkflowFactory(), variable_name="m5", history_summary=True
    )
    MappingFactory(
        workflow=w,
        variable_name="m6",
        history_summary=True,
        history_summary_order=1,
    )
    assert ["m6", "m1", "m4"] == [x.variable_name for x in w.history_summary()]


@pytest.mark.django_db
def test_history_summary_columns():
    w = WorkflowFactory()
    m1 = MappingFactory(
        workflow=w, variable_name="m1", history_summary_column=1
    )
    MappingFactory(workflow=w, variable_name="m2", history_summary_column=None)
    m3 = MappingFactory(
        workflow=w, variable_name="m3", history_summary_column=2
    )
    MappingFactory(workflow=w, variable_name="m4")
    MappingFactory(
        workflow=WorkflowFactory(), variable_name="m5", history_summary_column=2
    )
    w.set_history_summary_columns([m3, m1])
    assert ["m3", "m1"] == [
        x.variable_name for x in w.history_summary_columns()
    ]


@pytest.mark.django_db
def test_history_summary_columns_deleted():
    """Ignore deleted mappings."""
    w = WorkflowFactory()
    m1 = MappingFactory(
        workflow=w, variable_name="m1", history_summary_column=1
    )
    m1.set_deleted(UserFactory())
    m3 = MappingFactory(
        workflow=w, variable_name="m3", history_summary_column=2
    )
    w.set_history_summary_columns([m3, m1])
    assert ["m3"] == [x.variable_name for x in w.history_summary_columns()]


@pytest.mark.django_db
def test_is_system():
    workflow = WorkflowFactory(security_type=Workflow.SYSTEM)
    assert workflow.is_system is True


@pytest.mark.django_db
def test_is_system_not():
    workflow = WorkflowFactory(security_type=Workflow.PUBLIC)
    assert workflow.is_system is False


@pytest.mark.django_db
def test_is_url_name():
    """Does this workflow have a URL name?"""
    workflow = WorkflowFactory(url_name="workflow.list")
    assert workflow.is_url_name is True


@pytest.mark.django_db
def test_is_url_name_not():
    """Does this workflow have a URL name?"""
    workflow = WorkflowFactory()
    assert workflow.is_url_name is False


@pytest.mark.django_db
def test_mapping():
    user = UserFactory()
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="3")
    MappingFactory(workflow=workflow, variable_name="2").set_deleted(user)
    MappingFactory(workflow=workflow, variable_name="1")
    assert ["1", "3"] == sorted([x.variable_name for x in workflow.mapping()])


@pytest.mark.django_db
def test_ordering():
    WorkflowFactory(process_key="carrot-cake")
    WorkflowFactory(process_key="apple-cake")
    assert ["apple-cake", "carrot-cake"] == [
        x.process_key for x in Workflow.objects.all()
    ]


@pytest.mark.django_db
def test_security_history():
    user = UserFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    g3 = GroupFactory()
    g4 = GroupFactory()
    user.groups.add(g1, g3, g4)
    # workflow
    w1 = WorkflowFactory(process_key="w1")
    w1.security_history.add(g1, g3)
    w2 = WorkflowFactory(process_key="w2")
    w2.security_history.add(g2)
    w3 = WorkflowFactory(process_key="w3")
    w3.security_history.add(g2, g3)
    w4 = WorkflowFactory(process_key="w4")
    w4.security_history.add(g2, g3)
    w4.set_deleted(user)
    # test
    qs = Workflow.objects.security_history(user)
    assert ["w1", "w3"] == [x.process_key for x in qs]


@pytest.mark.django_db
def test_security_history_my():
    user = UserFactory()
    g1 = GroupFactory()
    g2 = GroupFactory()
    g3 = GroupFactory()
    g4 = GroupFactory()
    user.groups.add(g1, g3, g4)
    # workflow
    w1 = WorkflowFactory(process_key="w1")
    w1.security_history_my.add(g1, g3)
    w2 = WorkflowFactory(process_key="w2")
    w2.security_history_my.add(g2)
    w3 = WorkflowFactory(process_key="w3")
    w3.security_history_my.add(g2, g3)
    w4 = WorkflowFactory(process_key="w4")
    w4.security_history_my.add(g2, g3)
    w4.set_deleted(user)
    # test
    qs = Workflow.objects.security_history_my(user)
    assert ["w1", "w3"] == [x.process_key for x in qs]


@pytest.mark.django_db
def test_security_history_superuser():
    user = UserFactory(is_superuser=True)
    # workflow
    WorkflowFactory(process_key="w1")
    WorkflowFactory(process_key="w2")
    WorkflowFactory(process_key="w3")
    # test
    qs = Workflow.objects.security_history(user)
    assert ["w1", "w2", "w3"] == [x.process_key for x in qs]


@pytest.mark.django_db
def test_set_history_summary_columns():
    w = WorkflowFactory()
    MappingFactory(workflow=w, variable_name="m1")
    MappingFactory(workflow=w, variable_name="m2", history_summary_column=2)
    m3 = MappingFactory(workflow=w, variable_name="m3")
    m4 = MappingFactory(workflow=w, variable_name="m4")
    w.set_history_summary_columns([m3, m4])
    assert (
        Mapping.objects.get(variable_name="m1").history_summary_column is None
    )
    assert (
        Mapping.objects.get(variable_name="m2").history_summary_column is None
    )
    assert 1 == Mapping.objects.get(variable_name="m3").history_summary_column
    assert 2 == Mapping.objects.get(variable_name="m4").history_summary_column


@pytest.mark.django_db
def test_set_history_summary_columns_single_workflow():
    """Method should only update the current workflow."""
    workflow = WorkflowFactory()
    m1 = MappingFactory(
        workflow=workflow, variable_name="m1", history_summary_column=2
    )
    # different workflow
    MappingFactory(
        workflow=WorkflowFactory(), variable_name="m2", history_summary_column=1
    )
    workflow.set_history_summary_columns([None, m1])
    assert 1 == Mapping.objects.get(variable_name="m1").history_summary_column
    assert 1 == Mapping.objects.get(variable_name="m2").history_summary_column


@pytest.mark.django_db
def test_set_history_summary_columns_null_value():
    workflow = WorkflowFactory()
    m1 = MappingFactory(
        workflow=workflow, variable_name="m1", history_summary_column=2
    )
    MappingFactory(
        workflow=workflow, variable_name="m2", history_summary_column=1
    )
    workflow.set_history_summary_columns([m1, None])
    assert 1 == Mapping.objects.get(variable_name="m1").history_summary_column
    assert (
        Mapping.objects.get(variable_name="m2").history_summary_column is None
    )


@pytest.mark.django_db
def test_set_history_summary_columns_different_workflow():
    workflow = WorkflowFactory(process_key="abc")
    mapping = MappingFactory(
        workflow=WorkflowFactory(process_key="def"), variable_name="xyz"
    )
    with pytest.raises(WorkflowError) as e:
        workflow.set_history_summary_columns([mapping])
    assert (
        "Mapping for 'def/xyz' does not match the " "'abc' workflow"
    ) in str(e.value)


@pytest.mark.django_db
def test_str():
    workflow = WorkflowFactory(process_key="apple-cake")
    assert "apple-cake" == str(workflow)


@pytest.mark.django_db
def test_update_variables():
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="old_variable")
    # test
    workflow.update_variables(
        {"apple_pk": "long", "orange_pk": "long"}, UserFactory()
    )
    # check
    assert [
        "apple_pk",
        "orange_pk",
        "user_email",
        "user_full_name",
        "user_pk",
    ] == [row.variable_name for row in workflow.variables()]
    mapping = Mapping.objects.get(
        workflow=workflow, variable_name="old_variable"
    )
    assert mapping.is_deleted


@pytest.mark.django_db
def test_update_variables_already_deleted():
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="old_var", deleted=True)
    # test
    workflow.update_variables(
        {"apple_pk": "long", "orange_pk": "long"}, UserFactory()
    )
    # check
    assert [
        "apple_pk",
        "orange_pk",
        "user_email",
        "user_full_name",
        "user_pk",
    ] == [row.variable_name for row in workflow.variables()]
    mapping = Mapping.objects.get(workflow=workflow, variable_name="old_var")
    assert mapping.is_deleted


@pytest.mark.django_db
def test_security_type_as_str():
    workflow = WorkflowFactory(security_type=Workflow.PUBLIC)
    assert "Public" == workflow.security_type_as_str()


@pytest.mark.django_db
def test_security_type_as_str_system():
    workflow = WorkflowFactory(security_type=Workflow.SYSTEM)
    assert "System" == workflow.security_type_as_str()


@pytest.mark.django_db
def test_security_type_as_str_groups():
    group_1 = GroupFactory(name="Apple")
    group_2 = GroupFactory(name="Orange")
    workflow = WorkflowFactory(security_type=Workflow.GROUPS)
    workflow.groups.add(group_1)
    workflow.groups.add(group_2)
    assert "Groups (Apple, Orange)" == workflow.security_type_as_str()


@pytest.mark.django_db
def test_variables():
    workflow = WorkflowFactory()
    MappingFactory(workflow=workflow, variable_name="1")
    m2 = MappingFactory(workflow=workflow, variable_name="2")
    MappingFactory(workflow=workflow, variable_name="3")
    m2.set_deleted(UserFactory())
    assert ["1", "3"] == [row.variable_name for row in workflow.variables()]


@pytest.mark.django_db
def test_view_task_data():
    w = WorkflowFactory()
    MappingFactory(workflow=w, variable_name="m1", view_task_data=True)
    MappingFactory(workflow=w, variable_name="m2", view_task_data=False)
    m = MappingFactory(workflow=w, variable_name="m3", view_task_data=True)
    m.set_deleted(UserFactory())
    MappingFactory(workflow=w, variable_name="m4", view_task_data=True)
    MappingFactory(
        workflow=WorkflowFactory(), variable_name="m5", view_task_data=True
    )
    MappingFactory(
        workflow=w,
        variable_name="m6",
        history_summary=True,
        view_task_data=True,
    )
    assert ["m1", "m4", "m6"] == [x.variable_name for x in w.view_task_data()]
