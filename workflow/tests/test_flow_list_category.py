# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from workflow.tests.factories import (
    FlowListCategoryFactory,
    FlowListTypeFactory,
)
from workflow.models import FlowListCategory


@pytest.mark.django_db
def test_current():
    flow_list_type = FlowListTypeFactory(name="Colours")
    FlowListCategoryFactory(flow_list_type=flow_list_type, name="Z")
    flow_list_category = FlowListCategoryFactory(
        flow_list_type=flow_list_type, name="K"
    )
    flow_list_category.set_deleted(UserFactory())
    FlowListCategoryFactory(flow_list_type=flow_list_type, name="A")
    FlowListCategoryFactory(
        flow_list_type=FlowListTypeFactory(name="Apples"), name="C"
    )
    assert ["C", "A", "Z"] == [
        x.name for x in FlowListCategory.objects.current()
    ]


@pytest.mark.django_db
def test_current_flow_list_type():
    flow_list_type = FlowListTypeFactory(name="Colours")
    FlowListCategoryFactory(flow_list_type=flow_list_type, name="Z")
    flow_list_category = FlowListCategoryFactory(
        flow_list_type=flow_list_type, name="K"
    )
    flow_list_category.set_deleted(UserFactory())
    FlowListCategoryFactory(flow_list_type=flow_list_type, name="A")
    FlowListCategoryFactory(flow_list_type=FlowListTypeFactory(), name="B")
    assert ["A", "Z"] == [
        x.name for x in FlowListCategory.objects.current(flow_list_type)
    ]


@pytest.mark.django_db
def test_factory():
    flow_list_type = FlowListCategoryFactory(
        flow_list_type=FlowListTypeFactory(), name="Colours"
    )
    assert flow_list_type.name == "Colours"


@pytest.mark.django_db
def test_ordering():
    FlowListCategoryFactory(
        flow_list_type=FlowListTypeFactory(name="BB"), name="A"
    )
    FlowListCategoryFactory(
        flow_list_type=FlowListTypeFactory(name="AA"), name="Z"
    )
    assert ["Z", "A"] == [x.name for x in FlowListCategory.objects.all()]


@pytest.mark.django_db
def test_str():
    flow_list_type = FlowListCategoryFactory(
        flow_list_type=FlowListTypeFactory(name="Colours"),
        name="Pastel",
    )
    assert "Pastel (Colours)" == str(flow_list_type)
