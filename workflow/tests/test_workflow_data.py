# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.utils import timezone
from unittest import mock

from workflow.activiti import Variable
from workflow.models import (
    completed_reports,
    MappingPlugin,
    WorkflowData,
)
from report.tests.factories import (
    ReportSpecificationFactory,
    ReportScheduleFactory,
)
from .factories import (
    MappingFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowDataFactory,
    WorkflowFactory,
)


def _mock_historic_process():
    return {
        "size": 1,
        "order": "desc",
        "sort": "endTime",
        "start": 0,
        "total": 1,
        "data": [
            {
                "url": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/64",
                "deleteReason": None,
                "startUserId": "kermit",
                "name": None,
                "endActivityId": "sid-285E0710-74F1-4B10-80E3-97FFA88E5252",
                "endTime": "2017-06-14T10:39:11.188+01:00",
                "superProcessInstanceId": None,
                "startActivityId": "startEvent1",
                "startTime": "2017-06-14T10:38:59.001+01:00",
                "processDefinitionId": "timeOff:1:63",
                "tenantId": "",
                "processDefinitionName": "timeOff",
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "id": "64",
                "businessKey": None,
                "durationInMillis": 12187,
                "variables": [
                    {
                        "name": "contact",
                        "value": "joey",
                        "type": "string",
                        "scope": " local",
                    },
                    {
                        "name": "fromDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                    {
                        "name": "toDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                ],
            }
        ],
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "name": "objectPk",
                        "value": 29,
                        "type": "integer",
                        "scope": "local",
                    },
                    {
                        "name": "contact",
                        "value": "joey",
                        "type": "string",
                        "scope": " local",
                    },
                    {
                        "name": "toDate",
                        "value": "2017-06-27",
                        "type": "string",
                        "scope": "local",
                    },
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/61",
                "completed": True,
                "processDefinitionId": "timeOff:1:43",
                "processDefinitionName": "timeOff",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:43",
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


@pytest.mark.django_db
def test_completed_reports():
    report = ReportSpecificationFactory(slug=WorkflowData.REPORT_SLUG)
    ReportScheduleFactory(
        report=report,
        completed_date=timezone.now(),
        retries=1,
        parameters={"process_key": "timeOff"},
    )
    ReportScheduleFactory(
        report=report,
        completed_date=timezone.now(),
        retries=2,
        parameters={"process_key": "applePie"},
    )
    ReportScheduleFactory(
        report=report,
        completed_date=timezone.now(),
        retries=3,
        parameters={"process_key": "timeOff"},
    )
    assert [3, 1] == [x.retries for x in completed_reports("timeOff")]


@pytest.mark.django_db
def test_create_workflow_data():
    """

    .. note:: The form variables override the process variables because they
              haven't yet been written to the process.

    .. note:: This test sets the process status to ``completed``, so that it
              uses the data from the ``_historic_process`` API call.

    """
    sw = ScheduledWorkflowFactory(process_definition="timeOff")
    ScheduledWorkflowUserFactory(scheduled_workflow=sw, process_id=123)
    workflow = WorkflowFactory(process_key="timeOff")
    MappingFactory(
        workflow=workflow,
        variable_name="contact",
        mapping_type=MappingPlugin.IMPORT,
        data_download=True,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="from_date",
        mapping_type=MappingPlugin.IMPORT_DATE,
        data_download=False,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="to_date",
        mapping_type=MappingPlugin.IMPORT_DATE,
        data_download=True,
    )
    assert 0 == WorkflowData.objects.count()
    form_variables = {
        "from_date": Variable(value="2011-11-11", data_type=date),
        "to_date": Variable(value="2017-06-30", data_type=date),
    }
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process:
        mock_historic_process.return_value = _mock_historic_process()
        mock_process_status.return_value = _mock_process_status()
        result = WorkflowData.objects.create_workflow_data(123, form_variables)
    assert 1 == WorkflowData.objects.count()
    workflow_data = WorkflowData.objects.first()
    assert workflow_data == result
    assert {"contact": "joey", "to_date": "30/06/2017"} == workflow_data.data


@pytest.mark.django_db
def test_data_download():
    # workflow 1
    w1 = ScheduledWorkflowFactory(process_definition="timeOff")
    s1 = ScheduledWorkflowUserFactory(scheduled_workflow=w1, process_id=1)
    s2 = ScheduledWorkflowUserFactory(scheduled_workflow=w1, process_id=2)
    s3 = ScheduledWorkflowUserFactory(scheduled_workflow=w1, process_id=None)
    s4 = ScheduledWorkflowUserFactory(scheduled_workflow=w1, process_id=4)
    # workflow 2
    w2 = ScheduledWorkflowFactory(process_definition="invoiceApproval")
    s9 = ScheduledWorkflowUserFactory(scheduled_workflow=w2, process_id=2)
    # data
    WorkflowDataFactory(scheduled_workflow_user=s1, data="a")
    # a different workflow
    WorkflowDataFactory(scheduled_workflow_user=s9, data="b")
    # no data
    WorkflowDataFactory(scheduled_workflow_user=s2, data=None)
    # not created
    WorkflowDataFactory(scheduled_workflow_user=s3, data="d")
    # data
    WorkflowDataFactory(scheduled_workflow_user=s4, data="e")
    qs = WorkflowData.objects.data_download("timeOff")
    assert set(["a", "e"]) == {x.data for x in qs}


@pytest.mark.django_db
def test_factory():
    WorkflowDataFactory()


@pytest.mark.django_db
def test_pending_report():
    workflow = WorkflowFactory(process_key="timeOff")
    MappingFactory(workflow=workflow, variable_name="age", data_download=True)
    MappingFactory(workflow=workflow, variable_name="name", data_download=True)
    sw = ScheduledWorkflowFactory(process_definition="timeOff")
    su = ScheduledWorkflowUserFactory(scheduled_workflow=sw, process_id=2)
    # workflow data
    WorkflowDataFactory(scheduled_workflow_user=su, data={"age": 23})
    WorkflowDataFactory(scheduled_workflow_user=su, data={"name": "Pat"})
    WorkflowDataFactory(
        scheduled_workflow_user=su, data={"age": 45, "name": "Pat"}
    )
    qs = WorkflowData.objects.pending_report("timeOff")
    assert [{"age": 23}, {"name": "Pat"}, {"age": 45, "name": "Pat"}] == [
        x.data for x in qs.order_by("pk")
    ]


@pytest.mark.django_db
def test_str():
    w1 = ScheduledWorkflowFactory(process_definition="timeOff")
    s1 = ScheduledWorkflowUserFactory(scheduled_workflow=w1, process_id=1)
    obj = WorkflowDataFactory(scheduled_workflow_user=s1)
    assert "'timeOff' created" in str(obj)
