# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone
from unittest import mock

from login.tests.factories import UserFactory
from workflow.models import ScheduledWorkflowUser
from workflow.service import (
    delete_workflows_for_content_object,
    scheduled_workflow_user_for_content_object,
)
from workflow.tests.factories import (
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
)


def _mock_process_status(process_id=None):
    if process_id is None:
        process_id = 61
    return {
        "data": [
            {
                "businessKey": None,
                "id": process_id,
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    },
                    {
                        "value": "joey",
                        "type": "string",
                        "name": "contact",
                        "scope": " local",
                    },
                    {
                        "value": "admin",
                        "type": "string",
                        "name": "initiator",
                        "scope": "local",
                    },
                    {
                        "value": "Oil COSSH Assessment",
                        "type": "string",
                        "name": "documentTitle",
                        "scope": "local",
                    },
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": "http://localhost:8080/activiti-rest/service/runtime/process-instances/61",
                "completed": False,
                "processDefinitionName": "documentFamiliarisation",
                "processDefinitionId": "documentFamiliarisation:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/documentFamiliarisation:1:43",
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


@pytest.mark.django_db
def test_delete_workflows_for_content_object():
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        mock_process_status.return_value = _mock_process_status(61)
        user = UserFactory(username="patrick")
        workflow = ScheduledWorkflowFactory(content_object=user)
        ScheduledWorkflowUserFactory(process_id=61, scheduled_workflow=workflow)
        ScheduledWorkflowUserFactory(
            process_id=None, scheduled_workflow=workflow
        )
        count = delete_workflows_for_content_object(user, user)
        assert 2 == count
        x = ScheduledWorkflowUser.objects.get(process_id=61)
        assert x.deleted is False
        assert x.deleted_process is True
        assert (
            "Delete workflows for content object 'patrick' "
            "(User {})".format(user.pk)
        ) == x.deleted_comment
        assert timezone.now().date() == x.date_deleted_process.date()
        assert user == x.user_deleted_process
        obj = ScheduledWorkflowUser.objects.get(process_id__isnull=True)
        assert obj.deleted is True
        assert obj.deleted_process is False
        assert timezone.now().date() == obj.date_deleted.date()
        assert user == obj.user_deleted


@pytest.mark.django_db
def test_delete_workflows_for_content_object_process_definition():
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        mock_process_status.return_value = _mock_process_status()
        user = UserFactory()
        # workflow 1
        w1 = ScheduledWorkflowFactory(
            content_object=user, process_definition="1"
        )
        ScheduledWorkflowUserFactory(process_id=61, scheduled_workflow=w1)
        # workflow 2
        w2 = ScheduledWorkflowFactory(
            content_object=user, process_definition="2"
        )
        ScheduledWorkflowUserFactory(process_id=61, scheduled_workflow=w2)
        # test
        count = delete_workflows_for_content_object(
            user, user, process_definitions=["1"]
        )
        # assert
        assert 1 == count
        obj = ScheduledWorkflowUser.objects.get(
            process_id=61, scheduled_workflow=w1
        )
        assert obj.deleted_process is True
        obj = ScheduledWorkflowUser.objects.get(
            process_id=61, scheduled_workflow=w2
        )
        assert obj.deleted_process is False


@pytest.mark.django_db
def test_delete_workflows_for_content_object_uuid():
    with mock.patch(
        "workflow.activiti.Activiti._process_status"
    ) as mock_process_status, mock.patch(
        "workflow.activiti.Activiti.process_delete"
    ):
        mock_process_status.return_value = _mock_process_status(
            "d0d794a4-8b82-11ea-8aab-002248012004"
        )
        user = UserFactory()
        workflow = ScheduledWorkflowFactory(content_object=user)
        ScheduledWorkflowUserFactory(
            process_uuid="d0d794a4-8b82-11ea-8aab-002248012004",
            process_id=None,
            scheduled_workflow=workflow,
        )
        ScheduledWorkflowUserFactory(
            process_id=None, scheduled_workflow=workflow
        )
        assert 2 == delete_workflows_for_content_object(user, user)


@pytest.mark.django_db
def test_scheduled_workflow_user_for_content_object():
    user = UserFactory()
    workflow = ScheduledWorkflowFactory(content_object=user)
    ScheduledWorkflowUserFactory(process_id=99, scheduled_workflow=workflow)
    qs = scheduled_workflow_user_for_content_object(user)
    assert [99] == [obj.process_id for obj in qs]


@pytest.mark.django_db
def test_scheduled_workflow_user_for_content_object_process_id_none():
    """Find the scheduled workflow user records for a content object."""
    user = UserFactory()
    # process has been started
    w1 = ScheduledWorkflowFactory(content_object=user)
    ScheduledWorkflowUserFactory(process_id=99, scheduled_workflow=w1)
    # schedule is deleted (process_id would normally be None)
    w1 = ScheduledWorkflowFactory(content_object=user)
    ScheduledWorkflowUserFactory(
        process_id=11, scheduled_workflow=w1, deleted=True
    )
    # process was deleted
    w1 = ScheduledWorkflowFactory(content_object=user)
    ScheduledWorkflowUserFactory(
        process_id=12, scheduled_workflow=w1, deleted_process=True
    )
    # process hasn't been created
    w2 = ScheduledWorkflowFactory(content_object=user)
    ScheduledWorkflowUserFactory(process_id=None, scheduled_workflow=w2)
    # ignore - another user
    w3 = ScheduledWorkflowFactory(content_object=UserFactory())
    ScheduledWorkflowUserFactory(process_id=88, scheduled_workflow=w3)
    # test
    qs = scheduled_workflow_user_for_content_object(user)
    assert [99, None] == [obj.process_id for obj in qs.order_by("process_id")]
