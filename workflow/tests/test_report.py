# -*- encoding: utf-8 -*-
import csv
import io
import pytest
import tempfile

from report.models import ReportError
from report.tests.factories import ReportScheduleFactory
from workflow.report import TableReport, WorkflowDataReport
from workflow.tests.factories import (
    MappingFactory,
    ScheduledWorkflowFactory,
    ScheduledWorkflowUserFactory,
    WorkflowDataFactory,
    WorkflowFactory,
)


@pytest.mark.django_db
def test_table_report():
    data = [["fruit", "Apple"], ["country", "UK"], ["age", 23]]
    with io.BytesIO() as buff:
        report = TableReport()
        report.create(buff, data, "Some Data")
        pdf = buff.getvalue()
    with open("temp.pdf", "wb") as f:
        f.write(pdf)


@pytest.mark.django_db
def test_workflow_data():
    workflow = WorkflowFactory(process_key="timeOff")
    MappingFactory(
        workflow=workflow,
        variable_name="age",
        data_download=True,
        history_summary_order=1,
    )
    MappingFactory(
        workflow=workflow,
        variable_name="name",
        data_download=True,
        history_summary_order=2,
    )
    sw = ScheduledWorkflowFactory(process_definition="timeOff")
    su = ScheduledWorkflowUserFactory(scheduled_workflow=sw, process_id=2)
    # workflow data
    WorkflowDataFactory(scheduled_workflow_user=su, data={"age": 23})
    WorkflowDataFactory(scheduled_workflow_user=su, data={"name": "Pat"})
    WorkflowDataFactory(
        scheduled_workflow_user=su, data={"age": 45, "name": "Pat"}
    )
    # report
    report = WorkflowDataReport()
    parameters = {"process_key": "timeOff"}
    report.schedule = ReportScheduleFactory(parameters=parameters)
    with tempfile.NamedTemporaryFile() as f:
        with open(f.name, "w", newline="", encoding="utf-8") as csv_file:
            output_stream = csv.writer(csv_file, dialect="excel")
            # test
            assert report.run_csv_report(output_stream) is True
        # check
        with open(f.name, "rt") as csv_file:
            reader = csv.reader(csv_file, "excel")
            data = list(reader)
    assert [["age", "name"], ["23", ""], ["", "Pat"], ["45", "Pat"]] == data


@pytest.mark.django_db
def test_workflow_data_no_parameter():
    report = WorkflowDataReport()
    report.schedule = ReportScheduleFactory(parameters={})
    with tempfile.NamedTemporaryFile() as f:
        with open(f.name, "w", newline="", encoding="utf-8") as csv_file:
            output_stream = csv.writer(csv_file, dialect="excel")
            # test
            with pytest.raises(ReportError) as e:
                report.run_csv_report(output_stream)
    assert "ReportError, No 'parameters' for report schedule" in str(e.value)
