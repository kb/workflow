# -*- encoding: utf-8 -*-
import factory
import os
import pytest

from django.core.files import File
from unittest import mock

from login.tests.factories import UserFactory
from workflow.activiti import ActivitiError, Definition
from workflow.models import ProcessDefinition
from workflow.tests.factories import (
    MappingFactory,
    ProcessDefinitionFactory,
    WorkflowFactory,
)


@pytest.mark.django_db
def test_default_key_version_number():
    ProcessDefinitionFactory(
        process_key=ProcessDefinition.DEFAULT_KEY, version=1
    )
    ProcessDefinitionFactory(
        process_key=ProcessDefinition.DEFAULT_KEY, version=2
    )
    ProcessDefinitionFactory(process_key="a", version=22)
    assert 3 == ProcessDefinition.objects.default_key_version_number()


@pytest.mark.django_db
def test_default_key_version_number_empty():
    assert 1 == ProcessDefinition.objects.default_key_version_number()


@pytest.mark.django_db
def test_deploy():
    with mock.patch(
        "workflow.activiti.Activiti.deploy_process_definition"
    ), mock.patch(
        "workflow.activiti.Activiti.process_definition_for_deployment"
    ) as mock_process_definition_for_deployment:
        # mock return values
        data = Definition(
            process_key="Apple", name="Fruit", version=3, workflow_id="apple"
        )
        mock_process_definition_for_deployment.return_value = data
        # setup
        bpmn_file_name = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "data",
            "timeOffRequestV1.bpmn20.xml",
        )
        with open(bpmn_file_name) as bpmn_file:
            from_file = factory.django.FileField(from_file=bpmn_file)
            process_definition = ProcessDefinitionFactory(
                process_key=ProcessDefinition.DEFAULT_KEY,
                version=1,
                process_definition=from_file,
            )
            ProcessDefinition.objects.deploy(process_definition, UserFactory())


@pytest.mark.django_db
def test_deploy_twice():
    """Does deploy create a new version record?"""
    with mock.patch(
        "workflow.activiti.Activiti.deploy_process_definition"
    ), mock.patch(
        "workflow.activiti.Activiti.process_definition_for_deployment"
    ) as mock_process_definition_for_deployment:
        # mock return values
        data = Definition(
            process_key="Apple", name="Fruit", version=3, workflow_id="apple"
        )
        mock_process_definition_for_deployment.return_value = data
        # setup
        bpmn_file_name_1 = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "data",
            "timeOffRequestV1.bpmn20.xml",
        )
        # version 1
        with open(bpmn_file_name_1) as bpmn_file_1:
            from_file = factory.django.FileField(from_file=bpmn_file_1)
            process_definition = ProcessDefinitionFactory(
                process_key=ProcessDefinition.DEFAULT_KEY,
                version=1,
                process_definition=from_file,
            )
            ProcessDefinition.objects.deploy(process_definition, UserFactory())
        assert 1 == ProcessDefinition.objects.count()
        # version 2
        bpmn_file_name_2 = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "data",
            "timeOffRequestV1.bpmn20.xml",
        )
        with open(bpmn_file_name_2) as bpmn_file_2:
            django_file = File(bpmn_file_2)
            process_definition.process_definition.save(
                "timeOffRequestV1.bpmn20.xml", django_file, save=True
            )
            ProcessDefinition.objects.deploy(process_definition, UserFactory())
        assert 1 == ProcessDefinition.objects.count()
        process_definition = ProcessDefinition.objects.first()
        assert "Apple" == process_definition.process_key
        assert 2 == process_definition.version
        assert "apple" == process_definition.workflow_id


@pytest.mark.django_db
def test_get_latest_version():
    ProcessDefinitionFactory(process_key="p", version=1)
    p3 = ProcessDefinitionFactory(process_key="p", version=3)
    ProcessDefinitionFactory(process_key="p", version=2)
    result = ProcessDefinition.objects.get_latest_version("p")
    assert p3.pk == result.pk


@pytest.mark.django_db
def test_get_latest_version_empty():
    with pytest.raises(ActivitiError) as e:
        ProcessDefinition.objects.get_latest_version("p")
    assert "workflow process key 'p' does not exist" in str(e.value)


@pytest.mark.django_db
def test_get_latest_version_or_zero():
    version = ProcessDefinition.objects.get_latest_version_or_zero("p")
    assert 0 == version


@pytest.mark.django_db
def test_get_latest_version_or_zero_two():
    ProcessDefinitionFactory(process_key="p", version=2)
    version = ProcessDefinition.objects.get_latest_version_or_zero("p")
    assert 2 == version


@pytest.mark.django_db
def test_parse_xml():
    xml = """
        <?xml version="1.0" encoding="UTF-8"?>
        <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.flowable.org/processdef">
          <process id="timeOffRequest" name="timeOffRequest" isExecutable="true">
            <startEvent id="startEvent1" flowable:isInterrupting="false">
              <extensionElements>
                <flowable:formProperty id="managerUserName" type="string" required="true"></flowable:formProperty>
                <flowable:formProperty id="managerUserPk" type="long" required="true"></flowable:formProperty>
                <flowable:formProperty id="objectPk" type="long" required="true"></flowable:formProperty>
                <flowable:formProperty id="userName" type="string" required="true"></flowable:formProperty>
                <flowable:formProperty id="userPk" type="long" required="true"></flowable:formProperty>
              </extensionElements>
            </startEvent>
            <userTask id="sid-EC1A150E-131B-4758-86BF-10025A5D2993" name="Please fill out the form and check before submitting your request to your manager" flowable:assignee="${userPk}" flowable:dueDate="P30D" flowable:formKey="flow">
              <documentation>When you have finished please click the 'Complete Task' button to send it to your Manager:</documentation>
              <extensionElements>
                <flowable:formProperty id="acceptRequest" type="enum" required="true"></flowable:formProperty>
                <flowable:formProperty id="auditDescription" name="Audit Description" type="string" expression="Time off requested" required="true"></flowable:formProperty>
                <flowable:formProperty id="checkTaskPk" type="long" required="true"></flowable:formProperty>
                <flowable:formProperty id="checkTaskText" type="string" required="true"></flowable:formProperty>
                <flowable:formProperty id="documentCode" type="string" required="true"></flowable:formProperty>
                <flowable:formProperty id="documentTitle" type="string" required="true"></flowable:formProperty>
                <flowable:formProperty id="editMetadata" type="boolean" expression="True"></flowable:formProperty>
                <flowable:formProperty id="fromDate" type="date" datePattern="MM-dd-yyyy" required="true"></flowable:formProperty>
                <modeler:initiator-can-complete xmlns:modeler="http://flowable.com/modeler">false</modeler:initiator-can-complete>
              </extensionElements>
            </userTask>
            <sequenceFlow id="sid-AFC57D8B-A054-4513-AE16-E5A8C4CE6E88" sourceRef="startEvent1" targetRef="sid-EC1A150E-131B-4758-86BF-10025A5D2993"></sequenceFlow>
            <exclusiveGateway id="sid-93F66AE0-3F0A-4C84-8D58-4B256B55FAD5" default="sid-07C8A0E9-12C4-42CC-9F03-54B56D8EF307"></exclusiveGateway>
          </process>
          <bpmndi:BPMNDiagram id="BPMNDiagram_timeOffRequest">
            <bpmndi:BPMNPlane bpmnElement="timeOffRequest" id="BPMNPlane_timeOffRequest">
              <bpmndi:BPMNShape bpmnElement="startEvent1" id="BPMNShape_startEvent1">
                <omgdc:Bounds height="30.0" width="30.0" x="0.0" y="238.0"></omgdc:Bounds>
              </bpmndi:BPMNShape>
            </bpmndi:BPMNPlane>
          </bpmndi:BPMNDiagram>
        </definitions>
    """
    process_definition = ProcessDefinitionFactory()
    result = process_definition.parse_xml(xml)
    expect = {
        "accept_request": "enum",
        "check_task_pk": "long",
        "check_task_text": "string",
        "document_code": "string",
        "document_title": "string",
        "from_date": "date",
        "manager_user_name": "string",
        "manager_user_pk": "long",
    }
    assert expect == result


@pytest.mark.django_db
def test_parse_xml_no_user_pk():
    xml = """
        <?xml version="1.0" encoding="UTF-8"?>
        <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.flowable.org/processdef">
          <process id="timeOffRequest" name="timeOffRequest" isExecutable="true">
            <startEvent id="startEvent1" flowable:isInterrupting="false">
              <extensionElements>
                <flowable:formProperty id="objectPk" type="long" required="true"></flowable:formProperty>
              </extensionElements>
            </startEvent>
            <userTask id="sid-EC1A150E-131B-4758-86BF-10025A5D2993" name="Please fill out the form and check before submitting your request to your manager" flowable:assignee="${userPk}" flowable:dueDate="P30D" flowable:formKey="flow">
              <documentation>When you have finished please click the 'Complete Task' button to send it to your Manager:</documentation>
              <extensionElements>
                <flowable:formProperty id="fromDate" type="date" datePattern="MM-dd-yyyy" required="true"></flowable:formProperty>
                <modeler:initiator-can-complete xmlns:modeler="http://flowable.com/modeler">false</modeler:initiator-can-complete>
              </extensionElements>
            </userTask>
            <sequenceFlow id="sid-AFC57D8B-A054-4513-AE16-E5A8C4CE6E88" sourceRef="startEvent1" targetRef="sid-EC1A150E-131B-4758-86BF-10025A5D2993"></sequenceFlow>
            <exclusiveGateway id="sid-93F66AE0-3F0A-4C84-8D58-4B256B55FAD5" default="sid-07C8A0E9-12C4-42CC-9F03-54B56D8EF307"></exclusiveGateway>
          </process>
          <bpmndi:BPMNDiagram id="BPMNDiagram_timeOffRequest">
            <bpmndi:BPMNPlane bpmnElement="timeOffRequest" id="BPMNPlane_timeOffRequest">
              <bpmndi:BPMNShape bpmnElement="startEvent1" id="BPMNShape_startEvent1">
                <omgdc:Bounds height="30.0" width="30.0" x="0.0" y="238.0"></omgdc:Bounds>
              </bpmndi:BPMNShape>
            </bpmndi:BPMNPlane>
          </bpmndi:BPMNDiagram>
        </definitions>
    """
    process_definition = ProcessDefinitionFactory(process_key="appleCake")
    with pytest.raises(ActivitiError) as e:
        process_definition.parse_xml(xml)
    expect = (
        "ScheduledWorkflow - cannot create workflow. "
        "The 'user_pk' variable is not defined in the 'startEvent' "
        "for 'appleCake' (variables ['from_date', 'object_pk']). "
        "The 'user_pk' is used by the 'started_by' filter for "
        "workflow processes"
    )
    assert expect in str(e.value)


@pytest.mark.django_db
def test_str():
    obj = ProcessDefinitionFactory()
    assert "incompleteDeployment 0" == str(obj)


@pytest.mark.django_db
def test_update_variables():
    bpmn_file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "data",
        "timeOffRequestV1.bpmn20.xml",
    )
    with open(bpmn_file_name) as bpmn_file:
        from_file = factory.django.FileField(from_file=bpmn_file)
        process_definition = ProcessDefinitionFactory(
            process_key=ProcessDefinition.DEFAULT_KEY,
            version=1,
            process_definition=from_file,
        )
        workflow = WorkflowFactory(process_key=process_definition.process_key)
    MappingFactory(workflow=workflow, variable_name="old_variable")
    process_definition.update_variables(UserFactory())
    result = [row.variable_name for row in workflow.variables()]
    assert [
        "orange_pk",
        "request_comment",
        "to_date",
        "user_email",
        "user_full_name",
        "user_pk",
    ] == result
