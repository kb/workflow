# -*- encoding: utf-8 -*-
"""
More 'FlowList' tests in 'example_work/tests/test_flow_list.py'.
"""
import pytest

from workflow.tests.factories import (
    FlowListCategoryFactory,
    FlowListFactory,
    FlowListTypeFactory,
)
from workflow.models import FlowList


@pytest.mark.django_db
def test_factory():
    flow_list_type = FlowListTypeFactory(name="Colours")
    flow_list = FlowListFactory(flow_list_type=flow_list_type, name="Green")
    assert "Green" == flow_list.name


@pytest.mark.django_db
def test_str():
    flow_list_type = FlowListTypeFactory(name="Colours")
    flow_list = FlowListFactory(flow_list_type=flow_list_type, name="Green")
    assert "Green (Colours)" == str(flow_list)


@pytest.mark.django_db
def test_str_category():
    flow_list_type = FlowListTypeFactory(name="Colours")
    flow_list_category = FlowListCategoryFactory(
        flow_list_type=flow_list_type, name="Pastel"
    )
    flow_list = FlowListFactory(
        flow_list_type=flow_list_type,
        category=flow_list_category,
        name="Green",
    )
    assert "Green - Pastel (Colours)" == str(flow_list)


@pytest.mark.django_db
def test_ordering():
    FlowListFactory(
        flow_list_type=FlowListTypeFactory(name="Weather"),
        name="Rain",
    )
    flow_list_type = FlowListTypeFactory(name="Colours")
    FlowListFactory(flow_list_type=flow_list_type, name="Red")
    FlowListFactory(flow_list_type=flow_list_type, name="Blue")
    assert ["Blue", "Red", "Rain"] == [x.name for x in FlowList.objects.all()]
