# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from workflow.models import Mapping, MappingPlugin
from workflow.tests.factories import MappingFactory


@pytest.mark.django_db
def test_mapping_update_config(client):
    user = UserFactory(is_superuser=True)
    obj = MappingFactory(mapping_type=MappingPlugin.USER, data_download=False)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.mapping.update.config", args=[obj.pk])
    data = {
        "mapping_type": MappingPlugin.GROUP,
        "help_text": "Can I help?",
        "css_class": "pure-input-2-3",
        "data_download": True,
        "history_summary_order": 12,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("workflow.mapping.list", args=[obj.workflow.pk])
    assert expect == response["Location"]
    obj.refresh_from_db()
    assert "group" == obj.mapping_type
    assert 12 == obj.history_summary_order
    assert "Can I help?" == obj.help_text
    assert "pure-input-2-3" == obj.css_class
    assert obj.data_download is True


@pytest.mark.parametrize(
    "mapping_type",
    [
        MappingPlugin.DECIMAL,
        MappingPlugin.GROUP,
        MappingPlugin.IMPORT,
        MappingPlugin.IMPORT_DECIMAL,
        MappingPlugin.INTEGER,
        MappingPlugin.UNDEFINED,
        MappingPlugin.USER,
    ],
)
@pytest.mark.django_db
def test_mapping_update_config_form_fields(client, mapping_type):
    """The user can change the mapping type for most types."""
    user = UserFactory(is_superuser=True)
    obj = MappingFactory(mapping_type=mapping_type)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.mapping.update.config", args=[obj.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert [
        "mapping_type",
        "help_text",
        "css_class",
        "multi_line",
        "history_summary",
        "history_summary_order",
        "data_download",
        "view_task_data",
    ] == [x for x in response.context["form"].fields.keys()]


@pytest.mark.parametrize(
    "mapping_type", [MappingPlugin.ENUM, MappingPlugin.IMPORT_DATE]
)
@pytest.mark.django_db
def test_mapping_update_config_form_fields_date(client, mapping_type):
    """The user cannot change the mapping type for date (or enum) fields."""
    user = UserFactory(is_superuser=True)
    obj = MappingFactory(mapping_type=mapping_type)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    url = reverse("workflow.mapping.update.config", args=[obj.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert [
        "help_text",
        "css_class",
        "multi_line",
        "history_summary",
        "history_summary_order",
        "data_download",
        "view_task_data",
    ] == [x for x in response.context["form"].fields.keys()]


@pytest.mark.django_db
def test_mapping_update_config_user(client):
    user = UserFactory(is_superuser=True)
    x = MappingFactory(
        variable_name="student_pk",
        mapping_type=MappingPlugin.USER,
        data_download=False,
    )
    assert client.login(username=user.username, password=TEST_PASSWORD)
    assert 1 == Mapping.objects.count()
    url = reverse("workflow.mapping.update.config", args=[x.pk])
    data = {
        "mapping_type": MappingPlugin.USER,
        "help_text": "Can I help?",
        "css_class": "pure-input-2-3",
        "data_download": True,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("workflow.mapping.list", args=[x.workflow.pk])
    assert expect == response.url
    assert 3 == Mapping.objects.count()
    assert set(
        [
            (MappingPlugin.USER, "student_pk"),
            (MappingPlugin.AUTO_GENERATE, "student_email"),
            (MappingPlugin.AUTO_GENERATE, "student_full_name"),
        ]
    ) == set([(x.mapping_type, x.variable_name) for x in Mapping.objects.all()])
