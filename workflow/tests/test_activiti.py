# -*- encoding: utf-8 -*-
import logging
import pytest
import pytz
import responses
import uuid
import workflow

from datetime import date, datetime
from dateutil.tz import tzoffset
from django.conf import settings
from django.utils.timezone import localtime
from enum import Enum
from http import HTTPStatus
from unittest import mock

from workflow.activiti import (
    Activiti,
    build_django_url,
    data_to_snake,
    HistoricProcessInstance,
    int_if_possible,
    process_id_to_key,
    snake_to_camel_case,
    Variable,
    variables_as_detail,
)


def _mock_deploy_process_definition():
    return [
        201,
        {
            "deploymentTime": "2016-05-27T16:32:07.005+01:00",
            "category": None,
            "tenantId": "",
            "name": "vacationRequest2Manager.bpmn20.xml",
            "id": "296",
            "url": (
                "http://localhost:8080/activiti-rest/service/repository/"
                "deployments/296"
            ),
        },
    ]


def _mock_deployments():
    """Mock data for the ``Activiti`` ``deployments`` method."""
    return {
        "order": "asc",
        "size": 4,
        "total": 4,
        "data": [
            {
                "id": "20",
                "tenantId": "",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20"
                ),
                "name": "Demo processes",
                "category": None,
                "deploymentTime": "2016-05-27T08:35:32.993+01:00",
            },
            {
                "id": "40",
                "tenantId": "",
                "url": (
                    "http://localhost:8080/activiti-rest/service/"
                    "repository/deployments/40"
                ),
                "name": "vacationRequest2Manager.bpmn20.xml",
                "category": None,
                "deploymentTime": "2016-05-27T15:43:20.861+01:00",
            },
            {
                "id": "44",
                "tenantId": "",
                "url": (
                    "http://localhost:8080/activiti-rest/service/"
                    "repository/deployments/44"
                ),
                "name": "vacationRequest2Manager.bpmn20.xml",
                "category": None,
                "deploymentTime": "2016-05-27T15:43:35.422+01:00",
            },
            {
                "id": "48",
                "tenantId": "",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/48"
                ),
                "name": "vacationRequest2Manager.bpmn20.xml",
                "category": None,
                "deploymentTime": "2016-05-27T15:43:50.236+01:00",
            },
        ],
        "sort": "id",
        "start": 0,
    }


def _mock_form_variable_info():
    return {
        "formProperties": [
            {
                "writable": True,
                "value": None,
                "required": True,
                "readable": True,
                "type": "long",
                "name": "Number of days",
                "id": "numberOfDays",
                "enumValues": [],
                "datePattern": None,
            },
            {
                "writable": True,
                "value": None,
                "required": True,
                "readable": True,
                "type": "date",
                "name": "First day of holiday (dd-MM-yyy)",
                "id": "startDate",
                "enumValues": [],
                "datePattern": "dd-MM-yyyy hh:mm",
            },
            {
                "writable": True,
                "value": None,
                "required": False,
                "readable": True,
                "type": "string",
                "name": "Motivation",
                "id": "vacationMotivation",
                "enumValues": [],
                "datePattern": None,
            },
            {
                "writable": True,
                "value": None,
                "required": False,
                "readable": True,
                "type": "string",
                "name": "Manager",
                "id": "manager",
                "enumValues": [],
                "datePattern": None,
            },
        ],
        "taskUrl": None,
        "processDefinitionUrl": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "process-definitions/vacationRequest:4:51"
        ),
        "taskId": None,
        "processDefinitionId": "vacationRequest:4:51",
        "formKey": None,
        "deploymentId": "48",
    }


def _mock_historic_process(process_id):
    return {
        "size": 1,
        "order": "desc",
        "sort": "endTime",
        "start": 0,
        "total": 1,
        "data": [
            {
                "url": "http://localhost:8080/activiti-rest/service/history/historic-process-instances/64",
                "deleteReason": None,
                "startUserId": "kermit",
                "name": None,
                "endActivityId": "sid-285E0710-74F1-4B10-80E3-97FFA88E5252",
                "endTime": "2017-06-14T10:39:11.188+01:00",
                "superProcessInstanceId": None,
                "startActivityId": "startEvent1",
                "startTime": "2017-06-14T10:38:59.001+01:00",
                "processDefinitionId": "timeOff:1:63",
                "tenantId": "",
                "processDefinitionName": "timeOff",
                "processDefinitionUrl": "http://localhost:8080/activiti-rest/service/repository/process-definitions/timeOffRequest:1:63",
                "id": process_id,
                "businessKey": None,
                "durationInMillis": 12187,
                "variables": [
                    {
                        "name": "contact",
                        "value": "joey",
                        "type": "string",
                        "scope": " local",
                    },
                    {
                        "name": "fromDate",
                        "value": "2017-06-27",
                        "type": "date",
                        "scope": "local",
                    },
                    {
                        "name": "toDate",
                        "value": "2017-06-27",
                        "type": "date",
                        "scope": "local",
                    },
                ],
            }
        ],
    }


def _mock_process_definition():
    return {
        "graphicalNotationDefined": False,
        "suspended": False,
        "resource": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "deployments/20/resources/createTimersProcess.bpmn20.xml"
        ),
        "name": "Create timers process",
        "startFormDefined": False,
        "id": "createTimersProcess:1:36",
        "diagramResource": None,
        "tenantId": "",
        "deploymentUrl": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "deployments/20"
        ),
        "key": "createTimersProcess",
        "url": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "process-definitions/createTimersProcess:1:36"
        ),
        "deploymentId": "20",
        "category": "Examples",
        "description": "Test process to create a number of timers.",
        "version": 1,
    }


def _mock_process_definitions():
    """Mock data for the ``Activiti`` ``process_definitions`` method."""
    return {
        "total": 5,
        "sort": "name",
        "data": [
            {
                "graphicalNotationDefined": False,
                "suspended": False,
                "resource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20/resources/createTimersProcess.bpmn20.xml"
                ),
                "name": "Create timers process",
                "startFormDefined": False,
                "id": "createTimersProcess:1:36",
                "diagramResource": None,
                "tenantId": "",
                "deploymentUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20"
                ),
                "key": "createTimersProcess",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/createTimersProcess:1:36"
                ),
                "deploymentId": "20",
                "category": "Examples",
                "description": "Test process to create a number of timers.",
                "version": 1,
            },
            {
                "graphicalNotationDefined": False,
                "suspended": False,
                "resource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20/resources/oneTaskProcess.bpmn20.xml"
                ),
                "name": "Famous One Task Process",
                "startFormDefined": False,
                "id": "oneTaskProcess:1:35",
                "diagramResource": None,
                "tenantId": "",
                "deploymentUrl": (
                    "http://localhost:8080/activiti-rest/"
                    "service/repository/deployments/20"
                ),
                "key": "oneTaskProcess",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/oneTaskProcess:1:35"
                ),
                "deploymentId": "20",
                "category": "Examples",
                "description": None,
                "version": 1,
            },
            {
                "graphicalNotationDefined": False,
                "suspended": False,
                "resource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20/resources/Helpdesk.bpmn20.xml"
                ),
                "name": "Helpdesk process",
                "startFormDefined": False,
                "id": "escalationExample:1:34",
                "diagramResource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20/resources/Helpdesk.png"
                ),
                "tenantId": "",
                "deploymentUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/20"
                ),
                "key": "escalationExample",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/escalationExample:1:34"
                ),
                "deploymentId": "20",
                "category": "Examples",
                "description": None,
                "version": 1,
            },
            {
                "graphicalNotationDefined": True,
                "suspended": False,
                "resource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/40/resources/"
                    "vacationRequest2Manager.bpmn20.xml"
                ),
                "name": "Vacation request",
                "startFormDefined": False,
                "id": "vacationRequest:2:43",
                "diagramResource": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/40/resources/"
                    "vacationRequest2Manager.vacationRequest.png"
                ),
                "tenantId": "",
                "deploymentUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "deployments/40"
                ),
                "key": "vacationRequest",
                "url": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/vacationRequest:2:43"
                ),
                "deploymentId": "40",
                "category": "http://activiti.org/bpmn20",
                "description": None,
                "version": 2,
            },
        ],
        "start": 0,
        "order": "asc",
        "size": 5,
    }


def _mock_process_start():
    return {
        "suspended": False,
        "processDefinitionId": "vacationRequest:4:51",
        "activityId": None,
        "variables": [],
        "processDefinitionUrl": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "process-definitions/vacationRequest:4:51"
        ),
        "ended": False,
        "url": (
            "http://localhost:8080/activiti-rest/service/runtime/"
            "process-instances/277"
        ),
        "businessKey": None,
        "completed": False,
        "tenantId": "",
        "id": "277",
    }


def _mock_process_status():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "value": 29,
                        "type": "integer",
                        "name": "objectPk",
                        "scope": "local",
                    },
                    {
                        "value": "joey",
                        "type": "string",
                        "name": "contact",
                        "scope": " local",
                    },
                    {
                        "value": "admin",
                        "type": "string",
                        "name": "initiator",
                        "scope": "local",
                    },
                    {
                        "value": "Oil COSSH Assessment",
                        "type": "string",
                        "name": "documentTitle",
                        "scope": "local",
                    },
                ],
                "startTime": "2016-05-29T18:36:25.000+01:00",
                "startUserId": "kermit",
                "suspended": False,
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/61"
                ),
                "completed": False,
                "processDefinitionName": "documentFamiliarisation",
                "processDefinitionId": "documentFamiliarisation:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/documentFamiliarisation:1:43"
                ),
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


def _mock_process_status_missing_object_pk():
    return {
        "data": [
            {
                "businessKey": None,
                "id": "61",
                "variables": [
                    {
                        "value": "joey",
                        "type": "string",
                        "name": "contact",
                        "scope": " local",
                    },
                    {
                        "value": "admin",
                        "type": "string",
                        "name": "initiator",
                        "scope": "local",
                    },
                    {
                        "value": "Oil COSSH Assessment",
                        "type": "string",
                        "name": "documentTitle",
                        "scope": "local",
                    },
                ],
                "suspended": False,
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/61"
                ),
                "completed": False,
                "processDefinitionId": "documentFamiliarisation:1:43",
                "activityId": None,
                "ended": False,
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/documentFamiliarisation:1:43"
                ),
                "tenantId": "",
            }
        ],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


def _mock_process_status_multi_row():
    return {
        "data": [{"id1": "61"}, {"id2": "62"}],
        "start": 0,
        "total": 1,
        "sort": "id",
        "order": "asc",
        "size": 1,
    }


def _mock_response(status_code):
    class MockResponse:
        def __init__(self, status_code):
            self.reason = "mock-reason"
            self.status_code = status_code
            self.url = "http://mock-url/"

        def json(self):
            return {}

    return MockResponse(status_code)


def _mock_response_json(json_data, status_code):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.reason = "mock-reason"
            self.status_code = status_code
            self.url = "http://mock-url/"

        def json(self):
            return self.json_data

    return MockResponse(json_data, status_code)


def _mock_task_status():
    return {
        "url": "http://localhost:8080/activiti-rest/service/runtime/tasks/336",
        "suspended": False,
        "parentTaskUrl": None,
        "processInstanceUrl": (
            "http://localhost:8080/activiti-rest/service/runtime/"
            "process-instances/321"
        ),
        "id": "336",
        "variables": [],
        "priority": 50,
        "processDefinitionId": "documentFamiliarisation:1:303",
        "delegationState": None,
        "assignee": "45",
        "formKey": None,
        "processInstanceId": "321",
        "parentTaskId": None,
        "executionUrl": (
            "http://localhost:8080/activiti-rest/service/runtime/executions/"
            "333"
        ),
        "processDefinitionUrl": (
            "http://localhost:8080/activiti-rest/service/repository/"
            "process-definitions/documentFamiliarisation:1:303"
        ),
        "name": "Oil COSSH Assessment",
        "createTime": "2016-05-27T16:52:42.248+01:00",
        "category": None,
        "tenantId": "",
        "dueDate": None,
        "description": (
            "You are required you to be familiar with the document Oil COSSH "
            "Assessment (29) to safely carry out your work. admin"
        ),
        "executionId": "333",
        "owner": None,
        "taskDefinitionKey": "familiarisationConfirmed",
    }


def _mock_task_identity_list():
    return [
        {
            "url": (
                "http://localhost:8080/activiti-rest/service/runtime/tasks"
                "/2843/identitylinks/groups/1/candidate"
            ),
            "user": None,
            "group": "111",
            "type": "candidate",
        }
    ]


def _mock_user_task_list():
    return {
        "start": 0,
        "total": 4,
        "data": [
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/tasks"
                    "/110"
                ),
                "processDefinitionId": "vacationRequest:4:51",
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/vacationRequest:4:51"
                ),
                "executionUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "executions/107"
                ),
                "description": (
                    "${employeeName} would like to take "
                    "${numberOfDays} day(s) of vacation (Motivation: "
                    "${vacationMotivation})."
                ),
                "priority": 50,
                "tenantId": "",
                "processInstanceId": "277",
                "parentTaskId": None,
                "category": None,
                "variables": [
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "employeeName",
                        "value": "kermit",
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "manager",
                        "value": "fozzie",
                    },
                    {
                        "type": "integer",
                        "scope": "global",
                        "name": "numberOfDays",
                        "value": 5,
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "startDate",
                        "value": "2016-06-12",
                    },
                ],
                "parentTaskUrl": None,
                "executionId": "107",
                "delegationState": None,
                "formKey": None,
                "taskDefinitionKey": "handleRequest",
                "assignee": "fozzie",
                "id": "110",
                "suspended": False,
                "createTime": "2016-05-27T15:57:51.139+01:00",
                "processInstanceUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/277"
                ),
                "name": "Handle vacation request",
                "owner": None,
                "dueDate": None,
            },
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "tasks/125"
                ),
                "processDefinitionId": "vacationRequest:4:51",
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/vacationRequest:4:51"
                ),
                "executionUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "executions/122"
                ),
                "description": (
                    "${employeeName} would like to take ${numberOfDays} "
                    "day(s) of vacation (Motivation: ${vacationMotivation})."
                ),
                "priority": 50,
                "tenantId": "",
                "processInstanceId": "112",
                "parentTaskId": None,
                "category": None,
                "variables": [
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "employeeName",
                        "value": "kermit",
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "manager",
                        "value": "fozzie",
                    },
                    {
                        "type": "integer",
                        "scope": "global",
                        "name": "numberOfDays",
                        "value": 5,
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "startDate",
                        "value": "2016-06-12",
                    },
                ],
                "parentTaskUrl": None,
                "executionId": "122",
                "delegationState": None,
                "formKey": None,
                "taskDefinitionKey": "handleRequest",
                "assignee": "fozzie",
                "id": "125",
                "suspended": False,
                "createTime": "2016-05-27T15:58:07.543+01:00",
                "processInstanceUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/112"
                ),
                "name": "Handle vacation request",
                "owner": None,
                "dueDate": None,
            },
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "tasks/140"
                ),
                "processDefinitionId": "vacationRequest:4:51",
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/vacationRequest:4:51"
                ),
                "executionUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "executions/137"
                ),
                "description": (
                    "${employeeName} would like to take "
                    "${numberOfDays} day(s) of vacation (Motivation: "
                    "${vacationMotivation})."
                ),
                "priority": 50,
                "tenantId": "",
                "processInstanceId": "127",
                "parentTaskId": None,
                "category": None,
                "variables": [
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "employeeName",
                        "value": "kermit",
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "manager",
                        "value": "fozzie",
                    },
                    {
                        "type": "integer",
                        "scope": "global",
                        "name": "numberOfDays",
                        "value": 5,
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "startDate",
                        "value": "2016-06-12",
                    },
                ],
                "parentTaskUrl": None,
                "executionId": "137",
                "delegationState": None,
                "formKey": None,
                "taskDefinitionKey": "handleRequest",
                "assignee": "fozzie",
                "id": "140",
                "suspended": False,
                "createTime": "2016-05-27T16:00:09.697+01:00",
                "processInstanceUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/127"
                ),
                "name": "Handle vacation request",
                "owner": None,
                "dueDate": None,
            },
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "tasks/155"
                ),
                "processDefinitionId": "vacationRequest:4:51",
                "processDefinitionUrl": (
                    "http://localhost:8080/activiti-rest/service/repository/"
                    "process-definitions/vacationRequest:4:51"
                ),
                "executionUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "executions/152"
                ),
                "description": (
                    "${employeeName} would like to take ${numberOfDays} "
                    "day(s) of vacation (Motivation: ${vacationMotivation})."
                ),
                "priority": 50,
                "tenantId": "",
                "processInstanceId": "142",
                "parentTaskId": None,
                "category": None,
                "variables": [
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "employeeName",
                        "value": "kermit",
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "manager",
                        "value": "fozzie",
                    },
                    {
                        "type": "integer",
                        "scope": "global",
                        "name": "numberOfDays",
                        "value": 5,
                    },
                    {
                        "type": "string",
                        "scope": "global",
                        "name": "startDate",
                        "value": "2016-06-12",
                    },
                ],
                "parentTaskUrl": None,
                "executionId": "152",
                "delegationState": None,
                "formKey": None,
                "taskDefinitionKey": "handleRequest",
                "assignee": "fozzie",
                "id": "155",
                "suspended": False,
                "createTime": "2016-05-27T16:01:11.015+01:00",
                "processInstanceUrl": (
                    "http://localhost:8080/activiti-rest/service/runtime/"
                    "process-instances/142"
                ),
                "name": "Handle vacation request",
                "owner": None,
                "dueDate": None,
            },
        ],
        "sort": "id",
        "order": "asc",
        "size": 4,
    }


def deploy_vacation_request():
    activiti = workflow.activiti.Activiti()
    resp = activiti.deploy_process_definition(
        "example/tests/data/vacationRequest2Manager.bpmn20.xml"
    )
    if "processDefinitionId" in resp:
        return resp["processDefinitionId"]
    return None


# PJK 21/04/2020, I checked our projects and I don't think
# 'process_definition_by_key' is used anywhere
# (other than in the tests for this app).
#
# def get_vacation_request_process_definition():
#    activiti = workflow.activiti.Activiti()
#    if not activiti.is_deployed("vacationRequest2Manager.bpmn20.xml"):
#        holiday_request_process_id = deploy_vacation_request()
#    else:
#        process = activiti.process_definition_by_key("vacationRequest")
#        holiday_request_process_id = process["id"]
#    return holiday_request_process_id


def test_activiti_date():
    result = workflow.activiti.activiti_date("2016-05-29T18:36:25.000+01:00")
    expect = datetime(2016, 5, 29, 17, 36, 25, tzinfo=pytz.utc)
    assert localtime(expect) == localtime(result)


def test_activiti_date_empty():
    result = workflow.activiti.activiti_date("")
    assert result is None


def test_activiti_date_very_old():
    """This is the year 202, but the function is still working!"""
    result = workflow.activiti.activiti_date("202-01-06")
    assert datetime(202, 1, 6, 0, 0) == result


def test_activiti_date_none():
    result = workflow.activiti.activiti_date(None)
    assert result is None


def test_activiti_bool():
    assert workflow.activiti.activiti_bool("1") is True


def test_activiti_bool_empty():
    assert workflow.activiti.activiti_bool("") is None


def test_activiti_bool_false():
    assert workflow.activiti.activiti_bool("false") is False


def test_activiti_bool_none():
    assert workflow.activiti.activiti_bool(None) is None


def test_activiti_bool_true():
    assert workflow.activiti.activiti_bool("true") is True


def test_activiti_bool_zero():
    assert workflow.activiti.activiti_bool("0") is False


@pytest.mark.parametrize(
    "url_name,expect",
    [
        ("example.task", "/example/task/123/"),
        (
            "example.task.missing.data.document",
            "/example/task/123/missing/data/document/",
        ),
    ],
)
def test_build_django_url(url_name, expect):
    url, is_work = build_django_url(url_name, 123, "my-process-id", "my-name")
    assert expect == url
    assert is_work is False


def test_build_django_url_does_not_exist():
    url, is_work = build_django_url(
        "custom-ember-url", 123, "my-process-id", "my-name"
    )
    assert is_work is True
    assert "https://www.hatherleigh.net/task/custom/ember/url/123" == url


def test_build_django_url_ember_task():
    """Workflow task for Ember.

    Workflow tasks with an empty ``url_name`` (``formKey``) will be executed
    by the Ember workflow app.

    """
    url, is_work = build_django_url(
        "", "c5f079d1-2779-11ec-b413-000d3a7f022c", "my-process-id", "my-name"
    )
    assert is_work is True
    assert (
        "https://www.hatherleigh.net/task/c5f079d1-2779-11ec-b413-000d3a7f022c"
        == url
    )


def test_build_django_url_ember_task_custom_form():
    """Workflow task for Ember.

    Workflow tasks with an empty ``url_name`` (``formKey``) will be executed
    by the Ember workflow app.  This is an Ember custom URL!

    """
    url, is_work = build_django_url(
        "my-custom-form",
        "c5f079d1-2779-11ec-b413-000d3a7f022c",
        "my-process-id",
        "my-name",
    )
    assert is_work is True
    assert (
        "https://www.hatherleigh.net/task/my/custom/form/c5f079d1-2779-11ec-b413-000d3a7f022c"
        == url
    )


def test_data_to_snake():
    assert {
        "first_name": "Patrick",
        "cake_count": 23,
    } == data_to_snake({"firstName": "Patrick", "cakeCount": 23})


@pytest.mark.django_db
def test_deployment():
    """Test a deployment"""
    with mock.patch(
        "workflow.activiti.Activiti.deploy_process_definition"
    ) as mock_deploy_proc_def, mock.patch(
        "workflow.activiti.Activiti.deployment_list"
    ) as mock_deployments:
        # mock return values
        mock_deploy_proc_def.return_value = _mock_deploy_process_definition()
        mock_deployments.return_value = _mock_deployments()
        # test
        activiti = workflow.activiti.Activiti()
        deploy_vacation_request()
        assert activiti.is_deployed("vacationRequest2Manager.bpmn20.xml")


# 20/04/2020, Does not seem to be used.
# def test_form_variables_as_dict():
#    data = [
#        {
#            "id": "auditDescription",
#            "datePattern": None,
#            "writable": True,
#            "readable": True,
#            "value": "selected:",
#            "name": "Audit Description",
#            "type": "string",
#            "required": True,
#            "enumValues": [],
#        },
#        {
#            "id": "acceptRequest",
#            "datePattern": None,
#            "writable": True,
#            "readable": True,
#            "value": None,
#            "name": None,
#            "type": "enum",
#            "required": True,
#            "enumValues": [
#                {"id": "accept", "name": "Accept the new document request"},
#                {
#                    "id": "reject_comment",
#                    "name": "Reject the new document request",
#                },
#            ],
#        },
#        {
#            "id": "requestComment",
#            "datePattern": None,
#            "writable": True,
#            "readable": True,
#            "value": None,
#            "name": "Comment",
#            "type": "string",
#            "required": False,
#            "enumValues": [],
#        },
#    ]
#    assert {
#        "audit_description": "selected:",
#        "request_comment": None,
#        "accept_request": ["accept", "reject_comment"],
#    } == form_variables_as_dict(data)
#
#
# def test_form_variables_as_dict_null_boolean():
#    data = [
#        {
#            "datePattern": None,
#            "required": False,
#            "type": "boolean",
#            "writable": True,
#            "enumValues": [],
#            "readable": True,
#            "name": "Boolean",
#            "value": None,
#            "id": "tempBoolean",
#        }
#    ]
#    assert {"temp_boolean": None} == form_variables_as_dict(data)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "process_id,expect_process_id", [("64", 64), ("abc64abc", "abc64abc")]
)
def test_historic_process(process_id, expect_process_id):
    activiti = workflow.activiti.Activiti()
    with mock.patch(
        "workflow.activiti.Activiti._historic_process"
    ) as mock_historic_process:
        mock_historic_process.return_value = _mock_historic_process(process_id)
        expect = HistoricProcessInstance(
            pk=expect_process_id,
            # process_id=expect_process_id,
            process_key="timeOff",
            user=None,
            completed=True,
            start_time=datetime(
                2017, 6, 14, 10, 38, 59, 1000, tzinfo=tzoffset(None, 3600)
            ),
            end_time=datetime(
                2017, 6, 14, 10, 39, 11, 188000, tzinfo=tzoffset(None, 3600)
            ),
            variables={
                "contact": Variable(
                    value="joey",
                    data_type=str,
                    java_name="contact",
                    name="contact",
                ),
                "from_date": Variable(
                    value="2017-06-27",
                    data_type=date,
                    java_name="fromDate",
                    name="fromDate",
                ),
                "to_date": Variable(
                    value="2017-06-27",
                    data_type=date,
                    java_name="toDate",
                    name="toDate",
                ),
            },
            # variables=OrderedDict(
            #    [
            #        ("contact", "joey"),
            #        ("from_date", "2017-06-27"),
            #        ("to_date", "2017-06-27"),
            #    ]
            # ),
        )
        result = activiti.historic_process(123)
        assert expect == result


@pytest.mark.parametrize(
    "data,expect", [("456", 456), ("abc", "abc"), (123, 123), (None, None)]
)
def test_int_if_possible(data, expect):
    assert expect == int_if_possible(data)


@pytest.mark.django_db
def test_process_id_to_key():
    assert "vacationRequest" == process_id_to_key("vacationRequest:4:51")


@responses.activate
def test_process_identity_list():
    responses.add(
        responses.GET,
        "http://{}:{}/{}service/runtime/process-instances/cf2d71f6-eff4-4724-ba12-8271e81e6923/identitylinks".format(
            settings.ACTIVITI_HOST,
            settings.ACTIVITI_PORT,
            (
                "{}/".format(settings.ACTIVITI_PATH)
                if settings.ACTIVITI_PATH
                else ""
            ),
        ),
        json=[
            {
                "url": "http://localhost:8080/flowable-rest/service/runtime/process-"
                "instances/1ef99cb3-6e1b-11ec-bed7-002248012004/identitylinks/users/2/"
                "participant",
                "user": "2",
                "group": None,
                "type": "participant",
            },
            {
                "url": (
                    "http://localhost:8080/activiti-rest/service/runtime/tasks"
                    "/2843/identitylinks/groups/1/candidate"
                ),
                "user": None,
                "group": "111",
                "type": "candidate",
            },
            {
                "url": "http://localhost:8080/flowable-rest/service/runtime/process-"
                "instances/1ef99cb3-6e1b-11ec-bed7-002248012004/identitylinks/users/"
                "18/participant",
                "user": "18",
                "group": None,
                "type": "participant",
            },
            {
                "url": "http://localhost:8080/flowable-rest/service/runtime/process-"
                "instances/1ef99cb3-6e1b-11ec-bed7-002248012004/identitylinks/users/"
                "kermit/participant",
                "user": "kermit",
                "group": None,
                "type": "participant",
            },
            {
                "url": "http://localhost:8080/flowable-rest/service/runtime/process-"
                "instances/1ef99cb3-6e1b-11ec-bed7-002248012004/identitylinks/users/"
                "kermit/starter",
                "user": "kermit",
                "group": None,
                "type": "starter",
            },
        ],
        status=HTTPStatus.OK,
    )
    activiti = workflow.activiti.Activiti()
    identity_list = activiti.process_identity_list(
        "cf2d71f6-eff4-4724-ba12-8271e81e6923"
    )
    assert [
        (2, None, "participant"),
        (None, 111, "candidate"),
        (18, None, "participant"),
        (None, None, "participant"),
        (None, None, "starter"),
    ] == [(x.user, x.group, x.identity_type) for x in identity_list]


# PJK 21/04/2020, I checked our projects and I don't think
# 'process_definition_by_key' is used anywhere
# (other than in the tests for this app).
#
# @pytest.mark.django_db
# def test_process_start():
#    """ test a deployment """
#    with mock.patch(
#        "workflow.activiti.Activiti.deployment_list"
#    ) as mock_deployments, mock.patch(
#        "workflow.activiti.Activiti.process_definition_list"
#    ) as mock_process_definitions, mock.patch(
#        "workflow.activiti.Activiti.process_start"
#    ) as mock_process_start, mock.patch(
#        "workflow.activiti.Activiti.form_variable_info"
#    ) as mock_form_variable_info, mock.patch(
#        "workflow.activiti.Activiti.user_task_list"
#    ) as mock_user_tasks:
#        # mock return values
#        mock_deployments.return_value = _mock_deployments()
#        mock_form_variable_info.return_value = _mock_form_variable_info()
#        mock_process_definitions.return_value = _mock_process_definitions()
#        mock_process_start.return_value = _mock_process_start()
#        mock_user_tasks.return_value = _mock_user_task_list()
#        # test
#        activiti = workflow.activiti.Activiti()
#        pid = get_vacation_request_process_definition()
#        form_data = activiti.form_variable_info(process_id=pid)
#        variables = []
#        if "formProperties" in form_data:
#            # Assign some values to the fields
#            for field in form_data["formProperties"]:
#                if field["id"] == "startDate":
#                    variables.append(
#                        {"name": field["id"], "value": "2016-06-12"}
#                    )
#                if field["id"] == "numberOfDays":
#                    variables.append({"name": field["id"], "value": 5})
#                if field["id"] == "reasonForHoliday":
#                    variables.append(
#                        {"name": field["id"], "value": "fun fun fun"}
#                    )
#                if field["id"] == "manager":
#                    variables.append({"name": field["id"], "value": "fozzie"})
#
#        process = activiti.process_start("vacationRequest", variables)
#        instance_id = None
#        if "id" in process:
#            instance_id = process["id"]
#        assert instance_id
#        # check it's assigned to fozzie
#        tasks = activiti.user_task_list("fozzie")
#        assert instance_id in [
#            task["processInstanceId"] for task in tasks["data"]
#        ]


@pytest.mark.django_db
def test_process_status():
    with mock.patch("workflow.activiti.Activiti._process_status") as mock_json:
        mock_json.return_value = _mock_process_status()
        activiti = workflow.activiti.Activiti()
        process = activiti._process_status(61)
        assert "data" in process


@pytest.mark.django_db
def test_process_status_data():
    with mock.patch("workflow.activiti.Activiti._process_status") as mock_json:
        mock_json.return_value = _mock_process_status()
        activiti = workflow.activiti.Activiti()
        data = activiti.process_status(61)
        assert 61 == data.pk
        assert "admin" == data.variables["initiator"].value
        assert "joey" == data.variables["contact"].value
        assert "Oil COSSH Assessment" == data.variables["document_title"].value
        assert 29 == data.variables["object_pk"].value


@pytest.mark.django_db
def test_process_status_data_multi_row():
    with mock.patch("workflow.activiti.Activiti._process_status") as mock_json:
        mock_json.return_value = _mock_process_status_multi_row()
        activiti = workflow.activiti.Activiti()
        with pytest.raises(workflow.activiti.ActivitiError) as e:
            activiti.process_status(61)
        assert "expected no data or one row of data" in str(e.value)


@pytest.mark.django_db
def test_process_status_not_found():
    with mock.patch("workflow.activiti.Activiti._process_status") as mock_json:
        mock_json.return_value = {"data": []}
        activiti = workflow.activiti.Activiti()
        process = activiti.process_status(61)
        assert 61 == process.pk
        assert process.start_time is None
        assert process.variables is None


def test_response_message(caplog):
    message = workflow.activiti.response_message(
        _mock_response_json({"abc": 34}, 200)
    )
    expect = '200 error: mock-reason for url: http://mock-url/: {"abc": 34}'
    assert expect == message
    assert expect in [
        x.msg for x in caplog.records if x.levelno == logging.ERROR
    ]


def test_response_message_json_none():
    """Check the error message routine works when there is no json data."""
    message = workflow.activiti.response_message(_mock_response(404))
    assert "404 error: mock-reason for url: http://mock-url/: {}" == message


@pytest.mark.django_db
def test_row_to_process_definition():
    row = _mock_process_definition()
    activiti = workflow.activiti.Activiti()
    result = activiti._row_to_process_definition(row)
    assert "createTimersProcess" == result.process_key
    assert "Create timers process" == result.name
    assert 1 == result.version
    assert "createTimersProcess:1:36" == result.workflow_id


def test_row_to_task_data():
    row = _mock_task_status()
    activiti = workflow.activiti.Activiti()
    result = activiti._row_to_task_data(row)
    assert "Oil COSSH Assessment" == result.name
    assert "documentFamiliarisation:1:303" == result.definition_id
    assert 45 == result.assignee
    assert 2016 == result.created.year
    assert 321 == result.process_id
    assert 336 == result.pk
    assert result.category is None
    assert result.url_name is None


def test_row_to_identity_link():
    activiti = workflow.activiti.Activiti()
    for row in _mock_task_identity_list():
        result = activiti._row_to_identity_link(row)
        assert result.user is None
        assert 111 == result.group
        assert "candidate" == result.identity_type


@pytest.mark.parametrize(
    "user,group,expect_user,expect_group",
    [
        ("123", None, 123, None),
        ("abc", None, None, None),
        (123, None, 123, None),
        (None, "456", None, 456),
        (None, "def", None, None),
        (None, 456, None, 456),
        (None, None, None, None),
    ],
)
def test_row_to_identity_link_errors(user, group, expect_user, expect_group):
    mock_identity_list = [
        {
            "url": (
                "http://localhost:8080/activiti-rest/service/runtime/tasks"
                "/2843/identitylinks/groups/1/candidate"
            ),
            "user": user,
            "group": group,
            "type": "candidate",
        }
    ]
    activiti = workflow.activiti.Activiti()
    count = 0
    for row in mock_identity_list:
        count = count + 1
        result = activiti._row_to_identity_link(row)
        assert expect_user == result.user
        assert expect_group == result.group
    assert 1 == count


def test_service_url(settings):
    settings.ACTIVITI_HOST = "localhost"
    settings.ACTIVITI_PORT = 8080
    settings.ACTIVITI_PATH = "activiti-rest"
    activiti = workflow.activiti.Activiti()
    expect = "http://localhost:8080/activiti-rest/service"
    assert expect == activiti._service_url


@pytest.mark.parametrize(
    "activiti_port,activiti_path,expect",
    [
        (8080, "activiti-rest", "http://localhost:8080/activiti-rest/service"),
        (80, "activiti-rest", "http://localhost:80/activiti-rest/service"),
        (8080, "", "http://localhost:8080/service"),
        (8080, None, "http://localhost:8080/service"),
    ],
)
def test_service_url_new(settings, activiti_port, activiti_path, expect):
    settings.ACTIVITI_HOST = "localhost"
    settings.ACTIVITI_PORT = activiti_port
    settings.ACTIVITI_PATH = activiti_path
    activiti = workflow.activiti.Activiti()
    assert expect == activiti._service_url


@pytest.mark.parametrize(
    "variable_name,expect",
    [
        ("accept_request", "acceptRequest"),
        ("hr_manager_group_id", "hrManagerGroupId"),
        ("object_pk", "objectPk"),
    ],
)
def test_snake_to_camel_case(variable_name, expect):
    assert expect == snake_to_camel_case(variable_name)


@responses.activate
def test_task_complete():
    task_uuid = str(uuid.uuid4())
    # note: 'responses' will return the data below for every document
    url_path = f"{settings.ACTIVITI_PATH}/" if settings.ACTIVITI_PATH else ""
    responses.add(
        responses.POST,
        (
            f"http://{settings.ACTIVITI_HOST}:{settings.ACTIVITI_PORT}/"
            f"{url_path}service/runtime/tasks/{task_uuid}"
        ),
        json={
            "data": [],
            "total": 0,
        },
    )
    variables = {
        "audit_description": Variable(
            name="Audit", value="Apple", data_type=str
        ),
        "accept_request": Variable(name="Accept", value="Yes", data_type=str),
    }
    activiti = Activiti()
    response = activiti.task_complete(task_uuid, variables)
    assert HTTPStatus.OK == response.status_code


@responses.activate
def test_task_complete_additional_variables():
    task_uuid = str(uuid.uuid4())
    # note: 'responses' will return the data below for every document
    url_path = f"{settings.ACTIVITI_PATH}/" if settings.ACTIVITI_PATH else ""
    responses.add(
        responses.POST,
        (
            f"http://{settings.ACTIVITI_HOST}:{settings.ACTIVITI_PORT}/"
            f"{url_path}service/runtime/tasks/{task_uuid}"
        ),
        json={
            "data": [],
            "total": 0,
        },
    )
    variables = {
        "audit_description": Variable(
            name="Audit", value="Apple", data_type=str
        ),
        "accept_request": Variable(name="Accept", value="Yes", data_type=str),
    }
    additional_variables = {
        "group_task_claimed_by_user_pk": Variable(
            name="Group Task Completed By", value="123", data_type=int
        ),
    }
    activiti = Activiti()
    response = activiti.task_complete(
        task_uuid, variables, additional_variables=additional_variables
    )
    assert HTTPStatus.OK == response.status_code


def test_task_identity_list():
    with mock.patch(
        "workflow.activiti.Activiti._task_identity_links"
    ) as mock_json:
        mock_json.return_value = _mock_task_identity_list()
        activiti = workflow.activiti.Activiti()
        for result in activiti.task_identity_list(2843):
            assert result.user is None
            assert 111 == result.group
            assert "candidate" == result.identity_type


@pytest.mark.django_db
def test_task_status_data():
    with mock.patch("workflow.activiti.Activiti._task_status") as mock_json:
        mock_json.return_value = _mock_task_status()
        activiti = workflow.activiti.Activiti()
        data = activiti.task_status(336)
        assert 336 == data.pk
        assert 45 == data.assignee
        assert "Oil COSSH Assessment" == data.name
        assert 321 == data.process_id


@pytest.mark.django_db
def test_user_tasks_generator():
    """PK 13/08/2020, Not sure this test is any use to anyone."""
    with mock.patch("workflow.activiti.Activiti.user_tasks_generator") as m:
        m.return_value = _mock_user_task_list()
        activiti = workflow.activiti.Activiti()
        response = activiti.user_tasks_generator("fozzie")
        assert len(response["data"]) == response["size"]
        for task in response["data"]:
            assert task["assignee"] == "fozzie"


def test_variables_as_detail():
    data = [
        {
            "id": "orderNumber",
            "enumValues": [],
            "name": "Order Number",
            "required": True,
            "scope": "global",
            "type": "integer",
            "value": 435,
            "writable": True,
        },
        {
            "id": "colour",
            "enumValues": [
                {"id": "blue", "name": "Blue"},
                {"id": "orange", "name": "Orange"},
            ],
            "name": "Colour",
            "required": False,
            "scope": "global",
            "type": "enum",
            "value": "blue",
            "writable": False,
        },
    ]
    expect = {
        "colour": Variable(
            data_type=Enum,
            choices={"blue": "Blue", "orange": "Orange"},
            value="blue",
            java_name="colour",
            name="Colour",
            required=False,
            writable=False,
        ),
        "order_number": Variable(
            data_type=int,
            choices={},
            value=435,
            java_name="orderNumber",
            name="Order Number",
            required=True,
            writable=True,
        ),
    }
    result = variables_as_detail(data)
    assert expect == result


def test_variables_as_detail_runtime():
    data = [
        {
            "type": "integer",
            "name": "orderNumber",
            "scope": "global",
            "value": 435,
        },
        {"type": "boolean", "name": "Check", "scope": "global", "value": False},
        {"type": "enum", "name": "colour", "scope": "global", "value": "blue"},
        {
            "type": "string",
            "name": "description",
            "scope": "global",
            "value": "Invoice",
        },
    ]
    expect = {
        "check": Variable(
            data_type=bool,
            value=False,
            java_name="Check",
            name="Check",
        ),
        "colour": Variable(
            data_type=Enum,
            value="blue",
            java_name="colour",
            name="colour",
        ),
        "description": Variable(
            data_type=str,
            value="Invoice",
            java_name="description",
            name="description",
        ),
        "order_number": Variable(
            data_type=int,
            value=435,
            java_name="orderNumber",
            name="orderNumber",
        ),
    }
    result = variables_as_detail(data, is_runtime=True)
    assert expect == result


def test_variables_to_activiti():
    x = Activiti()
    result = x._variables_to_activiti(
        {
            "audit_description": Variable(
                name="Audit", value="Apple", data_type=str
            ),
            "accept_request": Variable(
                name="Accept", value="Yes", data_type=str
            ),
        }
    )
    assert [
        {"name": "auditDescription", "value": "Apple"},
        {"name": "acceptRequest", "value": "Yes"},
    ] == result
