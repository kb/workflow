# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory
from workflow.models import (
    FlowList,
    FlowListCategory,
    FlowListType,
    Mapping,
    MappingPlugin,
    ProcessDefinition,
    ScheduledWorkflow,
    ScheduledWorkflowUser,
    Workflow,
    WorkflowData,
)


class FlowListTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FlowListType


class FlowListCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FlowListCategory


class FlowListFactory(factory.django.DjangoModelFactory):
    flow_list_type = factory.SubFactory(FlowListTypeFactory)

    class Meta:
        model = FlowList


class WorkflowFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Workflow

    @factory.sequence
    def process_key(n):
        return "process_key_{:02d}".format(n)


class MappingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Mapping

    workflow = factory.SubFactory(WorkflowFactory)
    mapping_type = MappingPlugin.USER
    user = factory.SubFactory(UserFactory)

    @factory.sequence
    def variable_name(n):
        return "variable_name_{:02d}".format(n)


class ProcessDefinitionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProcessDefinition

    process_definition = factory.django.FileField()


class ScheduledWorkflowFactory(factory.django.DjangoModelFactory):
    initiator = factory.SubFactory(UserFactory)

    class Meta:
        model = ScheduledWorkflow

    @factory.sequence
    def content_object(n):
        return UserFactory(username="content_object_user_{:02d}".format(n))

    @factory.sequence
    def initiator(n):
        return UserFactory(username="initiator_user_{:02d}".format(n))

    @factory.sequence
    def process_definition(n):
        return "process_{:02d}".format(n)


class ScheduledWorkflowUserFactory(factory.django.DjangoModelFactory):
    scheduled_workflow = factory.SubFactory(ScheduledWorkflowFactory)

    @factory.sequence
    def user(n):
        return UserFactory(username="user_{:02d}".format(n))

    class Meta:
        model = ScheduledWorkflowUser


class WorkflowDataFactory(factory.django.DjangoModelFactory):
    scheduled_workflow_user = factory.SubFactory(ScheduledWorkflowUserFactory)

    class Meta:
        model = WorkflowData
