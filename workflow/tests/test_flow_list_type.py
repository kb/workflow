# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from workflow.tests.factories import FlowListTypeFactory
from workflow.models import FlowListType


@pytest.mark.django_db
def test_current():
    FlowListTypeFactory(name="Z")
    flow_list_type = FlowListTypeFactory(name="K")
    flow_list_type.set_deleted(UserFactory())
    FlowListTypeFactory(name="A")
    assert ["A", "Z"] == [x.name for x in FlowListType.objects.current()]


@pytest.mark.django_db
def test_factory():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
    )
    assert flow_list_type.name == "Colours"


@pytest.mark.django_db
def test_ordering():
    FlowListTypeFactory(name="Z")
    FlowListTypeFactory(name="A")
    assert ["A", "Z"] == [x.name for x in FlowListType.objects.all()]


@pytest.mark.django_db
def test_str():
    flow_list_type = FlowListTypeFactory(
        name="Colours",
    )
    assert "Colours" == str(flow_list_type)
