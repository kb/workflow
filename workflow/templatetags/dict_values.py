from django.template.defaulttags import register


@register.filter
def dict_value(dictionary, key):
    return dictionary.get(key)
