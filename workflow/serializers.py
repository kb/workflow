# -*- encoding: utf-8 -*-
from rest_framework import serializers


class TaskDetailSerializer(serializers.Serializer):
    task_id = serializers.CharField()
    name = serializers.CharField()
    description = serializers.CharField()
    task_id = serializers.CharField()
    assignee = serializers.IntegerField()
    category = serializers.CharField()
    created = serializers.DateTimeField()
    # definition_id='timeOffRequest:1:71100c8c-121d-11ea-8249-be0e43b18252',
    description = serializers.CharField()
    due_date = serializers.DateTimeField()
    name = serializers.CharField()
    # process_id='c192ded2-1221-11ea-8249-be0e43b18252',
    # process_key='timeOffRequest',
    url_name = serializers.CharField()
    variables = serializers.JSONField()


class TaskListSerializer(serializers.Serializer):
    task_id = serializers.CharField(source="id")
    name = serializers.CharField()
    description = serializers.CharField()
