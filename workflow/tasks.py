# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.db import transaction

from workflow.activiti import ActivitiError


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def create_workflows(scheduled_workflow_pk, max_retries=1):
    """Create Activiti workflows for scheduled workflow."""
    result = []
    try:
        logger.info("'create_workflows' for '{}'".format(scheduled_workflow_pk))
        from workflow.models import ScheduledWorkflow

        with transaction.atomic():
            scheduled_users = ScheduledWorkflow.objects.create_workflows(
                scheduled_workflow_pk
            )
        result = [x.get_process_id() for x in scheduled_users]
        logger.info(
            "'create_workflows' for '{}': {} - complete".format(
                scheduled_workflow_pk, result
            )
        )
    except ActivitiError as e:
        logger.error(
            "Could not 'create_workflows' for '{}': {}".format(
                scheduled_workflow_pk, e.value
            )
        )
    return result
