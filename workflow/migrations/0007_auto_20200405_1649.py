# Generated by Django 2.2.9 on 2020-04-05 15:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("workflow", "0006_scheduledworkflowuser_process_uuid")]

    operations = [
        migrations.AddField(
            model_name="mapping",
            name="multi_line",
            field=models.IntegerField(
                blank=True,
                help_text="Number of rows to display in a text field (0 for default)",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="mapping",
            name="view_task_data",
            field=models.BooleanField(
                default=False, help_text="View data on task screen"
            ),
        ),
        migrations.AlterField(
            model_name="mapping",
            name="mapping_type",
            field=models.CharField(
                choices=[
                    ("auto-generate", "Auto-Generate"),
                    ("decimal", "Money"),
                    ("group", "Group"),
                    ("import", "Imported"),
                    ("import-date", "Imported Date"),
                    ("import-decimal", "Imported Money"),
                    ("integer", "Whole Number"),
                    ("undefined", "Undefined"),
                    ("user", "User"),
                    ("user-in-group", "User in Group"),
                ],
                default="undefined",
                max_length=20,
            ),
        ),
    ]
