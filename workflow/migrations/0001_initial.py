# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import workflow.models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("contenttypes", "0002_remove_content_type_name"),
    ]

    operations = [
        migrations.CreateModel(
            name="ProcessDefinition",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                (
                    "process_key",
                    models.CharField(
                        max_length=200, default="incompleteDeployment"
                    ),
                ),
                ("version", models.PositiveIntegerField(default=0)),
                ("workflow_id", models.CharField(max_length=200)),
                (
                    "process_definition",
                    models.FileField(
                        upload_to=workflow.models.process_file_name
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "Process Definitions",
                "ordering": ("process_definition",),
                "verbose_name": "Process Definition",
            },
        ),
        migrations.CreateModel(
            name="ScheduledWorkflow",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("deleted", models.BooleanField(default=False)),
                ("date_deleted", models.DateTimeField(blank=True, null=True)),
                ("process_definition", models.SlugField(max_length=200)),
                ("object_id", models.PositiveIntegerField()),
                (
                    "content_type",
                    models.ForeignKey(
                        to="contenttypes.ContentType", on_delete=models.CASCADE
                    ),
                ),
                (
                    "initiator",
                    models.ForeignKey(
                        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
                (
                    "user_deleted",
                    models.ForeignKey(
                        to=settings.AUTH_USER_MODEL,
                        blank=True,
                        related_name="+",
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "Scheduled workflows",
                "verbose_name": "Scheduled workflow",
            },
        ),
        migrations.CreateModel(
            name="ScheduledWorkflowUser",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("deleted", models.BooleanField(default=False)),
                ("date_deleted", models.DateTimeField(blank=True, null=True)),
                ("process_id", models.IntegerField(blank=True, null=True)),
                (
                    "scheduled_workflow",
                    models.ForeignKey(
                        to="workflow.ScheduledWorkflow",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
                    ),
                ),
                (
                    "user_deleted",
                    models.ForeignKey(
                        to=settings.AUTH_USER_MODEL,
                        blank=True,
                        related_name="+",
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name_plural": "Scheduled user workflows",
                "ordering": ("scheduled_workflow", "user__username"),
                "verbose_name": "Scheduled user workflow",
            },
        ),
        migrations.AlterUniqueTogether(
            name="processdefinition",
            unique_together=set([("process_key", "version")]),
        ),
        migrations.AlterUniqueTogether(
            name="scheduledworkflowuser",
            unique_together=set([("scheduled_workflow", "user")]),
        ),
    ]
