# Generated by Django 4.0.10 on 2023-02-27 11:42

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("workflow", "0013_workflow_url_redirect"),
    ]

    operations = [
        migrations.AddField(
            model_name="mapping",
            name="mapping_code",
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
