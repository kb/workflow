# -*- encoding: utf-8 -*-
import attr
import decimal
import importlib
import logging
import os.path
import xml.etree.ElementTree as ET

from datetime import date
from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.serializers.json import DjangoJSONEncoder
from django.db import DatabaseError, models
from django.db import transaction
from django.db.models import Max, Q
from django.urls import reverse
from django.utils import timezone
from enum import Enum
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel
from login.models import SYSTEM_GENERATED
from report.models import ReportSpecification
from .activiti import (
    Activiti,
    activiti_bool,
    activiti_date,
    ActivitiError,
    camelcase_to_snake,
    get_data_type,
    snake_to_camel_case,
    Variable,
)


logger = logging.getLogger(__name__)


# Hidden variables (together with ``WorkflowFormMixin._is_comment``).
HIDDEN_VARIABLE_NAMES = ["audit_description"]

# Used internally by the workflow system.  Do NOT appear on forms.
#
# When naming the variables, finish the name with the model name and ``pk``
# e.g. for ``managerUserPk`` the ``manager`` is the primary key of a ``User``.
#
RESERVED_VARIABLE_NAMES = [
    "api_url",
    "copy_to_document_management",
    "data_document",
    "data_download",
    "delete_workflow",
    "edit_document",
    "edit_metadata",
    "object_pk",
    "user_email",
    "user_full_name",
    "user_name",
    "user_pk",
    # 17/06/2020, We are creating generic (non-legacy) workflows which want to
    # use ``document_code`` and ``document_title``.
    # To get them to work, we will remove these olde variable names from the
    # list.
    # The remaining variable names should probably be *reserved*.
    # https://www.kbsoftware.co.uk/crm/ticket/4922/
    # "author_pk",
    # "co_group_pk",
    # "document_code",
    # "document_title",
    # "manager_user_name",
    # "manager_user_pk",
]

# Variable names
# https://www.kbsoftware.co.uk/crm/ticket/7478/
GROUP_TASK_CLAIMED_BY_USER_PK = "group_task_claimed_by_user_pk"


@attr.s
class Choice:
    caption = attr.ib()
    data_type = attr.ib()


@attr.s
class Map:
    """'Mapping' data."""

    variable_name = attr.ib()
    mapping_type = attr.ib()

    def auto_generate(self):
        return self.mapping_type == MappingPlugin.AUTO_GENERATE


class WorkflowError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


def _check_user_pk_in_variables(process_definition, variables):
    """Check 'user_pk' is in the variable list.

    The ``user_pk`` is required for the ``started_by`` filter in
    ``_historic_process`` and ``_process_instances`` (search for ``userPk``).

    https://www.kbsoftware.co.uk/docs/app-workflow.html#start-event-startevent


    .. note:: 13/01/2022, We are adding a user to the process on creation
              (``process_identity_add_participant``), so perhaps we won't
              need the ``userPk`` in future? For details, see
              `commit 8361c1103cf09021a795c648cc867ee2549736a6`_

    """
    if "user_pk" in variables:
        pass
    else:
        message = (
            "ScheduledWorkflow - cannot create workflow. The 'user_pk' "
            "variable is not defined in the 'startEvent' for '{}' "
            "(variables {}). The 'user_pk' is used by the 'started_by' "
            "filter for workflow processes".format(
                process_definition, sorted(list(variables))
            )
        )
        logger.error(message)
        raise ActivitiError(message)


def _contact_for_user(user):
    contact_model = get_contact_model()
    try:
        return contact_model.objects.get(user=user)
    except contact_model.DoesNotExist:
        message = f"Contact for user '{user.username}' does not exist"
        logger.error(message)
        raise ActivitiError(message)


def _contact_for_user_pk(user_pk):
    contact_model = get_contact_model()
    try:
        return contact_model.objects.get(user__pk=user_pk)
    except contact_model.DoesNotExist:
        message = f"Contact for user '{user_pk}' does not exist"
        logger.error(message)
        raise ActivitiError(message)


def _make_process_file_name(num, file_name):
    return os.path.join("process", str(num) + "_" + file_name)


def _mapping_choices():
    return MappingPlugin().mapping_choices()


# def auto_generate_user(variable_name, user_pk):
#    result = {}
#    try:
#        user = User.objects.get(pk=user_pk)
#        if "email" in variable_name.lower():
#            result[name] = Variable(data_type=str, value=user.email)
#        if (
#            "fullname" in variable_name.lower()
#            or "full_name" in variable_name.lower()
#        ):
#            result[name] = Variable(data_type=str, value=user.get_full_name())
#    except User.DoesNotExist:
#        pass
#    return result


def completed_reports(process_key):
    """List of completed workflow data reports for a process key."""
    report = ReportSpecification.objects.get(slug=WorkflowData.REPORT_SLUG)
    qs = report.completed()
    return qs.filter(parameters__process_key=process_key)


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def import_class(plugin_class_name):
    pos = plugin_class_name.rfind(".")
    if pos == -1:
        raise WorkflowError(
            "Cannot find '{}' (no '.' in the class name)".format(
                plugin_class_name
            )
        )
    module_name = plugin_class_name[:pos]
    plugin_module = import_module(module_name)
    plugin_class = plugin_class_name[pos + 1 :]
    return getattr(plugin_module, plugin_class)


def import_module(module_name):
    try:
        plugin_module = importlib.import_module(module_name)
    except ImportError as e:
        # ``ModuleNotFoundError`` not available until python 3.6
        message = "Cannot find '{}' ({})".format(module_name, str(e))
        raise WorkflowError(message)
    return plugin_module


def is_comment(name):
    check = name.lower()
    return check.endswith("comment") or check.endswith("notes")


def match_data_type(mapping_choices, data_type):
    """Return the 'mapping_choices' with a matching 'data_type'.

    .. note:: If the ``data_type`` is ``None`` we return all choices
              (to allow the form to validate).

    """
    result = {}
    for field_id, choice in mapping_choices.items():
        if data_type is None or data_type == choice.data_type:
            result[field_id] = choice
    return result


def process_file_name(instance, file_name):
    num = 1
    file_path = _make_process_file_name(num, file_name)
    while os.path.exists(os.path.join(settings.MEDIA_ROOT, file_path)):
        num += 1
        file_path = _make_process_file_name(num, file_name)
    return file_path


def system_generated_user():
    return get_user_model().objects.get(username=SYSTEM_GENERATED)


def user_in_task_group(activiti, user, task):
    """Is the user in the group for this task."""
    result = False
    group_pks = set()
    identity_list = activiti.task_identity_list(task.pk)
    for identity in identity_list:
        if identity.group:
            group_pks.add(identity.group)
    if group_pks:
        contact_model = get_contact_model()
        # we ask the contact model to check if the user is in a group
        # because the contact model in some projects uses Django groups,
        # others use categories...
        result = contact_model.objects.user_in_a_group(user, group_pks)
    return result


def user_owns_task(activiti, user, task):
    """Does the user own the task?

    Is the user:

    1. The ``assignee`` on the task.
    2. Delegated to do the task.
    2. In the group for this task.

    Keyword arguments:
    activiti -- the flowable instance (so we can check groups)
    task -- the task (``TaskData`` from ``workflow.activiti``)
    user -- the user (a Django ``User``)

    """
    result = False
    is_group_task = True
    assignee = None
    if task.assignee:
        is_group_task = False
        try:
            # convert the ``assignee`` to an integer
            assignee = int(task.assignee)
        except ValueError:
            logger.error(
                "Assignee for task '{}' is not an integer: '{}'".format(
                    task.pk, task.assignee
                )
            )
            pass
    if assignee:
        result = assignee == user.pk
        if not result:
            # task is assigned to a user, so check for a delegate
            contact = _contact_for_user(user)
            contact_assignee = _contact_for_user_pk(assignee)
            result = contact.is_delegate_for(contact_assignee)
    elif is_group_task:
        # only check for a group task if the ``task.assignee`` is empty...
        result = user_in_task_group(activiti, user, task)
    return result


def workflow_form_includes_delete_workflow(activiti, task_id):
    """Check the workflow form fields - do they include 'delete_workflow'?"""
    form_variable_info = activiti.form_variable_info(task_id=task_id)
    return bool("delete_workflow" in form_variable_info)


class FlowListTypeManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)


class FlowListType(TimedCreateModifyDeleteModel):
    """Workflow list type e.g. invoice approval state or colour.

    - name e.g. Colours
    - description e.g. Free text describing the type of list.
    - The ``group_app``, ``group_module`` and ``group_class`` allow us to
      create a drop down using a model from another (customer specific) app.
      For more information, see ``FlowListTypeGroupViewSet``
      in ``work/api.py``.

    """

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    group_app = models.CharField(max_length=100)
    group_module = models.CharField(max_length=100)
    group_class = models.CharField(max_length=100)
    group_description = models.CharField(
        max_length=100,
        blank=True,
        help_text="Name of the group e.g. Location",
    )
    filter_category_by_group_pk = models.BooleanField(default=False)
    objects = FlowListTypeManager()

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return f"{self.name}"

    def has_group_class(self):
        """Do we have a 'group_class' i.e. can we call 'import_group_class'?

        .. tip:: Used by the ``FlowListSerializer`` (``work/serializers.py``).

        """
        return bool(self.group_class.strip() and self.group_description.strip())

    def import_group_class(self):
        """Import the module and get the class."""
        class_name = f"{self.group_app}.{self.group_module}"
        try:
            group_module = importlib.import_module(class_name)
        except (ImportError, TypeError) as e:
            # ``ModuleNotFoundError`` not available until python 3.6
            message = (
                f"Cannot find '{self.group_class}'. "
                f"Do you have '{self.group_module}.py' "
                f"in your '{self.group_app}' package? ({str(e)})"
            )
            raise WorkflowError(message)
        # get the class
        return getattr(group_module, self.group_class)


class FlowListCategoryManager(models.Manager):
    def current(self, flow_list_type=None):
        qs = self.model.objects.exclude(deleted=True)
        if flow_list_type:
            qs = qs.filter(flow_list_type=flow_list_type)
        return qs


class FlowListCategory(TimedCreateModifyDeleteModel):
    """Workflow list category."""

    flow_list_type = models.ForeignKey(FlowListType, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    objects = FlowListCategoryManager()

    class Meta:
        ordering = ["flow_list_type__name", "name"]
        verbose_name = "Flow List"
        verbose_name_plural = "Flow Lists"

    def __str__(self):
        return f"{self.name} ({self.flow_list_type.name})"


class FlowListManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)


class FlowList(TimedCreateModifyDeleteModel):
    """Workflow lists e.g. invoice approval state.

    - flow_list_type i.e. The type of the list (``FlowListType``)
    - name e.g. Green, Blue, Orange
    - category - optional category
    - group_pk - optional group (the ID of an instance of a ```group_class``)

    """

    flow_list_type = models.ForeignKey(FlowListType, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    category = models.ForeignKey(
        FlowListCategory, blank=True, null=True, on_delete=models.CASCADE
    )
    group_pk = models.IntegerField(blank=True, null=True)
    objects = FlowListManager()

    class Meta:
        ordering = ["flow_list_type__name", "name"]
        verbose_name = "Flow List"
        verbose_name_plural = "Flow Lists"

    def __str__(self):
        category = ""
        if self.category:
            category = f" - {self.category.name}"
        return f"{self.name}{category} ({self.flow_list_type.name})"


class WorkflowManager(models.Manager):
    def create_workflow(self, process_key):
        obj = self.model(process_key=process_key)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def for_task(self, task):
        """Get the workflow for a task (create if it does not exist)."""
        try:
            workflow = Workflow.objects.get(process_key=task.process_key)
        except Workflow.DoesNotExist:
            # create the workflow if it does not exist
            workflow = Workflow.objects.init_workflow(task.process_key)
            logger.info(
                "Created workflow with process key: '{}'".format(
                    task.process_key
                )
            )
        return workflow

    def for_user(self, user):
        """Which workflows does a user have permission to create?

        Return public workflows and workflows for user groups.

        """
        group_pks = [x.pk for x in user.groups.all()]
        qs = self.model.objects.filter(
            Q(security_type=Workflow.GROUPS) & Q(groups__pk__in=group_pks)
            | Q(security_type=Workflow.PUBLIC)
        )
        return qs.distinct()

    def init_workflow(self, process_key):
        try:
            obj = self.model.objects.get(process_key=process_key)
            if obj.is_deleted:
                obj.undelete()
        except self.model.DoesNotExist:
            obj = self.create_workflow(process_key)
        return obj

    def has_security_history(self, user):
        """Does the user have permission to view security for any workflow.

        .. note:: Both the model and manager have ``has_security_history``
                  methods.

        """
        return self.security_history(user).exists()

    def security_history(self, user):
        """Workflows where the user has permission to view the history."""
        if user.is_superuser:
            qs = self.current()
        else:
            qs = self.current().filter(security_history__in=user.groups.all())
        return qs.order_by("process_key").distinct()

    def security_history_my(self, user):
        """The user can view workflows which they have been involved with."""
        if user.is_superuser:
            qs = self.current()
        else:
            qs = self.current().filter(
                security_history_my__in=user.groups.all()
            )
        return qs.order_by("process_key").distinct()


class Workflow(TimedCreateModifyDeleteModel):
    """Workflow is a list of process keys with variable mappings.

    We wanted to use a ``ForeignKey`` from ``Mapping`` to ``ProcessDefinition``
    but the process definition is not unique on the ``process_key``.  This
    table acts as a unique link to the process key (workflow).

    The workflow security levels are:

    - system: Access to this workflow is controlled by the system
    - public: help_text="Any logged in user can start this workflow
    - groups: Workflow can be started by a user in one of these groups

    To set-up a process for testing by a single user, create a ``Development``
    group and add the user to that group.  Set the ``security_type`` to
    ``GROUP`` and set ``groups`` to the ``Development`` group.

    The ``url_name`` is used when a workflow isn't automatically created.  The
    view for the ``url_name`` will handle creation of the workflow instead.

    The ``config_description`` and ``config_url_name`` fields are used to
    describe and get access to configuration options which are not available
    from the ``workflow`` app.  I can't think of a way to display these options
    at the moment!

    ``url_redirect`` is a URL field (rather than a Django URL name) because we
    often need to redirect to another site e.g. Ember.

    """

    GROUPS = "groups"
    PUBLIC = "public"
    SYSTEM = "system"

    SECURITY_CHOICES = (
        (GROUPS, "Groups"),
        (PUBLIC, "Public"),
        (SYSTEM, "System"),
    )

    process_key = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=255)
    icon = models.CharField(
        max_length=30,
        help_text=(
            "Choose an icon from http://fontawesome.io/icons/ "
            "e.g. 'fa-adjust'"
        ),
    )
    security_type = models.CharField(
        max_length=10, choices=SECURITY_CHOICES, default=SYSTEM
    )
    groups = models.ManyToManyField(
        Group,
        blank=True,
        related_name="workflow_groups",
        help_text=(
            "For a 'Security type' of 'Groups', the workflow can be started "
            "by a user in one of these groups"
        ),
    )
    security_delete = models.ManyToManyField(
        Group,
        blank=True,
        related_name="security_delete_workflow",
        help_text="A user in one of these groups can delete workflows",
    )
    security_history = models.ManyToManyField(
        Group,
        blank=True,
        related_name="security_history",
        help_text=(
            "Workflow history can be viewed by a user in one of these groups."
        ),
    )
    security_history_my = models.ManyToManyField(
        Group,
        blank=True,
        related_name="security_history_my",
        help_text=(
            "A user in one of these groups "
            "can view workflows they have been involved with"
        ),
    )
    url_name = models.CharField(
        max_length=200,
        blank=True,
        help_text="Workflow is started from this view",
    )
    config_description = models.CharField(
        max_length=200,
        blank=True,
        help_text="Description of the extra configuration options",
    )
    config_url_name = models.CharField(
        max_length=200,
        blank=True,
        help_text="Extra configuration options for this workflow",
    )
    url_redirect = models.URLField(
        blank=True,
        help_text="Redirect to this URL after delete",
    )
    objects = WorkflowManager()

    class Meta:
        ordering = ["process_key"]
        verbose_name = "Workflow"
        verbose_name_plural = "Workflows"

    def __str__(self):
        return "{}".format(self.process_key)

    def can_create(self, user):
        """Does this user have permission to create this workflow?"""
        result = user.is_superuser
        if not result:
            result = self.security_type == self.PUBLIC
        if not result:
            if self.security_type == self.GROUPS:
                group_pks = [x.pk for x in user.groups.all()]
                qs = self.groups.filter(pk__in=group_pks)
                result = bool(qs.count())
        return result

    def data_download(self):
        """Download data for these mapping rows."""
        return (
            self.mapping_set.exclude(deleted=True)
            .filter(data_download=True)
            .order_by("history_summary_order")
        )

    def has_security_delete(self, user):
        """Does the user have permission to delete workflows of this type?"""
        result = user.is_superuser
        if not result:
            pks = [x.pk for x in user.groups.all()]
            result = self.security_delete.filter(pk__in=pks).exists()
        return result

    def has_security_history(self, user):
        """Does the user have permission to view history for this workflow?

        .. note:: Both the model and manager have ``has_security_history``
                  methods.

        """
        result = user.is_superuser
        if not result:
            pks = [x.pk for x in user.groups.all()]
            result = self.security_history.filter(pk__in=pks).exists()
        return result

    def has_security_history_my(self, user):
        result = user.is_superuser
        if not result:
            pks = [x.pk for x in user.groups.all()]
            result = self.security_history_my.filter(pk__in=pks).exists()
        return result

    def history_summary(self):
        return (
            self.mapping_set.exclude(deleted=True)
            .filter(history_summary=True)
            .order_by("history_summary_order", "variable_name")
        )

    def history_summary_columns(self):
        return (
            self.mapping_set.exclude(deleted=True)
            .exclude(history_summary_column__isnull=True)
            .order_by("history_summary_column")
        )

    @property
    def is_system(self):
        return self.security_type == self.SYSTEM

    @property
    def is_url_name(self):
        """Does this workflow have a URL name?"""
        return bool(len(self.url_name.strip()))

    def mapping(self):
        return self.mapping_set.exclude(deleted=True)

    def security_type_as_str(self):
        choices = dict(self.SECURITY_CHOICES)
        result = choices[self.security_type]
        if self.security_type == self.GROUPS:
            names = [x.name for x in self.groups.all()]
            result = "{} ({})".format(result, ", ".join(names))
        return result

    def set_history_summary_columns(self, mappings):
        Mapping.objects.filter(workflow=self).update(
            history_summary_column=None
        )
        pks = []
        for mapping in mappings:
            if mapping:
                pks.append(mapping.pk)
        count = 1
        for pk in pks:
            mapping = Mapping.objects.get(pk=pk)
            if self == mapping.workflow:
                mapping.history_summary_column = count
                mapping.save()
                count = count + 1
            else:
                message = (
                    "Mapping for '{}/{}' does not match the '{}' "
                    "workflow".format(
                        mapping.workflow.process_key,
                        mapping.variable_name,
                        self.process_key,
                    )
                )
                logger.error(message)
                raise WorkflowError(message)

    def update_variables(self, variables, user):
        """Update the variables when deploying a new workflow."""
        variable_names = []
        for name, flowable_data_type in variables.items():
            variable_names.append(name)
            mapping_type = Mapping.DATA_TYPE_MAPPING.get(flowable_data_type)
            mapping = Mapping.objects.init_mapping(
                self, name, flowable_data_type, mapping_type
            )
            variable_names = variable_names + Mapping.objects.generate_extra(
                mapping
            )
        variable_names = variable_names + Mapping.objects.generate_default(self)
        # delete old variables
        qs = self.mapping_set.exclude(variable_name__in=variable_names)
        for row in qs:
            if not row.is_deleted:
                row.set_deleted(user)

    def variables(self):
        return self.mapping_set.exclude(deleted=True)

    def view_task_data(self):
        return (
            self.mapping_set.exclude(deleted=True)
            .filter(view_task_data=True)
            .order_by("variable_name")
        )


reversion.register(Workflow)


class MappingPlugin:
    """This is the controller / manager for the mapping plugins.

    The controller takes care of the (legacy) fields listed below and then
    delegates to the plugins for custom mappings.

    ``AUTO_GENERATE``

      The ``AUTO_GENERATE`` mapping is for the ``WorkflowPlugin`` system.
      The workflow plugins can generate data on their own or based on existing
      data e.g. the full name of a user based on the user ID.

    ``IMPORT``

       The ``IMPORT`` (imported) mappings are for data imported from external
       systems e.g. invoice import.  The non-import mappings allow us to set a
       fixed value e.g. a VAT rate of 20 or a maximum value of 10000.

    """

    # Note: These constants are duplicated in the 'task-form-component-name.js'
    # helper.  See 'ember-kb-work' for more information.
    AUTO_GENERATE = "auto-generate"
    DECIMAL = "decimal"
    ENUM = "lookup"
    GROUP = "group"
    IMPORT = "import"
    IMPORT_BOOLEAN = "import-boolean"
    IMPORT_DATE = "import-date"
    IMPORT_DECIMAL = "import-decimal"
    IMPORT_INTEGER = "import-integer"
    IMPORT_USER = "import-user"
    INTEGER = "integer"
    UNDEFINED = "undefined"
    USER = "user"

    MAPPING_CHOICES = {
        AUTO_GENERATE: Choice(caption="Auto-Generate", data_type=None),
        DECIMAL: Choice(caption="Money", data_type=str),
        ENUM: Choice(caption="Lookup", data_type=Enum),
        GROUP: Choice(caption="Group", data_type=int),
        IMPORT: Choice(caption="Imported", data_type=str),
        IMPORT_BOOLEAN: Choice(caption="Imported True/False", data_type=bool),
        IMPORT_DATE: Choice(caption="Imported Date", data_type=date),
        IMPORT_DECIMAL: Choice(caption="Imported Money", data_type=str),
        IMPORT_INTEGER: Choice(caption="Imported Whole Number", data_type=int),
        IMPORT_USER: Choice(caption="Imported User", data_type=int),
        INTEGER: Choice(caption="Whole Number", data_type=int),
        UNDEFINED: Choice(caption="Undefined", data_type=None),
        USER: Choice(caption="User", data_type=int),
    }

    def _auto_generate_variables(self, mapping, value, map_for_process_key):
        """Create the auto-generated variables for this mapping."""
        result = {}
        extra_variables = mapping.generate_extra()
        for x in extra_variables:
            m = map_for_process_key.get(x)
            auto_generate = m and m.auto_generate()
            if auto_generate:
                if mapping.mapping_type == MappingPlugin.GROUP:
                    result.update(self._group(x, value))
                elif mapping.mapping_type in (
                    MappingPlugin.IMPORT_USER,
                    MappingPlugin.USER,
                ):
                    result.update(self._user(x, value))
                else:
                    for clazz in MappingPlugin.plugin_classes():
                        result.update(
                            clazz.auto_generate_variables(mapping, x, value)
                        )
        return result

    def _get_mapping(self, workflow, variable_name):
        mapping = None
        try:
            mapping = Mapping.objects.get(
                workflow=workflow, variable_name=variable_name
            )
        except Mapping.DoesNotExist:
            pass
        return mapping

    def _get_workflow(self, process_key):
        try:
            return Workflow.objects.get(process_key=process_key)
        except Workflow.DoesNotExist:
            raise WorkflowError(f"Cannot find '{process_key}' workflow")

    def _group(self, name, value):
        result = {}
        try:
            group = Group.objects.get(pk=value)
            result[name] = Variable(data_type=str, value=group.name)
        except Group.DoesNotExist:
            pass
        return result

    def _init_workflow_field_content_object(
        self, content_object, variable_name, data_type, mapping
    ):
        result = None
        try:
            result = content_object.get_workflow_field(
                variable_name, data_type, mapping
            )
        except AttributeError:
            logger.warning(
                f"'{content_object.__class__.__name__}' model "
                "has no 'get_workflow_field' method. "
                f"Looking for '{variable_name}' variable."
            )
        return result

    def _user(self, name, value):
        result = {}
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user__pk=value)
            if "email" in name.lower():
                result[name] = Variable(data_type=str, value=contact.email())
            if "fullname" in name.lower() or "full_name" in name.lower():
                result[name] = Variable(data_type=str, value=str(contact))
        except (User.DoesNotExist, contact_model.DoesNotExist):
            pass
        return result

    def auto_generate_variables(self, process_key, variables):
        """Create the auto-generated variables for this mapping.

        .. tip:: This method is in the ``MappingPlugin`` class.

        .. note:: The variable names are generated in the ``generate_extra``
                  method.

        """
        result = {}
        # all the variable mappings for this process
        map_for_process_key = Mapping.objects.map_for_process_key(process_key)
        auto_generate = bool(
            filter(
                lambda x: x.mapping_type == MappingPlugin.AUTO_GENERATE,
                map_for_process_key.values(),
            )
        )
        if auto_generate:
            for field_id, variable in variables.items():
                mapping = None
                # does the mapping exist?
                if field_id in map_for_process_key:
                    # get the mapping
                    mapping = Mapping.objects.get(
                        workflow__process_key=process_key,
                        variable_name=field_id,
                        deleted=False,
                    )
                if mapping and variable.value:
                    result.update(
                        self._auto_generate_variables(
                            mapping,
                            variable.value,
                            map_for_process_key,
                        )
                    )
        return result

    def can_edit_mapping_type(self, mapping_type):
        """Can the user change the mapping type?"""
        result = mapping_type in (
            self.DECIMAL,
            self.GROUP,
            self.IMPORT,
            self.IMPORT_DECIMAL,
            self.IMPORT_INTEGER,
            self.IMPORT_USER,
            self.INTEGER,
            self.UNDEFINED,
            self.USER,
        )
        if not result:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.can_edit_mapping_type(mapping_type)
                if result:
                    break
        return result

    def can_map(self, mapping_type):
        """Is this a valid mapping type?"""
        result = self.is_default_mapping_type(mapping_type)
        if not result:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.can_map(mapping_type)
                if result:
                    break
        return result

    def create_field(self, field_id, variable, mapping):
        """Create the field and a list of CSS classes."""
        field = css_class = None
        if MappingPlugin().is_plugin(mapping.mapping_type):
            for clazz in MappingPlugin.plugin_classes():
                if clazz.can_map(mapping.mapping_type):
                    field, css_class = clazz.create_field(
                        field_id, variable, mapping
                    )
                    break
        return field, css_class

    def field_value(self, field_id, field_value, mapping):
        """Transform the value from a workflow form field to the correct format.

        Keyword arguments:
        field_id -- the Flowable ``variable_name``
        field_value -- the ``cleaned_data`` from the form
        mapping -- ``workflow.models.Mapping`` instance for this field

        Called in ``workflow.views.WorkflowFormView``.

        """
        result = None
        if MappingPlugin().is_plugin(mapping.mapping_type):
            for clazz in MappingPlugin.plugin_classes():
                if clazz.can_map(mapping.mapping_type):
                    result = clazz.field_value(field_id, field_value, mapping)
                    break
        return result

    def generate_extra(self, mapping):
        """Generate extra variables for this mapping.

        .. tip:: This is run when deploying a workflow
                 and when completing a task.

        Return a list of variable names e.g. ``['user_email', 'user_full_name']

        .. note:: The values are added to the variables in the
                  ``auto_generate_variables`` method.

        """
        result = []
        if MappingPlugin().is_plugin(mapping.mapping_type):
            for clazz in MappingPlugin.plugin_classes():
                if clazz.can_map(mapping.mapping_type):
                    result.extend(clazz.generate_extra(mapping))
                    break
        return result

    def init_workflow_variable(self, mapping, data_type, contact):
        """Create variable to pass to the start task of the workflow.

        Called by ``init_workflow_variables`` when the workflows are created in
        Flowable (see below)

        Keyword arguments:
        mapping -- ``workflow.models.Mapping`` instance
        data_type -- data type e.g. ``str``
        contact -- is linked to the ``user`` from the ``ScheduledWorkflowUser``

        """
        result = {}
        for clazz in MappingPlugin.plugin_classes():
            result.update(
                clazz.init_workflow_variable(mapping, data_type, contact)
            )
            if result:
                break
        return result

    def init_workflow_variables(
        self, process_key, form_variables, user, content_object
    ):
        """Create variables to pass to the start task of the workflow."""
        variables = {}
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user=user)
        except contact_model.DoesNotExist:
            raise WorkflowError(
                f"Cannot find a 'Contact' record for user '{user.username}'"
            )
        workflow = self._get_workflow(process_key)
        for field_id, variable in form_variables.items():
            found = None
            if field_id in HIDDEN_VARIABLE_NAMES:
                found = True
            elif field_id == "api_url":
                variables[field_id] = Variable(
                    value=settings.WORKFLOW_API_URL, data_type=str
                )
                found = True
            elif field_id == "object_pk":
                variables[field_id] = Variable(
                    value=content_object.pk, data_type=variable.data_type
                )
                found = True
            elif field_id == "user_name":
                variables[field_id] = Variable(
                    value=contact.get_full_name(), data_type=variable.data_type
                )
                found = True
            elif field_id == "user_pk":
                variables.update(
                    {
                        field_id: Variable(
                            value=contact.user.pk,
                            data_type=variable.data_type,
                        ),
                        "user_email": Variable(
                            value=contact.email(), data_type=str
                        ),
                        "user_full_name": Variable(
                            value=contact.get_full_name(), data_type=str
                        ),
                    }
                )
                found = True
            mapping = value = None
            if not found:
                # lookup the workflow mapping
                mapping = self._get_mapping(workflow, field_id)
            if not found:
                # ask the content object for the field
                value = self._init_workflow_field_content_object(
                    content_object, field_id, variable.data_type, mapping
                )
                if value:
                    variables[field_id] = Variable(
                        value=value,
                        data_type=variable.data_type,
                    )
                    found = True
            if not found and mapping:
                # does the mapping have a default value?
                x = mapping.init_workflow_variable(variable.data_type, contact)
                if x:
                    variables.update(x)
                    found = True
            if mapping and mapping.mapping_type == MappingPlugin.IMPORT_USER:
                field_names = mapping.generate_extra()
                x = variables.get(field_id) or variable
                if field_names and x and x.value:
                    for name in field_names:
                        variables.update(self._user(name, x.value))
            if not found:
                # No mapping found for the variable, so create an empty one.
                # If the workflow is expecting a variable to exist e.g::
                #
                #   Unknown property used in expression:
                #   ${requestType == "newDocument"}
                #
                # In this example, if the ``requestType`` is empty or not
                # mapped, then a variable won't be created.
                #
                # https://www.kbsoftware.co.uk/crm/ticket/5142/
                #
                value = variable.value
                if not value:
                    if variable.data_type in (date, Enum, None, str):
                        value = ""
                    elif variable.data_type in (int,):
                        value = 0
                variables[field_id] = Variable(
                    value=value, data_type=variable.data_type
                )
                # 19/05/2020, Not sure why we needed to raise an exception here?
                #
                # We have started using script tasks to create temporary JSON
                # variables to pass to HTTP tasks. It doesn't matter if the
                # variable is initialised here, so why worry?
                #
                # API - create workflow and refresh mapping:
                # https://www.kbsoftware.co.uk/crm/ticket/4974/
                #
                logger.warning(
                    "Workflow '{}' cannot find mapping value for '{}' ('{}') "
                    "(created default / empty value)".format(
                        process_key,
                        field_id,
                        (
                            str(variable.data_type.__name__)
                            if variable.data_type
                            else "None"
                        ),
                    )
                )
        return variables

    def is_default_mapping_type(self, mapping_type):
        return mapping_type in (
            self.AUTO_GENERATE,
            self.DECIMAL,
            self.ENUM,
            self.GROUP,
            self.IMPORT,
            self.IMPORT_BOOLEAN,
            self.IMPORT_DATE,
            self.IMPORT_DECIMAL,
            self.IMPORT_INTEGER,
            self.IMPORT_USER,
            self.INTEGER,
            self.USER,
        )

    def is_map_extra(self, mapping_type):
        """Does the mapping type require another selection?"""
        result = mapping_type in (
            self.DECIMAL,
            self.GROUP,
            self.INTEGER,
            self.USER,
        )
        if not result:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.is_map_extra(mapping_type)
                if result:
                    break
        return result

    def is_plugin(self, mapping_type):
        """Is this mapping a plugin or not?"""
        return mapping_type not in self.MAPPING_CHOICES.keys()

    def mapping_choices(self, data_type=None):
        """User to select from these mapping choices (filtered by data type).

        .. note:: If the ``data_type`` is ``None`` we return all choices
                  (to allow the ``mapping_type`` field to validate on the form).

        Merge the ``MAPPING_CHOICES`` from this class with the
        ``mapping_choices`` from the plugin classes.

        """
        result = []
        choices = match_data_type(self.MAPPING_CHOICES, data_type)
        for clazz in MappingPlugin.plugin_classes():
            choices.update(clazz.mapping_choices(data_type))
        for field_id, choice in choices.items():
            result.append((field_id, choice.caption))
        return tuple(sorted(result))

    def mapping_value(self, mapping_type, field_value):
        """Save the extra configuration for the Mapping config view.

        This is used when configuring the mapping type and another selection
        is required *and* a field does not exist in the ``Mapping`` model.
        In this case we store the value in the JSON ``parameters`` field.

        See the ``mapping_value_form_kwargs`` method (below) to see where the
        data is retrieved.

        """
        result = {}
        for clazz in MappingPlugin.plugin_classes():
            result = clazz.mapping_value(mapping_type, field_value)
            if result:
                break
        return result

    def mapping_value_form_kwargs(self, mapping):
        """Get the 'get_form_kwargs' for a 'Mapping' config view.

        This is used when configuring the mapping type and another selection
        is required *and* a field does not exist in the ``Mapping`` model.
        In this case we will have stored the value in the JSON ``parameters``
        field.

        See the ``mapping_value`` method (above) to see where the data is
        stored.

        """
        result = {}
        for clazz in MappingPlugin.plugin_classes():
            result = clazz.mapping_value_form_kwargs(mapping)
            if result:
                break
        return result

    def map_as_str(self, mapping):
        """Description of mapping type (if another selection is required)."""
        result = None
        default = "Undefined"
        if mapping.mapping_type == self.DECIMAL:
            result = (
                mapping.decimal_number if mapping.decimal_number else default
            )
        elif mapping.mapping_type == self.ENUM:
            result = ""
        elif mapping.mapping_type == self.GROUP:
            result = mapping.group.name if mapping.group else default
        elif mapping.mapping_type in (
            self.AUTO_GENERATE,
            self.IMPORT,
            self.IMPORT_BOOLEAN,
            self.IMPORT_DATE,
            self.IMPORT_DECIMAL,
            self.IMPORT_INTEGER,
        ):
            result = ""
        elif mapping.mapping_type == self.INTEGER:
            result = (
                mapping.integer_number if mapping.integer_number else default
            )
        elif mapping.mapping_type == self.USER:
            result = mapping.user.get_full_name() if mapping.user else default
        else:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.map_as_str(mapping)
                if result:
                    break
            if not result:
                choice = self.MAPPING_CHOICES.get(mapping.mapping_type)
                result = choice.caption if choice else default
        return str(result)

    def map_url(self, mapping_type, pk):
        """URL to configure the mapping on workflow mapping."""
        result = None
        if mapping_type == self.DECIMAL:
            result = reverse("workflow.mapping.update.map.decimal", args=[pk])
        elif mapping_type == self.GROUP:
            result = reverse("workflow.mapping.update.map.group", args=[pk])
        elif mapping_type == self.INTEGER:
            result = reverse("workflow.mapping.update.map.integer", args=[pk])
        elif mapping_type == self.USER:
            result = reverse("workflow.mapping.update.map.user", args=[pk])
        if not result:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.map_url(mapping_type, pk)
                if result:
                    break
        return result

    @staticmethod
    def plugin_classes():
        plugin_classes = []
        for plugin_class_name in settings.WORKFLOW_PLUGIN:
            plugin_classes.append(import_class(plugin_class_name))
        return plugin_classes


class MappingManager(models.Manager):
    def _create_mapping(
        self,
        workflow,
        variable_name,
        flowable_data_type=None,
        mapping_type=None,
    ):
        if mapping_type is None:
            mapping_type = MappingPlugin.UNDEFINED
        x = self.model(
            workflow=workflow,
            variable_name=variable_name,
            flowable_data_type=flowable_data_type or "",
            mapping_type=mapping_type,
        )
        x.save()
        return x

    def for_process_key(self, process_key):
        return self.model.objects.filter(
            workflow__process_key=process_key
        ).exclude(deleted=True)

    def for_workflow(self, workflow):
        return self.model.objects.filter(workflow=workflow).exclude(
            deleted=True
        )

    def generate_default(self, workflow):
        """Generate default variable names.

        .. note:: Some variable names from the ``RESERVED_VARIABLE_NAMES`` list.

        """
        default_variable_names = ["user_email", "user_full_name", "user_pk"]
        for name in default_variable_names:
            self.init_mapping(
                workflow,
                name,
                flowable_data_type=None,
                mapping_type=MappingPlugin.AUTO_GENERATE,
            )
        return default_variable_names

    def generate_extra(self, mapping):
        """Create extra variables when deploying a workflow.

        .. tip:: This method is in the ``MappingManager`` class

        """
        extra_variables = mapping.generate_extra()
        for name in extra_variables:
            self.init_mapping(
                mapping.workflow,
                name,
                flowable_data_type=None,
                mapping_type=MappingPlugin.AUTO_GENERATE,
            )
        return extra_variables

    def init_mapping(
        self,
        workflow,
        variable_name,
        flowable_data_type=None,
        mapping_type=None,
    ):
        """Initialise the mapping.

        .. note:: ``init_mapping`` is called when a new workflow is uploaded.
                  We update the ``mapping_type`` only if it changes from editable
                  to not-editable (or vice-versa).  A user might edit the
                  workflow and change the data type of a variable e.g. from a
                  ``string`` to an ``enum``.  If we didn't update the mapping
                  type, the user would still be able to change it.

        """
        try:
            x = self.model.objects.get(
                workflow=workflow, variable_name=variable_name
            )
            x.flowable_data_type = flowable_data_type or ""
            x.save()
            if x.is_deleted:
                x.undelete()
            if mapping_type:
                # is the mapping type changing to/from an editable type
                can_edit = MappingPlugin().can_edit_mapping_type(mapping_type)
                if can_edit == x.can_edit_mapping_type():
                    pass
                else:
                    x.mapping_type = mapping_type
                    x.save()
        except self.model.DoesNotExist:
            x = self._create_mapping(
                workflow, variable_name, flowable_data_type, mapping_type
            )
        return x

    def map_for_process_key(self, process_key):
        result = {}
        data = self.for_process_key(process_key).values()
        for row in data:
            result[row["variable_name"]] = Map(
                variable_name=row["variable_name"],
                mapping_type=row["mapping_type"],
            )
        return result


class Mapping(TimedCreateModifyDeleteModel):
    """Mapping of a workflow variable to it's attributes.

    .. tip:: Use the ``Map`` type to pass these around.

    The mapping types are as follows:
    GROUP -- A specific group e.g. Accounts
    USER -- A selected user e.g. HR Manager

    """

    CSS_FULL_WIDTH = "pure-input-1"
    CSS_TWO_THIRDS = "pure-input-2-3"

    CSS_CLASS_CHOICES = (
        (CSS_FULL_WIDTH, "Full Width"),
        (CSS_TWO_THIRDS, "Two Thirds"),
    )

    DATA_TYPE_MAPPING = {
        "date": MappingPlugin.IMPORT_DATE,
        "enum": MappingPlugin.ENUM,
    }

    workflow = models.ForeignKey(Workflow, on_delete=models.CASCADE)
    variable_name = models.CharField(max_length=255)
    flowable_data_type = models.CharField(
        max_length=20, blank=True, help_text="Flowable data type"
    )
    mapping_type = models.CharField(
        max_length=50,
        choices=_mapping_choices(),
        default=MappingPlugin.UNDEFINED,
    )
    mapping_code = models.CharField(max_length=50, blank=True)
    help_text = models.TextField(blank=True)
    flow_list_type = models.ForeignKey(
        FlowListType, blank=True, null=True, on_delete=models.CASCADE
    )
    group = models.ForeignKey(
        Group, blank=True, null=True, on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    decimal_number = models.DecimalField(
        blank=True,
        null=True,
        help_text="Decimal Value",
        max_digits=6,
        decimal_places=2,
    )
    integer_number = models.IntegerField(
        blank=True, null=True, help_text="Whole Number"
    )
    css_class = models.CharField(
        max_length=50,
        choices=CSS_CLASS_CHOICES,
        default=CSS_TWO_THIRDS,
        blank=True,
    )
    multi_line = models.IntegerField(
        blank=True,
        null=True,
        help_text="Number of rows to display in a text field (0 for default)",
    )
    history_summary = models.BooleanField(
        default=True, help_text="Show in process summary for the task history"
    )
    history_summary_column = models.IntegerField(
        blank=True,
        null=True,
        help_text="Show in a separate column for the process history",
    )
    history_summary_order = models.IntegerField(
        blank=True,
        null=True,
        help_text="Order within the process summary for the task history",
    )
    data_download = models.BooleanField(
        default=False, help_text="Output data for download"
    )
    view_task_data = models.BooleanField(
        default=True, help_text="View data on task screen"
    )
    parameters = models.JSONField(
        blank=True, null=True, encoder=DjangoJSONEncoder
    )
    objects = MappingManager()

    class Meta:
        ordering = ("workflow__process_key", "variable_name")
        verbose_name = "Mapping"
        verbose_name_plural = "Mappings"

    def __str__(self):
        return (
            f"'{self.workflow.process_key}.{self.variable_name}' "
            f"['{self.mapping_type}']"
        )

    def _format_decimal(self, value):
        result = None
        try:
            x = decimal.Decimal(value)
            result = "{0:.2f}".format(x)
        except (decimal.InvalidOperation, TypeError):
            pass
        return result

    def _format_group(self, value):
        result = ""
        if value:
            try:
                group = Group.objects.get(pk=value)
                result = group.name
            except (Group.DoesNotExist, ValueError):
                result = "{}".format(value)
        return result

    def _format_integer(self, value):
        result = None
        try:
            result = int(value)
        except ValueError:
            pass
        return result

    def _format_user(self, value):
        result = ""
        if value:
            try:
                user = get_user_model().objects.get(pk=value)
                result = user.get_full_name()
            except (get_user_model().DoesNotExist, ValueError):
                result = "{}".format(value)
        return result

    def can_edit_mapping_type(self):
        """Can the user change the mapping type?"""
        return MappingPlugin().can_edit_mapping_type(self.mapping_type)

    def can_map(self):
        """Is this a valid mapping type?"""
        return MappingPlugin().can_map(self.mapping_type)

    def format_activiti_data(self, variable, sep=None):
        """Format data returned from Activiti ready for display."""
        result = None
        if self.mapping_type == MappingPlugin.IMPORT_BOOLEAN:
            result = activiti_bool(variable.value)
        elif self.mapping_type == MappingPlugin.IMPORT_DATE:
            x = activiti_date(variable.value)
            if x:
                result = x.strftime("%d/%m/%Y")
        elif self.mapping_type == MappingPlugin.IMPORT_INTEGER:
            result = self._format_integer(variable.value)
        elif self.is_decimal():
            result = self._format_decimal(variable.value)
        elif self.mapping_type == MappingPlugin.GROUP:
            result = self._format_group(variable.value)
        elif self.mapping_type in (
            MappingPlugin.IMPORT_USER,
            MappingPlugin.USER,
        ):
            result = self._format_user(variable.value)
        else:
            for clazz in MappingPlugin.plugin_classes():
                result = clazz.variable_as_str(variable, self, sep)
                if result:
                    break
            if not result:
                if variable.value is None:
                    pass
                else:
                    result = "{}".format(variable.value)
        return result

    def generate_extra(self):
        """Generate extra variables for this mapping.

        .. tip:: This is run when deploying a workflow
                 and when completing a task.

        .. tip:: This method is in the ``Mapping`` class

        Return a list of variable names e.g. ``user_email``, ``user_full_name``.

        """
        result = []
        if self.variable_name.endswith("_pk"):
            name = self.variable_name[: len(self.variable_name) - 3]
        else:
            name = self.variable_name
        if self.mapping_type == MappingPlugin.GROUP:
            result.append("{}_name".format(name))
        elif self.mapping_type in (
            MappingPlugin.IMPORT_USER,
            MappingPlugin.USER,
        ):
            for x in ("email", "full_name"):
                result.append("{}_{}".format(name, x))
        else:
            result.extend(MappingPlugin().generate_extra(self))
        return result

    def is_decimal(self):
        return self.mapping_type in (
            MappingPlugin.DECIMAL,
            MappingPlugin.IMPORT_DECIMAL,
        )

    def is_map_extra(self):
        """Does the mapping type require another selection?"""
        return MappingPlugin().is_map_extra(self.mapping_type)

    def is_plugin(self):
        return MappingPlugin().is_plugin(self.mapping_type)

    def is_valid(self):
        result = self.can_map()
        if result:
            if self.mapping_type == MappingPlugin.GROUP:
                result = bool(self.group)
            elif self.mapping_type == MappingPlugin.DECIMAL:
                result = bool(self.decimal_number)
            elif self.mapping_type == MappingPlugin.INTEGER:
                result = bool(self.integer_number)
            elif self.mapping_type == MappingPlugin.USER:
                result = bool(self.user)
            else:
                for clazz in MappingPlugin.plugin_classes():
                    if clazz.can_map(self.mapping_type):
                        result = clazz.is_valid_mapping(self)
                        break
        return result

    def map_as_str(self):
        return MappingPlugin().map_as_str(self)

    def init_workflow_variable(self, data_type, contact):
        """Create variable to pass to the start task of the workflow.

        Called by ``init_workflow_variables`` when the workflows are created in
        Flowable.

        Keyword arguments:
        contact -- is linked to the ``user`` from the ``ScheduledWorkflowUser``

        """
        result = {}
        if self.is_valid():
            if self.mapping_type == MappingPlugin.DECIMAL:
                result[self.variable_name] = Variable(
                    value=self.decimal_number,
                    data_type=data_type,
                )
            elif self.mapping_type == MappingPlugin.GROUP:
                result[self.variable_name] = Variable(
                    value=self.group.pk,
                    data_type=data_type,
                )
            elif self.mapping_type == MappingPlugin.INTEGER:
                result[self.variable_name] = Variable(
                    value=self.integer_number,
                    data_type=data_type,
                )
            elif self.mapping_type == MappingPlugin.USER:
                result[self.variable_name] = Variable(
                    value=self.user.pk,
                    data_type=data_type,
                )
            else:
                result.update(
                    MappingPlugin().init_workflow_variable(
                        self, data_type, contact
                    )
                )
        else:
            message = (
                f"Mapping '{self.workflow.process_key}:{self.variable_name}' "
                "has not been set-up"
            )
            logger.error(message)
            raise WorkflowError(message)
        return result

    def map_url(self):
        return MappingPlugin().map_url(self.mapping_type, self.pk)

    def mapping_choices(self):
        """User can select from these mapping choices (filtered by data type).

        .. note:: ``get_data_type`` converts a ``flowable_data_type`` to an
                  Activiti data type.

        """
        data = MappingPlugin().mapping_choices(
            get_data_type(self.flowable_data_type)
        )
        return sorted(data, key=lambda tup: tup[1])

    def mapping_type_as_str(self):
        if self.mapping_type == MappingPlugin.ENUM:
            result = "Choice"
        elif self.mapping_type == MappingPlugin.IMPORT_BOOLEAN:
            result = "Imported True/False"
        else:
            result = self.get_mapping_type_display()
        return result

    def variable_name_as_java(self):
        return snake_to_camel_case(self.variable_name)


reversion.register(Mapping)


class ProcessDefinitionManager(models.Manager):
    def _get_latest_version(self, process_key):
        return (
            self.model.objects.filter(process_key=process_key)
            .order_by("version")
            .last()
        )

    def get_latest_version(self, process_key):
        result = self._get_latest_version(process_key)
        if not result:
            message = "workflow process key '{}' does not exist".format(
                process_key
            )
            logger.error(message)
            raise ActivitiError(message)
        return result

    def get_latest_version_or_zero(self, process_key):
        result = 0
        process_definition = self._get_latest_version(process_key)
        if process_definition:
            result = process_definition.version
        return result

    def default_key_version_number(self):
        version_info = self.model.objects.filter(
            process_key=self.model.DEFAULT_KEY
        ).aggregate(max_version=Max("version"))
        if not version_info["max_version"]:
            return 1
        return version_info["max_version"] + 1

    def deploy(self, process_definition, user):
        """Deploy a process definition.

        .. note:: This method expects the ``process_definition.file`` to already
                  exist.

        22/09/2020, Moved method from the model, so we can
        ``get_latest_version_or_zero`` from the manager (useful when restoring
        data sets).

        Example usage::

          import os
          from django.core.files import File
          from workflow.models import ProcessDefinition

          file_path = "dash/bpmn/timeOffRequest.bpmn20.xml"

          definition = ProcessDefinition()
          with open(file_path) as f:
              file_name = os.path.basename(file_path)
              definition.process_definition.save(file_name, File(f))
          message = ProcessDefinition.objects.deploy(
              definition, contact.user
          )

        """
        activiti = Activiti()
        file_path = os.path.join(
            settings.MEDIA_ROOT, process_definition.process_definition.file.name
        )
        deployment_id = activiti.deploy_process_definition(file_path)
        info = activiti.process_definition_for_deployment(deployment_id)
        process_definition.process_key = info.process_key
        process_definition.version = (
            self.get_latest_version_or_zero(process_definition.process_key) + 1
        )
        process_definition.workflow_id = info.workflow_id
        process_definition.save()
        process_definition.update_variables(user)
        message = "Process '{}' deployed.".format(
            process_definition.process_key
        )
        return message

    def process_definitions(self):
        return self.model.objects.all().order_by("process_definition")


class ProcessDefinition(models.Model):
    """Deployed Activiti Process definitions.

    'documentFamiliarisation'      1
    'documentFamiliarisation'      2
    'documentFamiliarisation'      3

    .. note:: This model is to help with the file upload and to track process
              definition deployments to Activiti.

    .. note:: The process_key is not known until the process definition is
              deployed hence the default is incompleteDeployment.

    """

    DEFAULT_KEY = "incompleteDeployment"

    process_key = models.CharField(max_length=200, default=DEFAULT_KEY)
    version = models.PositiveIntegerField(default=0)
    workflow_id = models.CharField(max_length=200)
    process_definition = models.FileField(upload_to=process_file_name)
    objects = ProcessDefinitionManager()

    class Meta:
        ordering = ("process_definition",)
        unique_together = ("process_key", "version")
        verbose_name = "Process Definition"
        verbose_name_plural = "Process Definitions"

    def __str__(self):
        return "{} {}".format(self.process_key, self.version)

    def parse_xml(self, xml):
        """Extract form variables from the workflow XML file.

        Used for mapping a workflow to the users and groups on the user
        database.

        - We exclude date variables because there is no mapping to do.
        - We exclude reserved variables.
        - We exclude fields which are set to require a comment.

        The ``_check_user_pk_in_variables`` should be checking only the
        variables in the ``startEvent``, but this is a good start:
        https://www.kbsoftware.co.uk/docs/app-workflow.html#start-event-startevent

        """
        result = {}
        variables = set()
        root = ET.fromstring(xml.strip())
        for x in root.findall(".//{http://flowable.org/bpmn}formProperty"):
            name = camelcase_to_snake(x.attrib["id"])
            data_type = x.attrib["type"]
            variables.add(name)
            exclude = name in HIDDEN_VARIABLE_NAMES
            if not exclude:
                exclude = name in RESERVED_VARIABLE_NAMES
            if not exclude:
                exclude = name.endswith("Comment")
            if exclude:
                pass
            else:
                result[name] = data_type
        # see docs above...
        _check_user_pk_in_variables(self.process_key, variables)
        return result

    def update_variables(self, user):
        """Update the variables when deploying a new workflow."""
        with open(self.process_definition.file.name) as f:
            variables = self.parse_xml(f.read())
        workflow = Workflow.objects.init_workflow(self.process_key)
        workflow.update_variables(variables, user)


reversion.register(ProcessDefinition)


class ScheduledWorkflowManager(models.Manager):
    def create_scheduled_workflow(
        self, process_definition, initiator, content_object, users
    ):
        """Create a scheduled workflow.

        Keyword arguments:
        process_definition -- e.g. ``ProcessDefinition``
        initiator -- the user who asked for the workflow to be started
        content_object -- link to a related model
        users -- create one workflow for each user in this list (``User``)

        """
        scheduled_workflow = ScheduledWorkflow(
            process_definition=process_definition,
            initiator=initiator,
            content_object=content_object,
        )
        scheduled_workflow.save()
        for user in users:
            ScheduledWorkflowUser(
                scheduled_workflow=scheduled_workflow, user=user
            ).save()
        return scheduled_workflow

    def _create_workflow(
        self, process_definition, initiator, content_object, user
    ):
        """Create the workflow for a user.

        Process these variables:

        <activiti:formProperty id="objectPk" name="Document ID" type="long"
            required="true"></activiti:formProperty>
        <activiti:formProperty id="userName" name="Contact" type="string"
            required="true"></activiti:formProperty>

        """
        logger.info(
            f"creating '{process_definition}' workflow "
            f"for '{content_object}', '{user.username}'"
        )
        activiti = Activiti()
        process_def = ProcessDefinition.objects.get_latest_version(
            process_definition
        )
        # get form variables and configure
        form_variables = activiti.form_variable_info(
            process_id=process_def.workflow_id
        )
        _check_user_pk_in_variables(process_definition, form_variables)
        # create the variables to pass to the start task of the process
        variables = MappingPlugin().init_workflow_variables(
            process_def.process_key, form_variables, user, content_object
        )
        # start the process
        process_id = activiti.process_start(process_def.process_key, variables)
        logger.info(
            f"created '{process_definition}' workflow, process_id "
            f"'{process_id}' (for '{content_object}', '{user.username}')"
        )
        activiti.process_identity_add_participant(process_id, user)
        logger.info(
            f"workflow 'process_id' '{process_definition}' "
            f"('{process_id}') added user '{user.username}'"
        )
        return process_id

    def create_workflows(self, pk):
        """Create the workflows in Activiti.

        This method is expecting to be run inside an active transaction.

        Returns a list of ``ScheduledWorkflowUser`` objects for the created
        processes.

        """
        result = []
        try:
            # only create once (if function is called twice at the same time)
            workflow = ScheduledWorkflow.objects.select_for_update(
                nowait=True
            ).get(pk=pk)
            for scheduled in workflow.scheduled_users():
                if not scheduled.get_process_id():
                    process_id = self._create_workflow(
                        workflow.process_definition,
                        workflow.initiator,
                        workflow.content_object,
                        scheduled.user,
                    )
                    if process_id:
                        try:
                            scheduled.process_id = int(process_id)
                        except ValueError:
                            # If no bean is provided the StrongUuidGenerator
                            # will be used
                            # https://blog.flowable.org/2018/10/02/flowable-6-4-0-release/
                            scheduled.process_uuid = process_id
                        scheduled.save()
                        result.append(scheduled)
        except ScheduledWorkflow.DoesNotExist:
            # record is locked, so leave alone this time
            message = f"ScheduledWorkflow '{pk}' does not exist"
            logger.error(message)
            raise ActivitiError(message)
        except DatabaseError as e:
            # record is locked, so leave alone this time
            message = (
                f"ScheduledWorkflow item number '{pk}' is locked: {str(e)}"
            )
            logger.error(message)
            raise ActivitiError(message)
        return result

    def for_content_object(self, obj, process_definitions=None):
        """Return the ``ScheduledWorkflow`` instance for ``obj``.

        Keyword arguments:
        obj -- content object
        process_definitions -- list of process definitions (default None)

        """
        if process_definitions and isinstance(process_definitions, str):
            raise WorkflowError(
                "'process_definitions' in 'for_content_object' is "
                "a list (not a string): '{}'".format(process_definitions)
            )
        content_type = ContentType.objects.get_for_model(obj)
        qs = self.model.objects.filter(
            content_type=content_type, object_id=obj.pk
        )
        if process_definitions:
            qs = qs.filter(process_definition__in=process_definitions)
        return qs


class ScheduledWorkflow(TimedCreateModifyDeleteModel):
    """List of workflows waiting to be started.

    .. note:: The ``process_definition`` is a constant in the code e.g.
              ``documentFamiliarisation``.

    .. note:: The process_definition could be confused as a foreign key to
              the ProcessDefinition model above (perhaps ``process_key``
              would be a better field name).
              Also since the key will likely be camel case rather
              than python slug format perhaps it should be a ``CharField``.

    """

    process_definition = models.SlugField(max_length=200)
    initiator = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    # link to the object in the system which is starting the process
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()
    objects = ScheduledWorkflowManager()

    class Meta:
        verbose_name = "Scheduled workflow"
        verbose_name_plural = "Scheduled workflows"

    def __str__(self):
        return "'{}' initiator '{}', created {}".format(
            self.process_definition,
            self.initiator.username,
            self.created.strftime("%d/%m/%Y %H:%M"),
        )

    def scheduled_users(self):
        return self.scheduledworkflowuser_set.exclude(deleted=True).exclude(
            process_id__isnull=False
        )


reversion.register(ScheduledWorkflow)


class ScheduledWorkflowUserManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True).exclude(
            deleted_process=True
        )

    def get_process_id(self, process_id):
        """A process ID can be an integer or a UUID."""
        try:
            int(process_id)
            return self.model.objects.get(process_id=process_id)
        except ValueError:
            return self.model.objects.get(process_uuid=process_id)

    def pending(self):
        return (
            self.current()
            .filter(process_id__isnull=True)
            .filter(process_uuid__isnull=True)
        )

    def pending_with_options(
        self, process_definition=None, start_date=None, end_date=None
    ):
        qs = self.pending()
        if process_definition:
            qs = qs.filter(
                scheduled_workflow__process_definition=process_definition
            )
        if start_date:
            qs = qs.filter(created__gte=start_date)
        if end_date:
            qs = qs.filter(created__lte=end_date)
        return qs


class ScheduledWorkflowUser(TimedCreateModifyDeleteModel):
    """List of workflows waiting to be started.

    The ``process_id`` will be set when the process has been created.

    1 patrick
    2 malcolm
    0 greg
    0 tim
    0 andrea
    0 ben
    3 matt

    .. note:: The variables for each ``process_definition`` will be set by some
              custom code.

    """

    scheduled_workflow = models.ForeignKey(
        ScheduledWorkflow, on_delete=models.CASCADE
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    process_id = models.IntegerField(blank=True, null=True)
    process_uuid = models.UUIDField(
        blank=True, null=True, editable=False, unique=True
    )
    deleted_process = models.BooleanField(default=False)
    deleted_comment = models.TextField(blank=True)
    date_deleted_process = models.DateTimeField(blank=True, null=True)
    user_deleted_process = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    objects = ScheduledWorkflowUserManager()

    class Meta:
        ordering = ("scheduled_workflow", "user__username")
        unique_together = ("scheduled_workflow", "user")
        verbose_name = "Scheduled user workflow"
        verbose_name_plural = "Scheduled user workflows"

    def __str__(self):
        message = "'{}' initiator '{}' created {} for '{}'".format(
            self.scheduled_workflow.process_definition,
            self.scheduled_workflow.initiator.username,
            self.scheduled_workflow.created.strftime("%d/%m/%Y %H:%M"),
            self.user.username,
        )
        process_id = self.get_process_id()
        if process_id:
            message = message + f" process '{process_id}'"
        else:
            message = message + " pending"
        return message

    def _process_message(self, process_status):
        return "process id '{}' completed: {}, suspended: {}".format(
            process_status.pk,
            process_status.completed,
            process_status.suspended,
        )

    def delete_workflow_process(self, user, comment):
        """Delete the workflow process."""
        result = False
        # do we have an active process?
        process_id = self.get_process_id()
        if self.deleted_process:
            pass
        elif process_id:
            if not comment:
                raise WorkflowError(
                    "A workflow cannot be deleted without a comment"
                )
            activiti = Activiti()
            # check the status of the process (to make sure it exists)
            process_status = activiti.process_status(process_id)
            if not process_id == process_status.pk:
                raise WorkflowError(
                    "process_id does not match {} != {}".format(
                        process_id, process_status.pk
                    )
                )
            message = "scheduled workflow user: '{}' {}".format(
                self.pk, self._process_message(process_status)
            )
            if process_status.completed or process_status.suspended:
                # Flowable process is already completed (or suspended)
                logger.info(
                    "already completed (or suspended) {}".format(message)
                )
            else:
                # delete the process
                with transaction.atomic():
                    logger.info("deleting {}".format(message))
                    self.set_deleted_process(user, comment)
                    activiti.process_delete(process_id)
                    logger.info("deleted {}".format(message))
                    result = True
        else:
            # pending create, so set the process as deleted
            self.set_deleted(user)
            result = True
        return result

    def get_process_id(self):
        """The legacy 'process_id' was an integer.  It is now a UUID.

        .. note:: This method needs to return an ``int`` or ``str``.

        """
        result = None
        if self.process_id:
            result = int(self.process_id)
        elif self.process_uuid:
            result = str(self.process_uuid)
        return result

    def get_workflow(self):
        process_key = self.scheduled_workflow.process_definition
        return Workflow.objects.get(process_key=process_key)

    @property
    def is_deleted_process(self):
        return self.deleted_process

    @property
    def rest_framework_task(self):
        return None

    @property
    def rest_framework_update_action(self):
        return None

    def set_deleted_process(self, user, comment):
        if self.is_deleted_process:
            raise WorkflowError(
                "Cannot delete '{}', pk '{}'.  It is already "
                "deleted".format(self.__class__.__name__, self.pk)
            )
        self.deleted_process = True
        self.deleted_comment = comment
        self.date_deleted_process = timezone.now()
        self.user_deleted_process = user
        self.save()


class WorkflowDataManager(models.Manager):
    def _pending(self):
        return (
            self.model.objects.exclude(scheduled_workflow_user__deleted=True)
            .exclude(scheduled_workflow_user__process_id__isnull=True)
            .filter(data__isnull=False)
            .filter(reported=False)
        )

    def create_workflow_data(self, process_id, form_variables):
        """Merge process (and form) variables to create a data object.

        .. note:: The form variables override the process variables because
                  they haven't yet been written to the process.

        Example data structures for form and process variables:

        form_variables::

          [
              {'name': 'data_download', 'value': None},
              {'name': 'from_date', 'value': '2017-06-27'},
          ]

        process variables::

          Variables(
              user_name='Maria Anders',
              object_pk=1,
              manager_group_pk='9'
          )

        Copied to ``_variables_for_pdf`` (``work.api.WorkDownloadView``).

        """
        activiti = Activiti()
        scheduled = ScheduledWorkflowUser.objects.get_process_id(process_id)
        process_key = scheduled.scheduled_workflow.process_definition
        try:
            workflow = Workflow.objects.get(process_key=process_key)
        except Workflow.DoesNotExist:
            raise WorkflowError(
                f"Cannot find workflow with 'process_key={process_key}'"
            )
        # get process data
        status = activiti.process_status(process_id)
        if status.completed:
            status = activiti.historic_process(process_id)
        # merge process and form variables
        variables = {}
        variables.update(status.variables)
        variables.update(form_variables)
        # collect data
        data = {}
        for mapping in workflow.data_download():
            key = mapping.variable_name
            variable = variables.get(key)
            if variable:
                data[key] = mapping.format_activiti_data(variable)
        obj = self.model(scheduled_workflow_user=scheduled, data=data)
        obj.save()
        return obj

    def data_download(self, process_key):
        """List of objects where we can download data."""
        return (
            self.model.objects.exclude(scheduled_workflow_user__deleted=True)
            .exclude(scheduled_workflow_user__process_id__isnull=True)
            .filter(
                scheduled_workflow_user__scheduled_workflow__process_definition=process_key
            )
            .filter(data__isnull=False)
        )

    def pending_report(self, process_key):
        """List of objects where we can create a CSV file."""
        return self._pending().filter(
            scheduled_workflow_user__scheduled_workflow__process_definition=process_key
        )


class WorkflowData(TimeStampedModel):
    """Data exported from Activiti on request of the workflow."""

    REPORT_SLUG = "workflow_data"

    scheduled_workflow_user = models.ForeignKey(
        ScheduledWorkflowUser, on_delete=models.CASCADE
    )
    data = models.JSONField(
        encoder=DjangoJSONEncoder,
        blank=True,
        null=True,
        help_text="Workflow data for download as CSV",
    )
    reported = models.BooleanField(default=False)
    objects = WorkflowDataManager()

    class Meta:
        ordering = ["-created"]
        verbose_name = "Scheduled user workflow data"
        verbose_name_plural = "Scheduled user workflow data"

    def __str__(self):
        scheduled_workflow = self.scheduled_workflow_user.scheduled_workflow
        message = "'{}' created {}".format(
            scheduled_workflow.process_definition,
            self.created.strftime("%d/%m/%Y %H:%M"),
        )
        return message
